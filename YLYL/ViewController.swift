//
//  ViewController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 02/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit

class ViewController: FixUIController,BWHorizontalTableViewDelegate,BWHorizontalTableViewDataSource {
    func numberOfSections(in tableView: BWHorizontalTableView!) -> Int {
        return 1
    }
    
    func horizontalTableView(_ tableView: BWHorizontalTableView!, numberOfColumnsInSection section: Int) -> Int {
        return 4
    }
    
    func horizontalTableView(_ tableView: BWHorizontalTableView!, cellForColumnAt indexPath: IndexPath!) -> BWHorizontalTableViewCell! {
        var cell  = tableView.dequeueReusableCell(withIdentifier: "HorizontalTableViewCell") as? JokeBarViewCell
        if cell == nil {
            cell = JokeBarViewCell(reuseIdentifier: "HorizontalTableViewCell")
            cell?.selectionStyle = UITableViewCell.SelectionStyle.blue
            cell?.RoundedView.clipsToBounds = true
            
            
            
        cell?.RoundedView.layer.cornerRadius = (cell?.RoundedView.frame.width)! / 2
          
            
        cell?.RoundedView.layer.borderWidth = 1
        cell?.RoundedView.layer.borderColor = UIColor.blue.cgColor
        cell?.RoundedView.layer.backgroundColor = UIColor.white.cgColor
        }
        return cell
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, didSelectColumnAt indexPath: IndexPath!) {
        print("selected:",indexPath.row)
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForColumnAt indexPath: IndexPath!) -> CGFloat {
        return 70.0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, viewForHeaderInSection section: Int) -> UIView! {
        let H = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return H
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, viewForFooterInSection section: Int) -> UIView! {
        let H = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return H
    }
    
    @IBOutlet weak var tableH : BWHorizontalTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableH.delegate = self
        tableH.dataSource = self
        tableH.reloadData()
        tableH.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        tableH.delegate?.horizontalTableView!(tableH, didSelectColumnAt: IndexPath(row: 0, section: 0))
        // Do any additional setup after loading the view, typically from a nib.
    }


}

