import Foundation
import UIKit
import Photos
import BMPlayer
import VideoPlaybackKit
extension UIAlertController {
   

    /// Add PhotoLibrary Picker
    ///
    /// - Parameters:
    ///   - flow: scroll direction
    ///   - pagging: pagging
    ///   - images: for content to select
    ///   - selection: type and action for selection of image/images
    
    func addPhotoLibraryPicker(flow: UICollectionView.ScrollDirection, paging: Bool, selection: PhotoLibraryPickerViewController.Selection,actuality : [VideoYLYL]) {
        let selection: PhotoLibraryPickerViewController.Selection = selection
        var asset: VideoYLYL?
        var assets: [VideoYLYL] = []
        
        let buttonAdd = UIAlertAction(title: "Delete", style: .destructive) { action in
            switch selection {
                
            case .single(let action):
                action?(asset)
                
            case .multiple(let action):
                action?(assets)
            }
        }
        buttonAdd.isEnabled = false
        
        let vc = PhotoLibraryPickerViewController(flow: flow, paging: paging, selection: {
            switch selection {
            case .single(_):
                return .single(action: { new in
                    buttonAdd.isEnabled = new != nil
                    asset = new
                })
            case .multiple(_):
                return .multiple(action: { new in
                    buttonAdd.isEnabled = new.count > 0
                    assets = new
          
                })
            }
        }())
        vc.acutality = actuality
        if UIDevice.current.userInterfaceIdiom == .pad {
            vc.preferredContentSize.height = vc.preferredSize.height * 0.9
            vc.preferredContentSize.width = vc.preferredSize.width * 0.9
        } else {
            vc.preferredContentSize.height = vc.preferredSize.height
        }
        
        addAction(buttonAdd)
        set(vc: vc)
    }
}

final class PhotoLibraryPickerViewController: UIViewController {
    
    public typealias SingleSelection = (VideoYLYL?) -> Swift.Void
    public typealias MultipleSelection = ([VideoYLYL]) -> Swift.Void
     public var videoPrefetcher: VPKCollectionViewPrefetchSynchronize?
    public enum Selection {
        case single(action: SingleSelection?)
        case multiple(action: MultipleSelection?)
    }
    // MARK: UI Metrics
    
    var preferredSize: CGSize {
        return UIScreen.main.bounds.size
    }
    
    var columns: CGFloat {
        switch layout.scrollDirection {
        case .vertical: return UIDevice.current.userInterfaceIdiom == .pad ? 3 : 2
        case .horizontal: return 1
        }
    }
    
    var itemSize: CGSize {
        switch layout.scrollDirection {
        case .vertical:
            return CGSize(width: view.bounds.width / columns, height: view.bounds.width / columns)
        case .horizontal:
            return CGSize(width: view.bounds.width, height: view.bounds.height / columns)
        }
    }
    
    // MARK: Properties
    
    fileprivate lazy var collectionView: UICollectionView = { [unowned self] in
        $0.dataSource = self
        $0.delegate = self
        $0.register(ItemWithImage.self, forCellWithReuseIdentifier: String(describing: ItemWithImage.self))
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.contentInsetAdjustmentBehavior = .always
        $0.bounces = true
        $0.backgroundColor = .clear
        $0.layer.masksToBounds = false
        $0.clipsToBounds = false
        return $0
        }(UICollectionView(frame: .zero, collectionViewLayout: layout))
    
    fileprivate lazy var layout: UICollectionViewFlowLayout = {
        $0.minimumInteritemSpacing = 0
        $0.minimumLineSpacing = 0
        $0.sectionInset = .zero
        return $0
    }(UICollectionViewFlowLayout())

    fileprivate var selection: Selection?
    fileprivate var assets: [VideoYLYL] = []
    fileprivate var selectedAssets: [VideoYLYL] = []
    
    // MARK: Initialize
    
    required public init(flow: UICollectionView.ScrollDirection, paging: Bool, selection: Selection) {
        super.init(nibName: nil, bundle: nil)
        
        self.selection = selection
        self.assets = acutality
        self.layout.scrollDirection = flow
        
        self.collectionView.isPagingEnabled = paging
        
        switch selection {
            
        case .single(_):
            collectionView.allowsSelection = true
        case .multiple(_):
            collectionView.allowsMultipleSelection = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NSLog("has deinitialized")
    }
    
    override func loadView() {
        view = collectionView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var videoItems = [VPKVideoType]()
        for vid in self.acutality{
            videoItems.append(VPKVideoType.remote(url: ScriptBase.Video_URL + vid.getPathVideo(), placeholderURLName: "https://usweb-cdn.ustwo.com/ustwo-production/uploads/2017/03/Ustwo-Final-2-640x320.jpg"))
        }
        self.videoPrefetcher = VPKCollectionViewPrefetchSynchronize(videoItems: videoItems)
        self.collectionView.prefetchDataSource = self
        self.collectionView.reloadData()
        
    }
   var acutality : [VideoYLYL] = []
    
    func checkStatus(completionHandler: @escaping ([PHAsset]) -> ()) {
        switch PHPhotoLibrary.authorizationStatus() {
            
        case .notDetermined:
            /// This case means the user is prompted for the first time for allowing contacts
            Assets.requestAccess { [unowned self] status in
                self.checkStatus(completionHandler: completionHandler)
            }
            
        case .authorized:
            /// Authorization granted by user for this app.
            DispatchQueue.main.async {
                self.fetchPhotos(completionHandler: completionHandler)
            }
            
        case .denied, .restricted:
            /// User has denied the current app to access the contacts.
            let productName = Bundle.main.infoDictionary!["CFBundleName"]!
            let alert = UIAlertController(title: "Permission denied", message: "\(productName) does not have access to contacts. Please, allow the application to access to your photo library.", preferredStyle: .alert)
            alert.addAction(title: "Settings", style: .destructive) { action in
                if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(settingsURL)
                }
            }
            let action  = UIAlertAction(title: "OK", style: .cancel) { alert in
            self.alertController?.dismiss(animated: true)
            }
           alert.addAction(action)
            
        }
    }
    
    func fetchPhotos(completionHandler: @escaping ([PHAsset]) -> ()) {
        Assets.fetch { [unowned self] result in
            switch result {
                
            case .success(let assets):
                completionHandler(assets)
                
            case .error(let error):
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                let action  = UIAlertAction(title: "OK", style: .cancel) { alert in
                    self.alertController?.dismiss(animated: true)
                }
                alert.addAction(action)
              
            }
        }
    }
}

// MARK: - CollectionViewDelegate

extension PhotoLibraryPickerViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        let asset =  self.acutality[indexPath.row]
        switch selection {
            
        case .single(let action)?:
            action?(asset)
            
        case .multiple(let action)?:
            var found = false
            for element in selectedAssets {
                if element.getPathVideo() == asset.getPathVideo(){
                    found = true
                }
            }
            if found {
                let cell = collectionView.cellForItem(at: indexPath) as! ItemWithImage
                cell.imageView.player?.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
                cell.imageView.player?.pause()
            }else{
                let cell = collectionView.cellForItem(at: indexPath) as! ItemWithImage
                 cell.imageView.player?.play()
            }
            found
                ? selectedAssets.remove( asset)
                : selectedAssets.append(asset)
            action?(selectedAssets)
            
        case .none: break }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let videoCell = cell as? ItemWithImage else {return }
        videoCell.imageView.player?.pause()
        videoCell.imageView.player = nil
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
         let asset =  self.acutality[indexPath.row]
        switch selection {
        case .multiple(let action)?:
            var found = false
            for element in selectedAssets {
                if element.getPathVideo() == asset.getPathVideo(){
                    found = true
                }
            }
            if found {
                let cell = collectionView.cellForItem(at: indexPath) as! ItemWithImage
                cell.imageView.player?.seek(to: CMTime(seconds: 0, preferredTimescale: 1))
                cell.imageView.player?.pause()
            }else{
                let cell = collectionView.cellForItem(at: indexPath) as! ItemWithImage
                cell.imageView.player?.play()
            }
            found
                ? selectedAssets.remove(asset)
                : selectedAssets.append(asset)
            action?(selectedAssets)
        default: break }
    }
}

// MARK: - CollectionViewDataSource

extension PhotoLibraryPickerViewController: UICollectionViewDataSource,UICollectionViewDataSourcePrefetching {
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        videoPrefetcher?.collectionView(collectionView, prefetchItemsAt: indexPaths)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return acutality.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let item = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ItemWithImage.self), for: indexPath) as? ItemWithImage else { return UICollectionViewCell() }
        
        //let videoType = VPKVideoType.remote(url: ScriptBase.Video_URL + acutality[indexPath.row].getPathVideo(), placeholderURLName: "https://usweb-cdn.ustwo.com/ustwo-production/uploads/2017/03/Ustwo-Final-2-640x320.jpg")
        let asset  = AVAsset(url: URL(string: ScriptBase.Video_URL + acutality[indexPath.row].getPathVideo())!)
       
            let player = AVPlayerItem(asset: asset)
        
        print("URL : ",ScriptBase.Video_URL + acutality[indexPath.row].getPathVideo())
            let videoPlayer = AVPlayer(playerItem: player)
        item.imageView.playerLayer.player = videoPlayer
        return item
    }
}

// MARK: - CollectionViewDelegateFlowLayout

extension PhotoLibraryPickerViewController: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize
    }
}

extension Array {
    
    @discardableResult
    mutating func append(_ newArray: Array) -> CountableRange<Int> {
        let range = count..<(count + newArray.count)
        self += newArray
        return range
    }
    
    @discardableResult
    mutating func insert(_ newArray: Array, at index: Int) -> CountableRange<Int> {
        let mIndex = Swift.max(0, index)
        let start = Swift.min(count, mIndex)
        let end = start + newArray.count
        
        let left = self[0..<start]
        let right = self[start..<count]
        self = left + newArray + right
        return start..<end
    }
    
    mutating func remove<T: AnyObject> (_ element: T) {
        let anotherSelf = self
        
        removeAll(keepingCapacity: true)
        
        anotherSelf.each { (index: Int, current: Element) in
            if (current as! T) !== element {
                self.append(current)
            }
        }
    }
    
    func each(_ exe: (Int, Element) -> ()) {
        for (index, item) in enumerated() {
            exe(index, item)
        }
    }
}

extension Array where Element: Equatable {
    
    /// Remove Dublicates
    var unique: [Element] {
        // Thanks to https://github.com/sairamkotha for improving the method
        return self.reduce([]){ $0.contains($1) ? $0 : $0 + [$1] }
    }
    
    /// Check if array contains an array of elements.
    ///
    /// - Parameter elements: array of elements to check.
    /// - Returns: true if array contains all given items.
    public func contains(_ elements: [Element]) -> Bool {
        guard !elements.isEmpty else { // elements array is empty
            return false
        }
        var found = true
        for element in elements {
            if !contains(element) {
                found = false
            }
        }
        return found
    }
    
    /// All indexes of specified item.
    ///
    /// - Parameter item: item to check.
    /// - Returns: an array with all indexes of the given item.
    public func indexes(of item: Element) -> [Int] {
        var indexes: [Int] = []
        for index in 0..<self.count {
            if self[index] == item {
                indexes.append(index)
            }
        }
        return indexes
    }
    
    /// Remove all instances of an item from array.
    ///
    /// - Parameter item: item to remove.
    public mutating func removeAll(_ item: Element) {
        self = self.filter { $0 != item }
    }
    
    /// Creates an array of elements split into groups the length of size.
    /// If array can’t be split evenly, the final chunk will be the remaining elements.
    ///
    /// - parameter array: to chunk
    /// - parameter size: size of each chunk
    /// - returns: array elements chunked
    public func chunk(size: Int = 1) -> [[Element]] {
        var result = [[Element]]()
        var chunk = -1
        for (index, elem) in self.enumerated() {
            if index % size == 0 {
                result.append([Element]())
                chunk += 1
            }
            result[chunk].append(elem)
        }
        return result
    }
    
}

public extension Array {
    
    /// Random item from array.
    public var randomItem: Element? {
        if self.isEmpty { return nil }
        let index = Int(arc4random_uniform(UInt32(count)))
        return self[index]
    }
    
    /// Shuffled version of array.
    public var shuffled: [Element] {
        var arr = self
        for _ in 0..<10 {
            arr.sort { (_,_) in arc4random() < arc4random() }
        }
        return arr
    }
    
    /// Shuffle array.
    public mutating func shuffle() {
        // https://gist.github.com/ijoshsmith/5e3c7d8c2099a3fe8dc3
        for _ in 0..<10 {
            sort { (_,_) in arc4random() < arc4random() }
        }
    }
    
    /// Element at the given index if it exists.
    ///
    /// - Parameter index: index of element.
    /// - Returns: optional element (if exists).
    public func item(at index: Int) -> Element? {
        guard index >= 0 && index < count else { return nil }
        return self[index]
    }
}
extension UIViewController {
    
    var alertController: UIAlertController? {
        guard let alert = UIApplication.topViewController() as? UIAlertController else { return nil }
        return alert
    }
}
extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        return viewController
    }
}
