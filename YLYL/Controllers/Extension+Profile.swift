//
//  Extension+Profile.swift
//  YLYL
//
//  Created by macbook on 2/14/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation


class ExtensionProfile: RoundedUIImageView {
@IBInspectable var profil : Bool = true {
        didSet {
            self.isUserInteractionEnabled = true
            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.goToProfile(_:)))
            gesture.numberOfTapsRequired = 1
            self.addGestureRecognizer(gesture)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
    }
    @objc func goToProfile(_ sender: UITapGestureRecognizer) {
        print("GoToProfile")
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navigation = UINavigationController(rootViewController: storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable)
        
    appDelegate.navigation.present(navigation, animated: true, completion: nil)
        //appDelegate.navigation.pushViewController(storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable, animated: true)
        
        
    }
}

class NavigationProfile : ServerUpdateDelegate{
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.navigation = self.navigationController ?? UINavigationController()
        //NotificationCenter.default.addObserver(self, selector: #selector(self.RoomListener(_:)), name: NSNotification.Name.init("VideoRoom"), object: nil)
        
        }
    
    override func viewWillDisappear(_ animated: Bool) {
      //  NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init("VideoRoom"), object: nil)
    }
}
