//
//  AuthentificationController.swift
//  YLYL
//
//  Created by macbook on 1/4/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import NotificationBannerSwift
import OneSignal
import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
import AuthenticationServices
class AuthentificationController: ServerUpdateDelegate,UITextFieldDelegate,OSSubscriptionObserver ,GIDSignInDelegate,GIDSignInUIDelegate{
    
    @IBOutlet weak var appleView: UIView!
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user != nil{
            print("User : ",user.profile.familyName)
            
            let settings : Parameters = [
                "firstName" : user.profile.givenName ?? "",
                "lastName" : user.profile.familyName ?? "",
                "password" : String(arc4random()),
                "email" : user.profile.email ?? "",
                "username" : (user.profile.familyName ?? "") + "_" + user.userID,
                "googleId" :  user.userID,
                "userImageURL" : user.profile.imageURL(withDimension: 400).absoluteString
            ]
            let header: HTTPHeaders = [
                "Content-Type" : "application/json"
            ]
            Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                if response.error == nil {
                    var resp = JSON(response.data)
                    resp = resp["user"]
                    print("responseGoogle : ",resp)
                    GIDSignIn.sharedInstance()?.signOut()
                    SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                    if resp != JSON.null {
                    if resp["pushEnabled"].exists() {
                        if resp["pushEnabled"].stringValue == "1" {
                            self.ConnectTopush()
                        }else{
                            OneSignal.setSubscription(false)
                        }
                    }else{
                        self.ConnectTopush()
                    }
                   
                    UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    
                    if self.determineEligibality() {
                        self.performSegue(withIdentifier: "go_to_home", sender: self)
                    }else{
                        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
                       
                        vc.navigation = self.navigationController
                        print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    print("errorGoogle: ",response.error)
                    GIDSignIn.sharedInstance()?.signOut()
                }
                }
            }
        }
    }
    func determineEligibality() -> Bool{
        if UserDefaults.standard.object(forKey: "UserZonzay") != nil {
            
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            let a =  JSON(data: dataFromString!)
            if (a["userEmail"].exists() == false ) || (a["userBirthday"].exists() == false) || (a["userGender"].exists() == false) || (a["userName"].exists() == false) || (a["alphaCode"].exists() == false) {
                return false
            }else {
                
                return true
            }
        }else{
            return false
        }
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        //Retrieving Alert
    }
    
    
    /*
     * username TextField
     */
    @IBOutlet weak var usernameTF:TextField!
    /*
     * password TextField
     */
    @IBOutlet weak var passwordTF:PasswordTextField!
    /*
     * Show Password CheckBox
     */
    @IBOutlet weak var ShowMP:Checkbox!
    /*
     * Login Button
     */
    @IBOutlet weak var ConnectBTN: UIButton!
    
    var CanLogin = true
    
    @IBOutlet weak var passwordForgotContainer: UIVisualEffectView!
    
    @IBOutlet weak var forgotPasswordTF: UITextField!
   
     let _notificationToast = CustomToast.shared
    
    func configureAppleSign() {
        if #available(iOS 13.0, *) {
            let authorizationButton = ASAuthorizationAppleIDButton(type: .continue, style: .black)
            authorizationButton.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false
            //authorizationButton.frame = self.appleView.frame
           
            let heightConstraint = authorizationButton.heightAnchor.constraint(equalToConstant: 40)
                   
            authorizationButton.heightAnchor.constraint(equalToConstant: 30).isActive = false
           
            authorizationButton.addConstraint(heightConstraint)
            let widthConstraint = authorizationButton.widthAnchor.constraint(equalToConstant: self.appleView.frame.width)
            authorizationButton.addConstraint(widthConstraint)
            self.appleView.addSubview(authorizationButton)
            self.appleView.backgroundColor = .clear
            print("Authorization : ",authorizationButton.frame)
        } else {
            // Fallback on earlier versions
            self.appleView.isHidden = true
        }
   
       
    }
    @available(iOS 13.0, *)
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
        
        // Create an authorization controller with the given requests.
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    @available(iOS 13.0, *)
    @objc private func handleLogInWithAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    var oneTimeAppleId = true
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
     /*   if #available(iOS 13.0, *) {
            if oneTimeAppleId {
                oneTimeAppleId = false
            performExistingAccountSetupFlows()
            }
        } else {
            // Fallback on earlier versions
        } */
    }
    /*
     * Init Of Functions
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureAppleSign()
        createPaddingAndRadius(textField: usernameTF)
        createPaddingAndRadiusMP(textField: passwordTF)
        usernameTF.delegate = self
        passwordTF.delegate = self
        forgotPasswordTF.delegate = self
        forgotPasswordTF.nextNavigationField = self.forgotPasswordTF
        //configureCheckBox()
        configureConnectButton(button: ConnectBTN)
        configureGoogle()
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToHome(_:)), name: NSNotification.Name.init("FromEditToHome"), object: nil)
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    @objc func goToHome(_ notification: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init("FromEditToHome"), object: nil)
      self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
    // self.performSegue(withIdentifier: "go_to_home", sender: self)
    }
    func configureGoogle(){
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.clientID = "745644600435-12172m5etk5kvljb3pp7cjr9g28mja7h.apps.googleusercontent.com"
        GIDSignIn.sharedInstance()?.shouldFetchBasicProfile  = true
        GIDSignIn.sharedInstance()?.scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance()?.scopes.append("https://www.googleapis.com/auth/plus.me")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * Configure the style and action of the checkbox
     */
    func configureCheckBox(){
        ShowMP.borderStyle = .square
        ShowMP.checkmarkStyle = .tick
        ShowMP.checkmarkSize = 0.7
        ShowMP.uncheckedBorderColor = UIColor.gray
        ShowMP.checkedBorderColor = UIColor(red: 23/255, green: 118/255, blue: 174/255, alpha: 1)
        ShowMP.valueChanged = { (value) in
             print("square checkbox value change: \(value)")
            self.passwordTF.isSecureTextEntry = !value
        }
        print("Size: ",ShowMP.frame.size)
    }
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
        button.layer.cornerRadius = 9.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(LoginButtonAction(_:)), for: .touchUpInside)
    }
    /*
     * Creating the padding textFields styles
     */
    func createPaddingAndRadiusMP(textField: PasswordTextField){
        textField.layer.cornerRadius = 7
        
        textField.layer.masksToBounds = true
        if textField.tag == 0 {
            let image = UIImage(named: "EmailTextField")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            //textField.leftView?.frame.size = CGSize(width: image!.size.width + 20, height: image!.size.height )
            
            textField.leftViewMode = .always
        }else{
            let image = UIImage(named: "PasswordTextField")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            //textField.leftView?.frame.size = CGSize(width: image!.size.width + 20, height: image!.size.height )
            
            textField.leftViewMode = .always
            textField.isSecureTextEntry = true
        }
        
    }
    /*
     * Creating the padding textFields styles
     */
    func createPaddingAndRadius(textField: TextField){
        textField.layer.cornerRadius = 7
        
        textField.layer.masksToBounds = true
        if textField.tag == 0 {
            let image = UIImage(named: "EmailTextField")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            //textField.leftView?.frame.size = CGSize(width: image!.size.width + 40, height: image!.size.height )
            //textField.leftView?.layoutIfNeeded()
            //imageView.frame = textField.leftViewRect(forBounds: textField.bounds)
           // textField.leftViewRect(forBounds: textField.leftView!.frame)
            textField.leftViewMode = .always
        }else{
            let image = UIImage(named: "PasswordTextField")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
           // textField.leftView?.frame.size = CGSize(width: image!.size.width + 20, height: image!.size.height )
            
            textField.leftViewMode = .always
            textField.isSecureTextEntry = true
        }
        
    }
    /*
     * Forgot My Password Action
     */
    @IBAction func ForgotMPAction(_ sender: UITapGestureRecognizer) {
        self.passwordForgotContainer.alpha = 0
        self.passwordForgotContainer.isHidden = false
        UIView.animate(withDuration: 0.8, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.passwordForgotContainer.alpha = 1
        }) { _ in
            
        }
    }
    /*
     * Hide the navigation Bar
     */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func LoginButtonAction(_ sender: UIButton) {
        //self.LoginUser()
        if CanLogin {
            self.CanLogin = false
        self.LoginUser { (verif) in
            self.CanLogin = true
        }
        }
    }
  

    func LoginUser( _ completion: @escaping ( _ bool:Bool) -> Void){
        print("Go...")
        if usernameTF.text != "" && passwordTF.text != "" {
            
            var settings : Parameters = [:]
            if usernameTF.text!.isValidEmail() == false {
                settings = [
                    "firstName" : usernameTF.text!,
                    "password" : passwordTF.text!
                ]
            }else{
                settings = [
                    "email" : usernameTF.text!,
                    "password" : passwordTF.text!
                ]
            }
            SVProgressHUD.show()
            Alamofire.request(ScriptBase.sharedInstance.login, method: .post, parameters: settings,encoding: JSONEncoding.default).responseJSON { response in
                DispatchQueue.main.async(execute: {
                    SVProgressHUD.dismiss()
                })
                if response.error == nil {
                  
                    var resp = JSON(response.data!)
                 
                  
                      print(resp)
                    if resp["message"].exists() {
                        
                        if resp["message"].stringValue == "Wrong password" || resp["message"].stringValue == "Please check credentials"{
                            
                            print("error: ",resp["message"].stringValue)
                            let banner = NotificationBanner(customView: self._notificationToast)
                            self._notificationToast.LabelN.text = Localization("Wrong credentiels")
                            banner.customBannerHeight = 80
                            banner.show(queuePosition: .front, bannerPosition: .top)
                            completion(true)
                        }else{
                            if resp["message"].stringValue == "Please Validate Your Email" {
                                
                                let banner = NotificationBanner(customView: self._notificationToast)
                                self._notificationToast.LabelN.text = Localization("PleaseValidateEmail")
                                banner.customBannerHeight = 80
                                banner.show(queuePosition: .front, bannerPosition: .top)
                                completion(true)
                            }else{
                                let banner = NotificationBanner(customView: self._notificationToast)
                                self._notificationToast.LabelN.text = Localization("PleaseValidateEmail")
                                banner.customBannerHeight = 80
                                banner.show(queuePosition: .front, bannerPosition: .top)
                                completion(true)
                            }
                          
                        }
                    }else{
                        resp = resp["user"]
                        
                        SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                        if resp["pushEnabled"].exists() {
                            if resp["pushEnabled"].stringValue == "1" {
                                self.ConnectTopush()
                            }else{
                                OneSignal.setSubscription(false)
                            }
                        }else{
                            self.ConnectTopush()
                        }
                        completion(true)
                        
                     UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                        print("Userdefautls Save : ",UserDefaults.standard.synchronize())
                        if resp["listInterests"].stringValue.contains(";") {
                       self.performSegue(withIdentifier: "go_to_home", sender: self)
                        }else{
                            self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
                        }
                    }
                }else{
                    let banner = NotificationBanner(customView: self._notificationToast)
                    self._notificationToast.LabelN.text = "There was an error"
                    banner.customBannerHeight = 80
                    banner.show(queuePosition: .front, bannerPosition: .top)
                    completion(true)
                    print("there was an error")
                }
            }
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            
            self._notificationToast.LabelN.text = Localization("FillBlanks")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
            completion(true)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_to_passionFromLogin" {
            (segue.destination as! PassionController).isFirstTime = true
        }
        
    }
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
    @IBAction func connectViaTwitter(_ sender:UIButton){
     
        TWTRTwitter.sharedInstance().logIn() { (session, errorOne) in
            if session != nil {
               
                let client = TWTRAPIClient.withCurrentUser()
                
                client.requestEmail(forCurrentUser: { email, error  in
                    TWTRAPIClient.withCurrentUser().loadUser(withID: session!.userID, completion: { (user, errorTwo) in
                        if user != nil {
                    if email != nil {
                        
                       print("sign in as ", email)
                        let settings : Parameters = [
                            "firstName" : user!.screenName,
                            "lastName" : user!.name,
                            "password" : String(arc4random()),
                            "email" :   email!,
                            "username" : session!.userName,
                            "twitterId" :  session!.userID,
                            "userImageURL" : user!.profileImageURL
                        ]
                       
                        let header: HTTPHeaders = [
                            "Content-Type" : "application/json"
                        ]
                        Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                            if response.error == nil {
                                var resp = JSON(response.data)
                                resp = resp["user"]
                                SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                                print("responseTwitter : ",resp)
                                GIDSignIn.sharedInstance()?.signOut()
                                
                                if resp["pushEnabled"].exists() {
                                    if resp["pushEnabled"].stringValue == "1" {
                                        self.ConnectTopush()
                                    }else{
                                        OneSignal.setSubscription(false)
                                    }
                                }else{
                                    self.ConnectTopush()
                                }
                                
                                UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                                UserDefaults.standard.synchronize()
                                if self.determineEligibality() {
                                    if resp["listInterests"].stringValue.contains(";"){
                                    self.performSegue(withIdentifier: "go_to_home", sender: self)
                                    }else{
                                        self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
                                    }
                                }else{
                                    let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
                                   
                                    print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                
                            }else{
                                print("errorTwitter: ",response.error)
                                
                            }
                        }
                    }else{
                        
                    }
                        }
                          })
                })
            }else{
                print("errorTwitter: ",errorOne?.localizedDescription)
            }
        }
    }
    @IBAction func connectViaGoogle(_ sender: UIButton){
        GIDSignIn.sharedInstance()?.signIn()
    }
   @IBAction func connectViaFacebook(_ sender : UIButton){
    let fbLoginManager : LoginManager = LoginManager()
    if AccessToken.current == nil {
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self){(result,error) in
                if error == nil {
                    let fbLoginResult : LoginManagerLoginResult = result!
                    if fbLoginResult.grantedPermissions.count != 0{
                        if fbLoginResult.grantedPermissions.contains("email"){
                            self.getFBUserData()
                        }
                    }else{
                        print("no permissions")
                    }
                }else{
                    print(error?.localizedDescription)
                }
                
            }
        }else{
            self.getFBUserData()
        }
    }
    func getFBUserData(){
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["fields" : "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if error == nil {
                    LoginManager.init().logOut()
                    let res = JSON(result as Any)
                     print("facebook : ",res)
                    let settings : Parameters = [
                        "firstName" : res["first_name"].stringValue,
                        "lastName" : res["last_name"].stringValue,
                        "password" : String(arc4random()),
                        "email" : res["email"].stringValue,
                        "facebookId" :  res["id"].stringValue
                    ]
                    //                        "userImageURL" : res["picture"]["data"]["url"].stringValue

                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json"
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                        if response.error == nil {
                             var resp = JSON(response.data)
                            resp = resp["user"]
                            print("responseFacebook : ",resp)
                    SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                            if resp["pushEnabled"].exists() {
                                if resp["pushEnabled"].stringValue == "1" {
                                    self.ConnectTopush()
                                }else{
                                    OneSignal.setSubscription(false)
                                }
                            }else{
                                self.ConnectTopush()
                            }
                            
                            UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                            if self.determineEligibality() {
                            self.performSegue(withIdentifier: "go_to_home", sender: self)
                            }else{
                                let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EdcallitProfileReworked
                               
                               vc.navigation = self.navigationController
                                print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                                self.navigationController?.pushViewController(vc, animated: true)
                                //self.navigationController?.present(vc, animated: true, completion: nil)
                            }
                        }else{
                            print("errorFacebook: ",response.error)
                        }
                         }
                }else{
                    print("FBSDK : ",error?.localizedDescription)
                }
                
            })
        }
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    let params : Parameters = [
                        "userId" : a["_id"].stringValue ,
                        "playerId" : stateChanges.to.userId,
                        "type" : "ios"
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.setIosPlayerId , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            //      LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            print("NotificationsParser : ",b)
                            UserDefaults.standard.setValue(b.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
                }catch{
                    
                }
            }
            
        }
    }
    
    @IBAction func HidePasswordContainer(_ sender: UITapGestureRecognizer) {
        self.passwordForgotContainer.alpha = 1
        self.passwordForgotContainer.isHidden = false
        UIView.animate(withDuration: 0.8, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.passwordForgotContainer.alpha = 0
        }) { _ in
            self.passwordForgotContainer.isHidden = true
            self.passwordForgotContainer.alpha = 1
        }
    }
    @IBAction func BackForgotPassword(_ sender: UIButton) {
        self.passwordForgotContainer.alpha = 1
        self.passwordForgotContainer.isHidden = false
        
        UIView.animate(withDuration: 0.8, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.passwordForgotContainer.alpha = 0
        }) { _ in
            self.passwordForgotContainer.isHidden = true
            self.passwordForgotContainer.alpha = 1
            self.forgotPasswordTF.text = ""
        }
    }
    
    @IBAction func SubmitForgotPassword(_ sender: Any) {
        if (forgotPasswordTF.text?.isValidEmail())! {
            
            
            
            let settings : Parameters = [
                "userEmail" :   self.forgotPasswordTF.text!
            ]
            
            print(settings)
            Alamofire.request(ScriptBase.sharedInstance.forgotPassword, method: .post, parameters: settings, encoding: JSONEncoding.default).responseString { response in
                if response.data != nil {
                    let res = String(data: response.data ?? Data(), encoding: .utf8)!
                    print("REQU1 : ",res)
                    //self.jokes = data
                    if res == "invalid" {
                        let banner = NotificationBanner(customView: self._notificationToast)
                        self._notificationToast.LabelN.text = Localization("ForgotPasswordEmail")
                        banner.customBannerHeight = 80
                        banner.show(queuePosition: .front, bannerPosition: .top)
                    }else{
                        let alert = UIAlertController(title: Localization("Forgot password"), message: Localization("ForgotSuccess"), preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .cancel, handler: { alert in
                            self.passwordForgotContainer.alpha = 1
                            self.passwordForgotContainer.isHidden = false
                            UIView.animate(withDuration: 0.8, delay: 0.1, options: [.curveEaseInOut], animations: {
                                self.passwordForgotContainer.alpha = 0
                            }) { _ in
                                self.passwordForgotContainer.isHidden = true
                                self.passwordForgotContainer.alpha = 1
                            }
                        })
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    let banner = NotificationBanner(customView: self._notificationToast)
                    self._notificationToast.LabelN.text = Localization("FillBlanks")
                    banner.customBannerHeight = 80
                    banner.show(queuePosition: .front, bannerPosition: .top)
                    //self.defaultMessage.isHidden = false
                    
                }
            }
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = Localization("ValidEmailAddress")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
    }
}
class customTextfieldImageView: UIImageView {
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width / 8, height: targetSize.height)
    }
}
@available(iOS 13.0, *)
extension AuthentificationController : ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("error on apple")
    }
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        // if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential
       {
            print("new account")
            // Create an account in your system.
        //let appleIDCredential = authorization.credential as! ASAuthorizationAppleIDCredential
        if KeychainItem.currentUserEmail == nil {
           
            let settings : Parameters = [
                                       "firstName" : appleIDCredential.fullName?.givenName ?? "",
                                       "lastName" : appleIDCredential.fullName?.familyName ?? "",
                                       "password" : String(arc4random()),
                                       "email" :   appleIDCredential.email ?? "",
                                       "username" : "",
                                       "appleId" :  appleIDCredential.user,
                                       "userImageURL" : ""
                                   ]
                                  
                                   let header: HTTPHeaders = [
                                       "Content-Type" : "application/json"
                                   ]
            
                                  print(settings)
            Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                                       if response.error == nil {
                                        KeychainItem.currentUserIdentifier = appleIDCredential.user
                                        KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
                                        KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
                                        KeychainItem.currentUserEmail = appleIDCredential.email
                                           var resp = JSON(response.data)
                                           resp = resp["user"]
                                           SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                                           print("responseTwitter : ",resp)
                                           GIDSignIn.sharedInstance()?.signOut()
                                           
                                           if resp["pushEnabled"].exists() {
                                               if resp["pushEnabled"].stringValue == "1" {
                                                   self.ConnectTopush()
                                               }else{
                                                   OneSignal.setSubscription(false)
                                               }
                                           }else{
                                               self.ConnectTopush()
                                           }
                                           
                                           UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                                           UserDefaults.standard.synchronize()
                                           if self.determineEligibality() {
                                               if resp["listInterests"].stringValue.contains(";"){
                                               self.performSegue(withIdentifier: "go_to_home", sender: self)
                                               }else{
                                                   self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
                                               }
                                           }else{
                                               let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                                               let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
                                              
                                               print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                                               self.navigationController?.pushViewController(vc, animated: true)
                                           }
                                           
                                       }else{
                                           print("errorApple: ",response.error)
                                           
                                       }
                                   }
        }else{
            
            print("not okay : ", KeychainItem.currentUserEmail)
            let settings : Parameters = [
                "firstName" : KeychainItem.currentUserFirstName ?? "",
                "lastName" : KeychainItem.currentUserLastName ?? "",
                                                  "password" : String(arc4random()),
                                                  "email" :   KeychainItem.currentUserEmail ?? "",
                                                  "username" : "",
                                                  "appleId" :  KeychainItem.currentUserIdentifier ?? "",
                                                  "userImageURL" : ""
                                              ]
                                             
                                              let header: HTTPHeaders = [
                                                  "Content-Type" : "application/json"
                                              ]
                       
                                             print(settings)
                       Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                                                  if response.error == nil {
                                                   KeychainItem.currentUserIdentifier = appleIDCredential.user
                                                   KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
                                                   KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
                                                   KeychainItem.currentUserEmail = appleIDCredential.email
                                                      var resp = JSON(response.data)
                                                      resp = resp["user"]
                                                      SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                                                      print("responseTwitter : ",resp)
                                                      GIDSignIn.sharedInstance()?.signOut()
                                                      
                                                      if resp["pushEnabled"].exists() {
                                                          if resp["pushEnabled"].stringValue == "1" {
                                                              self.ConnectTopush()
                                                          }else{
                                                              OneSignal.setSubscription(false)
                                                          }
                                                      }else{
                                                          self.ConnectTopush()
                                                      }
                                                      
                                                      UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                                                      UserDefaults.standard.synchronize()
                                                      if self.determineEligibality() {
                                                          if resp["listInterests"].stringValue.contains(";"){
                                                          self.performSegue(withIdentifier: "go_to_home", sender: self)
                                                          }else{
                                                              self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
                                                          }
                                                      }else{
                                                          let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                                                          let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
                                                         
                                                          print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                                                          self.navigationController?.pushViewController(vc, animated: true)
                                                      }
                                                      
                                                  }else{
                                                      print("errorApple: ",response.error)
                                                      
                                                  }
                                              }
        }
            //Navigate to other view controller
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            print("existing account")
            // Sign in using an existing iCloud Keychain credential.
            print(KeychainItem.currentUserEmail)
            print(passwordCredential)
            let settings : Parameters = [
                 "firstName" : KeychainItem.currentUserFirstName ?? "",
                 "lastName" : KeychainItem.currentUserLastName ?? "",
                 "password" : String(arc4random()),
                 "email" :   KeychainItem.currentUserEmail ?? "",
                 "username" : "",
                 "appleId" :  passwordCredential.user,
                 "userImageURL" : ""
             ]
            
             let header: HTTPHeaders = [
                 "Content-Type" : "application/json"
             ]
             Alamofire.request(ScriptBase.sharedInstance.registerApi, method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header).responseJSON { response in
                 if response.error == nil {
                     var resp = JSON(response.data)
                     resp = resp["user"]
                     SocketIOManager.sharedInstance.updateSocketId(userId: resp["_id"].stringValue)
                     print("responseTwitter : ",resp)
                     GIDSignIn.sharedInstance()?.signOut()
                     
                     if resp["pushEnabled"].exists() {
                         if resp["pushEnabled"].stringValue == "1" {
                             self.ConnectTopush()
                         }else{
                             OneSignal.setSubscription(false)
                         }
                     }else{
                         self.ConnectTopush()
                     }
                     
                     UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                     UserDefaults.standard.synchronize()
                     if self.determineEligibality() {
                         if resp["listInterests"].stringValue.contains(";"){
                         self.performSegue(withIdentifier: "go_to_home", sender: self)
                         }else{
                             self.performSegue(withIdentifier: "go_to_passionFromLogin", sender: self)
                         }
                     }else{
                         let storyboard = UIStoryboard(name: "Profile", bundle: nil)
                         let vc = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
                        
                         print("NavigationBefore :",self.navigationController?.restorationIdentifier)
                         self.navigationController?.pushViewController(vc, animated: true)
                     }
                     
                 }else{
                     print("errorApple: ",response.error)
                     
                 }
             }
            //Navigate to other view controller
        }
    }
    
    
}
