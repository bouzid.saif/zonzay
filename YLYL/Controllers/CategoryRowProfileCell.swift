//
//  CategoryRowProfileCell.swift
//  YLYL
//
//  Created by macbook on 2/5/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
class CategoryRow: UITableViewCell , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var collectionV : UICollectionView!
    var VideoOrJoke = "Video"
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if VideoOrJoke == "Video" {
            return Actuality.count
        }else if VideoOrJoke == "Joke"{
            return jokes.count
        }else {
            return 1
        }
    }
    var Actuality : [VideoYLYL] = []
    var jokes : [String] = []
    var userId: String  = ""
    var navigation: UINavigationController? = nil
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if VideoOrJoke == "Video" {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoCell", for: indexPath)
        cell.contentView.backgroundColor = UIColor(red: 228/255, green: 228/255, blue: 228/255, alpha: 0.5)
        cell.contentView.layer.cornerRadius = 5.0
        cell.contentView.layer.borderColor = UIColor.gray.cgColor
        cell.contentView.layer.borderWidth = 2.0
        cell.contentView.layer.shadowColor = UIColor(red: 225/255, green: 228/255, blue: 228/255, alpha: 1).cgColor
        cell.contentView.layer.shadowOpacity = 1.0
        cell.contentView.layer.shadowRadius = 5.0
        cell.contentView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
            self.configVideo(cell: cell, video: self.Actuality[indexPath.row],userId: userId)
        return cell
        }else if VideoOrJoke == "Joke"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "jokeCell", for: indexPath)
            cell.contentView.backgroundColor = UIColor(red: 228/255, green: 228/255, blue: 228/255, alpha: 0.5)
            cell.contentView.layer.cornerRadius = 5.0
            cell.contentView.layer.borderColor = UIColor.gray.cgColor
            cell.contentView.layer.borderWidth = 2.0
            cell.contentView.layer.shadowColor = UIColor(red: 225/255, green: 228/255, blue: 228/255, alpha: 1).cgColor
            cell.contentView.layer.shadowOpacity = 1.0
            cell.contentView.layer.shadowRadius = 5.0
            cell.contentView.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
            self.configJoke(cell: cell, joke: self.jokes[indexPath.row])
             return cell
        }else if VideoOrJoke == "VideoCellDef" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCellDef", for: indexPath)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JokesCellDef", for: indexPath)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeNext") as! HomeNextViewController
            
            vc.videoURL = self.Actuality[indexPath.row].getPathVideo()
            navigation!.pushViewController(vc, animated: true)
        }
    }
    func configVideo(cell:UICollectionViewCell,video:VideoYLYL,userId:String){
        let firstName = cell.viewWithTag(3) as! UILabel
         firstName.text = video.getVideoFirstUser().getFirstName() + " " + video.getVideoFirstUser().getLastName()
        let secondName = cell.viewWithTag(4) as! UILabel
        secondName.text = video.getVideoSecondUser().getFirstName() + " " + video.getVideoSecondUser().getLastName()
        let score = cell.viewWithTag(5) as! UILabel
        if video.getVideoFirstUser().getId() == userId {
            score.text = "Lost"
        }else{
            score.text = "Won"
        }
    }
    func configJoke(cell:UICollectionViewCell,joke:String){
        let jokeText = cell.viewWithTag(6) as! UILabel
        jokeText.text = joke
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if VideoOrJoke != "VideoCellDef" && VideoOrJoke != "JokesCellDef" {
        let itemsPerRow:CGFloat = 2
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        print(CGSize(width: itemWidth, height: itemHeight))
        return CGSize(width: itemWidth, height: itemHeight)
        }else{
            let itemsPerRow:CGFloat = 2
            let hardCodedPadding:CGFloat = 5
            let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
            print(CGSize(width: collectionView.frame.width, height: itemHeight))
            return CGSize(width: collectionView.frame.width, height: itemHeight)
        }
    }
    
}
