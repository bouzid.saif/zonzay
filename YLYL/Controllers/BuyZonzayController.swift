//
//  BuyZonzayController.swift
//  YLYL
//
//  Created by macbook on 1/11/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import StoreKit
class BuyZonzayController: FixUIController,UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate{
   
    
    @IBOutlet weak var tableV: UITableView!
    @IBOutlet weak var bottom_message: UILabel!
    @IBOutlet weak var top_number_zonzay:UILabel!
    let Zonz100 = "com.regystone.zonzay.zonz"
     let Zonz200 = "com.regystone.zonzay.zonz200"
     let Zonz300 = "com.regystone.zonzay.zonz300"
     let Zonz500 = "com.regystone.zonzay.zonz500"
     let Zonz900 = "com.regystone.zonzay.zonz900"
     let Zonz1000 = "com.regystone.zonzay.zonz1000"
    var products : [SKProduct] = []
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableV.delegate = self
        tableV.dataSource = self
        
        PKIAPHandler.shared.setProductIds(ids: [Zonz100,Zonz200,Zonz300,Zonz500,Zonz900,Zonz1000])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([Zonz100,Zonz200,Zonz300,Zonz500,Zonz900,Zonz1000]))
        
         PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            for product in skProducts {
                print(product)
            }
        }
       
        self.navigationController?.isNavigationBarHidden = true
    }
    var zonayPoint = ["100","200","300","500","900","1000"]
    var zonzayMoney = ["1","2","2.99","5.99","8.99","9.5"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath)
        let numberZonzay = cell.viewWithTag(1) as! UILabel
        let zonzay_buy_btn = cell.viewWithTag(2) as! buttonZonz
        
            numberZonzay.text = zonayPoint[indexPath.row]
            zonzay_buy_btn.setTitle(zonzayMoney[indexPath.row] + "€", for: .normal)
        zonzay_buy_btn.index = indexPath.row
        zonzay_buy_btn.addTarget(self, action: #selector(self.buyZonzays(_:)), for: .touchUpInside)
       
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getZonz()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        buyZonzay(index: indexPath.row)
    }
    @objc func buyZonzays(_ sender: buttonZonz){
        
        PKIAPHandler.shared.purchase(product: products[sender.index]) { (purchaseAlertType, product, transaction) in
            if transaction != nil {
                
          
       
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "purchaseZonz" : self.zonayPoint[sender.index],
                "purchaseMoney" : product!.price.description(withLocale: Locale.current)
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.zonzPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                if data != JSON.null {
                    
                    let yesAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                        //do the work
                        self.dismiss(animated: true, completion: nil)
                    }
                    let zonzTotal = String(Int(self.top_number_zonzay.text! != "" ?self.top_number_zonzay.text! : "0" )! + Int(self.zonayPoint[sender.index])!)
                    self.top_number_zonzay.text = zonzTotal
                    let alert = UIAlertController(title: "Zonzay Point", message: "You have successfully brought " + self.zonayPoint[sender.index] + " zonzay!", preferredStyle: .alert)
                    alert.addAction(yesAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
            }else{
                
                let alert = UIAlertController(title: Localization("ZonzayPoints"), message: purchaseAlertType.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
       }
    }
    func restorePurchase(){
        PKIAPHandler.shared.restorePurchase()
    }
    func buyZonzay(index: Int){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "purchaseZonz" : zonayPoint[index],
                "purchaseMoney" : zonzayMoney[index]
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.zonzPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                if data != JSON.null {
                    
                    let yesAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                        //do the work
                        self.dismiss(animated: true, completion: nil)
                    }
                    let zonzTotal = String(Int(self.top_number_zonzay.text!)! + Int(self.zonayPoint[index])!)
                    self.top_number_zonzay.text = zonzTotal
                    let alert = UIAlertController(title: Localization("ZonzayPoints"), message: Localization("You have successfully brought") + self.zonayPoint[index] + " zonzs!", preferredStyle: .alert)
                    alert.addAction(yesAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func DoneAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                //print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                if zonz != "" {
                    self.top_number_zonzay.text = zonz
                    
                }else{
                    self.top_number_zonzay.text = "0"
                }
               
            }
            
        }catch {
            
        }
    }
}
class buttonZonz : UIButton {
    var index: Int = 0
}
