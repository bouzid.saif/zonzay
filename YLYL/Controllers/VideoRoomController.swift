
//
//  ViedeoRoomController.swift
//  YLYL
//
//  Created by macbook on 1/21/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
class VideoRoomController {
   
    
    func randomStringWithLength(len:Int) -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0...(len - 1){
            let length = UInt32(letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
            
        }
        return randomString as String
    }
    func attachRoomToJanus(roomId:NSNumber,completionHandler : @escaping (_ messageInfo : String) -> Void){
        
        let transaction  = self.randomStringWithLength(len: 12)
        let settings : Parameters = [
            "janus" : "create",
            "transaction" : transaction ,
            "id" : Int(truncating: roomId)
        ]
        
        let headers : HTTPHeaders = [
            "content-type" : "application/json"
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.JANUS_URL, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
           // print("ReponseCreate: ",JSON(response.data))
            let settingsTwo : Parameters = [
                "janus" : "attach",
                "transaction" : transaction,
                "plugin" : "janus.plugin.videoroom"
            ]
            Alamofire.request(ScriptBase.sharedInstance.JANUS_URL + roomId.stringValue, method: .post, parameters: settingsTwo, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
               //print("ReponseAttch: ",JSON(response.data))
                let roomTransaction = JSON(response.data ?? Data())
                //512000
                //64000
                let settingsThree : Parameters = [
                    "janus" : "message",
                    "transaction" : transaction,
                    "body" : [
                        "request" : "create" ,
                        "room" : Int(truncating: roomId) ,
                       // "videoorient_ext" : false,
                        "bitrate" : 512000,
                        "audiocode" : "isac32,opus",
                        "transport_wide_cc_ext" : true,
                        "audio_level_average" : 15,
                        "videocodec" : "vp8",
                        "notify_joining" : true
                        //"video_svc" : true,
                        //"fir_freq" : 10
                    ]
                ]
                Alamofire.request(ScriptBase.sharedInstance.JANUS_URL + roomId.stringValue + "/" + roomTransaction["data"]["id"].stringValue, method: .post, parameters: settingsThree, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    print("ReponseRoom: ",JSON(response.data ?? Data()))
                    let resp = JSON(response.data ?? Data())
                    if resp["plugindata"]["data"]["videoroom"].stringValue == "created" {
                        completionHandler("master")
                    }else{
                        completionHandler("slave")
                    }
                }
            }
        }
    }
}
