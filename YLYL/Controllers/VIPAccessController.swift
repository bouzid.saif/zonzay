//
//  VIPAccessController.swift
//  YLYL
//
//  Created by macbook on 2019-04-26.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import StoreKit
import SnapKit
import ChameleonFramework
class VIPAccessController: ServerUpdateDelegate,DoubleSliderValueChanged,SKProductsRequestDelegate {
    //VIP SECTION
    let ZonzWeek = "com.regystone.zonzay.PlusWeek"
    let ZonzMonth = "com.regystone.zonzay.PlusMonth"
    let ZonzYear = "com.regystone.zonzay.PlusYear"
    var products : [SKProduct] = []
    
    @IBOutlet weak var VIPLabel : UILabel!
    
    @IBOutlet weak var TrancheAgeLBL : UILabel!
    @IBOutlet weak var CibleLBL  : UILabel!
    @IBOutlet weak var choisirPref : UILabel!
    @IBOutlet weak var masculinLBL : UILabel!
     @IBOutlet weak var femininLBL : UILabel!
     @IBOutlet weak var autreLBL : UILabel!
     @IBOutlet weak var toutLBL : UILabel!
    @IBOutlet weak var ViewAgeRange: UIView!
    
    @IBOutlet weak var WeekBuyButton: UIButton!
    
    @IBOutlet weak var WeekBuyZonzButton: UIButton!
    @IBOutlet weak var zonzIMG: UIImageView!
    @IBOutlet weak var ViewGender: UIView!
    var animateView = true
    @IBOutlet weak var backButtonImage : UIButton!
    
    @IBOutlet weak var ViewContainerWorldLaugh: UIView!
    
    @IBOutlet weak var VIPBuyContainer: UIView!
    
    @IBOutlet weak var BuyBlurred: UIButton!
    @IBOutlet weak var container300Zonz : UIView!
     @IBOutlet weak var containerLabel300Zonz : UILabel!
     @IBOutlet weak var subscriptionMoneyButton : UIButton!
     @IBOutlet weak var subscriptionZonzButton : UIButton!
     @IBOutlet weak var subscriptionTitleButton : UIButton!
    func valueChanged(forMin value: Double) {
        print("MIN",value)
        minAge.text = String(Int(value))
        
    }
    
    @IBOutlet weak var diamondsContainer: UIView!
    
    @IBOutlet weak var playAllOverLBL: UILabel!
    
    @IBOutlet weak var chooseYPrefLBL: UILabel!
    
    @IBOutlet weak var determinYARangeLBL: UILabel!
    
    func valueChanged(forMax value: Double) {
        print("MAX",value)
        maxAge.text = String(Int(value))
    }
    @IBOutlet weak var ageSlider: DoubleSlider!
    
    @IBOutlet weak var minAge: UILabel!
    
    @IBOutlet weak var maxAge: UILabel!
    
    
    
    
    @IBOutlet weak var HeightContainer: NSLayoutConstraint!
    @IBOutlet weak var VIPContainer : UIView!
    
    @IBOutlet weak var HeightVIPBuy: NSLayoutConstraint!
    
    @IBOutlet weak var weekContainer: UIView!
    
    @IBOutlet weak var monthContainer: UIView!
    
    @IBOutlet weak var yearContainer: UIView!
    
    @IBOutlet weak var weekBTN: UIButton!
    @IBOutlet weak var monthBTN: UIButton!
    @IBOutlet weak var yearBTN: UIButton!
    @IBOutlet weak var plusBTN: UIButton!
    @IBOutlet weak var semaineLBL: UILabel!
    @IBOutlet weak var moisLBL: UILabel!
    @IBOutlet weak var anLBL: UILabel!
    @IBOutlet weak var choissirExpLBL: UILabel!
    @IBOutlet weak var rireMondeLBL: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
    }
    @IBAction func plusButtonJokes(_ sender: UIButton) {
        if animateView  {
        self.VIPBuyContainer.isHidden = false
        self.VIPBuyContainer.layoutIfNeeded()
        self.HeightVIPBuy.constant = 340
        animateView = false
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [.curveEaseIn], animations: {
            self.VIPBuyContainer.alpha = 1
            self.HeightContainer.constant = 340
            //self.HeightVIPBuy.constant = 340
            self.VIPBuyContainer.layoutIfNeeded()
            //self.BuyContainer.alpha = 0.92
        }) { _ in
            self.animateWeekContainer()
        }
        }
    }
    var notifEnabled = false
    var privateEnabled = false
    
    @IBOutlet weak var MaleLBL: UILabel!
    
    @IBOutlet weak var FemaleLBL: UILabel!
    
    @IBOutlet weak var OtherLBL: UILabel!
     @IBOutlet weak var AllLBL: UILabel!
    @IBOutlet weak var MaleButton : DLRadioButton!
    @IBOutlet weak var FemaleButton : DLRadioButton!
    @IBOutlet weak var OtherButton : DLRadioButton!
    @IBOutlet weak var AllButton : DLRadioButton!
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocalization()
        configureGestures()
        PKIAPHandler.shared.setProductIds(ids: [ZonzWeek,ZonzMonth,ZonzYear])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([ZonzWeek,ZonzMonth,ZonzYear]))
        
        //PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            
            for product in skProducts {
                if product.productIdentifier == self.ZonzWeek {
                     DispatchQueue.main.async {
                    self.weekBTN.setTitle(product.localizedPrice!, for: .normal)
                    self.subscriptionMoneyButton.setTitle(product.localizedPrice!, for: .normal)
                    self.WeekBuyButton.setTitle(product.localizedPrice!, for: .normal)
                    self.subscriptionZonzButton.setTitle("300", for: .normal)
                      self.WeekBuyZonzButton.setTitle("300", for: .normal)
                    }
                    //self.subscriptionZonzButton.addImage(imageName: "ZonzBig", afterLabel: true)
                }
                if product.productIdentifier == self.ZonzMonth {
                    DispatchQueue.main.async {
                        self.monthBTN.setTitle(product.localizedPrice!, for: .normal)
                    }
                    
                }
                if product.productIdentifier == self.ZonzYear {
                      DispatchQueue.main.async {
                    self.yearBTN.setTitle(product.localizedPrice!, for: .normal)
                    }
                }
                print(product)
            }
        }
        self.HeightContainer.constant = 275
        self.VIPBuyContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.HeightVIPBuy.constant = 0
        self.ageSlider.isUserInteractionEnabled = false
        self.ageSlider.valueChangedSlider = self
       
        
        guard let ab = UserDefaults.standard.value(forKey: "Subscription") as? String else {
            return
        }
      
        print("Subscription " , ab)
        if ab == "true"{
            
            self.animateView = false
            self.configureGenderLabels()
            if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                //GenderBTN.setTitle(UserDefaults.standard.value(forKey: "MatchChoice") as? String, for: .normal)
                switch UserDefaults.standard.value(forKey: "MatchChoice") as? String {
                case "Male" :
                    
                   
                self.MaleButton.isSelected = true
                case "Female" :
                  
                     self.FemaleButton.isSelected = true
                case "Other" :
                  
                     self.OtherButton.isSelected = true
                case "All" :
                    self.AllButton.isSelected = true
                default :
                    break
                }
                minAge.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxAge.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                ageSlider.lowerValue =  Double(minAge.text!)!
                ageSlider.upperValue = Double(maxAge.text!)!
                self.permuteColorObjects()
                self.redoAnimation = true
                
                self.animateWeekContainer()
                //hideContainers(false)
            }else{
                let ac = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ac.data(using: String.Encoding.utf8, allowLossyConversion: false)
                let a = JSON(data: dataFromString!)
                
                UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                UserDefaults.standard.setValue("18", forKey: "minChoice")
                UserDefaults.standard.setValue("75", forKey: "maxChoice")
                UserDefaults.standard.synchronize()
                switch UserDefaults.standard.value(forKey: "MatchChoice") as? String {
                case "Male" :
                    
                   
                    self.MaleButton.isSelected = true
                case "Female" :
                   
                    self.FemaleButton.isSelected = true
                case "Other" :
                    
                    self.OtherButton.isSelected = true
                case "All" :
                    self.AllButton.isSelected = true
                default :
                    break
                }
                minAge.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxAge.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                ageSlider.lowerValue =  Double(minAge.text!)!
                ageSlider.upperValue = Double(maxAge.text!)!
                self.permuteColorObjects()
                
            }
            
        }else{
            self.animateView = true
        }
    }
    func configureLocalization(){
        VIPLabel.text = Localization("VIPA")
         TrancheAgeLBL.text = Localization("tranche")
         CibleLBL.text = Localization("cible")
         choisirPref.text = Localization("Cpref")
         masculinLBL.text = Localization("Male")
         femininLBL.text = Localization("Female")
         autreLBL.text = Localization("Other")
         toutLBL.text = Localization("ALL")
         semaineLBL.text = Localization("Week")
         moisLBL.text = Localization("Month")
         anLBL.text = Localization("months")
         choissirExpLBL.text = Localization("ChooseExp")
         rireMondeLBL.text = Localization("LaughOverWorld")
         self.BuyBlurred.setTitle(Localization("Buy"), for: .normal)
         playAllOverLBL.text = Localization("playAllOverLBL")
        
         chooseYPrefLBL.text = Localization("chooseYPrefLBL")
        
         determinYARangeLBL.text = Localization("determinYARangeLBL")
    }
    func configureGenderLabels(){
        let tapGestureMale = UITapGestureRecognizer(target: self, action: #selector(self.MaleLBLTapped(_:)))
        let tapGestureFemale = UITapGestureRecognizer(target: self, action: #selector(self.FemaleLBLTapped(_:)))
        let tapGestureOther = UITapGestureRecognizer(target: self, action: #selector(self.OtherLBLTapped(_:)))
        let tapGestureAll =  UITapGestureRecognizer(target: self, action: #selector(self.AllLBLTapped(_:)))
        MaleLBL.isUserInteractionEnabled = true
        FemaleLBL.isUserInteractionEnabled = true
        OtherLBL.isUserInteractionEnabled = true
        AllLBL.isUserInteractionEnabled = true
        MaleButton.isUserInteractionEnabled = true
        FemaleButton.isUserInteractionEnabled = true
        OtherButton.isUserInteractionEnabled = true
        AllButton.isUserInteractionEnabled = true
        MaleLBL.addGestureRecognizer(tapGestureMale)
        FemaleLBL.addGestureRecognizer(tapGestureFemale)
        OtherLBL.addGestureRecognizer(tapGestureOther)
        AllLBL.addGestureRecognizer(tapGestureAll)
        
    }
    @objc func MaleLBLTapped(_ sender: UITapGestureRecognizer) {
      self.MaleButton.isSelected = true
    }
    @objc func FemaleLBLTapped(_ sender: UITapGestureRecognizer) {
       self.FemaleButton.isSelected = true
    }
    @objc func OtherLBLTapped(_ sender: UITapGestureRecognizer) {
       self.OtherButton.isSelected = true
    }
    @objc func AllLBLTapped(_ sender: UITapGestureRecognizer) {
        self.AllButton.isSelected = true
    }
    @IBOutlet weak var weekViewGesture : UIView!
    @IBOutlet weak var monthViewGesture : UIView!
    @IBOutlet weak var yearViewGesture : UIView!
    func configureGestures(){
        self.weekContainer.tag = 0
        self.monthContainer.tag = 1
        self.yearContainer.tag = 2
        let tapgestureWeek = UITapGestureRecognizer(target: self, action: #selector(self.weekAnimate(_:)))
        tapgestureWeek.numberOfTapsRequired = 1
        
        self.weekViewGesture.addGestureRecognizer(tapgestureWeek)
        self.weekContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.weekContainer.layer.borderWidth = 1
        self.weekViewGesture.isUserInteractionEnabled = true
        self.weekViewGesture.tag = 0
        self.monthViewGesture.tag = 1
        self.yearViewGesture.tag = 2
        let tapgestureMonth = UITapGestureRecognizer(target: self, action: #selector(self.monthAnimate(_:)))
        tapgestureMonth.numberOfTapsRequired = 1
        self.monthContainer.addGestureRecognizer(tapgestureMonth)
        self.monthContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.monthContainer.layer.borderWidth = 1
        self.monthContainer.isUserInteractionEnabled = true
        let tapgestureYear = UITapGestureRecognizer(target: self, action: #selector(self.yearAnimate(_:)))
        tapgestureYear.numberOfTapsRequired = 1
        self.yearContainer.addGestureRecognizer(tapgestureYear)
        self.yearContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.yearContainer.layer.borderWidth = 1
        self.yearContainer.isUserInteractionEnabled = true
    }
    var redoAnimation = true
    func animateWeekContainer(){
        
        UIView.animate(withDuration: 0.4, delay: 0.6, options: [.curveEaseInOut], animations: {
           self.WeekBuyButton.alpha = 0
            self.WeekBuyZonzButton.alpha = 1
            self.zonzIMG.alpha = 1
        }) { (complete) in
            UIView.animate(withDuration: 0.4, delay: 0.6, options: [.curveEaseInOut], animations: {
                self.WeekBuyButton.alpha = 1
                self.WeekBuyZonzButton.alpha = 0
                self.zonzIMG.alpha = 0
            }) { (complete) in
                if self.redoAnimation {
                    self.animateWeekContainer()
                }
            }
        }
    }
    func configureBlur(){
        self.ViewContainerWorldLaugh.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.ViewContainerWorldLaugh.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
        self.BuyBlurred.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.BuyBlurred.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.95))
        self.view.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.view.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
    }
    @objc func weekAnimate(_ sender: UITapGestureRecognizer) {
        
        if weekContainer.tag == 0 {
            deAnimateContainers()
            weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
        
    }
    
    @IBAction func weekButton (_ sender: UIButton){
        if weekContainer.tag == 0 {
            
            deAnimateContainers()
            weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    
    func deAnimateContainers(){
        self.BuyBlurred.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
            self.weekContainer.transform = CGAffineTransform.identity
            self.weekContainer.backgroundColor = UIColor.white
            self.weekContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.weekContainer.tag = 0
            self.monthContainer.transform = CGAffineTransform.identity
            self.monthContainer.backgroundColor = UIColor.white
            self.monthContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.monthContainer.tag = 1
            self.yearContainer.transform = CGAffineTransform.identity
            self.yearContainer.backgroundColor = UIColor.white
            self.yearContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.yearContainer.tag = 2
        }) { _ in
            
        }
    }
    @objc func monthAnimate(_ sender: UITapGestureRecognizer) {
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @IBAction func monthButton (_ sender: UIButton){
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @objc func yearAnimate(_ sender: UITapGestureRecognizer) {
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
    @IBAction func yearButton (_ sender: UIButton){
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
    func permuteColorObjects(){
        self.ViewGender.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ViewAgeRange.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.thumbTintColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.isUserInteractionEnabled = true
       self.MaleButton.indicatorColor =  UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.FemaleButton.indicatorColor =  UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.OtherButton.indicatorColor =  UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.AllButton.indicatorColor =  UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
    }
    @IBAction func ShowVIPBuy(_ sender: UITapGestureRecognizer) {
        if animateView {
            //self.BuyContainer.alpha  = 0
            //self.BuyContainer.isHidden = false
            //self.VIPBuyContainer.alpha = 0
            self.VIPBuyContainer.isHidden = false
            self.VIPBuyContainer.layoutIfNeeded()
            self.HeightVIPBuy.constant = 340
            animateView = false
            UIView.animate(withDuration: 0.4, delay: 0.1, options: [.curveEaseIn], animations: {
                self.animateWeekContainer()
                self.VIPBuyContainer.alpha = 1
                self.HeightContainer.constant = 340
                //self.HeightVIPBuy.constant = 340
                self.VIPBuyContainer.layoutIfNeeded()
                //self.BuyContainer.alpha = 0.92
            }) { _ in
                
            }
        }
    }
    @IBAction func BackVIPAction(_ sender: UIButton) {
        self.redoAnimation = false
        if (UserDefaults.standard.value(forKey: "Subscription") as! String) == "true" {
            if MaleButton.isSelected {
                UserDefaults.standard.setValue("Male", forKey: "MatchChoice")
            }else if FemaleButton.isSelected  {
                UserDefaults.standard.setValue("Female", forKey: "MatchChoice")
            }else if OtherButton.isSelected {
                UserDefaults.standard.setValue("Other", forKey: "MatchChoice")
            }else if AllButton.isSelected {
                  UserDefaults.standard.setValue("All", forKey: "MatchChoice")
            }
            //UserDefaults.standard.setValue(self.GenderBTN.title(for: .normal)!, forKey: "MatchChoice")
            UserDefaults.standard.setValue(self.minAge.text!, forKey: "minChoice")
            UserDefaults.standard.setValue(self.maxAge.text!, forKey: "maxChoice")
            UserDefaults.standard.synchronize()
        }
      /*  UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
            //self.BuyContainer.alpha  = 0
            self.VIPContainer.alpha = 0
            
        }) { _ in
            //self.BuyContainer.isHidden = true
            self.VIPContainer.isHidden = true
            self.VIPBuyContainer.isHidden = true
            self.VIPBuyContainer.alpha = 0
            self.HeightContainer.constant = 286
            self.animateView = true
        } */
        //self.dismiss(animated: true, completion: nil)
        UIView.performWithoutAnimation {
            hero.modalAnimationType = .zoomOut
            hero.dismissViewController()
        }
        
    }
    func determineType() -> PaymentType {
        if weekContainer.backgroundColor == UIColor.white {
            if monthContainer.backgroundColor == UIColor.white {
                if yearContainer.backgroundColor == UIColor.white {
                    return PaymentType.None
                }else{
                    return PaymentType.Year
                }
            }else{
                return PaymentType.Month
            }
        }else{
            return PaymentType.Week
        }
    }
    func determineIndex () -> Int {
        if weekContainer.backgroundColor == UIColor.white {
            if monthContainer.backgroundColor == UIColor.white {
                if yearContainer.backgroundColor == UIColor.white {
                    return -1
                }else{
                    return 2
                }
            }else{
                return 1
            }
        }else{
            return 0
        }
    }
    @IBAction func BuyWeekWithMoney(_ sender: UIButton) {
        PKIAPHandler.shared.purchase(product: products[0]) { (purchaseAlertType, product, transaction) in
            if transaction != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    
                    let settings : Parameters = [
                        "userId" : a["_id"].stringValue,
                         "duration" : self.determineType().rawValue
                    ]
                    let headers : HTTPHeaders = [
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                    ]
                    print(settings)
                    print(headers)
                    Alamofire.request(ScriptBase.sharedInstance.filterPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        let data = JSON(response.data)
                        if data != JSON.null {
                         
                            let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                                //do the work
                                self.permuteColorObjects()
                                UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
                                    self.container300Zonz.alpha = 0
                                    self.VIPContainer.alpha = 1
                                    self.VIPBuyContainer.alpha = 0
                                    self.HeightContainer.constant = 275
                                }) { _ in
                                     self.container300Zonz.isHidden = true
                                    self.VIPBuyContainer.isHidden = true
                                }
                                
                            }
                            let dateFormatterNow = DateFormatter()
                            dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                            dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                            dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                            let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                            let dateFormatterFinal = DateFormatter()
                            dateFormatterFinal.dateFormat = "dd-MM-yyy"
                            let date = dateFormatterFinal.string(from: dateNow!)
                            let alert = UIAlertController(title: "Zonzay Plus", message: Localization("PaymentVU") + date, preferredStyle: .alert)
                            alert.addAction(yesAction)
                            
                            UserDefaults.standard.setValue("true", forKey: "Subscription")
                            if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                                UserDefaults.standard.setValue(date, forKey: "DateSub")
                            }else{
                                UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                                UserDefaults.standard.setValue(date, forKey: "DateSub")
                                UserDefaults.standard.setValue("18", forKey: "minChoice")
                                UserDefaults.standard.setValue("75", forKey: "maxChoice")
                            }
                            UserDefaults.standard.synchronize()
                            self.present(alert, animated: true, completion: nil)
                          
                           
                        }else{
                            
                        }
                        
                    }
                    
                }catch {
                    
                }
            }else{
                
            }
        }
    }
    @IBAction func BuyWeekWithZonz(_ sender:UIButton){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.filterPaymentWithZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                print(data)
                if data != JSON.null {
                if data["message"].exists() == false {
                    let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                        //do the work
                        self.permuteColorObjects()
                        UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
                            self.VIPBuyContainer.alpha = 0
                             self.VIPContainer.alpha = 1
                            self.container300Zonz.alpha = 0
                            self.HeightContainer.constant = 275
                        }) { _ in
                           
                        }
                        
                    }
                    let dateFormatterNow = DateFormatter()
                    dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                    dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                    let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                    let dateFormatterFinal = DateFormatter()
                    dateFormatterFinal.dateFormat = "dd-MM-yyy"
                    let date = dateFormatterFinal.string(from: dateNow!)
                    let alert = UIAlertController(title: "Zonzay Plus", message: Localization("PaymentVU") + date, preferredStyle: .alert)
                    alert.addAction(yesAction)
                    UserDefaults.standard.setValue("true", forKey: "Subscription")
                    UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                    if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                        UserDefaults.standard.setValue(date, forKey: "DateSub")
                    }else{
                        UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                        UserDefaults.standard.setValue(date, forKey: "DateSub")
                        UserDefaults.standard.setValue("18", forKey: "minChoice")
                        UserDefaults.standard.setValue("75", forKey: "maxChoice")
                    }
                    UserDefaults.standard.synchronize()
                    self.present(alert, animated: true, completion: nil)
                            
                        }else{
                            let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                            }
                            let alert = UIAlertController(title: "Zonzay Plus", message: Localization("ZonzEnough") , preferredStyle: .alert)
                            alert.addAction(yesAction)
                            self.present(alert, animated: true, completion: nil)
                            
                    }
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    @IBAction func BuyAction(_ sender: UIButton) {
        if diamondsContainer.isHidden == true {
        
        if determineType() != PaymentType.None {
            if determineType() != .Week {
             PKIAPHandler.shared.purchase(product: products[determineIndex()]) { (purchaseAlertType, product, transaction) in
              if transaction != nil {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                
                let settings : Parameters = [
                    "userId" : a["_id"].stringValue,
                    "duration" : self.determineType().rawValue
                ]
                let headers : HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                print(settings)
                print(headers)
                Alamofire.request(ScriptBase.sharedInstance.filterPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    let data = JSON(response.data)
                    if data != JSON.null {
                        
                        let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                            //do the work
                            self.permuteColorObjects()
                            UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
                                self.VIPBuyContainer.alpha = 0
                                self.HeightContainer.constant = 275
                            }) { _ in
                                self.VIPBuyContainer.isHidden = true
                            }
                            
                        }
                        let dateFormatterNow = DateFormatter()
                        dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                        dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                        dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                        let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                        let dateFormatterFinal = DateFormatter()
                        dateFormatterFinal.dateFormat = "dd-MM-yyy"
                        let date = dateFormatterFinal.string(from: dateNow!)
                        let alert = UIAlertController(title: "Zonzay Plus", message: Localization("PaymentVU") + date, preferredStyle: .alert)
                        alert.addAction(yesAction)
                        UserDefaults.standard.setValue("true", forKey: "Subscription")
                        if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                            UserDefaults.standard.setValue(date, forKey: "DateSub")
                        }else{
                            UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                            UserDefaults.standard.setValue(date, forKey: "DateSub")
                            UserDefaults.standard.setValue("18", forKey: "minChoice")
                            UserDefaults.standard.setValue("75", forKey: "maxChoice")
                        }
                        UserDefaults.standard.synchronize()
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        
                    }
                    
                }
                
            }catch {
                
            }
            }else{
              
            }
        }
        
        
            }else{
                self.container300Zonz.alpha = 0
                self.container300Zonz.isHidden = false
                self.redoAnimation = false
                UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                    self.VIPContainer.alpha = 0
                      self.VIPContainer.alpha = 0
                    self.container300Zonz.alpha = 1
                }) { (complete) in
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear], animations: {
                        self.subscriptionZonzButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                        self.subscriptionMoneyButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                    }, completion: { (completeTwo) in
                        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear], animations: {
                            self.subscriptionZonzButton.transform = CGAffineTransform.identity
                            self.subscriptionMoneyButton.transform = CGAffineTransform.identity
                        }, completion: { (completeTwo) in
                            
                        })
                    })
                }
            }
        
         }
        }else{
            self.animateView = false
            self.transitionFromDiamondsToBuy()
        }
    }
    
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
extension UIButton
{
    func addImage(imageName: String, afterLabel bolAfterLabel: Bool = false)
    {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)?.sd_resizedImage(with: CGSize(width: 20, height: 22), scaleMode: .aspectFit)
      
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
       
        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string: self.title(for: .normal)!)
            let space  : NSMutableAttributedString = NSMutableAttributedString(string: " ")
            strLabelText.append(space)
            strLabelText.append(attachmentString)
            self.setAttributedTitle(strLabelText, for: .normal)
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: self.title(for: .normal)!)
            let space  : NSMutableAttributedString = NSMutableAttributedString(string: " ")
           
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
             mutableAttachmentString.append(space)
            mutableAttachmentString.append(strLabelText)
             self.setAttributedTitle(mutableAttachmentString, for: .normal)
        }
    }
    
   
}
extension VIPAccessController {
    func transitionFromDiamondsToBuy(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
            self.diamondsContainer.alpha = 0
        }) { (verif) in
            self.diamondsContainer.isHidden = true
            self.diamondsContainer.alpha = 1
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.weekContainer.alpha = 1
            }, completion: { (weekVerif) in
                UIView.animate(withDuration: 0.3, delay: 0.2, options: [.curveEaseInOut], animations: {
                    self.monthContainer.alpha = 1
                }, completion: { (monthVerif) in
                    UIView.animate(withDuration: 0.3, delay: 0.2, options: [.curveEaseInOut], animations: {
                        self.yearContainer.alpha = 1
                    }, completion: { (yearVerif) in
                        self.BuyBlurred.backgroundColor = UIColor.lightGray
                    })
                })
            })
        }
    }
}
