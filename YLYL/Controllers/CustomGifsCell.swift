//
//  CustomGifsCell.swift
//  YLYL
//
//  Created by macbook on 3/28/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import MapleBacon
class CustomGifsCell: UICollectionViewCell {
    
    var moviePlayer  : AVPlayer?
    var indexPath : IndexPath?
    /*
     * The layer screen of the video
     */
    var moviePlayerLayer = AVPlayerLayer()
    @IBOutlet weak var viewGif : UIView!
    @IBOutlet weak var GifName : UILabel!
    @IBOutlet weak var PriceGif : UILabel!
    @IBOutlet weak var PriceImageIcon : UIImageView!
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var favoriteIMG: UIImageView!
    var videoURLForAfter : String = ""
    var imageURLForAfter : String = ""
    override func prepareForReuse() {
        self.configureVideo(url: self.videoURLForAfter)
        thumbImage.setImage(with: URL(fileURLWithPath: imageURLForAfter), placeholder: nil, transformer: nil, progress: nil, completion: nil)
    }
    func configureVideo(url:String){
         let videoURL  = URL(fileURLWithPath: url)
        
    //let playerItem = CachingPlayerItem(url: videoURL)
        //playerItem.download()
        let assest = AVAsset(url: videoURL)
       videoURLForAfter = url
        
        moviePlayer =  AVPlayer(playerItem: AVPlayerItem(asset: assest))
        moviePlayer!.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
        moviePlayer!.automaticallyWaitsToMinimizeStalling = false
        moviePlayerLayer = AVPlayerLayer(player: moviePlayer)
        moviePlayerLayer.frame = self.viewGif.frame
        moviePlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        moviePlayerLayer.name = "Video"
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: moviePlayer?.currentItem)
        
       
      
    }
    func addPeriodicTimeObserver() {
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.5, preferredTimescale: timeScale)
        
        let timeObserverToken = moviePlayer?.addPeriodicTimeObserver(forInterval: time,
                                                           queue: .main) {
                                                            [weak self] time in
            print("Time :",time)
                                                            // update player transport UI
        }
    }
    func configureLBLS(name: String,price:String) {
        GifName.text = name
        PriceGif.text = price
        favoriteIMG.isHidden = true
        PriceGif.isHidden = false
        PriceImageIcon.isHidden = false
    }
    func updateFrame(){
    moviePlayerLayer.frame = self.viewGif.frame
    }
    func removeLayer(){
        
        moviePlayer!.pause()
        moviePlayer?.replaceCurrentItem(with: nil)
        
        NotificationCenter.default.removeObserver(self)
        guard let layers = thumbImage.layer.sublayers else {
            return
        }
        if layers.count != 0 {
          
            for layer in layers {
                if layer.name == "Video" {
                    print("Layer removed : ",indexPath?.row)
                   layer.removeFromSuperlayer()
                }
            }
        //self.moviePlayerLayer.removeFromSuperlayer()
        }
    }
    func playLayer(){
        if moviePlayer?.currentItem?.asset == nil {
            self.configureVideo(url: self.videoURLForAfter)
        }
        moviePlayerLayer = AVPlayerLayer(player: moviePlayer)
        moviePlayerLayer.frame = self.thumbImage.frame
        moviePlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        moviePlayerLayer.name = "Video"
        self.thumbImage.layer.insertSublayer(moviePlayerLayer, at: 0)
        //self.thumbImage.layer.addSublayer(moviePlayerLayer)
    //self.viewGif.layer.addSublayer(moviePlayerLayer)
        print("Will Start Playing")
       moviePlayer!.play()
        
    }
    @objc func playerDidFinishPlayingt(note : Notification) {
        moviePlayer!.seek(to: CMTime.zero)
        moviePlayer!.play()
    }
}
