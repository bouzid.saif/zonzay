//
//  ZonzayPlusPayment.swift
//  YLYL
//
//  Created by macbook on 2/14/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
enum PaymentType : String {
    case Week = "week"
    case Month = "month"
    case Year = "year"
    case None = "none"
}
class ZonzayPlusPayment: FixUIController {
    @IBOutlet weak var container : UIStackView!
    @IBOutlet weak var viewWeek: CustomPaymentView!
    @IBOutlet weak var viewMonth: CustomPaymentView!
    @IBOutlet weak var viewYear: CustomPaymentView!
    var navigation : UINavigationController = UINavigationController()
    var tabbar : UITabBarController = UITabBarController()
    let color = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureGestures()
        self.navigation.isNavigationBarHidden = false
         self.tabbar.setTabBarVisible(visible: false, duration: 0, animated: true)
        self.viewMonth.select()
        self.viewMonth.backgroundColor = color
        self.container.layer.borderColor = color.cgColor
        self.container.layer.borderWidth = 1.0
    }
    @IBAction func continueAction(_ sender: UIButton) {
        var paymentType = PaymentType.Month
        if viewWeek.isSelected(){
            paymentType = PaymentType.Week
        }
        if viewMonth.isSelected() {
            paymentType = PaymentType.Month
        }
        if viewYear.isSelected() {
            paymentType = PaymentType.Year
        }
        print(paymentType)
        buyFilter(type: paymentType)
    }
    func buyFilter(type:PaymentType){
       
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "duration" : type.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.filterPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                if data != JSON.null {
                    
                        let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                            //do the work
                            self.navigation.popViewController(animated: true)
                        }
                    let dateFormatterNow = DateFormatter()
                    dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                    dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                    let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                    let dateFormatterFinal = DateFormatter()
                    dateFormatterFinal.dateFormat = "dd-MM-yyy"
                    let date = dateFormatterFinal.string(from: dateNow!)
                        let alert = UIAlertController(title: "Zonzay Plus", message: Localization("PaymentVU") + date, preferredStyle: .alert)
                        alert.addAction(yesAction)
                    UserDefaults.standard.setValue("true", forKey: "Subscription")
                    if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                        
                    }else{
                        UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                        UserDefaults.standard.setValue("18", forKey: "minChoice")
                        UserDefaults.standard.setValue("75", forKey: "maxChoice")
                    }
                    UserDefaults.standard.synchronize()
                        self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func handleGesture(_ sender: UITapGestureRecognizer){
        print(sender.view)
        if sender.view  == self.viewWeek {
            if self.viewWeek.backgroundColor ==  color {
                return
            }else{
                if self.viewMonth.backgroundColor == color {
                    self.viewMonth.backgroundColor = .white
                    self.viewMonth.deselect()
                }
                if self.viewYear.backgroundColor == color {
                    self.viewYear.backgroundColor = .white
                     self.viewYear.deselect()
                }
                self.viewWeek.backgroundColor = color
                 self.viewWeek.select()
            }
        }else if sender.view == self.viewMonth {
            if self.viewMonth.backgroundColor ==  color {
                return
            }else{
                if self.viewWeek.backgroundColor == color {
                    self.viewWeek.backgroundColor = .white
                     self.viewWeek.deselect()
                }
                if self.viewYear.backgroundColor == color {
                    self.viewYear.backgroundColor = .white
                     self.viewYear.deselect()
                }
                self.viewMonth.backgroundColor = color
                 self.viewMonth.select()
            }
        }else if sender.view == self.viewYear{
            if self.viewYear.backgroundColor ==  color {
                return
            }else{
                if self.viewMonth.backgroundColor == color {
                    self.viewMonth.backgroundColor = .white
                     self.viewMonth.deselect()
                }
                if self.viewWeek.backgroundColor == color {
                    self.viewWeek.backgroundColor = .white
                     self.viewWeek.deselect()
                }
                self.viewYear.backgroundColor = color
                 self.viewYear.select()
            }
        }
    }
    func configureGestures(){
        self.viewMonth.isUserInteractionEnabled = true
        self.viewWeek.isUserInteractionEnabled = true
        self.viewYear.isUserInteractionEnabled = true
        self.viewWeek.addGestureRecognizer(setGestureRecognizer())
        self.viewMonth.addGestureRecognizer(setGestureRecognizer())
        self.viewYear.addGestureRecognizer(setGestureRecognizer())
    }

func setGestureRecognizer() -> UITapGestureRecognizer {
    
    var panRecognizer = UITapGestureRecognizer()
    
    panRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleGesture(_:)))
    panRecognizer.numberOfTapsRequired = 1
    return panRecognizer
}
}
class CustomPaymentView : UIView {
    
    @IBOutlet weak var TimeLBL : UILabel!
    @IBOutlet weak var MoneyLBL : UILabel!
    @IBOutlet weak var DiscountLBL : UILabel!
    func select(){
        TimeLBL.textColor = .white
        MoneyLBL.textColor = .white
        DiscountLBL.textColor = .white
    }
    func deselect(){
        TimeLBL.textColor = .black
        MoneyLBL.textColor = .black
        DiscountLBL.textColor = .lightGray
    }
    func isSelected() -> Bool {
    return TimeLBL.textColor == .white
    }
}
