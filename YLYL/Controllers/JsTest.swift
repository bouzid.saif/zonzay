//
//  JsTest.swift
//  YLYL
//
//  Created by macbook on 2/8/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileVision
import GoogleMVDataOutput
class EyePhysics : NSObject {
    var lastUpdated : Date! = Date()
    var xVelocity: CGFloat!  =  0.0
    var yVelocity : CGFloat! = 0.0
    var consecutiveBounces : NSInteger! = 0
let kFriction: CGFloat = 2.2
let kGravity: CGFloat = 10
let kBounceMultiplier: CGFloat = 20
let kZeroTolerance: CGFloat = 0.001
let kIrisRatio: CGFloat = 0.45
    
    override init() {
    super.init()
    lastUpdated = Date()
}
func nextIrisRect(from eyeRect: CGRect, withIrisRect irisRect: CGRect) -> CGRect {
    var irisRect = irisRect

    let now = Date()
    let elapsed: TimeInterval = now.timeIntervalSince(lastUpdated)
    lastUpdated = now
    let irisRadius: CGFloat = eyeRect.size.width * kIrisRatio / 2

    if irisRect.isNull || irisRect.isEmpty || irisRect.isInfinite {
        // Initialize eyeball at the top of the eye.
        irisRect = CGRect(x: eyeRect.midX - irisRadius, y: eyeRect.origin.y, width: irisRadius * 2, height: irisRadius * 2)
    }
if !isStopped(eyeRect, irisRect: irisRect) {
    // Only apply gravity when the iris is not stopped at the bottom of the eye.
    yVelocity += kGravity * CGFloat(elapsed)
}

// Apply friction in the opposite direction of motion, so that the iris slows in the absence
// of other head motion.
xVelocity = applyFriction(xVelocity, simulationRate: elapsed)
yVelocity = applyFriction(yVelocity, simulationRate: elapsed)

    let irisX: CGFloat = irisRect.origin.x + (xVelocity * irisRadius * CGFloat(elapsed))
    let irisY: CGFloat = irisRect.origin.y + (yVelocity * irisRadius * CGFloat(elapsed))

var nextIris = CGRect(x: irisX, y: irisY, width: irisRect.size.width, height: irisRect.size.height)
nextIris = makeIris(nextIris, inEyeBounds: eyeRect, simulationRate: elapsed)

return nextIris

}
// The iris is stopped if it is at the bottom of the eye and its velocity is zero.

func isStopped(_ eyeRect: CGRect, irisRect: CGRect) -> Bool {
    if eyeRect.contains(irisRect) {
        return false
    }
    let offsetY: CGFloat = irisRect.maxY - eyeRect.maxY
    let maxDistance: CGFloat = (eyeRect.size.height - irisRect.size.height) / 2
    if offsetY < maxDistance {
        return false
    }
    return isZero(xVelocity) && isZero(yVelocity)
}
func isZero(_ number: CGFloat) -> Bool {
    return number.isNaN || (number < kZeroTolerance && number > -kZeroTolerance)
}
// Friction slows velocity in the opposite direction of motion, until zero velocity is reached.

func applyFriction(_ velocity: CGFloat, simulationRate elapsed: TimeInterval) -> CGFloat {
    var velocity = velocity
    if isZero(velocity) {
        velocity = 0
    } else if velocity > 0 {
        velocity = CGFloat(fmaxf(0, Float(velocity - CGFloat(kFriction * CGFloat(elapsed)))))
    } else {
        velocity = CGFloat(fminf(0, Float(velocity + CGFloat(kFriction * CGFloat(elapsed)))))
    }
    return velocity
}
    // Correct the iris position to be in-bounds within the eye, if it is now out of bounds.  Being
    // out of bounds could have been due to a sudden movement of the head and/or camera, or the
    // result of just bouncing/rolling around.
    // In addition, modify the velocity to cause a bounce in the opposite direction.
func makeIris(_ nextIrisRect: CGRect, inEyeBounds eyeRect: CGRect, simulationRate elapsed: TimeInterval) -> CGRect {

    if eyeRect.contains(nextIrisRect) {
        consecutiveBounces = 0
        return nextIrisRect
    }

    // Accumulate a consecutive bounce count to aid for velocity calculation.
    consecutiveBounces += 1

    // Move the iris back to where it would have been when it would have contacted the side of
    // the eye.
    var newOrigin: CGPoint = nextIrisRect.origin
    let intersectRect: CGRect = eyeRect.intersection(nextIrisRect)
    if !intersectRect.isNull {
        // Handle overlapping case.
        newOrigin.x += (intersectRect.origin.x <= nextIrisRect.origin.x ? -1 : 1) * (nextIrisRect.size.width - intersectRect.size.width)
        newOrigin.y += (intersectRect.origin.y > eyeRect.origin.y ? -1 : 1) * (nextIrisRect.size.height - intersectRect.size.height)
    } else {
        // Handle not overlapping case.
        if nextIrisRect.origin.x < eyeRect.origin.x {
            // Iris to the left of the eye.
            newOrigin.x = eyeRect.origin.x
        } else {
            // Iris to the right of the eye.
            newOrigin.x = eyeRect.origin.x + eyeRect.size.width - nextIrisRect.size.width
        }
        if nextIrisRect.origin.y < eyeRect.origin.y {
            // Iris to the top of the eye.
            newOrigin.y = eyeRect.origin.y
        } else {
            // Iris to the bottom of the eye.
            newOrigin.y = eyeRect.origin.y + eyeRect.size.height - nextIrisRect.size.height
        }
    }

// Update the velocity direction and magnitude to cause a bounce.
    let dx: CGFloat = newOrigin.x - nextIrisRect.origin.x
xVelocity = applyBounce(xVelocity, distanceOutBound: dx, simulationRate: elapsed, irisRect: nextIrisRect) / CGFloat(consecutiveBounces)
    let dy: CGFloat = newOrigin.y - nextIrisRect.origin.y
yVelocity = applyBounce(yVelocity, distanceOutBound: dy, simulationRate: elapsed, irisRect: nextIrisRect) / CGFloat(consecutiveBounces)
return CGRect(x: newOrigin.x, y: newOrigin.y, width: nextIrisRect.size.width, height: nextIrisRect.size.height)

}
func applyBounce(_ velocity: CGFloat, distanceOutBound distance: CGFloat, simulationRate elapsed: TimeInterval, irisRect: CGRect) -> CGFloat {
    var velocity = velocity
    if isZero(distance) {
        return velocity
    }
    velocity *= -1

    let bounce = CGFloat(kBounceMultiplier * CGFloat(abs(Float(distance / irisRect.size.width / 2))))
    if velocity > 0 {
        velocity += bounce * CGFloat(elapsed)
    } else {
        velocity -= bounce * CGFloat(elapsed)
    }
    return velocity
}



}
class GoogleEyeView : UIView {
var physics: EyePhysics?
var irisRect = CGRect.zero
    
 override init(frame: CGRect) {
    super.init(frame: frame)
    physics = EyePhysics()
    irisRect = CGRect.zero
    isOpaque = false
    backgroundColor = UIColor.white
    layer.borderColor = UIColor.black.cgColor
    layer.borderWidth = 4
    layer.masksToBounds = true
}
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
func updateEyeRect(_ eyeRect: CGRect) {
    frame = eyeRect
    layer.cornerRadius = frame.size.height / 2
    setNeedsDisplay()
}

    override func draw(_ rect: CGRect) {
        irisRect = (physics?.nextIrisRect(from: frame, withIrisRect: irisRect))!
        let iris: CGRect = (superview?.convert(irisRect, to: self))!
    UIColor.black.setFill()
    let irisPath = UIBezierPath(ovalIn: iris)
    irisPath.fill()
}

}
protocol FaceTrackerDatasource: NSObjectProtocol {
    // Display scaling offset.
    var xScale : CGFloat { get }
    func yScale() -> CGFloat
    func offset() -> CGPoint
    // View to display googly eyes.
    func overlayView() -> UIView?
}
class FaceTracker: NSObject ,GMVOutputTrackerDelegate {
    var leftEyeView : GoogleEyeView!
    var rightEyeView : GoogleEyeView!
var lastLeftEyePosition = CGPoint.zero
var lastRightEyePosition = CGPoint.zero

var delegate: FaceTrackerDatasource?

    func dataOutput(_ dataOutput: GMVDataOutput!, detectedFeature feature: GMVFeature!) {
        print("detected")
        
       
            self.leftEyeView = GoogleEyeView(frame: CGRect.zero)
            self.rightEyeView = GoogleEyeView(frame: CGRect.zero)
       
        self.delegate?.overlayView()?.addSubview(self.leftEyeView)
        self.delegate?.overlayView()?.addSubview(self.rightEyeView)
        
    }
    override init() {
        super.init()
       
       
    }
    
    func dataOutput(_ dataOutput: GMVDataOutput!, updateFocusing feature: GMVFeature!, forResultSet features: [GMVFeature]!) {
        if self.leftEyeView != nil && self.rightEyeView != nil {
        self.leftEyeView.isHidden = false
        self.rightEyeView.isHidden = false
        guard let  face = feature as? GMVFaceFeature else {
            return
        }
            let faceR = face.bounds
            // For converting the Core Image Coordinates to UIView Coordinates
            let detectedImageSize = faceR.size
            var transform = CGAffineTransform(scaleX: 1, y: -1)
            transform = transform.translatedBy(x: 0, y: -detectedImageSize.height)
            print("BeforeRect : ",face.bounds)
            //let rectFace = face.bounds.applying(CGAffineTransform(rotationAngle: (.pi / 2)))
           // let rectFace = rotateRect(face.bounds)
           // print("afterRect :",rectFace)
            let rectFace = face.bounds.applying(transform)
        let newLeftEyePosition = face.hasLeftEyePosition ? face.leftEyePosition : CGPoint.zero
        
            let leftEyeRect: CGRect = eyeRect(lastLeftEyePosition, newEyePosition: newLeftEyePosition, face: rectFace, faceOld: faceR)
        leftEyeView.updateEyeRect(leftEyeRect)
        

// Update right eye rect.
        let newRightEyePosition = face.hasRightEyePosition ? face.rightEyePosition : CGPoint.zero
            let rightEyeRect: CGRect = eyeRect(lastRightEyePosition, newEyePosition: newRightEyePosition, face: rectFace, faceOld: faceR)
rightEyeView.updateEyeRect(rightEyeRect)
        self.updateLast(face)

        //let height = face.cont
        //self.mouth.origin = face.leftMouthPosition
        //print("ORGIN:",self.mouth.origin)
        }
    }
    
    func dataOutput(_ dataOutput: GMVDataOutput!, updateMissing features: [GMVFeature]!) {
        self.leftEyeView.isHidden = true
        self.rightEyeView.isHidden = true
    }
    func dataOutputCompleted(withFocusingFeature dataOutput: GMVDataOutput!) {
        self.leftEyeView.removeFromSuperview()
        self.rightEyeView.removeFromSuperview()
    }
    func rotateRect(_ rect : CGRect) -> CGRect {
       var oldRect = rect
        let oldX = oldRect.origin.x
        let oldY = oldRect.origin.y
        let totalX = (self.delegate?.overlayView()?.height)!
        let scaleXDecrease = totalX - oldX
        let percX = scaleXDecrease / totalX
        let newX = totalX + (-(percX * totalX))
        let totalY = (self.delegate?.overlayView()?.width)!
         let scaleYDecrease = totalY  - oldY
         let percY = scaleYDecrease / totalY
         let newY = totalY + (-(percY * totalY))
        oldRect = CGRect(x: newX, y: newY, width: rect.height, height: rect.width)
       return oldRect
    }
func scaledRect(_ rect: CGRect, xScale xscale: CGFloat, yScale yscale: CGFloat, offset: CGPoint) -> CGRect {

    var resultRect = CGRect(x: floor(rect.origin.x * xscale), y: floor(rect.origin.y * yscale), width: floor(rect.size.width * xscale), height: floor(rect.size.height * yscale))
    resultRect = resultRect.offsetBy(dx: offset.x, dy: offset.y)
    return resultRect
}

    func eyeRect(_ lastEyePosition: CGPoint, newEyePosition: CGPoint, face faceRect: CGRect, faceOld faceBefore: CGRect) -> CGRect {
    var eye: CGPoint = lastEyePosition
    if !newEyePosition.equalTo(CGPoint.zero) {
        eye = newEyePosition
    }

    let faceToEyeRatio: CGFloat = 4.0
   
    let viewSize = (self.delegate?.overlayView())!.size
    
    
    let Scale = min (viewSize.width / faceBefore.width, viewSize.height / faceBefore.height)
    
        
    let offsetX = (viewSize.width - faceBefore.width * Scale ) / 2
    let offsetY = (viewSize.height - faceBefore.height * Scale ) / 2
    let offset = CGPoint(x: offsetX, y: offsetY)
        var finalRect = faceRect.applying(CGAffineTransform(scaleX: Scale, y: Scale))
        finalRect.origin.x += offsetX
        finalRect.origin.y += offsetY
        
    let width: CGFloat = finalRect.size.width / faceToEyeRatio
    var rect = CGRect(x: eye.x - width / 2, y: eye.y - width / 2, width: width, height: width)
   // rect = scaledRect(rect, xScale: xScale, yScale: yScale, offset: offset)
    print(rect)
    return rect
}

func updateLast(_ feature: GMVFaceFeature?) {
    if feature?.hasLeftEyePosition != nil {
        lastLeftEyePosition = (feature?.leftEyePosition)!
    }
    if feature?.hasRightEyePosition != nil {
        lastRightEyePosition = (feature?.rightEyePosition)!
    }
}

    
    
}
class JsTest: UIViewController , AVCaptureVideoDataOutputSampleBufferDelegate,GMVMultiDataOutputDelegate,FaceTrackerDatasource,GMVDataOutputDelegate{
    
    var xScale: CGFloat {
       
        return min((self.previewLayer?.bounds.width)! , (self.previewLayer?.bounds.height)!)
        
    }
    
    
    
    func yScale() -> CGFloat {
       return min((self.previewLayer?.bounds.width)! , (self.previewLayer?.bounds.height)!)
    }
    
    func offset() -> CGPoint {
        return CGPoint(x: (self.previewLayer?.bounds.width)!, y: (self.previewLayer?.bounds.height)!)
    }
    
    func overlayView() -> UIView? {
       return self.uiview
    }
    
    func dataOutput(_ dataOutput: GMVDataOutput!, trackerFor feature: GMVFeature!) -> GMVOutputTrackerDelegate! {
        let tracker = FaceTracker()
        tracker.delegate = self
        return tracker
    }
    
    @IBOutlet weak var uiview: UIView!
     let captureSession = AVCaptureSession()
    private let context = CIContext()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var captureDevice : AVCaptureDevice?
    let stillImageOutput = AVCaptureVideoDataOutput()

    let detector = GMVDetector(ofType: GMVDetectorTypeFace, options: [
        GMVDetectorFaceLandmarkType : GMVDetectorFaceLandmark.all.rawValue,
        GMVDetectorFaceClassificationType : GMVDetectorFaceClassification.all.rawValue,
        GMVDetectorFaceTrackingEnabled : true, GMVDetectorFaceMode : GMVDetectorFaceModeOption.fastMode.rawValue , GMVDetectorFaceMinSize : 0.15])
    var dataOutput : GMVMultiDataOutput!
    override func viewDidLoad() {
        super.viewDidLoad()
        captureSession.sessionPreset = AVCaptureSession.Preset.medium
    
        let devices  = cameraWithPosition(position: AVCaptureDevice.Position.front )
        
        
        
        captureDevice = devices
        if captureDevice != nil
        {
            print("Capture device found")
            beginSession()
        }else{
            print("No Capture device found")
        }
    }
        
        
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
        
        
        //self.previewLayer?.position = CGPoint(x: self.uiview.center.x, y: self.uiview.center.y)
        //self.dataOutput.multiDataDelegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         previewLayer?.frame = self.uiview.layer.bounds
    }
func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice?
{
    let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                       mediaType: AVMediaType.video,
                                                                       position: position)
    
    for device in deviceDescoverySession.devices {
        if device.position == position {
            return device
        }
        
    }
    
    return nil
}
    func beginSession()
    {
        do
        {
            if captureSession.canAddInput(try! AVCaptureDeviceInput(device: captureDevice!)) {
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
                stillImageOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "sample buffer"))
            
                if captureSession.canAddOutput(stillImageOutput)
                {
                    captureSession.addOutput(stillImageOutput)
                }
                self.dataOutput = GMVMultiDataOutput(detector: self.detector)
              // self.dataOutput.multiDataDelegate = self
                
                if self.captureSession.canAddOutput(self.dataOutput) {
                  //  captureSession.addOutput(self.dataOutput)
                    
                }
                let output = self.stillImageOutput.connection(with: .video)
                output?.videoOrientation = .portrait
            }
        }
        catch
        {
            print("error: \(error.localizedDescription)")
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
       previewLayer?.videoGravity = .resizeAspectFill
        previewLayer?.connection?.videoOrientation = .portrait
        self.uiview.layer.addSublayer(previewLayer!)
         previewLayer?.frame = self.uiview.layer.bounds
        captureSession.commitConfiguration()
 
       //  previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
       // previewLayer?.frame = self.uiview.bounds
        //self.view.addSubview(tempImage)
        //self.tempImage.frame = self.uiview.layer.bounds
        captureSession.startRunning()
      
        
    }
    // MARK: Sample buffer to UIImage conversion
    private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
    var i = 0
    var noiseImageView: UIImageView! = UIImageView(image: UIImage(named: "nose07"))
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if i % 60 == 0 {
            i = i + 1
            print(i)
        guard let uiImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else { return }
        DispatchQueue.main.async { [unowned self] in
            guard let finalImageRotated = uiImage.rotate(radians: (.pi / 2)) else {
                return
            }
            print(uiImage.imageOrientation.rawValue)
           // self.tempImage.contentMode = UIImageView.ContentMode.scaleAspectFill
           // self.tempImage.image = finalImageRotated
            let faces = self.detector?.features(in: uiImage, options: nil)  as? [GMVFaceFeature]
            if (faces?.count)! > 0 {
                
                
                let face = faces![0]
                guard let faceImage = CIImage(image: uiImage) else { return }
                let detectedImageSize = faceImage.extent.size
                var transform = CGAffineTransform(scaleX: 1, y: -1)
                transform = transform.translatedBy(x: 0, y: -detectedImageSize.height)
                // Apply the transform to convert the coordinates
                var faceViewBounds =  face.bounds.applying(transform)
                // Calculate the actual position and size of the rectangle in the image view
                let viewSize = self.previewLayer!.bounds.size
                let scaleX = viewSize.width / detectedImageSize.width
                let scaleY = viewSize.height / detectedImageSize.height
                let offsetX = (viewSize.width - detectedImageSize.width * scaleX) / 2
                let offsetY = (viewSize.height - detectedImageSize.height * scaleY) / 2
                 faceViewBounds = faceViewBounds.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
                print("faceBounds = \(faceViewBounds)")
                faceViewBounds.origin.x += offsetX
                faceViewBounds.origin.y += offsetY
                let widthScaleFactor = self.view.frame.width / (self.previewLayer?.frame.width)!
                let heightScaleFactor =  self.view.frame.height / (self.previewLayer?.frame.height)!
                let leftMouth = face.leftMouthPosition
                let rightMouth = face.rightMouthPosition
                print("UImage:",uiImage.size)
                print("UIView:",self.uiview.size)
                print(leftMouth)
                print(rightMouth)
                if leftMouth != CGPoint.zero && rightMouth != CGPoint.zero {
                    var mouthLength = (leftMouth.x - rightMouth.x) * widthScaleFactor
                    
                   
                   // self.mouthImage = UIImageView(frame: )
                   
                   
                    var verif = false
                    for view in self.view.subviews {
                        if view == self.noiseImageView {
                            verif = true
                        }
                    }
                    if verif == false {
                        DispatchQueue.main.async {
                            print("View added")
                            self.view.addSubview(self.noiseImageView)
                            self.view.bringSubviewToFront(self.noiseImageView)
                             //self.noiseImageView.frame = CGRect(x: leftMouth.x, y: leftMouth.y, width: mouthLength, height: face.bottomMouthPosition.y - leftMouth.y)
                            self.noiseImageView.frame = faceViewBounds
                            self.noiseImageView.setNeedsDisplay()
                        }
                        
                    }else{
                        print("View already added")
                        // self.noiseImageView.frame = CGRect(x: leftMouth.x, y: leftMouth.y, width: mouthLength, height: face.bottomMouthPosition.y - leftMouth.y)
                        self.noiseImageView.frame = faceViewBounds
                        self.noiseImageView.setNeedsDisplay()
                    }
                    
                    print("image:",self.noiseImageView.frame)
                }
                
            }
            if faces?.count == 0 {
                print("Scan Failed: Found nothing to scan")
                for view in self.view.subviews {
                    if view == self.noiseImageView {
                        view.removeFromSuperview()
                    }
                }
            }else{
                print("detected")
            }
            
        }
        }else{
            i = i + 1
        }
    }
    var widthScaleFactor : CGFloat = 0
    var heightScaleFactor  : CGFloat = 0
    var mouthImage : UIImageView!
    var tempImage = UIImageView()
}
extension JsTest {
    private func translateX(x: CGFloat) -> CGFloat {
    return self.view.frame.width - scaleX(x)
    }
    private func translateY(y: CGFloat) -> CGFloat {
    return scaleY(y)
    }
    private func scaleX(_ x: CGFloat) -> CGFloat {
    return x * widthScaleFactor
    }
    private func scaleY(_ y: CGFloat)-> CGFloat {
    return y * heightScaleFactor
    }
}
