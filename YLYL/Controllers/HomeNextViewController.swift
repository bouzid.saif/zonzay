//
//  HomeNextViewController.swift
//  YLYL
//
//  Created by macbook on 1/23/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import BMPlayer
import SnapKit
import GoogleMobileAds
import Alamofire
import SwiftyJSON
class BMCustomPlayer: BMPlayer {
    class override func storyBoardCustomControl() -> BMPlayerControlView? {
        return BMPlayerCustomControlView()
    }
}
class HomeNextViewController: FixUIController,BMPlayerDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
   
    
    
    @IBOutlet weak var viewHeaderTopOfTable: UIView!
    var rotateCount: CGFloat = 0
    var didFinishVideo = false
    @IBOutlet weak var containerComment: UIView!
    
    @IBOutlet weak var userPictureIMG: RoundedUIImageView!
    
    @IBOutlet weak var commentTextView: GrowingTextView!
    @IBOutlet weak var userPictureTop: RoundedUIImageView!
    @IBOutlet weak var nameUserTop : UILabel!
    @IBOutlet weak var zonzLabel : UILabel!
    
    @IBOutlet weak var nameVsName : UILabel!
    @IBOutlet weak var seenLabel : UILabel!
    @IBOutlet weak var timeLabel : UILabel!
    
    @IBOutlet weak var commentsTriangleOpener: UIImageView!
    
    @IBOutlet weak var commentsCount: UILabel!
    @IBOutlet weak var sendBTN :UIButton!
    @IBOutlet weak var tableViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var tableV : UITableView!
   
    var ownedLikes : [String] = []
    var commentsHeight : [IndexPath : CGFloat] = [:]
    var playerItem : AVPlayerItem!
    var videoPlayer : AVPlayer!
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
        let shareLink = ["https://zonzay.com/getApplication"]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    var interstitial: GADInterstitial!
    
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        
        didFinishVideo = true
        if state == .playedToTheEnd {
             presentAds()
        }
    }
    
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        
    }
    var doWorkOneTime = true
    var doWorkAdsOneTime = true
    var timer : AutoCancellingTimer? = nil
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        if (currentTime.toMM_SS().components(separatedBy: ":")[1]) == "01" {
            if doWorkOneTime {
                doWorkOneTime = false
               
                timer =  AutoCancellingTimer(interval: 5, repeats: false) {
                   
                    self.sendSeenVideo()
               
                }
            
            }
            
        }
        didFinishVideo = false
        //print( (currentTime.toMM_SS().components(separatedBy: ":")[1]))
        if (currentTime.toMM_SS().components(separatedBy: ":")[1]) == "30" {
            if doWorkAdsOneTime {
                doWorkAdsOneTime = false
                print("30 arrived")
            presentAds()
            }
        }
       
    }
    func sendSeenVideo(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "videoId" : self.videoObject.getVideoId()
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.seenVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.data != nil {
                let data = JSON(response.data ?? Data())
                print("REQUSeen : ",data)
                
            }
        }
    }
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        print("startedPlaying")
    }
    override var prefersStatusBarHidden: Bool {
        return isfullScreen
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.popViewController(animated: false)
        self.playerView.pause()
        if isAdActive == false {
        self.playerView.removeFromSuperview()
        }else{
            
        }
        if timer != nil {
            timer?.cancel()
        }
    }
    var isAdActive = false
    var isfullScreen : Bool = false
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    func bmPlayer(player: BMPlayer, didPressButton button: UIButton) {
        if button.tag == 105 {
            if isfullScreen == false {
                isfullScreen = true
                UIView.animate(withDuration: 0.25) {
                    self.setNeedsStatusBarAppearanceUpdate()
                }
                
                 self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                
                playerView.updateUI(true)
            //this is full Screen Button
             self.playerView.center = self.view.center
            self.playerView.layoutIfNeeded()
           
            let scaleX = UIScreen.main.bounds.height / self.playerView.size.width
            let scaleY = UIScreen.main.bounds.width / self.playerView.size.height
        
           UIView.animate(withDuration: 0.6, delay: 0.2, options: [.curveLinear], animations: {
                self.playerView.transform = CGAffineTransform(scaleX: scaleX + 0.011, y: scaleY + 0.011).rotated(by: CGFloat(Double.pi / 2)).translatedBy(x: 18, y: 0)
            }, completion: {(cmon) in
                  
                self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                
           })
           
            }else{
                isfullScreen = false
                UIView.animate(withDuration: 0.25) {
                    self.setNeedsStatusBarAppearanceUpdate()
                }
               
                playerView.updateUI(false)
                
                UIView.animate(withDuration: 0.6, delay: 0.2, options: [.curveLinear], animations: {
                    self.playerView.transform = CGAffineTransform.identity
                }, completion: {(cmon) in
                     
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                    }
                })
            }
        }
    }
    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    @objc func showKeyboard(notification: Notification) {
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            print("Show Keyboard")
             sendBTN.isHidden = false
            let height = frame.cgRectValue.height
            self.commentTextView.placeholderColor = UIColor(red: 88 / 255, green: 88 / 255, blue: 88 / 255, alpha: 1)
            self.commentTextView.textColor = UIColor(red: 72 / 255, green: 72 / 255, blue: 72 / 255, alpha: 1)
            self.containerComment.backgroundColor = .white
            if self.allComments.count > 0 {
                self.tableV.scrollToBottomKeyboard()
            }
            // self.tableV.contentInset.bottom =  2
            //self.bottomIsTyping.constant = 8
            //self.tableV.scrollIndicatorInsets.bottom = height + 50
            //self.bottomConstraint.constant = height + 50
            //self.view.layoutIfNeeded()
            
          /*  if self.messagesJSON.count > 0 {
                self.tableV.scrollToBottomKeyboard()
            } */
        }
    }
    @objc func hideKeyboard(notification: Notification) {
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            print("HideKeyboard")
            if self.tableViewHeightConst.constant == 0 {
                self.sendBTN.isHidden = true
                
            }
            self.commentTextView.placeholderColor = UIColor.white
             self.commentTextView.textColor = .white
            self.containerComment.backgroundColor = .clear
        
        }
    }
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        print("isFullScreen : ",isFullscreen)
        if isFullscreen {
            
            self.tabBarController?.tabBar.isHidden = true
            self.TopBarName.isHidden = true
            self.TopBarBackground.isHidden = true
            player.snp.remakeConstraints { (constraints) in
                constraints.top.equalTo(self.view.superview!).offset(0)
                constraints.right.equalTo(self.view.superview!).offset(0)
                constraints.bottom.equalTo(self.view.superview!).offset(0)
                constraints.left.equalTo(self.view.superview!).offset(0)
            }
        }else{
            self.tabBarController?.tabBar.isHidden = false
            self.TopBarName.isHidden = false
            self.TopBarBackground.isHidden = false
            player.snp.removeConstraints()
            player.snp.makeConstraints({ (constraints) in
                constraints.top.equalTo(self.TopBarBackground).offset(64)
                constraints.right.equalTo(self.view.superview!).offset(0)
                constraints.left.equalTo(self.view.superview!).offset(0)
                constraints.height.equalTo(299)
            })
        }
    }
    @IBOutlet weak var playerView : BMCustomPlayer!
    @IBOutlet weak var TopBarBackground : UIView!
    @IBOutlet weak var TopBarName : UIView!
    
    var videoURL : String = ""
    var videoObject : VideoYLYL!
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return true
        }
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("hideBar"), object: nil, queue: OperationQueue.main) { (notif) in
            print("Notif received")
            self.tabBarController?.setTabBarVisible(visible: false, duration: 1, animated: false)
        }
        configureMasterSoundOff()
        print(videoURL)
        self.commentTextView.nextNavigationField = self.commentTextView
        self.commentTextView.delegate = self
        commentsCount.text = "0 " + Localization("comments")
        sendBTN.isHidden = true
        self.commentsTriangleOpener.image = self.commentsTriangleOpener.image?.maskWithColor(color: .lightGray)
        self.getAllComments(animated: true)
       //Production
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-1626125969955246/8494582550")
        //Debug
        //interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let a = JSON(data: dataFromString!)
        
        self.userPictureTop.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
        self.nameUserTop.text = a["userName"].stringValue
        self.zonzLabel.text = a["zonz"].stringValue
        print(videoObject)
        let stringNames = videoObject.getVideoFirstUser().getUserName() + " vs " + videoObject.getVideoSecondUser().getUserName()
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: stringNames)
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: stringNames.count))
         attributedString.addAttribute( NSAttributedString.Key.underlineColor, value: UIColor.white, range: NSRange(location: 0, length: stringNames.count ))
       
        nameVsName.attributedText = attributedString
        seenLabel.text = "\(videoObject.getSeen()) " + Localization("seenVideo")
        let dateFormatter = DateFormatter()
        let date = dateFormatter.date(fromSwapiString: videoObject.getVideoDate())
        let dateFormatterNow = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
         print("TodayDate : ", Date())
        var final = dateFormatter.string(from: Date())
        final = final.transformDateToOrigin()
        
        
        //let dateNow = dateFormatter.date(fromTodayDate: final)
        let dateNow = dateFormatterNow.date(fromSwapiString: dateFormatter.string(from: Date()))
        if monthsBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
            if daysBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
                if hoursBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
                    //time here
                    if Locale.currentLanguage == .en || Locale.currentLanguage == .it {
                    self.timeLabel.text = "\(minutesBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("minutesAgo")
                        
                    }else{
                        self.timeLabel.text = Localization("Ago") + " \(minutesBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("minutes")
                    }
                   
                }else{
                    //hours equal
                     if Locale.currentLanguage == .en || Locale.currentLanguage == .it {
                    self.timeLabel.text = "\(hoursBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("hoursAgo")
                     }else{
                         self.timeLabel.text = Localization("Ago") +  " \(hoursBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("hours")
                    }
                }
            }else{
                //days equal
                 if Locale.currentLanguage == .en || Locale.currentLanguage == .it {
                    
                self.timeLabel.text = "\(daysBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("daysAgo")
                 }else{
                    self.timeLabel.text = Localization("Ago") +  " \(daysBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("days")
                }
            }
        }else{
            //months equal
             if Locale.currentLanguage == .en || Locale.currentLanguage == .it {
                
            self.timeLabel.text = "\(monthsBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("monthsAgo")
             }else{
                self.timeLabel.text = Localization("Ago") + " \(monthsBetweenDates(startDate: date!, endDate: dateNow!)) " + Localization("months")
            }
        }
        if determineContains(videoObject.getLikes(), self.ownedLikes) {
            
            likeButton.setImage(UIImage(named: "HeartFull")?.maskWithColor(color: UIColor(red: 255 / 255, green: 61 / 255, blue: 61 / 255, alpha: 1)), for: .normal)
        }else{
            likeButton.setImage(UIImage(named: "HeartFull"), for: .normal)
        }
        likeNumberLBL.text = "\(videoObject.getLikes().count)"
        shareLBL.text = Localization("share")
        let request = GADRequest()
        interstitial.load(request)
        interstitial.delegate = self
    
      
        let ressource = BMPlayerResource(url: URL(string: videoURL)!,name: Localization("Zonzay Challenge"))
    
        BMPlayerConf.allowLog = true
        BMPlayerConf.topBarShowInCase = .always
        BMPlayerConf.shouldAutoPlay = true
        playerView.setVideo(resource: ressource)
        
        //let item = playerView.avPlayer?.currentItem
        playerView.play()
        playerView.delegate = self
        tableV.delegate = self
        tableV.dataSource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
       NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        playerView.backBlock = { [unowned self] (isFullScreen) in
            
            if self.isfullScreen {
                self.isfullScreen = false
            
              
                UIView.animate(withDuration: 0.25) {
                    self.setNeedsStatusBarAppearanceUpdate()
                }
                
                self.playerView.updateUI(false)
                
                UIView.animate(withDuration: 0.6, delay: 0.2, options: [.curveLinear], animations: {
                    self.playerView.transform = CGAffineTransform.identity
                }, completion: {(complete) in
                     self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                })
            }else{
                self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
                let _ = self.navigationController?.popViewController(animated: true)
            }
           
            
            
            
        }
        
        containerComment.layer.borderWidth = 1.0
        containerComment.layer.borderColor = UIColor.white.cgColor
       
       
    }
    func configureMasterSoundOff(){
        if SoundBackGround.enabled {
            if let url = SoundBackGround.soundsBundle.url(forResource: "background_music", withExtension: "mp3") {
                if let playerXX = SoundBackGround.sounds[url] {
                    playerXX.pause()
                }
            }
        }
    }
    func configureMasterSoundON(){
        if SoundBackGround.enabled {
            if let url = SoundBackGround.soundsBundle.url(forResource: "background_music", withExtension: "mp3") {
                if let playerXX = SoundBackGround.sounds[url] {
                    playerXX.play()
                }
            }
        }
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.landscapeLeft,UIInterfaceOrientationMask.landscapeRight,UIInterfaceOrientationMask.portrait]
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeRight
    }
    override var shouldAutorotate: Bool {
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    deinit {
        configureMasterSoundON()
        if playerView != nil {
        playerView.prepareToDealloc()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.allComments.count != 0 {
            return self.allComments.count
        }else{
            return 0
        }
    }
    func resetPlayerManager() {
        BMPlayerConf.allowLog = true
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .horizantalOnly
      
    }
    
    
    func determineContains(_ tab:[String], _ tabTwo:[String]) -> Bool{
        for t in tab {
            for ti in tabTwo {
                if ti == t {
                    return true
                }
            }
        }
        return false
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let userPic = cell.viewWithTag(1) as! RoundedUIImageView
        cell.selectionStyle = .none
        
        userPic.setImage(with: URL(string: self.allComments[indexPath.row].getUser().getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
        let messageView = cell.viewWithTag(2) as! UITextView
        messageView.text = self.allComments[indexPath.row].getMessage()
       
        messageView.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        let messageBackground = cell.viewWithTag(3)
        messageBackground?.layer.cornerRadius = 7
        messageBackground?.clipsToBounds = true
        let nameUser = cell.viewWithTag(5) as! UILabel
        nameUser.text = self.allComments[indexPath.row].getUser().getFirstName() + " " + self.allComments[indexPath.row].getUser().getLastName()
        return cell
    }
    @IBAction func sendComment(_ sender: UIButton) {
        if commentTextView.text.trimmingCharacters(in: .whitespaces) != "" {
           
            self.addComments(sender:sender)
        }
    }
    var commentsNotEmpty = false
    @IBAction func openCloseComments(_ sender : UIButton) {
        if commentsNotEmpty {
        if self.commentsTriangleOpener.transform != CGAffineTransform.identity {
            //close
            //myButton.transform = .identity
            self.tableViewHeightConst.constant = 0
            self.viewHeaderTopOfTable.backgroundColor = .clear
            
            self.sendBTN.isHidden = true
            self.tableV.alpha = 0
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                self.commentsTriangleOpener.transform = .identity
                self.tableV.layoutIfNeeded()
            }) { (complete) in
                
            }
        }else{
            //open
            // myButton.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * 0.5))
           self.tableViewHeightConst.constant = 177
            self.viewHeaderTopOfTable.backgroundColor = UIColor(red: 21 / 255, green: 126 / 255, blue: 149 / 255, alpha: 1)
              self.sendBTN.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                self.commentsTriangleOpener.transform = CGAffineTransform(rotationAngle: CGFloat(.pi * 0.5) + CGFloat(.pi * 0.5))
                self.tableV.layoutIfNeeded()
            }) { (complete) in
                self.tableV.alpha = 1
            }
        }
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //presentAds()
    }
    func presentAds(){
        if interstitial.isReady {
            if playerView.isPlaying  {
                 playerView.pause()
            }
            
            
            interstitial.present(fromRootViewController: self)
           
            //animateFadeTransition(to: damnViewController)
            
        } else {
            print("Ad wasn't ready")
        }
    }
   
    private func animateFadeTransition(to new: UIViewController, completion: (() -> Void)? = nil) {
        self.willMove(toParent: nil)
        addChild(new)
       
       transition(from: self, to: new, duration: 0.3, options: [.transitionCrossDissolve, .curveEaseOut], animations: {
       }) { completed in
        self.removeFromParent()
        new.didMove(toParent: self)
            //self = new
            completion?()  //1
       }
    }
    @IBAction func backTouchUpInside(_ sender: Any) {
         self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
        navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var likeButton: UIButton!
    
    @IBOutlet weak var likeNumberLBL: UILabel!
    @IBOutlet weak var shareLBL : UILabel!
    @IBAction func likeAction(_ sender: UIButton) {
        if determineContains(videoObject.getLikes(), self.ownedLikes) {
            
            //dislike
            self.disLikeVideo(index: videoObject.getVideoId())
        }else{
            //like
            self.likeVideo(index: videoObject.getVideoId())
        }
    }
    func likeVideo(index:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "videoId" : index
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.likeVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.data != nil {
                let data = JSON(response.data ?? Data())
                print("REQU1 : ",data)
                if data["like"]["_id"].exists() {
                    self.ownedLikes.append(data["like"]["_id"].stringValue)
                    var  count = Int(self.likeNumberLBL.text!)!
                              
                    count = count + 1
                        
                    
                    DispatchQueue.main.async {
                        self.likeNumberLBL.text = "\(count)"
                        self.likeButton.setImage(UIImage(named: "HeartFull")?.maskWithColor(color: UIColor(red: 255 / 255, green: 61 / 255, blue: 61 / 255, alpha: 1)), for: .normal)
                    }
                    
                }
            }
        }
        
    }
    func disLikeVideo(index:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "videoId" : index
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.disLikeVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.data != nil {
                let data = JSON(response.data ?? Data())
                print("REQU1 : ",data)
                if data["like"]["_id"].exists() {
                    self.ownedLikes.removeAll(data["like"]["_id"].stringValue)
                    var  count = Int(self.likeNumberLBL.text!)!
                    if count != 0 {
                        count = count - 1
                    }
                   
                    DispatchQueue.main.async {
                        self.likeNumberLBL.text = "\(count)"
                        self.likeButton.setImage(UIImage(named: "HeartFull"), for: .normal)

                    }
                }
            }
        }
        
    }
    @IBAction func shareAction(_ sender: UIButton) {
        let shareLink = ["http://zonzay.com/video/"  + videoObject.getVideoId()]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    var allComments : [CommentBean] = []
    
    func getAllComments(animated:Bool){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "videoId" : self.videoObject.getVideoId(),
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getAllComments, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getAllComments : ",data)
               
                //let zonz = data["zonz"].stringValue
                if data != JSON.null  {
                        self.allComments = []
                
        self.commentsCount.text = "\(data.arrayObject?.count ?? 0) " + Localization("comments")
                    
                    if data.arrayObject?.count != 0 {
                        self.commentsNotEmpty = true
                        self.commentsTriangleOpener.image = UIImage(named: "moreLessTriangle")
                        for i in 0...((data.arrayObject?.count)! - 1) {
                           
            self.allComments.append(CommentBean(id: data[i]["_id"].stringValue, message: data[i]["bodyComment"].stringValue, userComment: User(id: data[i]["commentUserId"]["_id"].stringValue, userFirstName: data[i]["commentUserId"]["userFirstName"].stringValue, userLastName: data[i]["commentUserId"]["userLastName"].stringValue, userAge: data[i]["commentUserId"]["userBirthday"].stringValue, userEmail: data[i]["commentUserId"]["userEmail"].stringValue, token: data[i]["commentUserId"]["userToken"].stringValue, photo: data[i]["commentUserId"]["userImageURL"].stringValue, username: data[i]["commentUserId"]["userName"].stringValue), date: data[i]["dateComment"].stringValue, videoId: data[i]["commentVideoId"].stringValue))
                        }
                         self.tableV.reloadData()
                        if animated == false {
                            if self.commentsTriangleOpener.transform == CGAffineTransform.identity {
                                self.openCloseComments(UIButton())
                            }
                            self.tableV.scrollToRow(at: IndexPath(row: self.allComments.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: false)
                        }
                    }
                       /* self.ownedLikes = []
                        if data["likes"].arrayObject?.count != 0 {
                            for i in 0...((data["likes"].arrayObject?.count)! - 1) {
                                self.ownedLikes.append(data["likes"][i].stringValue)
                                
                            }
                        } */
                        
                    
                }
               
            }
            
        }catch {
            
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     /*   if self.commentsHeight.index(forKey: indexPath) != nil {
            let cell = tableView.cellForRow(at: indexPath)
            var frame = cell?.frame
            frame?.size.height += self.commentsHeight[indexPath]!
        } */
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       
        
        let textView = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 96, height: 44))
        textView.font = UIFont(name: "AvenirNext-Regular", size: 15)
        textView.text = self.allComments[indexPath.row].getMessage()
        let height = textView.roundHeight()
        self.commentsHeight[indexPath] = height
        if height < 44 {
           return 44
        }else{
            return 7.5 + 8 + height
        }
    }
    func addComments(sender:UIButton){
    
    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
    do {
        let a = try JSON(data: dataFromString!)
        
        let settings : Parameters = [
            "videoId" : self.videoObject.getVideoId(),
            "userId": a["_id"].stringValue,
            "bodyComment" : self.commentTextView.text!.trimmingCharacters(in: .whitespaces)
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.addComment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            let data = JSON(response.data ?? Data())
            self.commentTextView.text = ""
            self.view.endEditing(true)
            print("addComments : ",data)
           
            self.getAllComments(animated: false)
            
        }
        
    }catch {
        
    }
}
    
}
extension HomeNextViewController : GADInterstitialDelegate {
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
         self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
    }
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("ADs errror : ",error.localizedDescription)
      
        interstitial = reloadInterstitialAd()
    }
    func reloadInterstitialAd() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
         self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        isAdActive = false
        if didFinishVideo == false {
           interstitial = reloadInterstitialAd()
            self.playerView.play()
        }
    }
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        isAdActive = true
        NotificationCenter.default.removeObserver(self)
    }
    func yearsBetweenDates(startDate:Date, endDate:Date) -> Int
    {
        
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.year]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        print("Months:",components.year!)
        return components.year!
        
    }
    func monthsBetweenDates(startDate:Date, endDate:Date) -> Int
    {
        
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.month]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        print("Months:",components.month!)
        return components.month!
        
    }
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.day]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.day!
        
    }
    func hoursBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.hour]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        print("startDate:",startDate)
        print("startDate:",endDate)
        print("count:",components.hour!)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.hour!
        
    }
    func minutesBetweenDates(startDate:Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.minute]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.minute!
        
    }
    
}
extension TimeInterval {
    func toMM_SS() -> String {
        let interval = self
        let componentFormatter = DateComponentsFormatter()
        
        componentFormatter.unitsStyle = .positional
        componentFormatter.zeroFormattingBehavior = .pad
        componentFormatter.allowedUnits = [.minute, .second]
        return componentFormatter.string(from: interval) ?? ""
    }
}
extension UITextView {
     func roundHeight() -> CGFloat {
        var newHeight: CGFloat = 0
        
        if let font = self.font {
            let attributes = [NSAttributedString.Key.font: font]
            let boundingSize = CGSize(width: self.frame.size.width - self.textContainerInset.left - self.textContainerInset.right, height: .greatestFiniteMagnitude)
            let size = self.text.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
            newHeight = ceil(size.height)
        }
        
        if let font = font, newHeight < font.lineHeight {
            newHeight = font.lineHeight
        }
        
        return newHeight + self.textContainerInset.top + self.textContainerInset.bottom
    }
}
extension String {
    func transformDateToOrigin() -> String {
        var temp = self
        for _ in 0...4 {
        temp.removeLast()
        }
        temp = temp + "+0000"
        return temp
    }
}
