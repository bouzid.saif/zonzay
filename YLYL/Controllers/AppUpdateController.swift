//
//  AppUpdateController.swift
//  YLYL
//
//  Created by macbook on 2019-05-20.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

import Foundation
import UIKit
class AppUpdateController: UIViewController {
    
    @IBOutlet weak var WeAreBetterEverLBL : UILabel!
    @IBOutlet weak var BetterLocation : UILabel!
    @IBOutlet weak var QuickerVideo : UILabel!
    @IBOutlet weak var DataSecure : UILabel!
    @IBOutlet weak var PleaseUpdateAPP : UILabel!
    @IBOutlet weak var updateBTN : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        WeAreBetterEverLBL.text = Localization("WeAreBetterEverLBL")
        BetterLocation.text = Localization("BetterLocation")
        QuickerVideo.text = Localization("QuickerVideo")
        DataSecure.text = Localization("DataSecure")
        PleaseUpdateAPP.text = Localization("PleaseUpdateAPP")
        updateBTN.setTitle(Localization("Update"), for: .normal)

    }
    @IBAction func UpdateAction(_ sender: UIButton) {
        if let url = URL(string: "https://itunes.apple.com/in/app/Zonzay/id1455610906?ls=1&mt=8") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
