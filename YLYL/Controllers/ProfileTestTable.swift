//
//  ProfileTestTable.swift
//  YLYL
//
//  Created by macbook on 2/5/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import StoreKit
import ChameleonFramework
import DKImagePickerController
import MapleBacon
import FirebaseMLVision
import AVKit
class ProfileTestTable: ServerUpdateDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.battlesCollection {
       return self.Acutality.count
        }else {
            return self.friends.count
        }
    }
    @IBOutlet weak var viewTopContainer: UIView!
    
    @IBOutlet weak var userProfileTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameLBL: UILabel!
    fileprivate var buffer:NSMutableData = NSMutableData()
    //MARK: - Vison detection
    private lazy var faceDetectorOption: VisionFaceDetectorOptions = {
        let option = VisionFaceDetectorOptions()
        option.classificationMode = .all
        option.performanceMode = .accurate
        option.landmarkMode = .all
        option.contourMode = .all
        return option
    }()
    lazy var vision = Vision.vision()
    private lazy var faceDetector = vision.faceDetector(options: faceDetectorOption)
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == self.friendsCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as UICollectionViewCell
            let imageUser = cell.viewWithTag(1) as! RoundedUIImageView
           
            if self.friends[indexPath.row].getProfileImage() != "" {
            imageUser.setImage(with: URL(string: self.friends[indexPath.row].getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            }else{
               imageUser.image = UIImage(named: "artist")
            }
           // imageUser.layer.cornerRadius = 25  / 2
            
           //imageUser.layer.masksToBounds = true
              return cell
        }else{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTwo", for: indexPath) as UICollectionViewCell
            let imageUser1 = cell.viewWithTag(1) as! UIImageViewWithMask
            print(imageUser1.frame.width)
            //imageUser1.contentMode = .scaleAspectFill
            //let backgroundImage = cell.viewWithTag(3) as! UIImageView
            //let imageToWrite = UIImage(named: "background_profile*")
            //backgroundImage.image  = imageToWrite?.imageWithBorder(width: 4, color: .white)
            imageUser1.setImage(with: URL(string: self.Acutality[indexPath.row].getVideoFirstUser().getProfileImage()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
            let imageUser2 = cell.viewWithTag(2) as! UIImageViewWithMask
            imageUser2.setImage(with: URL(string: self.Acutality[indexPath.row].getVideoSecondUser().getProfileImage()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
            let MiddleImageContainer = cell.viewWithTag(15) as! UIImageView
            if self.ViewHidderOne.isHidden == true {
                 MiddleImageContainer.image = UIImage(named: "ZonzayProfileLogoMerged")
            }else{
                MiddleImageContainer.image = UIImage(named: "TrashVideoProfil")
                
            }
              return cell
        }
      
    }
    var ViewType = "Profile"
    
    @IBOutlet weak var triangleFriend: Triangle!
    
    @IBOutlet weak var UnfriendBlockContainer: UIStackView!
    @IBOutlet weak var unfriendBTN : UIButton!
    @IBOutlet weak var blockBTN : UIButton!
    
    @IBOutlet weak var buttonTriangleFake: UIButton!
    
    @IBOutlet weak var buttonSendFake: UIButton!
    
    @IBOutlet weak var triangleFake: Triangle!
    
    @IBOutlet weak var triangleBTN: UIButton!
    
    @IBOutlet weak var UnfriendBlockCalque: UIView!
    
    @IBOutlet weak var CustomPoUp: UIView!
    
    @IBOutlet weak var textPoUp: UILabel!
    
    @IBOutlet weak var userUnfriendedAnimation: UILabel!
    
    
    @IBOutlet weak var topVIPButtonConst: NSLayoutConstraint!
    var JokesJSON : [Jokes] = []
    var Acutality : [VideoYLYL] = []
    var jokes : [String] = []
    var friends : [User] = []
    var headerView : GSKSpotyLikeHeaderView!
    var userTarget: JSON = []
    var navigation : UINavigationController = UINavigationController()
    let color = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
    
    @IBOutlet weak var VipButtonTop: NSLayoutConstraint!
    
    @IBOutlet weak var battlesContainer: UIView!
    @IBOutlet weak var jokesContainer: UIView!
    @IBOutlet weak var battlesCollection: UICollectionView!
    @IBOutlet weak var jokesTableView : UITableView!
    @IBOutlet weak var friendsCollection : UICollectionView!
    
    @IBOutlet weak var FullNameLBL : UILabel!
    @IBOutlet weak  var UserImageContainer : UIImageView!
    
    @IBOutlet weak var NoVideoView: UIView!
    
    @IBOutlet weak var NoVideoImage: UIImageView!
    
    @IBOutlet weak var NoVideoLabel: UILabel!
    
    @IBOutlet weak var NoJokesView: UIView!
    
    @IBOutlet weak var NoJokesImage: UIImageView!
    
    @IBOutlet weak var NoJokesLabel: UILabel!
    @IBOutlet weak var FriendsButton : UIButton!
    @IBOutlet weak var VIPButton : UIButton!
    @IBOutlet weak var PopUpImageVerticalConstraint: NSLayoutConstraint!
    var FriendJSON : JSON = []
    @IBAction func VIPAction(_ sender : Any) {
        let vc = UIStoryboard(name: "VIPAccess", bundle: nil).instantiateViewController(withIdentifier: "VIPAccessController") as! VIPAccessController
        vc.hero.modalAnimationType = .zoom
        //vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
      // showVIPContainer()
    }
    
    @IBOutlet weak var PrivateAccountVisual: UIVisualEffectView!
    
    @IBOutlet weak var EditPictureButton : UIButton!
    @IBAction func EditPictureBTN(_ sender: Any) {
        reportAction()
    }
    @IBOutlet weak var EditProfileButton : UIButton!
    
    @IBOutlet weak var lastNameLBL: UILabel!
    
    @IBOutlet weak var viewHeader: UIView!
    
    @IBOutlet weak var HeightOfMore: NSLayoutConstraint!
    
    @IBOutlet weak var yearsLBL: UILabel!
    @IBAction func EditProfileBTN(_ sender: Any) {
        if EditProfileButton.title(for: .normal) == Localization("MoreP") {
            self.EditProfileButton.setTitle(Localization("LessP"), for: .normal)
   // self.viewHeader.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear], animations: {
            self.HeightOfMore.constant = 50
            self.userProfileTopConstraint.constant -= 18
            self.viewTopContainer.layoutIfNeeded()
            self.view.layoutIfNeeded()
        }) { (verif) in
            self.lastNameLBL.isHidden = false
            self.yearsLBL.isHidden = false
        }
        }else{
           self.EditProfileButton.setTitle(Localization("MoreP"), for: .normal)
            self.lastNameLBL.isHidden = true
            self.yearsLBL.isHidden = true
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear], animations: {
                self.HeightOfMore.constant = 32
                self.userProfileTopConstraint.constant += 18
                self.viewTopContainer.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }) { (verif) in
               
            }
        }
    }
    @IBOutlet weak var EditInterestsButton : UIButton!
    @IBAction func EditInterests(_ sender: Any) {
        
    }
    @IBOutlet weak var EditVideosBTN : UIButton!
    @IBOutlet weak var EditVideosBTNImage : UIButton!
    @IBAction func EditVideos(_ sender: Any) {
        self.hideToShowBattles(hide: false)
        
    }
    @IBOutlet weak var sendRequestButton : UIButton!
    @IBAction func sendRequestBTN(_ sender: Any) {
        self.requestAction(sender as? UIButton)
    }
    
    @IBOutlet weak var widthContainerLayout: NSLayoutConstraint!
    var tapgesture : UITapGestureRecognizer!
    @IBOutlet weak var heightContainerLayout: NSLayoutConstraint!
    @IBAction func closeMoreOptions(_ sender: UIButton) {
        self.UnfriendBlockCalque.removeGestureRecognizer(tapgesture)
        UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
            self.triangleFake.transform = CGAffineTransform.identity
        }) { (verif) in
            self.triangleFake.isHidden = true
            self.buttonTriangleFake.isHidden = true
        }
        self.UnfriendBlockContainer.isHidden = true
        
        self.UnfriendBlockCalque.isHidden = true
        self.buttonSendFake.isHidden = true
        self.widthContainerLayout.constant -= 24
        self.heightContainerLayout.constant -= 12
        self.UnfriendBlockContainer.layoutIfNeeded()
    }
    @IBAction func showMoreOptions(_ sender: UIButton) {
       
        self.UnfriendBlockCalque.isHidden = false
        tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.closeShowMoreOptions(_:)))
        self.UnfriendBlockCalque.addGestureRecognizer(tapgesture)
        self.UnfriendBlockCalque.isUserInteractionEnabled = true
        self.UnfriendBlockContainer.isHidden = false
        buttonTriangleFake.isHidden = false
        
         buttonSendFake.isHidden = false
        triangleFake.isHidden = false
        UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
            self.triangleFake.transform = CGAffineTransform(rotationAngle: CGFloat(GLKMathDegreesToRadians(180)))
        }) { (verif) in
            
        }
        
        self.widthContainerLayout.constant += 24
        self.heightContainerLayout.constant += 12
        UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveEaseInOut], animations: {
           self.UnfriendBlockContainer.layoutIfNeeded()

           
        }) { (verifTwo) in
            
        }
        
    }
    @objc func closeShowMoreOptions(_ sender: UITapGestureRecognizer){
       self.UnfriendBlockCalque.removeGestureRecognizer(tapgesture)
        UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
            self.triangleFake.transform = CGAffineTransform.identity
        }) { (verif) in
             self.triangleFake.isHidden = true
             self.buttonTriangleFake.isHidden = true
        }
        self.UnfriendBlockContainer.isHidden = true
      
        self.UnfriendBlockCalque.isHidden = true
        self.buttonSendFake.isHidden = true
        self.widthContainerLayout.constant -= 24
        self.heightContainerLayout.constant -= 12
       
       
        self.UnfriendBlockContainer.layoutIfNeeded()
        
    }
    @objc func closeShowReport(_ sender: UITapGestureRecognizer){
        self.UnfriendBlockCalque.removeGestureRecognizer(tapgesture)
          self.confirmReportBTN.isHidden = true
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomReport.alpha = 0
        }) { (verif) in
            self.CustomReport.isHidden = true
            self.CustomReport.alpha = 1
            self.UnfriendBlockCalque.isHidden = true
         
        }
    }
    @IBOutlet weak var EditJokes : UIButton!
    @IBAction func EditJokesAction(_ sender: UIButton) {
       // self.hideEditJokeContainer(hide: false)
        let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
        vc.isPlayingOrProfile = false
        self.navigationController?.pushViewController(vc, animated: true)
        //self.navigation.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var FlagUser: UIImageView!
    
    @IBOutlet weak var FriendsContainerView: UIView!
    @IBOutlet weak var CountryUser: UILabel!
    @IBOutlet weak var InterestsLBL: UILabel!
    @IBOutlet weak var InterestsContainerLBL: UILabel!
    @IBOutlet weak var MyIntersestesLBL: UILabel!
    @IBOutlet weak var MyBattlesLBL: UILabel!
    @IBOutlet weak var MyJokesLBL:UILabel!
    @IBOutlet weak var JokeContainerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var EditJokeVisualContainer: UIVisualEffectView!
    @IBOutlet weak var EditJokeTable: UITableView!
    @IBOutlet weak var ViewHidderOne : UIView!
     @IBOutlet weak var ViewHidderTwo : UIView!
    @IBAction func hideVideoBattles(_ sender: UITapGestureRecognizer){
            self.hideToShowBattles(hide: true)
    }
    @IBOutlet weak var PopUpDeleteBattle : UIView!
    
    @IBAction func  YesDelete(_ sender: UIButton) {
        self.deleteSomeVideos(videos: [Acutality[self.itemToRemove]])
        
    }
    @IBAction func  NoDelete(_ sender : UIButton) {
       self.battlesCollection.deselectItem(at: IndexPath(row: self.itemToRemove, section: 0), animated: false)
        self.hidePopup(hide: true)
    }
    @IBOutlet weak var FirstUserPopUp : UIImageView!
    @IBOutlet weak var SecondUserPopUp : UIImageView!
    @IBOutlet weak var BackgroundPopup : UIImageView!
    var itemToRemove : Int = -1
    
    @IBOutlet weak var UserPopUpIMG: RoundedUIImageView!
    
    
    @IBOutlet weak var CustomReport: UIView!
    
    @IBOutlet weak var ChoiceOne: DLRadioButton!
    
    @IBOutlet weak var ChoiceTwo: DLRadioButton!
    
    @IBOutlet weak var ChoiceThree: DLRadioButton!
    
    @IBOutlet weak var ChoiceFour: DLRadioButton!
   

    @IBOutlet weak var confirmReportBTN: UIButton!
    @IBOutlet weak var pleaseChooseWhyLBL: UILabel!
    
    @IBOutlet weak var activityIndictaor: UIActivityIndicatorView!
    @IBOutlet weak var reportOtherTV: PlaceholderTextView!
    @IBAction func goBack(_ sender: UIBarButtonItem){
        
        if self.EditJokeVisualContainer.isHidden == true {
            if self.wasPushed == false {
        self.navigation.isNavigationBarHidden = true
         self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        //self.navigation.popViewController(animated: true)
                
        self.dismiss(animated: true, completion: nil)
            }else{
                self.navigationController?.popToRootViewController(animated: false)
            }
        }else{
            if self.jokes.count != 0 {
            self.saveJokesIntoDataBase()
            }
            self.hideEditJokeContainer(hide: true)
        }
    }
    func accountPrivate(_ verif: Bool)  {
    self.PrivateAccountVisual.isHidden = !verif
    self.FriendsContainerView.isHidden = verif
    }
    func hidePopup(hide : Bool) {
        self.PopUpDeleteBattle.isHidden = hide
        
    }
    func configurePopup(){
        self.BackgroundPopup.image = UIImage(named: "background_profile*")?.imageWithBorder(width: 4, color: .white)

    }
    func hideToShowBattles(hide: Bool) {
    self.ViewHidderOne.isHidden = hide
    self.ViewHidderTwo.isHidden = hide
        self.battlesCollection.reloadData()
    }
    func hideVideoTable(hide: Bool) {
        self.NoVideoView.isHidden = hide
        self.NoVideoImage.isHidden = hide
        self.NoVideoLabel.isHidden = hide
    }
    func hidejokeTable(hide:Bool) {
        self.NoJokesView.isHidden = hide
        self.NoJokesImage.isHidden = hide
        self.NoJokesLabel.isHidden = hide
    }
    func hideFriendsTable(hide:Bool) {
        self.FriendsButton.isHidden = hide
    }
    func hideEditJokeContainer(hide:Bool) {
        if hide == false {
            self.EditJokeVisualContainer.alpha = 0
            self.EditJokeTable.alpha = 0
            self.EditJokeTable.isHidden = false
             self.EditJokeVisualContainer.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.EditJokeVisualContainer.alpha = 1
            self.EditJokeTable.alpha  = 1
        }
        }else{
        self.EditJokeVisualContainer.isHidden = hide
        self.EditJokeTable.isHidden = hide
        }
    }
    func prepareNavigationBar(){
    self.navigation.navigationBar.tintColor = UIColor(red: 31.0/255.0, green: 165.0/255.0, blue: 194.0/255.0, alpha: 1)
        let left = UIBarButtonItem(image: UIImage(named: "back_profile_saif"), style: .plain, target: self, action: #selector(self.gotBack))
        self.navigation.navigationItem.leftBarButtonItem = left
        self.navigation.navigationBar.isTranslucent = false
    }
    @objc func gotBack(){
        print("WasPushed: ",self.wasPushed)
        if self.wasPushed == false {
        self.dismiss(animated: true) {
            self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden  = true
        self.navigationController?.gsk_setNavigationBarTransparent(false, animated: false)
        self.arrangeLabels(ViewType)
        print("ViewType : ",ViewType)
        if ViewType == "Visitor" {
            self.EditInterestsButton.isHidden = true
            self.EditVideosBTN.isHidden = true
            self.EditVideosBTNImage.isHidden = true
            self.EditProfileButton.isHidden = true
            self.triangleBTN.isHidden = true
            
            self.triangleFriend.isHidden = true
            if userTarget["privacyAccount"].boolValue {
                self.accountPrivate(true)
            }else{
                self.accountPrivate(false)
                self.JokeContainerHeight.constant = 0
                self.NoJokesLabel.isHidden = true
                self.NoJokesLabel.alpha = 0
                self.NoJokesView.isHidden = true
                self.NoJokesImage.isHidden = true
              
                self.topVIPButtonConst.constant = 20
                
                //self.view.setNeedsDisplay()
               // self.view.layoutIfNeeded()
                //self.NoJokesView.setNeedsDisplay()
            }
            self.sendRequestButton.setTitle(Localization("Send_Request"), for: .normal)
            self.buttonSendFake.setTitle(Localization("Send_Request"), for: .normal)
            self.FullNameLBL.text = userTarget["userFirstName"].stringValue + " " + userTarget["userLastName"].stringValue
            self.FullNameLBL.adjustsFontSizeToFitWidth = true
            self.CountryUser.text = self.userTarget["paysUser"].stringValue
            if userTarget["userImageURL"].exists() {
                if userTarget["userImageURL"].stringValue != ""{
                    self.UserPopUpIMG.setImage(with: URL(string: userTarget["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    self.UserImageContainer.setImage(with: URL(string: userTarget["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    
                }
            }else{
                self.UserPopUpIMG.image = UIImage(named: "artist")
                  self.UserImageContainer.image = UIImage(named: "artist")
            }
                if userTarget["alphaCode"].exists() {
                    let alpha = userTarget["alphaCode"].stringValue
                    let image = UIImage(named: alpha)
                    self.FlagUser.image = image
                }else{
                    self.FlagUser.isHidden = true
                }
                if userTarget["listInterests"].stringValue != "" {
                    var resString = ""
                    let resIntersts = (userTarget["listInterests"].stringValue).components(separatedBy: ";")
                    var i = 0
                    for res in resIntersts {
                        if i < 4 {
                            resString  = resString + "#" + res
                        }
                        i+=1
                    }
                    self.InterestsContainerLBL.text = resString
                }else{
                    self.InterestsContainerLBL.isHidden = true
                }
                self.hideFriendsTable(hide: true)
                self.hideVideoTable(hide: false)
                self.hidejokeTable(hide: false)
                self.FriendsContainerView.removeFromSuperview()
                self.jokesContainer.removeFromSuperview()
            self.VipButtonTop.constant = 3
               /* self.VIPButton.snp.remakeConstraints { (const) in
                const.centerX.equalToSuperview()
                const.width.equalTo(198)
                const.height.equalTo(43)
                const.top.equalTo(battlesContainer.bottom).offset(10)
                                                } */
            
            self.categories.removeAll()
            self.getSendedFriendRequest()
        }else if ViewType == "Profile"{
            let tapGest = UITapGestureRecognizer(target: self, action: #selector(self.ChangePictureAction(_:)))
            tapGest.numberOfTapsRequired = 1
            self.UserImageContainer.isUserInteractionEnabled = true
            self.UserImageContainer.addGestureRecognizer(tapGest)
            self.EditJokeTable.delegate = self
            self.EditJokeTable.dataSource = self
            self.getFriends(who: "me")
            self.EditPictureButton.isHidden = true
            self.EditProfileButton.setTitle(Localization("MoreP"), for: .normal)
        
            self.sendRequestButton.isHidden = true
            self.triangleBTN.isHidden = true
            self.triangleFriend.isHidden = true
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                self.CountryUser.text = a["paysUser"].stringValue
                self.FullNameLBL.text =  a["userFirstName"].stringValue
                self.lastNameLBL.text = a["userLastName"].stringValue
                self.usernameLBL.text = a["userName"].stringValue
                let birth = a["userBirthday"].stringValue
                let dateFormatter = DateFormatter()
                let date = dateFormatter.date(fromSwapiString: birth)
                let dateFormatterNow = DateFormatter()
                dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                let dateNow = dateFormatterNow.date(fromSwapiString: dateFormatterNow.string(from: Date()))
                let howMuch = yearsBetweenDates(startDate: date!, endDate: dateNow!)
                self.yearsLBL.text = "\(howMuch) " + Localization("Years")
                if a["userImageURL"].exists() {
                    if a["userImageURL"].stringValue != ""{
                        self.UserImageContainer.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                        self.UserPopUpIMG.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                        
                        
                    }
                }else{
                    self.UserPopUpIMG.image = UIImage(named: "artist")
                      self.UserImageContainer.image = UIImage(named: "artist")
                }
                if a["alphaCode"].exists() {
                    let alpha = a["alphaCode"].stringValue
                    let image = UIImage(named: alpha)
                    self.FlagUser.image = image
                }else{
                    self.FlagUser.isHidden = true
                }
                if a["listInterests"].stringValue != "" {
                    var resString = ""
                    let resIntersts = (a["listInterests"].stringValue).components(separatedBy: ";")
                    var i = 0
                    for res in resIntersts {
                        if i < 4 {
                            resString  = resString + "#" + res
                        }
                        i+=1
                    }
                    self.InterestsContainerLBL.text = resString
                }else{
                    self.InterestsContainerLBL.isHidden = true
                }
            }catch{
                
            }
        }else if ViewType == "Friend" {
            self.EditInterestsButton.isHidden = true
            self.EditVideosBTN.isHidden = true
            self.EditVideosBTNImage.isHidden = true
            self.EditProfileButton.isHidden = true
            self.JokeContainerHeight.constant = 2
            self.view.setNeedsDisplay()
            self.sendRequestButton.setTitle(Localization("Message"), for: .normal)
            self.buttonSendFake.setTitle(Localization("Message"), for: .normal)
            //self.headerView.followButton.setTitle("  Message  ", for: .normal)
            self.triangleFriend.isHidden = false
            self.NoJokesLabel.isHidden = true
            self.NoJokesLabel.alpha = 0
            self.topVIPButtonConst.constant = 20
            self.FullNameLBL.text  = userTarget["userFirstName"].stringValue
            self.FullNameLBL.adjustsFontSizeToFitWidth = true
            self.CountryUser.text = userTarget["paysUser"].stringValue
            if userTarget["userImageURL"].exists() {
                if userTarget["userImageURL"].stringValue != ""{
                    self.UserImageContainer.setImage(with: URL(string: userTarget["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                     self.UserPopUpIMG.setImage(with: URL(string: userTarget["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    
                }
            }else{
             self.UserImageContainer.image = UIImage(named: "artist")
             self.UserPopUpIMG.image = UIImage(named: "artist")
            }
            if userTarget["alphaCode"].exists() {
                let alpha = userTarget["alphaCode"].stringValue
                let image = UIImage(named: alpha)
                self.FlagUser.image = image
            }else{
                self.FlagUser.isHidden = true
            }
            if userTarget["listInterests"].stringValue != "" {
                var resString = ""
                let resIntersts = (userTarget["listInterests"].stringValue).components(separatedBy: ";")
                var i = 0
                for res in resIntersts {
                    if i < 4 {
                        resString  = resString + "#" + res
                    }
                    i+=1
                }
                self.InterestsContainerLBL.text = resString
            }else{
                self.InterestsContainerLBL.isHidden = true
            }
            self.getVideo(who: "friend")
            self.getFriends(who: "friend")
        }
        if ViewType == "Profile" {
            self.getJokes()
        }
    }
    @objc func NothingAction(){
        //self.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
    }
    func arrangeLabels(_ type:String){
        if type == "Visitor" || type == "Friend" {
            self.MyJokesLBL.text = Localization("Jokes")
            self.MyBattlesLBL.text = Localization("Battles")
            self.MyIntersestesLBL.text = Localization("Interests")
        }else{
            self.MyJokesLBL.text = Localization("My_jokes")
            self.MyBattlesLBL.text = Localization("My_battles")
            self.MyIntersestesLBL.text = Localization("My_interests")
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.EditJokeTable {
            let jokeText = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 72, height: 24))
            jokeText.font = UIFont(name: "Roboto-Regular", size: 12)
            jokeText.text = self.jokes[indexPath.row]
            let count = offSetTableViewIfNeeded(textView: jokeText)
            return   CGFloat(40 + (12 * count))
            
        }else{
            return 44.0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
        if collectionView == self.battlesCollection {
             print("selected")
            if self.ViewHidderOne.isHidden == false {
                 print("selected")
                self.itemToRemove = indexPath.row
                self.FirstUserPopUp.setImage(with: URL(string: self.Acutality[indexPath.row].getVideoFirstUser().getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
                self.SecondUserPopUp.setImage(with: URL(string: self.Acutality[indexPath.row].getVideoSecondUser().getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
                self.hidePopup(hide: false)
                
            }else{
                ///Play video Cell
                guard let url = URL(string:                 self.Acutality[indexPath.row].getPathVideo()) else {
                       return
                   }
                 let player = AVPlayer(url: url)
                let controller = AVPlayerViewController()
                 controller.player = player
                present(controller, animated: true) {
                    player.play()
                }
            }
        }else if collectionView == self.friendsCollection {
            let user = self.friends[indexPath.row]
            var indexToRend = 0
            for i in 0...((self.FriendJSON.arrayObject!.count) - 1) {
                if self.FriendJSON[i]["_id"].stringValue == user.getId() {
                    indexToRend = i
                }
            }
            self.getFriendsChecker(userId: user.getId(), index: IndexPath(row: indexToRend, section: 0))
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideVideoTable(hide: true)
        self.hidejokeTable(hide: true)
        self.activityIndictaor.isHidden = true
        self.configureVIP()
        self.configurePopup()
        self.EditJokeTable.rowHeight = UITableView.automaticDimension
        self.EditJokeTable.estimatedRowHeight = 44.0
        self.navigationController?.isNavigationBarHidden = true
        self.FriendsButton.layer.cornerRadius = 7
        self.FriendsButton.layer.masksToBounds = true
        self.VIPButton.layer.cornerRadius = 5
        self.VIPButton.layer.masksToBounds = true
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
    
        //prepareNavigationBar()
        self.title = "Profile"
        print("userTarget : ", userTarget)
        self.usernameLBL.text = userTarget["userName"].stringValue
        prepareReportContainer()
        
    }
    func prepareReportContainer(){
       ChoiceOne.isSelected = true
        if let font = UIFont(name: "Roboto-Medium", size: 19.0) {
            print("True")
            ChoiceOne.titleLabel?.font = font
        }
        self.CustomReport.isHidden = true
        
    }
    @objc func goEditProfile(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileReworked
        //vc.actuality = self.Acutality
        //let navigation : UINavigationController = UINavigationController(rootViewController: vc)
       // vc.modalTransitionStyle = UIModalTransitionStyle.coverVertical
    
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(navigation, animated: true, completion: nil)
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.jokesTableView || tableView == self.EditJokeTable {
            return self.jokes.count
        }else{
            return self.friends.count
        }
    }
    var CellHeights : [IndexPath : CGFloat] = [:]
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == self.EditJokeTable {
            print("willDisplayCell")
           
            print("Cell \(indexPath.row) :",cell.frame.height)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == (self.friends.count - 1) {
            let number = collectionView.visibleCells.count
            if number < (self.friends.count ){
             let diff = (self.friends.count) - number
                self.FriendsButton.setTitle("+ " + String(diff), for: .normal)
            }
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.EditJokeTable {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    
            let IndexJoke = cell.viewWithTag(1) as! UILabel
            IndexJoke.text = "\(indexPath.row + 1)"
            let jokeText = cell.viewWithTag(2) as! UITextView
            jokeText.text = self.jokes[indexPath.row]
            let count = offSetTableViewIfNeeded(textView: jokeText)
            self.CellHeights[indexPath] =  CGFloat(40 + (4 * count))
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
            let IndexJoke = cell.viewWithTag(1) as! UILabel
            IndexJoke.text = "\(indexPath.row + 1)"
            let jokeText = cell.viewWithTag(2) as! EdgeInsetLabel
            jokeText.text = self.jokes[indexPath.row]
            return cell
        }
        
    }
    func offSetTableViewIfNeeded(textView:UITextView) -> Int {
        let numberOfGlyphs = textView.layoutManager.numberOfGlyphs
        var index : Int = 0
        var lineRange = NSRange(location: NSNotFound, length: 0)
        var currentNumOfLines : Int = 0
        var numberOfParagraphJump : Int = 0
        
        while index < numberOfGlyphs {
            textView.layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            currentNumOfLines += 1
            
            // Observing whether user went to line and if it's the first such line break, accounting for it.
            if textView.text.last == "\n", numberOfParagraphJump == 0 {
                numberOfParagraphJump = 1
            }
        }
        
        currentNumOfLines += numberOfParagraphJump
        print("Number of lines is:", currentNumOfLines)
        return currentNumOfLines
        
    }
    var categories = ["My Challenges" , "My Jokes"]
    func cancelFriendRequest(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "senderId" : a["_id"].stringValue,
                "receiverId" : userTarget["_id"].stringValue
            ]
            
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.deleteFriendRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    print(response.data)
                    let data = JSON(response.data ?? Data())
                    print("deleteFriendRequest : ",data)
                    if data["message"].stringValue == "deleted successfully"{
                        self.sendRequestButton.setTitle(Localization("Send_Request"), for: .normal)
                        self.buttonSendFake.setTitle(Localization("Send_Request"), for: .normal)
                    }
                    let alert = UIAlertController(title: Localization("Friends"), message: Localization("FriendRR"), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    //self.jokes = data
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    func sendFriendRequest(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let dateForm = DateFormatter()
            dateForm.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let settings : Parameters = [
                "senderId" : a["_id"].stringValue,
                "receiverId" : userTarget["_id"].stringValue,
                "friendRequestDate" : dateForm.string(from: Date())
            ]
            
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.sendFriendRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    print(response.data)
                    let data = JSON(response.data ?? Data())
                    print("sendFriendRequest : ",data)
                    if data["message"].stringValue == "Friend Request sended"{
                       self.sendRequestButton.setTitle(Localization("Cancel_Request"), for: .normal)
                         self.buttonSendFake.setTitle(Localization("Cancel_Request"), for: .normal)
                    }
                    let alert = UIAlertController(title: Localization("Friend"), message: Localization("FriendRS"), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    //self.jokes = data
                   
                }else{
                   
                }
                
            }
            
        }catch {
            
        }
    }
    func unFriendRequest(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let dateForm = DateFormatter()
            dateForm.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "userFriendId" : userTarget["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.unFriendRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    print(response.data)
                    let data = JSON(response.data ?? Data())
                    print("unFriendRequest : ",data)
                    if data["message"].exists() {
                       // self.sendRequestButton.setTitle(Localization("Cancel_Request"), for: .normal)
                        print("error")
                    }else{
                        let alert = UIAlertController(title: Localization("Friend"), message: Localization("FriendRemoved"), preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: {(okay) in
                            self.userUnfriendedAnimation.alpha = 0
                             self.userUnfriendedAnimation.isHidden = false
                            self.textPoUp.isHidden = true
                            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveLinear], animations: {
                                self.userUnfriendedAnimation.alpha = 1
                                
                                self.PopUpImageVerticalConstraint.constant = 0
                            }, completion: { (verify) in
                                self.UserPopUpIMG.image = UIImage(named: "testX")
                                UIView.animate(withDuration: 0.2, delay: 0.8, options: [.curveEaseInOut], animations: {
                                    self.CustomPoUp.alpha = 0
                                }) { (complete) in
                                    self.CustomPoUp.isHidden = true
                                    self.CustomPoUp.alpha = 1
                                    self.textPoUp.isHidden = false
                                    
                                    self.dismiss(animated: true) {
                                        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
                                    }
                                }
                            })
                            
                          
                        })
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    //self.jokes = data
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    
    @IBAction func YesPoUpAction(_ sender: UIButton) {
        if isBlockOrUnfriend{
            
         blockUserRequest()
            
        }else{
        self.unFriendRequest()
        }
    }
    
    @IBAction func NoPopUpAction(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomPoUp.alpha = 0
        }) { (complete) in
            self.CustomPoUp.isHidden = true
            self.CustomPoUp.alpha = 1
            
        }
        
    }
    
    func blockUserRequest(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "userBlockedId" : userTarget["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.blockUserRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    print(response.data)
                    let data = JSON(response.data ?? Data())
                    print("BlockRequest : ",data)
                    if data["message"].exists() {
                        // self.sendRequestButton.setTitle(Localization("Cancel_Request"), for: .normal)
                        print("error")
                    }else{
                        let alert = UIAlertController(title: Localization("Profile"), message: Localization("userBlock"), preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: {(okay) in
                            
                            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                                self.CustomPoUp.alpha = 0
                            }) { (complete) in
                                self.CustomPoUp.isHidden = true
                                self.CustomPoUp.alpha = 1
                            }
                        })
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    //self.jokes = data
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    func getSendedFriendRequest(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.sendedFriendRequestList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    
                    let data = JSON(response.data ?? Data())
                    
                    print("sendedFriendRequestList : ",data)
                    if data.arrayObject != nil {
                        if data.arrayObject?.count != 0 {
                            for i in 0...((data.arrayObject?.count )! - 1) {
                                if (data[i]["receiverId"].stringValue == self.userTarget["_id"].stringValue ) &&  (data[i]["FriendRequestIsAccepted"].boolValue == false ){
                        self.sendRequestButton.setTitle(Localization("Cancel_Request"), for: .normal)
                        self.buttonSendFake.setTitle(Localization("Cancel_Request"), for: .normal)
                                }
                            }
                        }
                    }
                    
                    
                    //self.jokes = data
                  
                }else{
                   
                }
                
            }
            
        }catch {
            
        }
    }
    func getJokes(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseString { response in
                if response.data != nil {
                    
                    if var stringToParse = response.result.value {
                    if stringToParse != "" {
                    print("stringToParse : ",stringToParse)
                    stringToParse.removeFirst()
                    stringToParse.removeLast()
                    stringToParse = stringToParse.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                    
                    let data = JSON(stringLiteral: response.result.value!)
                    
                    print("JokesResponse : ",data)
                    if data != JSON.null {
                        
                        let parsedString = stringToParse.data(using: String.Encoding.utf8, allowLossyConversion: false)
                        do{
                            
                            self.JokesJSON = try JSONDecoder().decode([Jokes].self, from: parsedString!)
                        print(self.JokesJSON.count)
                            self.jokes = []
                        if self.JokesJSON.count != 0 {
                            for i in 0...((self.JokesJSON.count) - 1) {
                            self.jokes.append(self.JokesJSON[i].joke)
                        }
                            self.jokesTableView.reloadData()
                            self.EditJokeTable.reloadData()
                        }
                        }catch{
                            print("error converting",error.localizedDescription)
                        }
                    }
                    
                    }
                    }
                    if self.jokes.count == 0 {
                        self.hidejokeTable(hide: false)
                        self.jokesTableView.reloadData()
                        self.EditJokeTable.reloadData()
                    }
                    
                    print("JOKESSSS: ",self.jokes)
                    //self.jokes = data
                    self.getVideo(who: "me")
                        
                }else{
                    self.getVideo(who: "me")
                }
                
            }
            
        }catch {
            
        }
        
    }
    func getVideo(who:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" :  who == "me" ? a["_id"].stringValue : self.userTarget["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getActualityUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    
                    //print("REQU1 : ",data)
                    if data.arrayObject != nil {
                        // self.JokesJSON = data
                        if data.arrayObject?.count != 0 {
                            for i in 0...((data.arrayObject?.count)! - 1) {
                                self.Acutality.append(VideoYLYL(id: data[i]["_id"].stringValue, videoFirstUserId: User(id: data[i]["videoFirstUserId"]["_id"].stringValue, userFirstName: data[i]["videoFirstUserId"]["userFirstName"].stringValue, userLastName: data[i]["videoFirstUserId"]["userLastName"].stringValue, userAge: data[i]["videoFirstUserId"]["userAge"].stringValue, userEmail: data[i]["videoFirstUserId"]["userEmail"].stringValue, token: data[i]["videoFirstUserId"]["userRefreshToken"].stringValue), videoSecondUserId: User(id: data[i]["videoSecondUserId"]["_id"].stringValue, userFirstName: data[i]["videoSecondUserId"]["userFirstName"].stringValue, userLastName: data[i]["videoSecondUserId"]["userLastName"].stringValue, userAge: data[i]["videoSecondUserId"]["userAge"].stringValue, userEmail: data[i]["videoSecondUserId"]["userEmail"].stringValue, token: data[i]["videoSecondUserId"]["userRefreshToken"].stringValue), videoroomId: Room(id: "", roomNumber: "", firstUserId: "", secondUserId: ""), videoDate: data[i]["videoDate"].stringValue, pathVideo: data[i]["pathVideo"].stringValue, commentList: []))
                                self.Acutality[i].getVideoFirstUser().setProfileImage(data[i]["videoFirstUserId"]["userImageURL"].stringValue)
                                self.Acutality[i].getVideoSecondUser().setProfileImage(data[i]["videoSecondUserId"]["userImageURL"].stringValue)
                            }
                          
                        }
                        if self.Acutality.count == 0 {
                            self.hideVideoTable(hide: false)
                        }
                        if self.jokes.count == 0 {
                            self.hidejokeTable(hide: false)
                        }
                        self.battlesCollection.reloadData()
                        if who == "friend" {
                            self.EditVideosBTN.isHidden = true
                            self.EditVideosBTNImage.isHidden = true
                            
                        }
                    }
                    
                    //self.jokes = data
                    
                   // self.tableView.reloadData()
                }else{
                    // self.defaultMessage.isHidden = false
                    //self.tableView.reloadData()
                    //MARK:TODO view on the top of the tableview
                }
                
            }
            
        }catch {
            
        }
    }
    func requestAction(_ button: UIButton!) {
        print("work")
        if button.title(for: .normal) == Localization("Send_Request") {
            self.sendFriendRequest()
        }else if  button.title(for: .normal) == Localization("Cancel_Request"){
            self.cancelFriendRequest()
        }else {
            //self.goToMessage()
            //self.visualBlurThing.isHidden = false
            
        }
    }
    @IBAction func unfriendAction(_ sender: UIButton) {
       let text = self.userTarget["userName"].stringValue.uppercased() + " ?"
        self.isBlockOrUnfriend = false
        self.UnfriendBlockCalque.isHidden = false
        let font = UIFont.init(name: "Roboto-Bold", size: 18.0)
        let attributes = [NSAttributedString.Key.font : font]
        let attributedQuote = NSAttributedString(string: text, attributes: attributes as [NSAttributedString.Key : Any])
        let textBlock = NSAttributedString(string: Localization("UnfriendUserText"))
        self.userUnfriendedAnimation.text = Localization("User_Unfriended")
        let res = NSMutableAttributedString()
        res.append(textBlock)
        res.append(attributedQuote)
          self.textPoUp.attributedText = res
        self.CustomPoUp.alpha = 0
        self.CustomPoUp.isHidden = false
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomPoUp.alpha = 1
        }) { (verif) in
            
        }
    }
    @IBAction func blockAction(_ sender: UIButton) {
       self.isBlockOrUnfriend = true
        let text = self.userTarget["userName"].stringValue.uppercased() + " ?"
       
        let font = UIFont.init(name: "Roboto-Bold", size: 18.0)
        let attributes = [NSAttributedString.Key.font : font]
        let attributedQuote = NSAttributedString(string: text, attributes: attributes as [NSAttributedString.Key : Any])
        let textBlock = NSAttributedString(string: Localization("BlockUserText"))
        self.userUnfriendedAnimation.text = Localization("User_Blocked")
        let res = NSMutableAttributedString()
        res.append(textBlock)
        res.append(attributedQuote)
        self.textPoUp.attributedText = res
        self.UnfriendBlockCalque.isHidden = false
        self.CustomPoUp.alpha = 0
        self.CustomPoUp.isHidden = false
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomPoUp.alpha = 1
        }) { (verif) in
            
        }
    }
    var isBlockOrUnfriend = false
    func reportAction() {
        print("report this")
        self.UnfriendBlockCalque.isHidden = false
        self.CustomReport.alpha = 0
        self.CustomReport.isHidden = false
        tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.closeShowReport(_:)))
        self.UnfriendBlockCalque.addGestureRecognizer(tapgesture)
        self.UnfriendBlockCalque.isUserInteractionEnabled = true
        self.UnfriendBlockCalque.isHidden = false
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomReport.alpha = 1
        }) { (verif) in
              self.confirmReportBTN.isHidden = false
        }
    }
    func goToMessage(){
        var haveChatRoom : Bool = false
        if SocketIOManager.sharedInstance.getChatRooms().count != 0 {
            for i in 0...(SocketIOManager.sharedInstance.getChatRooms().count - 1) {
                if SocketIOManager.sharedInstance.getChatRooms()[i].getReceiverId() == userTarget["_id"].stringValue || SocketIOManager.sharedInstance.getChatRooms()[i].getUserId() == userTarget["_id"].stringValue{
                    haveChatRoom = true
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatFriend") as! ChatFriendController
                    
                    vc.RoomQ = ChatRoom(id: SocketIOManager.sharedInstance.getChatRooms()[i].getId(), receiverId: "", userId: "")
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
        if haveChatRoom == false {
            SocketIOManager.sharedInstance.getCreatedChatRoom { (roomChat) in
                DispatchQueue.main.async(execute: {
                    let tempRoom = roomChat[0]
                    let roomToPass : ChatRoom = ChatRoom(id: tempRoom["_id"].stringValue, receiverId: tempRoom["receiverId"].stringValue, userId: tempRoom["userId"].stringValue)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatFriend") as! ChatFriendController
                    vc.RoomQ = roomToPass
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                })
            }
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.addRoom(userId: a["_id"].stringValue, FriendId: userTarget["_id"].stringValue)
            }catch{
        
            }
        }
    }
    func getFriends(who: String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : who != "friend" ? a["_id"].stringValue : self.userTarget["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.friendList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    
                    self.friends = []
                    let data = JSON(response.data ?? Data())
                    print("friends :",data)
                    if data["users"].arrayObject != nil {
                        //self.friends  = data["users"]
                        if data["users"].arrayObject?.count != 0 {
                            self.FriendJSON = data["users"]
                            for i in 0...((data["users"].arrayObject?.count)! - 1) {
                                
                                let user = User(id: data["users"][i]["_id"].stringValue, userFirstName: data["users"][i]["userFirstName"].stringValue, userLastName: data["users"][i]["userLastName"].stringValue, userAge: data["users"][i]["userAge"].stringValue, userEmail: data["users"][i]["userEmail"].stringValue, token: data["users"][i]["token"].stringValue,imageProfileUrl : data["users"][i]["userImageURL"].stringValue,username:data["users"][i]["userName"].stringValue )
                                if self.verifyLinkUser(user: user)  {
                                    if user.getId() != a["_id"].stringValue {
                                        self.friends.append(user)
                                    }
                                }
                                
                            }
                            if self.friends.count != 0 {
                                self.friendsCollection.isHidden = false
                                self.FriendsButton.isHidden = true
                            }else{
                                self.friendsCollection.isHidden = true
                                //self.FriendsButton.setTitle("+", for: .normal)
                                self.FriendsButton.isHidden = true
                              /*  DispatchQueue.main.async {
                                    self.FriendsButton.size = CGSize(width: 41, height: 41)
                                    self.FriendsButton.layer.cornerRadius = self.FriendsButton.frame.height / 2
                                    self.FriendsButton.layer.masksToBounds = true
                                    self.FriendsButton.setNeedsDisplay()
                                } */
                            }
                            self.friendsCollection.reloadData()
                        }else{
                            if self.friends.count != 0 {
                                self.friendsCollection.isHidden = false
                                self.FriendsButton.isHidden = true
                                self.friendsCollection.reloadData()
                            }else{
                                self.friendsCollection.isHidden = true
                                //self.FriendsButton.setTitle("+", for: .normal)
                                self.FriendsButton.isHidden = true
                               /*  DispatchQueue.main.async {
                                     self.FriendsButton.size = CGSize(width: 41, height: 41)
                                    self.FriendsButton.layer.cornerRadius = self.FriendsButton.frame.height / 2
                                    self.FriendsButton.layer.masksToBounds = true
                                    self.FriendsButton.setNeedsDisplay()
                                } */
                            }
                        }
                    }else {
                    //print("Friends : ",data)
                     self.friendsCollection.isHidden = true
                   // self.FriendsButton.setTitle("+", for: .normal)
                        self.FriendsButton.isHidden = true
                  /*  DispatchQueue.main.async {
                         self.FriendsButton.size = CGSize(width: 41, height: 41)
                        self.FriendsButton.layer.cornerRadius = self.FriendsButton.frame.height / 2
                        self.FriendsButton.layer.masksToBounds = true
                        self.FriendsButton.setNeedsDisplay()
                    } */
                    //self.jokes = data
                    }
                }else{
                    self.friendsCollection.reloadData()
                    self.friendsCollection.isHidden = true
                   // self.FriendsButton.setTitle("+", for: .normal)
                    self.FriendsButton.isHidden = true
                 /*   DispatchQueue.main.async {
                        self.FriendsButton.size = CGSize(width: 41, height: 41)

                        self.FriendsButton.layer.cornerRadius = self.FriendsButton.frame.height / 2
                        self.FriendsButton.layer.masksToBounds = true
                        self.FriendsButton.setNeedsDisplay()
                    } */
                    
                }
                
            }
            
        }catch {
            
        }
    }
    func getFriendsChecker(userId: String,index:IndexPath){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : userId
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.friendList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    print("friends :",data)
                    var tempFriends : [User] = []
                    if data["users"].arrayObject != nil {
                        //self.friends  = data["users"]
                        if data["users"].arrayObject?.count != 0 {
                            for i in 0...((data["users"].arrayObject?.count)! - 1) {
                                let user = User(id: data["users"][i]["_id"].stringValue, userFirstName: data["users"][i]["userFirstName"].stringValue, userLastName: data["users"][i]["userLastName"].stringValue, userAge: data["users"][i]["userAge"].stringValue, userEmail: data["users"][i]["userEmail"].stringValue, token: data["users"][i]["token"].stringValue,imageProfileUrl : data["users"][i]["userImageURL"].stringValue,username:data["users"][i]["userName"].stringValue )
                                 if self.verifyLinkUser(user: user)  {
                                    if user.getId() != a["_id"].stringValue {
                                    tempFriends.append(user)
                                        }
                                }
                                
                            }
                            if tempFriends.count != 0 {
                                var verif = false
                                for u in tempFriends {
                                    if u.getId() == userId {
                                        verif = true
                                    }
                                }
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                                vc.userTarget = self.FriendJSON[index.row]
                                if verif {
                                   
                                    
                                    vc.ViewType = "Friend"
                                }else{
                                    vc.ViewType = "Visitor"
                                }
                               self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                //who : not frien
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                                vc.userTarget = self.FriendJSON[index.row]
                                    vc.ViewType = "Visitor"
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }else{
                            if tempFriends.count != 0 {
                                var verif = false
                                for u in tempFriends {
                                    if u.getId() == userId {
                                        verif = true
                                    }
                                }
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                                vc.userTarget = self.FriendJSON[index.row]
                                if verif {
                                    
                                    
                                    vc.ViewType = "Friend"
                                }else{
                                    vc.ViewType = "Visitor"
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                            }else{
                                //who : not frien
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                                vc.userTarget = self.FriendJSON[index.row]
                                vc.ViewType = "Visitor"
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                        }
                    }else {
                      // who : not friend
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                        vc.userTarget = self.FriendJSON[index.row]
                        vc.ViewType = "Visitor"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }else{
                    // who : not friend
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
                    vc.userTarget = self.FriendJSON[index.row]
                    vc.ViewType = "Visitor"
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            }
            
        }catch {
            
        }
    }
    func verifyLinkUser(user:User) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a =  JSON(data: dataFromString!)
        
        if a["listBlockedUsers"].arrayObject?.count != 0 {
            for i in 0...((a["listBlockedUsers"].arrayObject!.count) - 1) {
                if a["listBlockedUsers"][i].stringValue == user.getId() {
                    return false
                    
                }
            }
        }
        if a["listBlockerUsers"].arrayObject?.count != 0 {
            for i in 0...((a["listBlockerUsers"].arrayObject!.count) - 1) {
                if a["listBlockerUsers"][i].stringValue == user.getId() {
                    return false
                    
                }
            }
        }
        
        return true
    }
func deleteSomeVideos(videos:[VideoYLYL]) {
        var  dict =  "["
        for vid in videos {
            dict = dict + "{\"videoId\" : \"\(vid.getVideoId())\"},"
        }
        dict.removeLast()
        dict = dict + "]"
        
        print("Dict :",dict)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "videos" : dict
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.deleteVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("deletation  : ",data)
                
                self.battlesCollection.performBatchUpdates({
                    self.Acutality.remove(at: self.itemToRemove)
                    let indexPath = IndexPath(row: self.itemToRemove, section: 0)
                    self.battlesCollection.deleteItems(at: [indexPath])
                }, completion: { (verif) in
                    print("delete Completed")
                })
            }
        }catch {
        }
    }
   @IBAction func longPressGestureRecognized(_ gestureRecognizer: UIGestureRecognizer) {
        
        let longpress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longpress.state
        let locationInView = longpress.location(in: self.EditJokeTable)
        var indexPath = self.EditJokeTable.indexPathForRow(at: locationInView)
        
        switch state {
        case .began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath
                let cell = self.EditJokeTable.cellForRow(at: indexPath!)!
                My.cellSnapShot = snapshopOfCell(inputView: cell)
                var center = cell.center
                My.cellSnapShot?.center = center
                My.cellSnapShot?.alpha = 0.0
                self.EditJokeTable.addSubview(My.cellSnapShot!)
                
                UIView.animate(withDuration: 0.25, animations: {
                    center.y = locationInView.y
                    My.cellSnapShot?.center = center
                    My.cellSnapShot?.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    My.cellSnapShot?.alpha = 0.98
                    cell.alpha = 0.0
                }, completion: { (finished) -> Void in
                    if finished {
                        cell.isHidden = true
                    }
                })
            }
            
        case .changed:
            var center = My.cellSnapShot?.center
            center?.y = locationInView.y
            My.cellSnapShot?.center = center!
            if ((indexPath != nil) && (indexPath != Path.initialIndexPath)) {
                self.jokes.swapAt((indexPath?.row)!, (Path.initialIndexPath?.row)!)
                let cell = self.EditJokeTable.cellForRow(at: indexPath!)!
                let cell2 = self.EditJokeTable.cellForRow(at: Path.initialIndexPath!)!
                let swap = (cell.viewWithTag(1) as! UILabel).text
                (cell.viewWithTag(1) as! UILabel).text = (cell2.viewWithTag(1) as! UILabel).text
                (cell2.viewWithTag(1) as! UILabel).text  = swap
                //swap(&self.wayPoints[(indexPath?.row)!], &self.wayPoints[(Path.initialIndexPath?.row)!])
                self.EditJokeTable.moveRow(at: Path.initialIndexPath!, to: indexPath!)
                Path.initialIndexPath = indexPath
            }
            
        default:
            let cell = self.EditJokeTable.cellForRow(at: Path.initialIndexPath!)!
            cell.isHidden = false
            cell.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                My.cellSnapShot?.center = cell.center
                My.cellSnapShot?.transform = .identity
                My.cellSnapShot?.alpha = 0.0
                cell.alpha = 1.0
            }, completion: { (finished) -> Void in
                if finished {
                    Path.initialIndexPath = nil
                    My.cellSnapShot?.removeFromSuperview()
                    My.cellSnapShot = nil
                }
            })
        }
    }
    
    func snapshopOfCell(inputView: UIView) -> UIView {
        
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    func saveJokesIntoDataBase(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let res = self.constructJson(json: self.jokes)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.EditJokeTable.reloadData()
                    self.jokesTableView.reloadData()
                }else{
                }
                
            }
            
        }catch {
            
        }
    }
    func constructJson(json:[String]) -> JSON{
        var result = "["
        for j in json {
            result = result + "{\"jokeId\":\"\",\"joke\":\"" + j + "\",\"userId\":\"\"},"
        }
        result.removeLast()
        result = result + "]"
        return JSON(stringLiteral: result)
    }
    // VIP ACCOUNT
    @IBOutlet weak var visualBlurThing: UIVisualEffectView!
    @IBOutlet weak var VIPBackgroundContainer : UIView!
    @IBOutlet weak var VIPLabel : UILabel!
    @IBOutlet weak var backButtonImage : UIButton!
    @IBOutlet weak var plusBTN : UIButton!
    @IBOutlet weak var VIPContainer : UIView!
    @IBOutlet weak var MaleLBL: EdgeInsetLabel!
    
    @IBOutlet weak var FemaleLBL: EdgeInsetLabel!
    
    @IBOutlet weak var OtherLBL: EdgeInsetLabel!
    
    @IBOutlet weak var ageSlider: DoubleSlider!
    
    @IBOutlet weak var minAge: UILabel!
    
    @IBOutlet weak var maxAge: UILabel!
    @IBOutlet weak var HeightContainer: NSLayoutConstraint!
    @IBOutlet weak var HeightVIPBuy: NSLayoutConstraint!
    @IBOutlet weak var GenderSlider: VerticalSlider!
    @IBOutlet weak var VIPBuyContainer: UIView!
    @IBOutlet weak var BuyBlurred: UIButton!
    @IBOutlet weak var LineToAnimate: UIImageView!
    @IBOutlet weak var ViewAgeRange: UIView!
    
    @IBOutlet weak var ViewGender: UIView!
    @IBOutlet weak var weekContainer: UIView!
    
    @IBOutlet weak var monthContainer: UIView!
    
    @IBOutlet weak var yearContainer: UIView!
    @IBOutlet weak var ViewContainerWorldLaugh: UIView!
    let ZonzWeek = "com.regystone.zonzay.PlusWeek"
    let ZonzMonth = "com.regystone.zonzay.PlusMonth"
    let ZonzYear = "com.regystone.zonzay.PlusYear"
    var products : [SKProduct] = []
   
     var animateView = true
    //Main in viewDidLoad
    func configureVIP(){
        self.HeightContainer.constant = 286
        self.VIPBuyContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.HeightVIPBuy.constant = 0
        self.visualBlurThing.isHidden = true
        self.VIPBackgroundContainer.isHidden = true
        self.ageSlider.isUserInteractionEnabled = false
        self.ageSlider.valueChangedSlider = self
        self.GenderSlider.isUserInteractionEnabled = false
        self.GenderSlider.slider.addTarget(self, action: #selector(self.sliderDidChange(_:)), for: [.touchUpInside,.touchUpOutside, .valueChanged, .touchCancel])
        self.GenderSlider.isContinuous = true
        let ab = UserDefaults.standard.value(forKey: "Subscription") as! String
        if ab == "true"{
            
            self.animateView = false
            
            if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                //GenderBTN.setTitle(UserDefaults.standard.value(forKey: "MatchChoice") as? String, for: .normal)
                switch UserDefaults.standard.value(forKey: "MatchChoice") as? String {
                case "Male" :
                    
                    self.markLabel(label: MaleLBL)
                case "Female" :
                    self.markLabel(label: FemaleLBL)
                case "Other" :
                    self.markLabel(label: OtherLBL)
                default :
                    break
                }
                minAge.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxAge.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                ageSlider.lowerValue =  Double(minAge.text!)!
                ageSlider.upperValue = Double(maxAge.text!)!
                self.permuteColorObjects()
                //hideContainers(false)
            }
            
        }else{
            
        }
        PKIAPHandler.shared.setProductIds(ids: [ZonzWeek,ZonzMonth,ZonzYear])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([ZonzWeek,ZonzMonth,ZonzYear]))
        
        //PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            
            for product in skProducts {
                print(product)
            }
        }
        configureGestures()
        configureBlur()
    }
    func configureBlur(){
        self.ViewContainerWorldLaugh.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.ViewContainerWorldLaugh.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
        self.BuyBlurred.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.BuyBlurred.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.95))
      
    }
    func permuteColorObjects(){
        self.ViewGender.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ViewAgeRange.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.thumbTintColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.GenderSlider.thumbTintColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.isUserInteractionEnabled = true
        self.GenderSlider.isUserInteractionEnabled = true
    }
    func showVIPContainer(){
        self.VIPLabel.isHidden = true
        self.backButtonImage.isHidden = true
        self.plusBTN.isHidden = true
        self.VIPContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.VIPContainer.transform = CGAffineTransform(scaleX: 0, y: 0)
        // self.VIPContainer.size = CGSize.zero
        VIPContainer.alpha = 0
        self.visualBlurThing.isHidden = false
        VIPContainer.isHidden = false
        UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
            self.VIPBackgroundContainer.isHidden = false
            self.VIPContainer.alpha = 1.0
            //self.VIPContainer.size = CGSize(width: width, height: heigh)
            self.VIPContainer.transform = CGAffineTransform.identity
        }) { _ in
            self.VIPLabel.isHidden = false
            self.backButtonImage.isHidden = false
            self.plusBTN.isHidden = false
            
        }
    }
    @IBAction func BackVIPAction(_ sender: UIButton) {
        if (UserDefaults.standard.value(forKey: "Subscription") as! String) == "true" {
            if MaleLBL.textColor == UIColor.white {
                UserDefaults.standard.setValue("Male", forKey: "MatchChoice")
            }else if FemaleLBL.textColor == UIColor.white {
                UserDefaults.standard.setValue("Female", forKey: "MatchChoice")
            }else if OtherLBL.textColor == UIColor.white {
                UserDefaults.standard.setValue("Other", forKey: "MatchChoice")
            }
            //UserDefaults.standard.setValue(self.GenderBTN.title(for: .normal)!, forKey: "MatchChoice")
            UserDefaults.standard.setValue(self.minAge.text!, forKey: "minChoice")
            UserDefaults.standard.setValue(self.maxAge.text!, forKey: "maxChoice")
            UserDefaults.standard.synchronize()
        }
        UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
            //self.BuyContainer.alpha  = 0
            self.VIPContainer.alpha = 0
            
        }) { _ in
            //self.BuyContainer.isHidden = true
            self.VIPContainer.isHidden = true
            self.visualBlurThing.isHidden = true
            self.VIPBackgroundContainer.isHidden = true
            self.VIPBuyContainer.isHidden = true
            self.VIPBuyContainer.alpha = 0
            self.HeightContainer.constant = 286
            self.animateView = true
        }
    }
    @IBAction func plusButtonVIP(_ sender: UIButton) {
        self.VIPBuyContainer.isHidden = false
        self.VIPBuyContainer.layoutIfNeeded()
        self.HeightVIPBuy.constant = 340
        animateView = false
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [.curveEaseIn], animations: {
            self.VIPBuyContainer.alpha = 1
            self.HeightContainer.constant = 340
            //self.HeightVIPBuy.constant = 340
            self.VIPBuyContainer.layoutIfNeeded()
            //self.BuyContainer.alpha = 0.92
        }) { _ in
            
        }
    }
    func configureGenderLabels(){
        let tapGestureMale = UITapGestureRecognizer(target: self, action: #selector(self.MaleLBLTapped(_:)))
        let tapGestureFemale = UITapGestureRecognizer(target: self, action: #selector(self.FemaleLBLTapped(_:)))
        let tapGestureOther = UITapGestureRecognizer(target: self, action: #selector(self.OtherLBLTapped(_:)))
        MaleLBL.isUserInteractionEnabled = true
        FemaleLBL.isUserInteractionEnabled = true
        OtherLBL.isUserInteractionEnabled = true
        MaleLBL.addGestureRecognizer(tapGestureMale)
        FemaleLBL.addGestureRecognizer(tapGestureFemale)
        OtherLBL.addGestureRecognizer(tapGestureOther)
        
    }
    @objc func MaleLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: MaleLBL)
    }
    @objc func FemaleLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: FemaleLBL)
    }
    @objc func OtherLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: OtherLBL)
    }
    func configureGestures(){
        let tapgestureWeek = UITapGestureRecognizer(target: self, action: #selector(self.weekAnimate(_:)))
        tapgestureWeek.numberOfTapsRequired = 1
        self.weekContainer.addGestureRecognizer(tapgestureWeek)
        self.weekContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.weekContainer.layer.borderWidth = 1
        self.weekContainer.isUserInteractionEnabled = true
        self.weekContainer.tag = 0
        self.monthContainer.tag = 1
        self.yearContainer.tag = 2
        let tapgestureMonth = UITapGestureRecognizer(target: self, action: #selector(self.monthAnimate(_:)))
        tapgestureMonth.numberOfTapsRequired = 1
        self.monthContainer.addGestureRecognizer(tapgestureMonth)
        self.monthContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.monthContainer.layer.borderWidth = 1
        self.monthContainer.isUserInteractionEnabled = true
        let tapgestureYear = UITapGestureRecognizer(target: self, action: #selector(self.yearAnimate(_:)))
        tapgestureYear.numberOfTapsRequired = 1
        self.yearContainer.addGestureRecognizer(tapgestureYear)
        self.yearContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.yearContainer.layer.borderWidth = 1
        self.yearContainer.isUserInteractionEnabled = true
    }
    
    
  //
}

extension UIImage {
    func imageWithBorder(width: CGFloat, color: UIColor) -> UIImage? {
        let square = CGSize(width: min(size.width, size.height) + width * 2, height: min(size.width, size.height) + width * 2)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .center
        imageView.image = self
        imageView.layer.borderWidth = width
        imageView.layer.borderColor = color.cgColor
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    

    
}
struct My {
    static var cellSnapShot: UIView? = nil
}

struct Path {
    static var initialIndexPath: IndexPath? = nil
}
extension ProfileTestTable : DoubleSliderValueChanged{
    func valueChanged(forMin value: Double) {
        print("MIN",value)
        minAge.text = String(Int(value))
        
    }
    func valueChanged(forMax value: Double) {
        print("MAX",value)
        maxAge.text = String(Int(value))
    }
    @objc func sliderDidChange(_ slider: UISlider) {
        print("hello")
        print(slider.value)
        if Float(0)...Float(0.25) ~= slider.value   {
            slider.value = 0.08
            self.unmarkLabels()
            self.markLabel(label: self.OtherLBL)
        }
        if Float(0.26)...Float(0.75) ~= slider.value   {
            slider.value = 0.5
            self.unmarkLabels()
            self.markLabel(label: self.FemaleLBL)
        }
        if Float(0.76)...Float(1) ~= slider.value   {
            slider.value = 0.92
            self.unmarkLabels()
            self.markLabel(label: self.MaleLBL)
        }
    }
    func markLabel(label:EdgeInsetLabel) {
        label.layer.cornerRadius = 4.25
        //label.backgroundColor = .black
        label.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
        label.layer.masksToBounds = true
        label.textColor = UIColor.white
        label.leftTextInset = 10
        label.rightTextInset = 10
        label.topTextInset = 5
        label.bottomTextInset = 5
        // label.layoutIfNeeded()
        switch label {
        case MaleLBL:
            GenderSlider.value = 0.92
            
            self.LineToAnimate.snp.remakeConstraints { (const) in
                const.centerY.equalTo(MaleLBL)
                const.right.equalTo(MaleLBL.snp.left).offset(-1.5)
                const.width.equalTo(28)
                const.height.equalTo(4)
            }
        // self.MaleLBL.size = CGSize(width: self.MaleLBL.size.width, height: 20)
        case FemaleLBL:
            GenderSlider.value = 0.5
            //self.LineToAnimate.frame = CGRect(x: self.LineToAnimate.frame.origin.x, y: self.FemaleLBL.centerY, width: self.LineToAnimate.frame.width, height: self.LineToAnimate.frame.height)
            
            self.LineToAnimate.snp.remakeConstraints { (const) in
                const.centerY.equalTo(FemaleLBL)
                const.right.equalTo(FemaleLBL.snp.left).offset(-1.5)
                const.width.equalTo(28)
                const.height.equalTo(4)
            }
        case OtherLBL:
            GenderSlider.value = 0.08
            
            self.LineToAnimate.snp.remakeConstraints { (const) in
                const.centerY.equalTo(OtherLBL)
                const.right.equalTo(OtherLBL.snp.left).offset(-1.5)
                const.width.equalTo(28)
                const.height.equalTo(4)
            }
        default:
            break
        }
    }
    func unmarkLabels(){
        MaleLBL.backgroundColor = UIColor.white
        MaleLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        MaleLBL.leftTextInset = 0
        MaleLBL.rightTextInset = 0
        MaleLBL.topTextInset = 0
        MaleLBL.bottomTextInset = 0
        /// ** //
        FemaleLBL.backgroundColor = UIColor.white
        FemaleLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        FemaleLBL.leftTextInset = 0
        FemaleLBL.rightTextInset = 0
        FemaleLBL.topTextInset = 0
        FemaleLBL.bottomTextInset = 0
        /// ** //
        OtherLBL.backgroundColor = UIColor.white
        OtherLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        OtherLBL.leftTextInset = 0
        OtherLBL.rightTextInset = 0
        OtherLBL.topTextInset = 0
        OtherLBL.bottomTextInset = 0
        
    }
}
extension ProfileTestTable {
    @objc func weekAnimate(_ sender: UITapGestureRecognizer) {
        
        if weekContainer.tag == 0 {
            deAnimateContainers()
            weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
        
    }
    @IBAction func weekButton (_ sender: UIButton){
        if weekContainer.tag == 0 {
            deAnimateContainers()
            weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    
    func deAnimateContainers(){
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
            self.weekContainer.transform = CGAffineTransform.identity
            self.weekContainer.backgroundColor = UIColor.white
            self.weekContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.weekContainer.tag = 0
            self.monthContainer.transform = CGAffineTransform.identity
            self.monthContainer.backgroundColor = UIColor.white
            self.monthContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.monthContainer.tag = 1
            self.yearContainer.transform = CGAffineTransform.identity
            self.yearContainer.backgroundColor = UIColor.white
            self.yearContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.yearContainer.tag = 2
        }) { _ in
            
        }
    }
    @objc func monthAnimate(_ sender: UITapGestureRecognizer) {
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @IBAction func monthButton (_ sender: UIButton){
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @objc func yearAnimate(_ sender: UITapGestureRecognizer) {
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
    @IBAction func yearButton (_ sender: UIButton){
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
}
extension ProfileTestTable : SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
}
extension ProfileTestTable {
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
}
extension ProfileTestTable {
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
       
            print(String(format: "%@ is selected.\n", radioButton.selected()!.titleLabel!.text!));
if let font = UIFont(name: "Roboto-Medium", size: 19.0) {
    print("True")
    radioButton.selected()!.titleLabel?.font = font
}
        if radioButton.selected()! == ChoiceFour {
            self.reportOtherTV.text = ""
            self.reportOtherTV.isUserInteractionEnabled = true
        }else{
            self.reportOtherTV.text = ""
            self.reportOtherTV.isUserInteractionEnabled = false
        }

    }
}
extension ProfileTestTable {
    func verifReport() -> Int{
        switch self.ChoiceOne.selected() {
        case ChoiceOne:
            return 0
            case ChoiceTwo:
            return 1
            case ChoiceThree:
            return 2
            case ChoiceFour:
                if (self.reportOtherTV.text?.count)! > 10 {
                    return 3
                }else{
                    return -1
            }
        default:
            return -2
        }
    }
    @IBAction func ConfirmReportAction(_ sender: UIButton){
        sender.isUserInteractionEnabled = false
        reportToDataBase { (finish) in
            sender.isUserInteractionEnabled = true
        }
    }
    func reportToDataBase(completionHandler :  @escaping (_ messageInfo : Bool) -> Void){
        
        let choice = verifReport()
        if choice >= 0 {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)

            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "reportedUserId" : self.userTarget["_id"].stringValue,
                "report" : choice != 3 ? (self.ChoiceOne.selected()?.titleLabel?.text)! : self.reportOtherTV.text!,
                "video" : ""
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            self.activityIndictaor.isHidden = false
            self.activityIndictaor.startAnimating()
            Alamofire.request(ScriptBase.sharedInstance.reportUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    self.activityIndictaor.stopAnimating()
                    self.activityIndictaor.isHidden = true
                    if data["message"].stringValue == "Reporting Email was sent" {
                        //afficher une alert OK
                        let alert = UIAlertController(title: Localization("Profile"), message: Localization("ReportUser"), preferredStyle: .alert)
                        let alertAction =  UIAlertAction(title: Localization("OK"), style: .default, handler: { (done) in
                self.UnfriendBlockCalque.removeGestureRecognizer(self.tapgesture)
                              self.confirmReportBTN.isHidden = true
                            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                                self.CustomReport.alpha = 0
                            }) { (verif) in
                                self.CustomReport.isHidden = true
                                self.CustomReport.alpha = 1
                                self.UnfriendBlockCalque.isHidden = true
                                self.reportOtherTV.text = ""
                                completionHandler(true)
                                
                            }
                        })
                       
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        //afficher error
                        let alert = UIAlertController(title: Localization("Profile"), message: data["message"].stringValue, preferredStyle: .alert)
                        let alertAction =  UIAlertAction(title: Localization("OK"), style: .default, handler: { (done) in
                             completionHandler(true)
                        })
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    
                }
                
            }
        }catch {
            
        }
        }else{
            if choice == -1 {
                //more specific
                let alert = UIAlertController(title: Localization("Profile"), message: Localization("ReportUserNE"), preferredStyle: .alert)
                let alertAction =  UIAlertAction(title: Localization("OK"), style: .default, handler: { (done) in
                     completionHandler(true)
                })
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
                
            }
            
            
        }
    }
    @objc func ChangePictureAction(_ button: UITapGestureRecognizer){
        let pickerController = DKImagePickerController()
        pickerController.singleSelect = true
        pickerController.autoCloseOnSingleSelect = false
        pickerController.maxSelectableCount = 1
        pickerController.assetType = .allPhotos
        pickerController.allowMultipleTypes = false
        pickerController.allowsLandscape = false
        pickerController.showsCancelButton = true
        pickerController.isModalInPopover = false
        pickerController.didSelectAssets = {(assets:[DKAsset]) in
            let asset = assets.first
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            asset?.fetchOriginalImage(completeBlock: { (image, info) in
            
                let img = self.fixOrientation(img: image!)
                
                let visionImage = VisionImage(image: img)
                //let metadata = VisionImageMetadata()
                           //metadata.orientation = .rightTop
               // metadata.orientation = image?.imageOrientation
                   //       visionImage.metadata = metadata
                
                let openCvWrapper = OpenCVWrapper()
                let resultCode = openCvWrapper.isThisWorking(img)
                print("ResultCode : ",resultCode)
                if resultCode == 0 {
                    let alert = UIAlertController(title: Localization("Profile"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                         return
                       
                }else if resultCode > 1 {
                    let alert = UIAlertController(title: Localization("Profile"), message: Localization("PhotoFaceMore"), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                }else{
                    self.UserImageContainer.image = img
                    self.uploadToServer()
                }
   /*                 self.faceDetector.process(visionImage) { features, error in
                      print("ERRROROROROR: ",error)
                        print("features: ",features?.count)
                     guard let faces = features else {
                        print("no faces : ")

                        let alert = UIAlertController(title: Localization("Profile"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                             return
                           }
                        if faces.count == 0 {

                            let alert = UIAlertController(title: Localization("Profile"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                            let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                            alert.addAction(alertAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                for feature in faces {
                    print("no complete face: ")

                    if self.detectAllFeatures(feature) {
                        self.UserImageContainer.image = img
                         self.uploadToServer()
                    }else{
                        let alert = UIAlertController(title: Localization("Profile"), message: Localization("PhotoFaceNF"), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: nil)
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    }
                }
                        
                
                        
               
                }     */
            })
        }
        pickerController.didCancel = {
            print("Not Now")
            /*    DispatchQueue.main.async {
             self.view.frame = frame
             } */
            self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            print("After : ",self.view.frame)
        }
        print("Before : ",view.frame)
        self.present(pickerController, animated: true) {
            print("When : ",self.view.frame)
        }
        // self.present(pickerController, animated: true, completion: nil)
    }
    func detectAllFeatures(_ feature:VisionFace) -> Bool{
        print("Face Contours : ",feature.contour(ofType: .face)?.points)
        if let _ = feature.landmark(ofType: .leftCheek),let _ = feature.landmark(ofType: .leftEar),let _ = feature.landmark(ofType: .leftEye),let _ = feature.landmark(ofType: .mouthLeft),let _ = feature.landmark(ofType: .mouthBottom),let _ = feature.landmark(ofType: .mouthRight),let _ = feature.landmark(ofType: .noseBase),let _ = feature.landmark(ofType: .rightCheek),let _ = feature.landmark(ofType: .rightEar),let _ = feature.landmark(ofType: .rightEye)
        {
            print("postion : ",landmarkPointFrom(feature.landmark(ofType: .leftEar)!.position))
            return true
        }else{
            print("wtf")
            return false
        }
        
        
        
    }

    private func landmarkPointFrom(_ visionPoint: VisionPoint) -> CGPoint {
        return CGPoint(x: CGFloat(visionPoint.x.floatValue), y: CGFloat(visionPoint.y.floatValue))
    }
}
extension UIViewController {
    
     var wasPushed: Bool {
        guard let vc = navigationController?.viewControllers.first , vc == self else {
            return true
        }
        
        return false
    }
}
extension ProfileTestTable {
    func randomStringWithLength() -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = 15
        let date = Date()
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0...(len - 1){
            let length = UInt32(letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
            
        }
        var resultFinal = "ios" + (randomString as String) + date.description
        resultFinal = resultFinal.replacingOccurrences(of: " ", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: ":", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: "+", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: "-", with: "")
        resultFinal = resultFinal.replacingOccurrences(of: ".", with: "")
        return resultFinal
    }
    func yearsBetweenDates(startDate:Date, endDate:Date) -> Int
    {
        
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.year]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        return components.year!
        
    }
    func uploadToServer(){
        // SwiftSpinner.show("Uploading Picture...")
        let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
        let contentType = "multipart/form-data; boundary=" + boundaryConstant
        
        let mimeType = "image/jpeg"
        
        
        let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
        //.rotate(radians: Float(-(Double.pi / 2)))
         let image = self.UserImageContainer.image
        let fileData : Data? = (image!.jpegData(compressionQuality: 1))
        let requestBodyData : NSMutableData = NSMutableData()
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let key = "userId"
            requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
        }catch{
            
        }
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
        do {
            let a = try JSON(data: dataFromString!)
            let fieldName = "picture"
            
            let filename = "zonz" + self.randomStringWithLength() + ".jpg"
            requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
        }catch{
            
        }
        requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
        
        //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
        requestBodyData.append(fileData!)
        // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
        requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
        requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
        var request = URLRequest(url: uploadScriptUrl!)
        
        
        request.httpMethod = "POST"
        request.httpBody = requestBodyData as Data
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        request.httpMethod = "POST"
        request.httpBody = requestBodyData as Data
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.uploadTask(withStreamedRequest: request)
        task.resume()
    }
}
extension ProfileTestTable : URLSessionDelegate,URLSessionTaskDelegate, URLSessionDataDelegate {
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if (error != nil ) {
            SwiftSpinner.show("Unexpected Error", animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                SwiftSpinner.hide()
            }
        }else{
            let q = JSON(buffer)
            SwiftSpinner.hide()
            if q["error"].exists() == false {
                do {
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    var a = try JSON(data: dataFromString!)
                   
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                    MapleBacon.shared.cache.removeImage(forKey: a["userImageUrl"].stringValue)
                    MapleBacon.shared.cache.clearMemory()
                    a["userImageURL"].stringValue = q["userImageURL"].stringValue
                   self.UserImageContainer.setImage(with: URL(string: q["userImageURL"].stringValue), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                   
                 
                }catch{
                    
                }
            }
        }
    }
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        let uploadProgress:Float = Float(totalBytesSent) / Float(totalBytesExpectedToSend)
        
        SwiftSpinner.show(progress: Double(uploadProgress), title: "\(Int(uploadProgress * 100))% \n Uploading" )
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        buffer.append(data)
    }
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    func fixOrientation(img: UIImage) -> UIImage {
        if (img.imageOrientation == .up) {
            return img
        }
        
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale)
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return normalizedImage
    }
}
