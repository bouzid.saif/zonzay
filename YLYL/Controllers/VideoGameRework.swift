//
//  VideoGameRework.swift
//  YLYL
//
//  Created by macbook on 3/12/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import GoogleMobileVision
import FirebaseMLVision
import WebRTC
import ReplayKit
enum GameWinner {
    case masterWinner
    case masterLoser
    case draw
}
enum GameType : String{
    case Friend = "Friend"
    case ReplayFriend = "ReplayFriend"
    case ReplayMatch = "ReplayMatch"
    case RevengeFriend = "RevengeFriend"
    case RevengeMatch = "RevengeMatch"
}
class VideoGameRework: ServerUpdateDelegate ,CAAnimationDelegate,RTCPeerConnectionDelegate,WebSocketDelegate,RTCEAGLVideoViewDelegate,RTCVideoRenderer{
    func videoView(_ videoView: RTCVideoRenderer, didChangeVideoSize size: CGSize) {
        print("Video Size changed to : ",size)
    }
    
     var inDisconnected = false
    @IBOutlet weak var ContainerStart : UIView!
    @IBOutlet weak var ContainerLabelStart : UIView!
    var RoundOwner : String = ""
    @IBOutlet weak var LabelStart : UILabel!
    @IBOutlet weak var JokeTopRemoteUser : UIView!
    @IBOutlet weak var JokeTopLocalUser : UIView!
    
    @IBOutlet weak var JokeBarLimit : UIView!
    @IBOutlet weak var JokeBar : ProgressBarView!
    
    @IBOutlet weak var RemoteUserView : RTCEAGLVideoView!
    @IBOutlet weak var LocalUserView : RTCCameraPreviewView!
    @IBOutlet weak var JokeOneView : UIView!
    @IBOutlet weak var JokeTwoView : UIView!
    @IBOutlet weak var JokeThreeView : UIView!
   // @IBOutlet weak var animationViewFire : AnimationView!

    @IBOutlet weak var soundBTN : UIButton!
    var soundPlayer : AVAudioPlayer! 
   // let animationViewForWinner  = AnimationView()
    var displayLink: CADisplayLink?
    var lastTime  = Date()
    var jokesCounter : [Int] = []
   /* func createAnimationFire(){
              
                 let animation = Animation.named("1667-firework")
                 
                 animationViewFire.animation = animation
                 animationViewFire.contentMode = .scaleAspectFit
                 animationViewFire.backgroundBehavior = .pauseAndRestore
           animationViewFire.backgroundColor = UIColor.black.withAlphaComponent(0)
                 DispatchQueue.main.async {
                     self.animationViewFire.play(fromProgress: 0,
                                        toProgress: 1,
                                        loopMode: LottieLoopMode.loop,
                                        completion: { (finished) in
                                         if finished {
                                           print("Animation Complete")
                                         } else {
                                           print("Animation cancelled")
                                         }
                     })
             }
                 
             } */
    /*func createAnimation(){
        let animation = Animation.named("trophyAnimation", subdirectory: "Files")
        animationViewForWinner.animation = animation
        animationViewForWinner.contentMode = .scaleAspectFit
        animationViewForWinner.backgroundBehavior = .pauseAndRestore
        animationViewForWinner.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
        animationViewForWinner.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        self.view.addSubview(animationViewForWinner)
        let height = self.view.frame.height * 0.3
        let width = height + (height * 0.27)
        DispatchQueue.main.async {
            
            self.animationViewForWinner.frame = CGRect(x: (self.view.frame.width / 2) - (width / 2), y: ((self.view.frame.height / 1.3) - height) - 20, width: width, height: height)
            self.view.layoutIfNeeded()
            self.view.bringSubviewToFront(self.animationViewForWinner)
            self.animationViewForWinner.play(fromProgress: 0,
                               toProgress: 1,
                               loopMode: LottieLoopMode.playOnce,
                               completion: { (finished) in
                                if finished {
                                  print("Animation Complete")
                                } else {
                                  print("Animation cancelled")
                                }
            })
    }
        
    } */
    @IBAction func soundBackgroundAction(_ button: UIButton) {
        if self.soundBTN.image(for: .normal) == UIImage(named: "SoundOnIcon") {
            self.soundBTN.setImage(UIImage(named: "SoundOffIcon"), for: .normal)
        self.soundBTN.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
            self.soundBTN.transform = CGAffineTransform(scaleX: 0.5, y: 0.5).inverted()
        }) { (verif) in
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                self.soundBTN.transform = CGAffineTransform.identity
            }) { (verifTwo) in
            self.soundPlayer.volume = 0
            }
        }
        }else{
            self.soundBTN.setImage(UIImage(named: "SoundOnIcon"), for: .normal)
            self.soundBTN.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                self.soundBTN.transform = CGAffineTransform(scaleX: 0.5, y: 0.5).inverted()
            }) { (verif) in
                UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                    self.soundBTN.transform = CGAffineTransform.identity
                }) { (verifTwo) in
                    self.soundPlayer.volume = 0.6
                }
            }
        }
    }
    @IBOutlet weak var JokeTextViewContainer : UITextView!
    @IBOutlet weak var JokeTextViewHeight : NSLayoutConstraint!
    
    @IBOutlet weak var LaughImageView:UIImageView!
    @IBOutlet weak var TextCounterLocal : UILabel!
    @IBOutlet weak var LaughImageViewLocal:UIImageView!
    @IBOutlet weak var TextCounterRemote : UILabel!
    @IBOutlet weak var ClockWithWise : UIImageView!
    @IBOutlet weak var ClockWithOutWise : UIImageView!
    @IBOutlet weak var ScorpionWise : UIImageView!
    
    @IBOutlet weak var ClockWithWiseLocal : UIImageView!
    @IBOutlet weak var ClockWithOutWiseLocal : UIImageView!
    @IBOutlet weak var ScorpionWiseLocal : UIImageView!
    @IBOutlet weak var jokeRoundLocal : UIImageView!
    @IBOutlet weak var jokeRoundRemote : UIImageView!
    
    
    @IBOutlet weak var containerDisconnected: UIView!
    
    @IBOutlet weak var disconnectedMSGLBL: UILabel!
    
    @IBOutlet weak var containerFaceDisconnected: UIView!
    
    @IBOutlet weak var faceDisconnectedLBL: UILabel!
    
    @IBOutlet weak var NoFaceDetectedLBL: UILabel!
    
    @IBOutlet weak var containerLeave: UIView!
    
    @IBOutlet weak var leaveMSGLBL: UILabel!
    
    @IBOutlet weak var YesButtonLeave: UIButton!
    
    @IBOutlet weak var NoButtonLeave: UIButton!
    
    
    @IBOutlet weak var containerNoFace: UIView!
    
    @IBOutlet weak var NoFaceNewDesign: UIImageView!
    
    @IBOutlet weak var NofaceNewLBL: UILabel!
    @IBAction func YesLeaveAction(_ sender: UIButton) {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let a =  JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.leaveGameWithUserDecision(userMaster: a["_id"].stringValue, userSlave: self.SecondUserId,pathVideo: self.PathMaser)
        
        SocketIOManager.sharedInstance.removeFromMatchMaking(roomId: self.roomId.stringValue)
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        var i = 0
        for key in self.peerConnectionDict.keys {
            var jc: JanusConnection? = nil
            
            jc = self.peerConnectionDict[key] as? JanusConnection
            if i == 0 && self.Owner == "master" {
                
                self.onLeaving(jc?.handleId)
                
            }else if i == 1 && self.Owner == "slave" {
                self.onLeaving(jc?.handleId)
            }
            i = i + 1
        }
        self.publisherPeerConnection?.close()
        if self.detectionFaceSleeper != nil {
        self.detectionFaceSleeper.invalidate()
        self.detectionFaceSleeper = nil
        }
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
        if self.navigationController?.topViewController == self {
             SocketIOManager.sharedInstance.reactivateOnFinishGame()
            if timerQ != nil {
                timerQ?.invalidate()
                timerQ = nil
            }
            soundPlayer.stop()
            self.navigationController?.popViewController(animated: true)
            //self.dismiss(animated: true, completion: nil)
        }
        
       // self.navigation?.popToRootViewController(animated: true)
    }
    
    @IBAction func leaveGameAction(_ sender: UIButton) {
       // print("Present Leave Game Action")
        self.detectFaceAtStart(valid: false)
        self.openLeaveGame()
    }
    @IBAction func NoLeaveAction(_ sender: UIButton) {
        self.closeLeaveGame()
    }
    var animationCompletions = Dictionary<CAAnimation, (Bool) -> Void>()
    var viewsByName: [String : UIView]!
    var timerJokeTextView : Timer!
    var WillAnimateLaugh = true
    var RoundNumber = 0
    var JokesResults : [Int] = []
    var JokesResults5 : [Int] = []
       var isFaceDisconnectedWorking = true
    var LaughAnimationPosition : (Float, Float, Float, Float) = (0,0,0,0)
    var jokes : [String] = ["Hello From The Other side", "Two satelites blalfkfazkd azdjazdka dkadkadka dkadk akadak qdkjqd adkld qqk,dqd,qds,kdsd ,sqdkqkdqdkqdk qkdkdkqsdkqd qkqdkqdkq kdkqdkqdkqd kqdkq kqdkqkdq kqkdqdqkqdk lqdkqddk qldqd kkqdq kmqdklqkqdqs qsd qdqsd dqsdqz qsd qsds"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
      /*  delay(delayTime: 2, completionHandler: {
            
            // do your code here
            
        }) */
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(rawValue: "CheckAudioRoute"), object: nil, queue: nil, using: routechange)
        self.NoFaceNewDesign.setGifImage( UIImage(gifName: "gif-no-face.gif"))
        self.NoFaceNewDesign.startAnimatingGif()
        SocketIOManager.sharedInstance.gameWasLeaved { (data) in
            self.timerProba?.invalidate()
            self.timerProba = nil
            SocketIOManager.sharedInstance.closeGameWasLeaved()
            SocketIOManager.sharedInstance.closeGameWasLeavedWithNoFace()
            self.openDisconnected(userDecision: false)
          
        }
        SocketIOManager.sharedInstance.gameWasLeavedWithUserDecision { (data) in
            self.timerProba?.invalidate()
            self.timerProba = nil
             SocketIOManager.sharedInstance.closeGameWasLeaved()
            SocketIOManager.sharedInstance.closeGameWasLeavedWithNoFace()
            self.openDisconnected(userDecision: true)
            
        }
        configureMasterSound()
        self.RoundNumber = 0
        if  UserDefaults.standard.object(forKey: "SfxVolumeZonzay") != nil {
            let value = UserDefaults.standard.value(forKey: "SfxVolumeZonzay") as! String
            if value == "true" {
        do {
            
        soundPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "video_call", ofType: "mp3")!))
        }catch{
            //print("ErrorSound : ",error.localizedDescription)
        }
            }else{
                self.soundBTN.setImage(UIImage(named: "SoundOffIcon"), for: .normal)
            }
        }else{
            do {
                
                soundPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "video_call", ofType: "mp3")!))
                
            }catch{
               // print("ErrorSound : ",error.localizedDescription)
            }
        }
        
        SocketIOManager.sharedInstance.RoundFinished { (lol) in
            //print("RoundFinsished: ",lol)
            if self.RoundNumber < 6 {
                if self.RoundOwner == "M" {
                    self.RoundOwner = "S"
                }else{
                    self.RoundOwner = "M"
                }
                self.StartClock(owner: self.RoundOwner, i: self.RoundNumber)
            }else{
                SocketIOManager.sharedInstance.RoundFinishedStop()
                self.determineTheWinner()
            }
            
        }
        self.JokeTextViewContainer.isHidden = true
        self.configureContainerLabelStart()
        self.configureJokeTops()
        self.configureJokeBarLimit()
        self.configureGradientView()
        self.LabelStart.text = Localization("Get_ready")
        self.LabelStart.font = UIFont(name: "FingerPaint-Regular", size: 50)
        NotificationCenter.default.addObserver(self, selector: #selector(self.listenFromResult(_:)), name: Notification.Name.init("Dissmiss"), object: nil)
        //self.TimeLabel.isHidden = true
        SocketIOManager.sharedInstance.listenFromAndroid { (tab) in
            SocketIOManager.sharedInstance.closeListenFromAndroid()
            if self.Owner != "master" {
                SocketIOManager.sharedInstance.synchronizeAndroid(userId: self.SecondUserId)
            }
            
            //self.startRecording(self.PathMaser)
            self.StreamBegin()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.streamJanusBegin(_:)), name: NSNotification.Name(rawValue: "NSNotifMediaBegin"), object: nil)
       /* SocketIOManager.sharedInstance.StopGameNotifier { json in
            //I am the winner
            self.winner = true
            
            self.GameOver(isBefore: true, winner: GameWinner.masterLoser)
        } */
       /* SocketIOManager.sharedInstance.StopGameNotifierAtEnd { json in
            
            self.GameOver(isBefore: false, winner: self.calculateTheWinner())
        } */
        
        PathMaser = self.randomStringWithLength()
        PathMaser = PathMaser.replacingOccurrences(of: " ", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: ":", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: "+", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: "-", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: ".", with: "")
        
        self.prepareSockets()
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
    }
    var oneTime = false
    private func routechange(_ n:Notification) {
        print("yes notification received")
        if oneTime == true {
            oneTime = true
          if JBSoundRouter.isHeadsetPluggedIn() == false {
             self.enableLoudspeaker()
        }
        }
        
        
        
    }
    private func  enableLoudspeaker(){
        let audioSession = AVAudioSession.sharedInstance()
        let options = audioSession.categoryOptions
        if (options.rawValue & AVAudioSession.CategoryOptions.defaultToSpeaker.rawValue) != 0 {
           return
        }
        JBSoundRouter.routeSound(route: JBSoundRoute.Speaker)
    }
    func configureMasterSound(){
            if SoundBackGround.enabled {
                if let url = SoundBackGround.soundsBundle.url(forResource: "background_music", withExtension: "mp3") {
                    if let playerXX = SoundBackGround.sounds[url] {
                        playerXX.pause()
                    }
                }
        }
    }
    var streamShouldBegin = false
    @objc func streamJanusBegin(_ notification:Notification) {
        if streamShouldBegin == false {
            streamShouldBegin = true
    SocketIOManager.sharedInstance.synchronizeAndroid(userId: self.SecondUserId)
        }
    }
    func synchroniseAndroid(){
        if streamShouldBegin == false {
            streamShouldBegin = true
            SocketIOManager.sharedInstance.synchronizeAndroid(userId: self.SecondUserId)
        }
    }
    var timerQ : Timer? = nil
    func detectFaceAtStart(valid :Bool){
        
        
        var timeStart = 0
        
        let timeStartRandom = Int.random(in: 1...3)
        if valid {
         timerQ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
           
            if self.tracked == false {
                timeStart = timeStart + 1
                if timeStart == 11 + timeStartRandom {
                    
                    self.openFaceDisconnected()
                    timer.invalidate()
                    self.timerQ = nil
                }
            }else{
                timer.invalidate()
                self.timerQ = nil
                self.hideNoFace()
            }
        }
            timerQ?.fire()
        }else{
            timerQ?.invalidate()
            timerQ = nil
        }
        // Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
    }
 
  /*  func dynamicHeighTextView(){
        let number = offSetTableViewIfNeeded(textView: self.JokeTextViewContainer)
        if number > 7 {
            let totalHeight = JokeTextViewHeight.constant + CGFloat((Double((number - 7)) * 18.285))
            if totalHeight < (self.LocalUserView.frame.height - self.JokeTopLocalUser.frame.height) {
                JokeTextViewHeight.constant = totalHeight
            }else{
                JokeTextViewHeight.constant = (self.LocalUserView.frame.height - self.JokeTopLocalUser.frame.height)
            }
            
        }else{
            JokeTextViewHeight.constant = 128
        }
    } */
    func offSetTableViewIfNeeded(textView:UITextView) -> Int {
        let numberOfGlyphs = textView.layoutManager.numberOfGlyphs
        var index : Int = 0
        var lineRange = NSRange(location: NSNotFound, length: 0)
        var currentNumOfLines : Int = 0
        var numberOfParagraphJump : Int = 0
        
        while index < numberOfGlyphs {
            textView.layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            currentNumOfLines += 1
            
            // Observing whether user went to line and if it's the first such line break, accounting for it.
            if textView.text.last == "\n", numberOfParagraphJump == 0 {
                numberOfParagraphJump = 1
            }
        }
        
        currentNumOfLines += numberOfParagraphJump
        //print("Number of lines is:", currentNumOfLines)
        return currentNumOfLines
        
    }
  
    func StartClock(owner:String,i : Int){
        //print("Clock Owner : ",owner, " Round :",i)
        self.RoundNumber = RoundNumber + 1
            if owner == "M" {
                if self.jokeRoundLocal != nil {
                self.jokeRoundLocal.image = UIImage(named: "JokeOn")
                self.jokeRoundRemote.image = UIImage(named: "JokeOff")
                self.ScorpionWiseLocal.isHidden = false
                self.ClockWithOutWiseLocal.isHidden = false
                self.ClockWithWiseLocal.isHidden = true
                var jokeNumber = 0
                if i == 0 || i == 1 {
                    jokeNumber = 0
                    
                    self.JokeOneView.backgroundColor = UIColor.lightGray
                    self.animateScale(View: self.JokeOneView, true)
                    self.JokeTwoView.backgroundColor = #colorLiteral(red: 0.1106568202, green: 0.7037875056, blue: 0.8051496744, alpha: 1)
                    self.animateScale(View: self.JokeTwoView, false)
                    self.JokeThreeView.backgroundColor = #colorLiteral(red: 0.1106568202, green: 0.7037875056, blue: 0.8051496744, alpha: 1)
                    self.animateScale(View: self.JokeThreeView, false)
                }
                if i == 2 || i == 3 {
                    jokeNumber = 1
                    self.JokeOneView.backgroundColor = UIColor.lightGray
                    self.animateScale(View: self.JokeOneView, false)
                    self.JokeTwoView.backgroundColor = UIColor.lightGray
                    self.animateScale(View: self.JokeTwoView, true)
                    self.JokeThreeView.backgroundColor = #colorLiteral(red: 0.1106568202, green: 0.7037875056, blue: 0.8051496744, alpha: 1)
                    self.animateScale(View: self.JokeThreeView, false)
                }
                if i == 4 || i == 5 {
                    jokeNumber = 2
                    self.JokeOneView.backgroundColor = UIColor.lightGray
                     self.animateScale(View: self.JokeOneView, false)
                    self.JokeTwoView.backgroundColor = UIColor.lightGray
                     self.animateScale(View: self.JokeTwoView, false)
                    self.JokeThreeView.backgroundColor = UIColor.lightGray
                     self.animateScale(View: self.JokeThreeView, true)
                }
                self.JokeTextViewContainer.text = self.selectedJokes[jokeNumber]
                self.JokeTextViewContainer.isHidden = false
                }
            }else{
                if self.jokeRoundLocal != nil {
                self.jokeRoundLocal.image = UIImage(named: "JokeOff")
                self.jokeRoundRemote.image = UIImage(named: "JokeOn")
                self.ScorpionWise.isHidden = false
                self.ClockWithOutWise.isHidden = false
                self.ClockWithWise.isHidden = true
                }
            }
            if owner == "M" {
                if self.LocalUserView != nil {
                self.addViewScaleAnimation(view: self.LocalUserView) { (verif) in
                    if self.LocalUserView != nil {
                    self.removeViewAnimation(view: self.LocalUserView)
                    }
                self.addClockRotationAnimation { (verif) in
                    if verif {
                        self.ClockWithWiseLocal.isHidden = false
                        self.ScorpionWiseLocal.isHidden = true
                        self.ClockWithOutWiseLocal.isHidden = true
                        self.removeClockRotationAnimation()
                        self.ClockWithWiseLocal.image = UIImage(named: "ClockBig")
                        self.someoneDidLaugh()
                        self.addClockScaleAnimation(completion: { (verif) in
                            if verif {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                                    if  (self.TextCounterLocal != nil ) {
                                        
                    self.JokesResults5.append(Int(self.TextCounterLocal.text!)!)
                                        
                                        }
                                }
                                self.addClockAlarmAnimation(completion: { (verif) in
                                    self.LaughFinished()
                                    self.removeAllAnimationsClock()
                                    if self.JokeTextViewContainer != nil {
                                    self.JokeTextViewContainer.isHidden = true
                                    
                                    if self.RoundOwner == "M" {
                                        self.RoundOwner = "S"
                                    }else{
                                        self.RoundOwner = "M"
                                    }
                                    self.JokesResults.append(Int(self.TextCounterLocal.text!)!)
                                    self.NotifyRound(owner: self.RoundOwner)
                                    if self.RoundNumber < 6 {
                                    self.StartClock(owner: self.RoundOwner, i: self.RoundNumber)
                                    }else{
                                        SocketIOManager.sharedInstance.RoundFinishedStop()
                                        self.determineTheWinner()
                                    }
                                    }
                                    
                                })
                            }
                        })
                    }
                }
            }
        }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                                if (self.TextCounterLocal != nil ) {
                                    
                self.JokesResults5.append(Int(self.TextCounterLocal.text!)!)
                                    }
                            }
                self.addClockRotationAnimationRemote { (verif) in
                    if verif {
                        self.JokesResults.append(Int(self.TextCounterLocal.text!)!)
                        self.removeClockRotationRemoteAnimation()
                        
                    }
                }
            }
        
    }
    func NotifyRound(owner:String){
        SocketIOManager.sharedInstance.notifyRound(userId: self.SecondUserId,owner: owner)
    }
    func configureJokeBarLimit(){
    
        JokeBar.initBar()
        JokeBar.setBackColor(color: self.view.backgroundColor!)
        JokeBar.setProgressColor(color: UIColor.init(gradientStyle: .topToBottom, withFrame: JokeBar.frame, andColors: [UIColor(red: 229/255, green: 44/255, blue: 44/255, alpha: 1),UIColor.white]))
        JokeBar.bringSubviewToFront(JokeBarLimit)
    }
    func configureContainerLabelStart(){
        ContainerLabelStart.backgroundColor = UIColor.init(gradientStyle: .radial, withFrame: self.ContainerLabelStart.bounds, andColors: [UIColor(red: 31/255, green: 121/255, blue: 139/255, alpha: 1),UIColor(red: 5/255, green: 41/255, blue: 49/255, alpha: 1),UIColor(red: 31/255, green: 121/255, blue: 139/255, alpha: 1)])
        self.TextCounterLocal.font = UIFont(name: "FingerPaint-Regular", size: 17)
        
    }
    func configureGradientView(){
        self.view.backgroundColor = UIColor.init(gradientStyle: .radial, withFrame: self.view.bounds, andColors: [UIColor(red: 21/255, green: 135/255, blue: 159/255, alpha: 1),UIColor(red: 18/255, green: 122/255, blue: 145/255,alpha: 1)])
        
    }
    func configureJokeTops(){
        self.JokeTopRemoteUser.roundCorners(corners: [.topLeft, .topRight], radius: 15.699)
        self.JokeTopLocalUser.roundCorners(corners: [.topLeft, .topRight], radius: 15.699)
        self.JokeTopLocalUser.layer.masksToBounds = true
        self.JokeTopRemoteUser.layer.masksToBounds = true
        let path = UIBezierPath(roundedRect: CGRect(x: self.RemoteUserView.bounds.origin.x, y: self.RemoteUserView.bounds.origin.y, width: self.RemoteUserView.frame.width, height: self.JokeTopRemoteUser.frame.height), byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 12, height: 12))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.name = "LOL"
        mask.backgroundColor = self.JokeTopRemoteUser.backgroundColor?.withAlphaComponent(0.6).cgColor
        mask.opacity = 0.6
        self.RemoteUserView.layer.addSublayer(mask)
        mask.frame.origin.x = 0
        mask.frame.origin.y = 0
        self.JokeTopRemoteUser.backgroundColor = UIColor.clear
        
        let SecondPath = UIBezierPath(roundedRect: CGRect(x: self.LocalUserView.bounds.origin.x, y: self.LocalUserView.bounds.origin.y, width: self.LocalUserView.frame.width, height: self.JokeTopLocalUser.frame.height), byRoundingCorners: [.topLeft,.topRight], cornerRadii: CGSize(width: 12, height: 12))
        let Secondmask = CAShapeLayer()
        Secondmask.path = SecondPath.cgPath
        Secondmask.backgroundColor = self.JokeTopLocalUser.backgroundColor?.withAlphaComponent(0.6).cgColor
        Secondmask.opacity = 0.6
        Secondmask.name = "LOL"
        self.LocalUserView.layer.addSublayer(Secondmask)
        Secondmask.frame.origin.x = 0
        Secondmask.frame.origin.y = 0
        self.JokeTopLocalUser.backgroundColor = UIColor.clear
        
    }
    var stepTimer = 3
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isIdleTimerDisabled = true
      // self.doAnimations()
        if self.view.subviews.count == 0 {
             self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
             SocketIOManager.sharedInstance.reactivateOnFinishGame()
            if timerQ != nil {
                timerQ?.invalidate()
                timerQ = nil
            }
            soundPlayer.stop()
            self.navigationController?.popViewController(animated: true)
        }
        //print("Navigation OnGame: ",self.navigation)
        if willAppear {
            self.RoundNumber = 0
            self.LabelStart.alpha = 0
            self.redoAnimation()
        }
       
        
            //Relunch the new Design like before in LoadingCounter
        
        
    }
    func doAnimations(){
        DispatchQueue.main.asyncAfter(deadline: .now()  + 1.5) {
            UIView.animate(withDuration: 0.5, delay: 0.4,
                           options: [ .curveEaseIn],
                           animations: {
                             self.LabelStart.text = "3"
            },
                           completion: nil
            )
           
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                if self.stepTimer > 0 {
                    
                    self.LabelStart.alpha = 0
                    UIView.animate(withDuration: 1, delay: 0,
                                   options: [ .curveEaseInOut],
                                   animations: {
                                    self.LabelStart.alpha = 1
                                   self.LabelStart.text = "\(self.stepTimer)"
                                    
                    },
                                   completion: { _ in
                                    self.LabelStart.alpha = 0
                    }
                    )
                    
                    self.stepTimer = self.stepTimer - 1
                }else{
                    self.LabelStart.alpha = 1
                    timer.invalidate()
                    
                    self.LabelStart.transform = CGAffineTransform(scaleX: -12, y: -12)
                    self.LabelStart.text = Localization("START")
                    UIView.animate(withDuration: 0.4, animations: {
                        self.LabelStart.transform = CGAffineTransform.identity
                      
                       
                        
                    },completion: { _ in
                        UIView.animate(withDuration: 0.5, delay: 0, options: [ .curveEaseOut], animations: {
                            //print("OKKKKK")
                            self.ContainerStart.isHidden = true
                               self.ContainerStart.alpha = 0
                        }, completion: nil)
                        
                    })
                    
                    
                }
            })
        }
    }
    func redoAnimation(){
        if stepTimer > 0 {
            UIView.animate(withDuration: 1, delay: 0,
                           options: [ .curveEaseInOut],
                           animations: {
                            self.LabelStart.alpha = 1
                            self.LabelStart.text = "\(self.stepTimer)"
                            
            },
                           completion: { _ in
                            self.stepTimer = self.stepTimer - 1
                            self.LabelStart.alpha = 0
                            self.redoAnimation()
            })
        }else{
            
            self.LabelStart.alpha = 1
            self.LabelStart.transform = CGAffineTransform(scaleX: -12, y: -12)
            self.LabelStart.text = Localization("START")
            self.timerDidEnd()
            UIView.animate(withDuration: 0.4, animations: {
                self.LabelStart.transform = CGAffineTransform.identity
            },completion: { _ in
                
                UIView.animate(withDuration: 1, delay: 0.5, options: [ .curveEaseOut], animations: {
                    //print("OKKKKK")
                    self.ContainerStart.alpha = 0
                }, completion: { _ in
                    self.ContainerStart.isHidden = true
                  self.synchroniseAndroid()
                    
                    
                })
            })
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func tellAjoke(){
        
        //janusStartToJoke.cancel()
        janusTimeElapsed = self.janusTempTime
        if self.Owner == "master" {
        self.LabelStart.text = Localization("TELLAJ")
        }else{
         self.LabelStart.text = Localization("TRYNTL")
        }
       
        timerProba =  Timer.scheduledTimer(timeInterval: 0.004, target: self, selector: #selector(self.handleSmileProba(timer:)), userInfo: nil, repeats: true)
        
        self.ContainerStart.isHidden = false
        UIView.animate(withDuration: 1, delay: 0.1, options : [.curveEaseIn], animations : {
            self.ContainerStart.alpha = 1
        },completion : { _ in
            UIView.animate(withDuration: 1, delay: 0.5, options : [.curveEaseOut], animations : {
                self.ContainerStart.alpha = 0
                
            },completion : { _ in
                self.ContainerStart.isHidden = true
              
                if self.Owner == "master" {
                    self.RoundOwner = "M"
                    self.JokeTextViewContainer.text = self.selectedJokes[0]
                    //self.JokeOneView.backgroundColor = UIColor.lightGray
                    
                    self.JokeTextViewContainer.isHidden = false
                }else{
                    self.RoundOwner = "S"
                 
                }
                self.StartClock(owner: self.RoundOwner,i: self.RoundNumber)
                self.MessageFinished = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.detectFaceAtStart(valid: true)
                }
                
            })
            
        })
    }
    func animateScale(View: UIView,_ bool : Bool){
        if bool {
            UIView.animate(withDuration: 0.2) {
                View.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
        }else{
            View.transform = CGAffineTransform.identity
        }
        
    }
    func someoneDidLaugh(){
        //print("Start Animation")
        let layer = CALayer()
        layer.backgroundColor = UIColor.black.withAlphaComponent(0.6).cgColor
        layer.name = "TempLayer"
        self.LocalUserView.layer.addSublayer(layer)
        layer.frame = CGRect(x: 0, y: self.JokeTopLocalUser.frame.height, width: self.JokeTopLocalUser.frame.width, height: 0)
        layer.layoutIfNeeded()
      let grow = CABasicAnimation(keyPath: "bounds.size.height")
        grow.fromValue = NSNumber(value: 0)
        grow.toValue = NSNumber(value: Int(self.LocalUserView.frame.height - self.JokeTopLocalUser.frame.height + 1)) // to a height of 200
        grow.duration = 0.6
        layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        grow.beginTime = CFTimeInterval(exactly: 0)!
        grow.fillMode = .both
        grow.isRemovedOnCompletion = false
        layer.add(grow, forKey: "anim")
    }
    func someoneDidLaughRemote(){
        //print("Start Animation")
        let layer = CALayer()
        layer.backgroundColor = UIColor.black.withAlphaComponent(0.6).cgColor
        layer.name = "TempLayer"
        self.RemoteUserView.layer.addSublayer(layer)
        layer.frame = CGRect(x: 0, y: self.JokeTopLocalUser.frame.height, width: self.JokeTopLocalUser.frame.width, height: 0)
        layer.layoutIfNeeded()
        let grow = CABasicAnimation(keyPath: "bounds.size.height")
        grow.fromValue = NSNumber(value: 0)
        grow.toValue = NSNumber(value: Int(self.LocalUserView.frame.height - self.JokeTopLocalUser.frame.height + 1)) // to a height of 200
        grow.duration = 0.6
        layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        grow.beginTime = CFTimeInterval(exactly: 0)!
        grow.fillMode = .both
        grow.isRemovedOnCompletion = false
        layer.add(grow, forKey: "anim")
    }
    func LaughFinished(){
        if self.LocalUserView != nil {
        let layer = self.LocalUserView.layer.sublayers!.filter{ $0.name == "TempLayer"}.first
        if layer != nil {
        let grow = CABasicAnimation(keyPath: "bounds.size.height")
        grow.fromValue = NSNumber(value: Int(self.LocalUserView.frame.height - self.JokeTopLocalUser.frame.height + 1))
        grow.toValue = NSNumber(value: 0) // to a height of 200
        grow.duration = 0.6
        layer!.anchorPoint = CGPoint(x: 0.5, y: 0)
        grow.beginTime = CFTimeInterval(exactly: 0)!
        grow.fillMode = .both
        grow.isRemovedOnCompletion = false
        // add any additional animation configuration here...
        layer!.add(grow, forKey: "animRoll")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.LocalUserView != nil {
            self.LocalUserView.layer.sublayers!.removeAll(layer!)
            }
        }
        }
        }
    }
    func LaughFinishedRemote(){
        if self.RemoteUserView != nil {
        let layer = self.RemoteUserView.layer.sublayers!.filter{ $0.name == "TempLayer"}.first
        if layer != nil {
            let grow = CABasicAnimation(keyPath: "bounds.size.height")
            grow.fromValue = NSNumber(value: Int(self.RemoteUserView.frame.height - self.JokeTopLocalUser.frame.height + 1))
            grow.toValue = NSNumber(value: 0) // to a height of 200
            grow.duration = 0.6
            layer!.anchorPoint = CGPoint(x: 0.5, y: 0)
            grow.beginTime = CFTimeInterval(exactly: 0)!
            grow.fillMode = .both
            grow.isRemovedOnCompletion = false
            // add any additional animation configuration here...
            layer!.add(grow, forKey: "animRoll")
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.RemoteUserView.layer.sublayers!.removeAll(layer!)
            }
        }
        }
    }
    var FriendOrMatch = false
    var TimeGame : Float = 20.0
    func setSize(_ size: CGSize) {
        
    }
    /*
     *
     *Our detector
     */
    var detector = GMVDetector(ofType: GMVDetectorTypeFace, options: [
        GMVDetectorFaceLandmarkType : GMVDetectorFaceLandmark.all.rawValue,
        GMVDetectorFaceClassificationType : GMVDetectorFaceClassification.all.rawValue,
        GMVDetectorFaceTrackingEnabled : false
        
        ])
    
    func convertCVPixelToSampleBuffer(cvPixel: CVPixelBuffer,time:Int64) -> CMSampleBuffer? {
        var sampleBuffer: CMSampleBuffer? = nil
        var sampleTimingInfo: CMSampleTimingInfo = CMSampleTimingInfo.invalid
        sampleTimingInfo.presentationTimeStamp = CMTime.zero
        sampleTimingInfo.decodeTimeStamp = CMTime.invalid
        var formatDesc: CMFormatDescription? = nil
        _ = CMVideoFormatDescriptionCreateForImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixel, formatDescriptionOut: &formatDesc)

        
            CMSampleBufferCreateReadyWithImageBuffer(allocator: kCFAllocatorDefault, imageBuffer: cvPixel, formatDescription: formatDesc!, sampleTiming: &sampleTimingInfo, sampleBufferOut: &sampleBuffer)
        

        
           return sampleBuffer!
        
    }
    var tracked = false
    //MARK: - Vison detection
    private lazy var faceDetectorOption: VisionFaceDetectorOptions = {
        let option = VisionFaceDetectorOptions()
        option.classificationMode = .all
        option.performanceMode = .accurate
        option.landmarkMode = .all
        return option
    }()
    lazy var vision = Vision.vision()
    private lazy var faceDetector = vision.faceDetector(options: faceDetectorOption)
    
    
    
    
    func renderFrame(_ frame: RTCVideoFrame?) {
       
    }
    var i = 0
    var isControllerWorking = false
    var MessageFinished = false
    var timerProba : Timer? = nil
    var smileProba : Float = 0.0
    var getSmile = true
    var getSmiled = true
    @IBOutlet weak var TimeLabel: UILabel!
    @objc func handleSmileProba(timer: Timer){
        if isControllerWorking {
            let smile = Int(self.smileProba * 100)
            if smile != -300 {
                JokeBar.setProgressValue(currentValue: CGFloat(smile))
            }else{
               JokeBar.setProgressValue(currentValue: 0)
            }
            if ((smile < 10 && smile > 0) || smile == 0) {
                if detectionFaceSleeper != nil {
                    detectionFaceSleeper.invalidate()
                    detectionFaceSleeper = nil
                }
               // self.userNotFound.isHidden = true
                self.hideNoFace()
                self.getSmile = true
                
               // JokeBar.animateProgress(duration: 0, progressValue: CGFloat(self.smileProba))
            }else if (smile >= 10 && smile <= 20){
                if detectionFaceSleeper != nil {
                    detectionFaceSleeper.invalidate()
                    detectionFaceSleeper = nil
                }
                //self.userNotFound.isHidden = true
                self.hideNoFace()
                self.getSmile = true
               // JokeBar.animateProgress(duration: 0, progressValue: CGFloat(self.smileProba))
            }else if (smile > 20 && smile <= 40) {
                if detectionFaceSleeper != nil {
                    detectionFaceSleeper.invalidate()
                    detectionFaceSleeper = nil
                }
                //self.userNotFound.isHidden = true
                  self.hideNoFace()
                self.getSmile = true
               // JokeBar.animateProgress(duration: 0, progressValue: CGFloat(self.smileProba))
            }else if (smile > 40 && smile < 60) {
                if detectionFaceSleeper != nil {
                    detectionFaceSleeper.invalidate()
                    detectionFaceSleeper = nil
                }
                //self.userNotFound.isHidden = true
                  self.hideNoFace()
                self.getSmile = true
              //  JokeBar.animateProgress(duration: 0, progressValue: CGFloat(self.smileProba))
            }else if (smile >= 60) {
                if detectionFaceSleeper != nil {
                    detectionFaceSleeper.invalidate()
                    detectionFaceSleeper = nil
                }
                if getSmiled {
                    self.getSmiled = false
                    self.changeLife()
                    SocketIOManager.sharedInstance.NotifyChangeLife(userId: self.SecondUserId)
                    if detectionSmileSleeper != nil {
                        detectionSmileSleeper.invalidate()
                        detectionSmileSleeper = nil
                    }
                    detectionSmileSleeper = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { time in
                        self.getSmiled = true
                    })
                }
               // self.userNotFound.isHidden = true
                  self.hideNoFace()
               // JokeBar.animateProgress(duration: 0, progressValue: CGFloat(self.smileProba))
            }else if (smile == -300 ) {
               // JokeBar.animateProgress(duration: 0, progressValue: 0)
                
                self.getSmiled = true
                if detectionSmileSleeper != nil {
                    detectionSmileSleeper.invalidate()
                    detectionSmileSleeper = nil
                }
                //self.userNotFound.isHidden = false
                self.showNoFace()
                if getSmile {
                    self.getSmile = false
                    if detectionFaceSleeper != nil {
                        detectionFaceSleeper.invalidate()
                        detectionFaceSleeper = nil
                    }
                    detectionFaceSleeper = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { time in
                        self.changeLife()
                        SocketIOManager.sharedInstance.NotifyChangeLife(userId: self.SecondUserId)
                        self.getSmile = true
                    })
                }
            }
        }
    }
    /*
     *
     *Util for conver frame to PixelBuffer
     */
    fileprivate func calculatePlaneSize(forFrame frame: RTCVideoFrame)
        -> (ySize: Int, uSize: Int, vSize: Int){
            let baseSize = Int(frame.width * frame.height) * MemoryLayout<GLubyte>.size
            return (baseSize, baseSize / 4, baseSize / 4)
    }
   var JokeOne = 0
    var JokeTwo = 0
    var JokeThree = 0
    var JokeFourth = 0
    var JokeFive = 0
    var JokeSix = 0

    @IBOutlet weak var userNotFound: UIImageView!
    private let kARDMediaStreamId = "ARDAMS"
    private let kARDAudioTrackId = "ARDAMSa0"
    private let kARDVideoTrackId = "ARDAMSv0"
    var factory: RTCPeerConnectionFactory?
    var websocket: WebSocketChannel?
    var peerConnectionDict: [AnyHashable : Any] = [:]
    var publisherPeerConnection: RTCPeerConnection?
    var localTrack: RTCVideoTrack?
    var localAudioTrack: RTCAudioTrack?
    var capturer : RTCCameraVideoCapturer?
    var height: Int = 0
    var Owner = "master"
    var selectedJokes : [String] = ["a","b","c"]
    var roomId : NSNumber!
    var SecondUserId: String!
    var detectionFaceSleeper: Timer!
    var detectionSmileSleeper : Timer!
    var streamCounter : Timer!
    var smilesCounter : Timer!
    var recordTime : Float = 0
    var PathMaser : String!
    var FaceUserX : CGFloat = 0
    var FaceUserY : CGFloat = 0
    var FaceUserXStart : CGFloat = 0
    var FaceUserYStart : CGFloat = 0
    var FaceFrameStart : Float = 0
    var winner : Bool = false
    var dataToSend : [String: Any] = [:]
    var FaceFrameDetection: Float  = 0.0
    var navigation : UINavigationController? = nil
    @objc func listenFromResult(_ notification :Notification){
             SocketIOManager.sharedInstance.reactivateOnFinishGame()
        if timerQ != nil {
            timerQ?.invalidate()
            timerQ = nil
        }
        soundPlayer.stop()
            self.navigationController?.popViewController(animated: true)
      
        
        
    }
     var willAppear = true
   var janusStartToJoke : AutoCancellingTimer!
    var janusTimeElapsed : Float = 0.0
    var janusTempTime : Float = 0.0
    func startJanus(){
         DispatchQueue.main.async { [unowned self] in
        let url = URL(string: ScriptBase.janus_URI)
              if let url = url {
                self.websocket = WebSocketChannel(url: url,number:self.roomId)
              }
            self.websocket!.delegate = self
              self.RemoteUserView.delegate = self
            self.peerConnectionDict = [AnyHashable : Any]()
            self.factory = RTCPeerConnectionFactory()
            self.localTrack = self.createLocalVideoTrack()
            self.localAudioTrack = self.createLocalAudioTrack()
        }
    }
    func timerDidEnd() {
    
        self.startJanus()
        janusStartToJoke =  AutoCancellingTimer.init(interval: 0.001, callback: {
                   self.janusTempTime += self.janusTempTime + 0.001
               })
      
          
        
        
    }
    @objc func StreamBegin(){
       
        if self.soundBTN.image(for: .normal) == UIImage(named: "SoundOnIcon"){
            soundPlayer.volume = 0.65
        soundPlayer.play()
            soundPlayer.numberOfLoops = -1
        }
        streamCounter = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true, block: { _ in
            self.recordTime = self.recordTime + 0.001
        })
        /*Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
           
        } */
       /* if AVAudioSession.sharedInstance().isHeadphonesConnected == false {
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord)
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            try AVAudioSession.sharedInstance().setActive(true)
        }catch {
            print("AVAUdioo StreamBegin: ",error.localizedDescription)
        }
        } */
       /* AutoCancellingTimer(interval: 1, repeats: true) {
             //print("RecordingTime : ",self.recordTime)
        } */
        smilesCounter =  Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
                 if self.TextCounterLocal != nil {
                      print("self.TextCounterLocal.text : ",self.TextCounterLocal.text)
                      self.jokesCounter.append(Int(self.TextCounterLocal.text ?? "0") ?? 0)
                  }
              })
        self.tellAjoke()
      
    }
    
    func performChangeOnString(changes:String) -> String {
        var result = ""
        var i = 0
        
        for char in changes {
            
            result = result + String(char)
            if i != 0 {
            if i % 41 == 0 {
                result = result + "\n "
            }
            }
            i = i + 1
        }
        return result
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         SocketIOManager.sharedInstance.closeListenFromAndroid()
    }
    func determineTheWinner(){
        self.isFaceDisconnectedWorking = false
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            if self.Owner == "master" {
                 /* print(self.JokesResults)
                  print(self.selectedJokes[0])
                  print(self.selectedJokes[1])
                  print(self.selectedJokes[2])
                  print(self.JokesResults[0])
                  print(self.JokesResults[1])
                  print(self.JokesResults[2])
                  print(self.JokesResults[3])
                  print(self.JokesResults[4])
                  print(self.JokesResults[5]) */
                SocketIOManager.sharedInstance.LoseGame(PathM: PathMaser, faceUserX: FaceUserX != 0 ? FaceUserX.description : FaceUserXStart.description, faceUserY: FaceUserY != 0 ? FaceUserY.description : FaceUserYStart.description, timeDetection: "0", receiverSlave: self.SecondUserId, userMaster: a["_id"].stringValue, gamemode: 60, jokeOne: self.performChangeOnString(changes: self.selectedJokes[0]), jokeTwo: self.performChangeOnString(changes: self.selectedJokes[1]), jokeThree: self.performChangeOnString(changes: self.selectedJokes[2]), scoreOneLocal: JokesResults[0], scoreTwoLocal: JokesResults[1], scoreThreeLocal: JokesResults[2], scoreOneRemote: JokesResults[3], scoreTwoRemote: JokesResults[4], scoreThreeRemote: JokesResults[5], faceFrame: self.FaceFrameDetection != 0 ? self.FaceFrameDetection : self.FaceFrameStart,timeElapsedJanus: self.janusTimeElapsed, score5One: self.JokesResults5[0], score5Two: self.JokesResults5[1], score5Three: self.JokesResults5[2], score5Fourth: self.JokesResults5[3], score5Five: self.JokesResults5[4], score5Six: self.JokesResults5[5],jokesCounter: self.jokesCounter)
                
               /* let alert = UIAlertController(title: "LoseGame", message: String(self.janusTimeElapsed), preferredStyle: .alert)
                let ok = UIAlertAction(title: "ok", style: .default, handler: nil)
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil) */
            }
        }catch{
            
        }
        if self.Owner == "master" {
        switch countHearts() {
        case .draw:
            self.GameOver(isBefore: false, winner: GameWinner.draw)
        case .masterWinner:
            self.GameOver(isBefore: false, winner: GameWinner.masterWinner)
        case .masterLoser:
            self.GameOver(isBefore: false, winner: GameWinner.masterLoser)
        }
        }else{
            switch countHearts() {
            case .draw:
                self.GameOver(isBefore: false, winner: GameWinner.draw)
            case .masterWinner:
                self.GameOver(isBefore: false, winner: GameWinner.masterWinner)
            case .masterLoser:
                self.GameOver(isBefore: false, winner: GameWinner.masterLoser)
            }
        }
    }
    func calculateTheWinner() -> GameWinner{
        switch countHearts() {
        case .draw:
            return .draw
        case .masterWinner:
            return .masterLoser
        case .masterLoser:
            
            return .masterWinner
        }
    }
    func countHearts() -> GameWinner{
        
       
        let local  = Int(self.TextCounterLocal.text!)!
        let remote = Int(self.TextCounterRemote.text!)!
        if local == remote {
            return GameWinner.draw
        }else if local > remote{
            return GameWinner.masterWinner
        }else{
            return GameWinner.masterLoser
        }
        
        
        
    }
    func randomStringWithLength() -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = 8
        let date = Date()
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0...(len - 1){
            let length = UInt32(letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
            
        }
        return "ios" + (randomString as String) + date.description
    }
    func GameOver(isBefore:Bool,winner:GameWinner){
        if self.detectionFaceSleeper != nil {
            self.detectionFaceSleeper.invalidate()
            self.detectionFaceSleeper = nil
           // print("detectionFaceSleeper nil")
        }
        if self.detectionSmileSleeper != nil {
            self.detectionSmileSleeper.invalidate()
            self.detectionSmileSleeper = nil
           // print("detectionSmileSleeper nil")
        }
        if timerProba != nil {
            self.timerProba?.invalidate()
            self.timerProba = nil
            //print("timerProba nil")
        }
        if streamCounter != nil{
            self.streamCounter.invalidate()
            self.streamCounter = nil
            //print("streamCounter nil")
        }
        if smilesCounter != nil {
            self.smilesCounter.invalidate()
            self.smilesCounter = nil
        }
    SocketIOManager.sharedInstance.closeGameWasLeaved()
        addFiveToJanus(isBefore:isBefore,winner: winner)
        
    }
    func addFiveToJanus(isBefore:Bool,winner:GameWinner){
        //print("One")
       /* if (Owner == "master" && winner == .masterWinner) || (Owner != "master" && winner == .masterWinner) {
            self.createAnimation()
            self.createAnimationFire()
        
        } */
       /* if winner == .masterLoser {
            if #available(iOS 13, *) {
            self.createAnimation()
            self.createAnimationFire()
            }
        } */
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NSNotifMediaBegin"), object: nil)
        SocketIOManager.sharedInstance.StopGameNotifierClose()
        SocketIOManager.sharedInstance.StopGameNotifierAtEndClose()
        SocketIOManager.sharedInstance.LifeChangedNotifierStop()
        SocketIOManager.sharedInstance.jokeChangedStop()
        SocketIOManager.sharedInstance.closeGameWasLeaved()
        SocketIOManager.sharedInstance.closeGameWasLeavedWithNoFace()

        _ = Timer.scheduledTimer(withTimeInterval: 4, repeats: false) {time in
            //print("Five")
            if isBefore {
                if self.winner {
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        self.dataToSend =  [
                            "PathMaster" : self.PathMaser,
                            "receiverSlave" : a["_id"].stringValue,
                            "userMaster" : self.SecondUserId,
                            "winnerResult" : GameWinner.masterLoser,
                            "who" : "slave",
                            "isBefore" : isBefore
                        ]
                    }catch{
                        
                    }
                }else{
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        self.dataToSend = [
                            "winnerResult" : winner,
                            "receiverSlave" : self.SecondUserId,
                            "userMaster" : a["_id"].stringValue,
                            "who" : "master"
                        ]
                    }catch{
                        
                    }
                }
            }else{
                if self.Owner != "master" {
                    
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        self.dataToSend =  [
                            "PathMaster" : self.PathMaser,
                            "receiverSlave" : a["_id"].stringValue,
                            "userMaster" : self.SecondUserId,
                            "winnerResult" : winner,
                            "isBefore" : isBefore,
                            "who" : "slave"
                            
                        ]
                        
                        
                    }catch{
                        
                    }
                }else{
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        self.dataToSend = [
                            "winnerResult" : winner,
                            "who" : "master",
                            "isBefore" : isBefore,
                            "receiverSlave" : self.SecondUserId,
                            "userMaster" : a["_id"].stringValue,
                        ]
                    }catch{
                        
                    }
                }
            }
            var i = 0
            for key in self.peerConnectionDict.keys {
                var jc: JanusConnection? = nil
                
                jc = self.peerConnectionDict[key] as? JanusConnection
                if i == 0 && self.Owner == "master" {
                    
                    self.onLeaving(jc?.handleId)
                    
                }else if i == 1 && self.Owner == "slave" {
                    self.onLeaving(jc?.handleId)
                }
                i = i + 1
            }
            self.publisherPeerConnection?.close()
            self.websocket?.closeConnection()
            self.isControllerWorking = false
            self.recordTime = 0.0
            SocketIOManager.sharedInstance.closeGameWasLeaved()
            
          /*  DispatchQueue.main.async {
                if winner == .masterLoser {
                    if #available(iOS 13, *) {
                self.animationViewForWinner.stop()
                self.animationViewFire.stop()
                    }
                //self.animationViewForWinner.removeFromSuperview()
                //self.animationViewFire.removeFromSuperview()
                }
                 
            } */
            self.soundPlayer.stop()
            self.performSegue(withIdentifier: "push_finished", sender: self)
            
            
           
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "push_finished" {
            (segue.destination as! VideoGameResultController).modalPresentationStyle = .fullScreen
            (segue.destination as! VideoGameResultController).results = dataToSend
            (segue.destination as! VideoGameResultController).FriendOrMatch = self.FriendOrMatch
            (segue.destination as! VideoGameResultController).Owner = self.Owner
            (segue.destination as! VideoGameResultController).roomId = self.roomId
            (segue.destination as! VideoGameResultController).jokeOne = self.performChangeOnString(changes: self.selectedJokes[0])
            (segue.destination as! VideoGameResultController).JokesResults = self.JokesResults
            (segue.destination as! VideoGameResultController).JokesResults5 = self.JokesResults5
            (segue.destination as! VideoGameResultController).faceFrame = self.FaceFrameDetection != 0 ? self.FaceFrameDetection : self.FaceFrameStart
            (segue.destination as! VideoGameResultController).faceUserX = self.FaceUserX != 0 ? self.FaceUserX.description : self.FaceUserXStart.description
            (segue.destination as! VideoGameResultController).faceUserY = self.FaceUserY != 0 ?self.FaceUserY.description : self.FaceUserYStart.description
            (segue.destination as! VideoGameResultController).jokeTwo = self.performChangeOnString(changes: self.selectedJokes[1])
            (segue.destination as! VideoGameResultController).jokeThree = self.performChangeOnString(changes: self.selectedJokes[2])
            (segue.destination as! VideoGameResultController).scoresAll = self.jokesCounter
            (segue.destination as! VideoGameResultController).navigation = self.navigation
            (segue.destination as! VideoGameResultController).timeElapsedJanus = self.janusTimeElapsed
            //self.jokesCounter = []
            resetView()
        }
    }
    func resetView(){
        self.willAppear = false
       // self.TimeLabel.isHidden = true
        websocket?.closeConnection()
        websocket?.delegate = nil
        self.websocket = nil
        self.RemoteUserView.delegate = nil
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NSNotifMediaBegin"), object: nil)
        SocketIOManager.sharedInstance.StopGameNotifierClose()
        SocketIOManager.sharedInstance.StopGameNotifierAtEndClose()
        SocketIOManager.sharedInstance.LifeChangedNotifierStop()
        SocketIOManager.sharedInstance.jokeChangedStop()
        if LocalUserView.captureSession.isRunning {
            LocalUserView.captureSession.stopRunning()
        }
    }
    func prepareSockets(){
        SocketIOManager.sharedInstance.LifeChangedNotifier { json in
            self.changeRemoteLife()
        }
    }
    func changeRemoteLife(){
        var total = Int(self.TextCounterRemote.text!)!
        if total < 10 {
            total += 1
            self.someoneDidLaughRemote()
           self.LaughAnimationPosition = self.addUntitledAnimation ()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
               
                    self.addLabelScaleAnimation(label: self.TextCounterRemote) { _  in
                        self.TextCounterRemote.text = "0\(total)"
                        self.removeLabelAnimation(label: self.TextCounterRemote)
                         self.addRollBackLaughAnimation(removedOnCompletion: true, MaxXLaugh: self.LaughAnimationPosition.0, MaxYLaugh: self.LaughAnimationPosition.1, MaxXText: self.LaughAnimationPosition.2, MaxYText: self.LaughAnimationPosition.3, completion: { _ in
                             self.removeUntitledAnimation()
                               })
                         self.LaughFinishedRemote()
                       
                    }
                    
                    
             
               
            }
        }else{
             total += 1
            self.someoneDidLaughRemote()
            self.LaughAnimationPosition = self.addUntitledAnimation ()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                
                    self.addLabelScaleAnimation(label: self.TextCounterRemote) { _  in
                        
                        self.TextCounterRemote.text = "\(total)"
                        self.removeLabelAnimation(label: self.TextCounterRemote)
                        self.addRollBackLaughAnimation(removedOnCompletion: true, MaxXLaugh: self.LaughAnimationPosition.0, MaxYLaugh: self.LaughAnimationPosition.1, MaxXText: self.LaughAnimationPosition.2, MaxYText: self.LaughAnimationPosition.3, completion: { _ in
                            self.removeUntitledAnimation()
                              })
                        self.LaughFinishedRemote()
                        
                    }
                    
                    
              
            }
        }
    }
    func changeLife(){
        var total = Int(self.TextCounterLocal.text!)!
        if total < 10 {
            total += 1
            self.addLabelScaleAnimation(label: self.TextCounterLocal) { _  in
                self.TextCounterLocal.text = "0\(total)"
                self.removeLabelAnimation(label: self.TextCounterLocal)
            }
        }else{
             total += 1
            self.addLabelScaleAnimation(label: self.TextCounterLocal) { _  in
                 self.TextCounterLocal.text = "\(total)"
                self.removeLabelAnimation(label: self.TextCounterLocal)
            }
        }
    }
    //JAnus Game Room Controller/////////////////////////////////////////////////
    func videoView(_ videoView: RTCEAGLVideoView, didChangeVideoSize size: CGSize) {
    var rect: CGRect = videoView.frame
    rect.size = size
    //print(String(format: "========didChangeVideoSize %fx%f", size.width, size.height))
    for layer in videoView.subviews {
    //print(layer)
    layer.layer.contentsGravity = .resizeAspect
    }
    videoView.contentMode = .redraw
    
    }
    
    func onPublisherJoined(_ handleId: NSNumber!) {
        offerPeerConnection(handleId)
    }
    func onPublisherRemoteJsep(_ handleId: NSNumber!, dict jsep: [AnyHashable : Any]!) {
        let jc: JanusConnection = peerConnectionDict[handleId] as! JanusConnection
        let answerDescription = RTCSessionDescription(fromJSONDictionary: jsep)
        print("jsep : ",jsep)
        
        jc.connection.setRemoteDescription(answerDescription!, completionHandler: { error in
        })
    }
    
    func subscriberHandleRemoteJsep(_ handleId: NSNumber!, dict jsep: [AnyHashable : Any]!) {
        let peerConnection: RTCPeerConnection? = createPeerConnection()
        let jc = JanusConnection()
        jc.connection = peerConnection
        jc.handleId = handleId
        peerConnectionDict[handleId] = jc
        let answerDescription = RTCSessionDescription(fromJSONDictionary: jsep)
        peerConnection?.setRemoteDescription(answerDescription!, completionHandler: { error in
        })
        let mandatoryConstraints = ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        peerConnection?.answer(for: constraints, completionHandler: { sdp, error in
            peerConnection?.setLocalDescription(sdp!, completionHandler: { error in
            })
            self.websocket!.subscriberCreateAnswer(handleId, sdp: sdp, number: self.roomId)
        })
        
    }
    
    func onLeaving(_ handleId: NSNumber!) {
        let jc: JanusConnection? = peerConnectionDict[handleId] as? JanusConnection
        jc?.connection.close()
        jc?.connection = nil
        var videoTrack: RTCVideoTrack? = jc?.videoTrack
        videoTrack?.remove((jc?.videoView)!)
        videoTrack = nil
        peerConnectionDict.removeValue(forKey: handleId)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        //here we will set our messages for the user where maybe the janus connection will be lost
        if stateChanged == .stable {
            
        }else if stateChanged == .closed {
            self.timerProba?.invalidate()
            self.timerProba = nil
            self.openDisconnected(userDecision: false)
        }
    }
     var OneTimePlease : Bool = true
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        var janusConnection: JanusConnection?
        for key in peerConnectionDict.keys {
            var jc: JanusConnection? = nil
            
            jc = peerConnectionDict[key] as? JanusConnection
            if peerConnection == jc?.connection {
                janusConnection = jc
                break
            }
        }
        
        DispatchQueue.main.async(execute: {
            //print("Stream Count: ",stream.videoTracks.count)
            if stream.videoTracks.count != 0 {
                let remoteVideoTrack = stream.videoTracks[0]
                guard let frame = self.createRemoteView()?.frame else {
                    print("Remote frame Error")
                    return
                }
                let remoteView: RTCEAGLVideoView? = self.createRemoteView()
                
                if let remoteView = remoteView {
                    remoteVideoTrack.add(remoteView)
                }
                janusConnection?.videoTrack = remoteVideoTrack
                janusConnection?.videoView = remoteView
                self.RemoteUserView.frame = frame
                NotificationCenter.default.post(name: NSNotification.Name(rawValue : "NSNotifMediaBegin"), object: nil)
                
                self.isControllerWorking = true
                //self.SetSessionPlayerOn()
            }
        })
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        //print("=========didRemoveStream")
        
    }
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
       // print("=========didGenerateIceCandidate==\(candidate.sdp)")
        var handleId: NSNumber?
        if peerConnectionDict.count != 0 {
        for key in peerConnectionDict.keys {
            var jc: JanusConnection? = nil
            jc = peerConnectionDict[key] as? JanusConnection
            if peerConnection == jc?.connection {
                handleId = jc?.handleId
                break
            }
        }
        if candidate != nil {
            websocket!.trickleCandidate(handleId, candidate: candidate)
        } else {
            websocket!.trickleCandidateComplete(handleId)
        }
        }else{
            self.YesLeaveAction(UIButton())
        }
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        //print("=========didRemoveIceCandidates")
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    func createRemoteView() -> RTCEAGLVideoView? {
        return self.RemoteUserView
    }
    func createPublisherPeerConnection() {
        publisherPeerConnection = createPeerConnection()
        _ = createAudioSender(publisherPeerConnection)
        _ = createVideoSender(publisherPeerConnection)
        
    }
    func defaultPeerConnectionConstraints() -> RTCMediaConstraints? {
        let optionalConstraints = ["DtlsSrtpKeyAgreement": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: optionalConstraints)
        return constraints
    }
    func defaultSTUNServer() -> [RTCIceServer]?{
     
        let array = ["turn:numb.viagenie.ca"]
     
        let turn = RTCIceServer(urlStrings: array, username: "webrtc@live.com", credential: "muazkh")
        let arrayStun = ["stun:stun.l.google.com:19302?transport=udp"]
        let stun = RTCIceServer(urlStrings: arrayStun)
        return [stun,turn]
       
    }
    func createPeerConnection() -> RTCPeerConnection? {
        let constraints: RTCMediaConstraints? = defaultPeerConnectionConstraints()
        let config = RTCConfiguration()
        let iceServers : NSMutableArray =  NSMutableArray(array: defaultSTUNServer()!)
        config.iceServers = iceServers as! [RTCIceServer]
        config.iceTransportPolicy = .all
        print("Bundle Policy : ",config.bundlePolicy == .balanced)
        print("Bundle Policy : ",config.bundlePolicy == .maxBundle)
        print("Bundle Policy : ",config.bundlePolicy == .maxCompat)

        let peerConnection: RTCPeerConnection? = factory!.peerConnection(with: config, constraints: constraints!, delegate: self)
        return peerConnection
    }
    func offerPeerConnection(_ handleId: NSNumber?) {
        createPublisherPeerConnection()
        let jc = JanusConnection()
        jc.connection = publisherPeerConnection
        jc.handleId = handleId
        if let handleId = handleId {
            peerConnectionDict[handleId] = jc
        }
        guard let publisherPeerConnection = publisherPeerConnection else{
            //print("this one")
            return
        }
        guard let defaultOffer = self.defaultOfferConstraints() else {
            //print("this")
            return
        }
        
        publisherPeerConnection.offer(for: defaultOffer , completionHandler: { sdp, error in
            self.sdpGlobal = sdp
            self.publisherPeerConnection!.setLocalDescription(sdp!, completionHandler: { error in
                
                self.websocket!.publisherCreateOffer(handleId, sdp: sdp, path: self.PathMaser)
                ScriptBase.sharedInstance.pathVideoJanus = self.PathMaser
            })
        })
    }
    var sdpGlobal : RTCSessionDescription?
    func startRecording(_ path : String) {
        var handleId: NSNumber?
        let peerConnection = self.publisherPeerConnection
               if peerConnectionDict.count != 0 {
               for key in peerConnectionDict.keys {
                   var jc: JanusConnection? = nil
                   jc = peerConnectionDict[key] as? JanusConnection
                   if peerConnection == jc?.connection {
                       handleId = jc?.handleId
                       break
                   }
               }
              
            }
        print("start Recording : ",peerConnectionDict.count)
       
        self.websocket!.startRecording(handleId, path: path,sdp:sdpGlobal )
    }
    func defaultMediaAudioConstraints() -> RTCMediaConstraints? {
        //let mandatoryConstraints = [kRTCMediaConstraintsLevelControl: kRTCMediaConstraintsValueFalse]
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        return constraints
    }
    func defaultOfferConstraints() -> RTCMediaConstraints? {
        let mandatoryConstraints = ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constraints
    }
    func createLocalAudioTrack() -> RTCAudioTrack? {
        let constraints: RTCMediaConstraints? = defaultMediaAudioConstraints()
        let source: RTCAudioSource? = factory!.audioSource(with: constraints)
        let track: RTCAudioTrack? = factory!.audioTrack(with: source!, trackId: kARDAudioTrackId)
        return track
    }
    func createAudioSender(_ peerConnection: RTCPeerConnection?) -> RTCRtpSender? {
        let sender: RTCRtpSender? = peerConnection?.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: kARDMediaStreamId)
        if (localAudioTrack != nil) {
            sender?.track = localAudioTrack
        }
        
        return sender
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd rtpReceiver: RTCRtpReceiver, streams mediaStreams: [RTCMediaStream]) {
        print("****************************************")
        print("Adding Stream")
        print("rtpReceiver : ",rtpReceiver.parameters )
        print("mediaStreams: ",mediaStreams.count)
        if mediaStreams.count == 1 {
            if (mediaStreams[0].audioTracks.count == 1 &&  mediaStreams[0].videoTracks.count == 1){
                print("re-routing audio")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.3) {
                      self.SetSessionPlayerOn()
                }
              
                //JBSoundRouter.routeSound(route: JBSoundRoute.Speaker)
            }
        }
        print("****************************************")
    }
    //Local Camera Flux Creation
      let sampleQueue = DispatchQueue(label: "com.bouzid.zonzay.facedetection", attributes: [])
    var  localVideoTrack: RTCVideoTrack?
    weak var externalVideoBufferDelegate : AVCaptureVideoDataOutputSampleBufferDelegate?
    func createLocalVideoTrack() -> RTCVideoTrack? {
        //let cameraConstraints = RTCMediaConstraints(mandatoryConstraints: currentMediaConstraint() as? [String : String], optionalConstraints: nil)
        
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],mediaType: AVMediaType.video,position: .front)
        var cameraId = ""
        for device in deviceDescoverySession.devices {
            if device.position == .front {
                cameraId = device.uniqueID
            }
        }
        let source  = factory?.videoSource()
        localVideoTrack = factory!.videoTrack(with: source!, trackId: kARDVideoTrackId)
        capturer = RTCCameraVideoCapturer(delegate: source!)
        
        LocalUserView!.captureSession =  capturer?.captureSession
        let captureDevice = AVCaptureDevice.init(uniqueID: cameraId)
        
        capturer!.startCapture(with: captureDevice!, format: self.selectFormat(for: captureDevice!, capturer: capturer!)!, fps: 30) { (error) in
        
            for outputs in self.LocalUserView.captureSession.outputs {
                print("OUTPUTTTT: ",outputs)
                
                let test = (outputs as! AVCaptureVideoDataOutput)
                
                self.externalVideoBufferDelegate = test.sampleBufferDelegate
                test.setSampleBufferDelegate(self, queue: self.sampleQueue)
                print("videoSetting : ",test.videoSettings)
                test.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_32BGRA  as UInt32)]
                
            }
        }
        //let source: RTCAVFoundationVideoSource? = factory!.avFoundationVideoSource(with: cameraConstraints)
       
       
        
        #if !targetEnvironment(simulator)
        //this is Real device running
        //localVideoTrack?.add(self)
        #endif
        
        
       
    /*     if #available(iOS 13.0, *) {
        LocalUserView!.captureSession.beginConfiguration()
               //source?.adaptOutputFormat(toWidth: 480, height: 640, fps: 30)
        if ( LocalUserView!.captureSession.canSetSessionPreset(AVCaptureSession.Preset.vga640x480)) {
                   print("yes we can")
                    LocalUserView!.captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
               }else{
                   print("yes we can't")
               }
        LocalUserView!.captureSession.commitConfiguration()
       /* let backgroundQueue = DispatchQueue(label: "com.app.videoquality")
        backgroundQueue.async(execute: {
            source?.captureSession.beginConfiguration()
            self.LocalUserView!.captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
            source?.captureSession.commitConfiguration()
        }) */
        
        //LocalUserView.captureSession.beginConfiguration()
        for i in 0...10000 {
            
        }
        } */
        /*print("LocalUserView : ",LocalUserView.captureSession)
        for outputs in LocalUserView.captureSession.outputs {
            print("OUTPUTTTT: ",outputs)
            
            let test = (outputs as! AVCaptureVideoDataOutput)
            
            externalVideoBufferDelegate = test.sampleBufferDelegate
            test.setSampleBufferDelegate(self, queue: sampleQueue)
            print("videoSetting : ",test.videoSettings)
            test.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_32BGRA  as UInt32)]
            
        } */
        let previewLayer = (LocalUserView!.layer as! AVCaptureVideoPreviewLayer)
        previewLayer.videoGravity = AVLayerVideoGravity.resize
        let frame = self.LocalUserView.frame
        previewLayer.frame.size = CGSize(width: frame.size.width, height: self.LocalUserView.bounds.height)
        //localView.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
        //LocalUserView.captureSession.commitConfiguration()
        //localView!.captureSession.
        return localVideoTrack
    }
    func selectFormat(for device: AVCaptureDevice,capturer:RTCCameraVideoCapturer) -> AVCaptureDevice.Format? {
        let formats = RTCCameraVideoCapturer.supportedFormats(for: device)
        let targetWidth = 640
        let targetHeight = 480
        var selectedFormat: AVCaptureDevice.Format? = nil
        var currentDiff = Int(Int32.max)
       
        for format in formats {
            print("Format: ",format)
            let dimension = CMVideoFormatDescriptionGetDimensions(format.formatDescription)
            let pixelFormat = CMFormatDescriptionGetMediaSubType(format.formatDescription)
            let diff = abs(targetWidth - Int(dimension.width)) + abs(targetHeight - Int(dimension.height))
            if diff < currentDiff {
                selectedFormat = format
                currentDiff = diff
            } else if diff == currentDiff && pixelFormat == capturer.preferredOutputPixelFormat() {
                selectedFormat = format
            }
        }

        return selectedFormat
    }
    func createVideoSender(_ peerConnection: RTCPeerConnection?) -> RTCRtpSender? {
        let sender: RTCRtpSender? = peerConnection?.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: kARDMediaStreamId)
        
        if (localTrack != nil) {
            //print("localTrack != nil")
            sender?.track = localTrack
          
        }else{
            //print("localTrack == nil")
        }
        return sender
    }
      var animateNoFace = true
      var stopWorking = false
    func currentMediaConstraint() -> [AnyHashable : Any]? {
        var mediaConstraintsDictionary: [AnyHashable : Any]? = nil
        //print(width.description)
        let widthConstraint = "640"
        let heightConstraint = "480"
        let frameRateConstrait = "15"
        if widthConstraint != "" && heightConstraint != "" {
            mediaConstraintsDictionary = [kRTCMediaConstraintsMinWidth: "0", kRTCMediaConstraintsMaxWidth: widthConstraint, kRTCMediaConstraintsMinHeight: "0", kRTCMediaConstraintsMaxHeight: heightConstraint, kRTCMediaConstraintsMaxFrameRate: frameRateConstrait]
        }
        return mediaConstraintsDictionary
    }
    ///////////////////////////////////////////////////////////////////////////
    func SetEarSpeackerOn(){
        do{
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [])
            
        }catch{
            
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        }catch {
            
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        }catch{
            
        }
    }
    func SetSessionPlayerOn(){
        do {
            //print("Category Play & Record")
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [])
        }catch{
            //print("PlayAndRecord: ",error.localizedDescription )
        }
        do {
              // print("Category Set ACtive")
            try AVAudioSession.sharedInstance().setActive(true)
        }catch{
           // print("setActive: ",error.localizedDescription )
        }
        do {
               //print("Category Speaker Done")
            if AVAudioSession.sharedInstance().isHeadphonesConnected == false {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
            }else{
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
            }
        }catch{
            //print("overrideOutputAudioPort: ",error.localizedDescription )
        }
    }
    func SetSessionPlayerOff(){
        do{
            try AVAudioSession.sharedInstance().setActive(false)
        }catch{
            
        }
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        //print("BOTTOM")
        self.setContentOffset(bottomOffset, animated: animated)
    }
    func scrollToTop(animated: Bool) {
        
        self.setContentOffset(.zero, animated: animated)
        self.delegate?.scrollViewDidScrollToTop?(self)
    }
}
extension VideoGameRework {
    // - MARK: Untitled Animation
    
    func addUntitledAnimation() -> (Float,Float,Float,Float) {
       let result =  addUntitledAnimation(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: nil)
        return(result.0,result.1,result.2,result.3)
    }
    
    func addUntitledAnimation(completion: ((Bool) -> Void)?) {
        _ = addUntitledAnimation(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    
    func addUntitledAnimation(removedOnCompletion: Bool) {
        _ = addUntitledAnimation(beginTime: 0, fillMode: removedOnCompletion ? CAMediaTimingFillMode.removed.rawValue : CAMediaTimingFillMode.both.rawValue, removedOnCompletion: removedOnCompletion, completion: nil)
    }
    
    func addUntitledAnimation(removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        _ = addUntitledAnimation(beginTime: 0, fillMode: removedOnCompletion ? CAMediaTimingFillMode.removed.rawValue : CAMediaTimingFillMode.both.rawValue, removedOnCompletion: removedOnCompletion, completion: completion)
    }
    func addRollBackLaughAnimation(removedOnCompletion:Bool,MaxXLaugh:Float,MaxYLaugh: Float,MaxXText:Float,MaxYText:Float, completion: ((Bool) ->
        Void)?) {
        RollBackAnimationLaugh(beginTime: 0, fillMode: removedOnCompletion ? CAMediaTimingFillMode.removed.rawValue : CAMediaTimingFillMode.both.rawValue, removedOnCompletion: removedOnCompletion, MaxXLaugh: MaxXLaugh, MaxYLaugh: MaxYLaugh, MaxXText: MaxXText, MaxYText: MaxYText, completion: completion)
    }
    func addUntitledAnimation(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) ->(Float,Float,Float,Float) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "LaughAnimation")
            representativeAnimation.duration = 0.600
            representativeAnimation.delegate = self
            self.LaughImageView.layer.add(representativeAnimation, forKey: "UntitledAnimation")
            self.animationCompletions[ self.LaughImageView.layer.animation(forKey: "UntitledAnimation")!] = complete
            let representativeAnimationTwo = CABasicAnimation(keyPath: "TextAnimation")
            representativeAnimationTwo.duration = 0.600
            representativeAnimationTwo.delegate = self
            self.TextCounterRemote.layer.add(representativeAnimationTwo, forKey: "TextAnimation")
            self.animationCompletions[self.TextCounterLocal.layer.animation(forKey: "TextAnimation")!] = complete
        }
        
        let jokeBigScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        jokeBigScaleXAnimation.duration = 0.600
        jokeBigScaleXAnimation.values = [1.000, 2.000] as [Float]
        jokeBigScaleXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigScaleXAnimation.timingFunctions = [linearTiming]
        jokeBigScaleXAnimation.beginTime = beginTime
        jokeBigScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
         self.LaughImageView.layer.add(jokeBigScaleXAnimation, forKey:"Untitled Animation_ScaleX")
        
        let jokeBigScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        jokeBigScaleYAnimation.duration = 0.600
        jokeBigScaleYAnimation.values = [1.000, 2.000] as [Float]
        jokeBigScaleYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigScaleYAnimation.timingFunctions = [linearTiming]
        jokeBigScaleYAnimation.beginTime = beginTime
        jokeBigScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
         self.LaughImageView.layer.add(jokeBigScaleYAnimation, forKey:"Untitled Animation_ScaleY")
        
        let jokeBigTranslationXAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        jokeBigTranslationXAnimation.duration = 0.600
        let NewWidth = self.LaughImageView.frame.width + (self.LaughImageView.frame.width * 0.360)
        let MaxX = self.RemoteUserView.frame.width / 2 - (NewWidth / 2) - 47
        
        jokeBigTranslationXAnimation.values = [0.000, Float(MaxX)] as [Float]
        jokeBigTranslationXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigTranslationXAnimation.timingFunctions = [linearTiming]
        jokeBigTranslationXAnimation.beginTime = beginTime
        jokeBigTranslationXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigTranslationXAnimation.isRemovedOnCompletion = removedOnCompletion
         self.LaughImageView.layer.add(jokeBigTranslationXAnimation, forKey:"Untitled Animation_TranslationX")
        let jokeBigTranslationYAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        jokeBigTranslationYAnimation.duration = 0.600
        
        let newHeight = self.LaughImageView.frame.height + (self.LaughImageView.frame.height * 0.180)
        let MaxY = self.RemoteUserView.frame.height / 2 - ( newHeight / 2)
        
        jokeBigTranslationYAnimation.values = [0.000, Float(MaxY)] as [Float]
        jokeBigTranslationYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigTranslationYAnimation.timingFunctions = [linearTiming]
        jokeBigTranslationYAnimation.beginTime = beginTime
        jokeBigTranslationYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigTranslationYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.LaughImageView.layer.add(jokeBigTranslationYAnimation, forKey:"Untitled Animation_TranslationY")
        
        let textElementScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        textElementScaleXAnimation.duration = 0.600
        textElementScaleXAnimation.values = [1.0, 2.0] as [Float]
        textElementScaleXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementScaleXAnimation.timingFunctions = [linearTiming]
        textElementScaleXAnimation.beginTime = beginTime
        textElementScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        
       self.TextCounterRemote.layer.add(textElementScaleXAnimation, forKey:"Untitled Animation_ScaleX")
        
        let textElementScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        textElementScaleYAnimation.duration = 0.600
        textElementScaleYAnimation.values = [1.0, 2.0] as [Float]
        textElementScaleYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementScaleYAnimation.timingFunctions = [linearTiming]
        textElementScaleYAnimation.beginTime = beginTime
        textElementScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.TextCounterRemote.layer.add(textElementScaleYAnimation, forKey:"Untitled Animation_ScaleY")
        
        let textElementTranslationXAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        textElementTranslationXAnimation.duration = 0.600
        let MaxXText = MaxX + NewWidth + 6
        let MaxYText = ((MaxY ) )
        textElementTranslationXAnimation.values = [0.000, Float(MaxXText)] as [Float]
        textElementTranslationXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementTranslationXAnimation.timingFunctions = [linearTiming]
        textElementTranslationXAnimation.beginTime = beginTime
        textElementTranslationXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementTranslationXAnimation.isRemovedOnCompletion = removedOnCompletion
       self.TextCounterRemote.layer.add(textElementTranslationXAnimation, forKey:"Untitled Animation_TranslationX")
        
        let textElementTranslationYAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        textElementTranslationYAnimation.duration = 0.600
        textElementTranslationYAnimation.values = [0.000, Float(MaxYText)] as [Float]
        textElementTranslationYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementTranslationYAnimation.timingFunctions = [linearTiming]
        textElementTranslationYAnimation.beginTime = beginTime
        textElementTranslationYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementTranslationYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.TextCounterRemote.layer.add(textElementTranslationYAnimation, forKey:"Untitled Animation_TranslationY")
        UIView.transition(with: self.TextCounterLocal, duration: 0.6, options: .transitionCrossDissolve, animations: {
            self.TextCounterRemote.font = UIFont(name: "FingerPaint-Regular", size: 26)
        }) { finished in
            
        }
        return (Float(MaxX),Float(MaxY),Float(MaxXText),Float(MaxYText))
    }
    func RollBackAnimationLaugh(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool,MaxXLaugh: Float,MaxYLaugh:Float,MaxXText:Float,MaxYText:Float, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "LaughAnimationRoll")
            representativeAnimation.duration = 0.600
            representativeAnimation.delegate = self
            self.LaughImageView.layer.add(representativeAnimation, forKey: "UntitledAnimationRoll")
            self.animationCompletions[ self.LaughImageView.layer.animation(forKey: "UntitledAnimationRoll")!] = complete
            let representativeAnimationTwo = CABasicAnimation(keyPath: "TextAnimationRoll")
            representativeAnimationTwo.duration = 0.600
            representativeAnimationTwo.delegate = self
            self.TextCounterRemote.layer.add(representativeAnimationTwo, forKey: "TextAnimationRoll")
            self.animationCompletions[self.TextCounterRemote.layer.animation(forKey: "TextAnimationRoll")!] = complete
        }
        
        let jokeBigScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        jokeBigScaleXAnimation.duration = 0.600
        jokeBigScaleXAnimation.values = [2.000, 1.000] as [Float]
        jokeBigScaleXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigScaleXAnimation.timingFunctions = [linearTiming]
        jokeBigScaleXAnimation.beginTime = beginTime
        jokeBigScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        self.LaughImageView.layer.add(jokeBigScaleXAnimation, forKey:"Untitled Animation_ScaleXRoll")
        
        let jokeBigScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        jokeBigScaleYAnimation.duration = 0.600
        jokeBigScaleYAnimation.values = [2.000, 1.000] as [Float]
        jokeBigScaleYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigScaleYAnimation.timingFunctions = [linearTiming]
        jokeBigScaleYAnimation.beginTime = beginTime
        jokeBigScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.LaughImageView.layer.add(jokeBigScaleYAnimation, forKey:"Untitled Animation_ScaleYRoll")
        
        let jokeBigTranslationXAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        jokeBigTranslationXAnimation.duration = 0.600
        jokeBigTranslationXAnimation.values = [MaxXLaugh,0.000] as [Float]
        jokeBigTranslationXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigTranslationXAnimation.timingFunctions = [linearTiming]
        jokeBigTranslationXAnimation.beginTime = beginTime
        jokeBigTranslationXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigTranslationXAnimation.isRemovedOnCompletion = removedOnCompletion
        self.LaughImageView.layer.add(jokeBigTranslationXAnimation, forKey:"Untitled Animation_TranslationXRoll")
        
        let jokeBigTranslationYAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        jokeBigTranslationYAnimation.duration = 0.600
        jokeBigTranslationYAnimation.values = [MaxYLaugh, 0.000] as [Float]
        jokeBigTranslationYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        jokeBigTranslationYAnimation.timingFunctions = [linearTiming]
        jokeBigTranslationYAnimation.beginTime = beginTime
        jokeBigTranslationYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        jokeBigTranslationYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.LaughImageView.layer.add(jokeBigTranslationYAnimation, forKey:"Untitled Animation_TranslationYRoll")
        
        let textElementScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        textElementScaleXAnimation.duration = 0.600
        textElementScaleXAnimation.values = [2.0, 1.0] as [Float]
        textElementScaleXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementScaleXAnimation.timingFunctions = [linearTiming]
        textElementScaleXAnimation.beginTime = beginTime
        textElementScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        
        self.TextCounterRemote.layer.add(textElementScaleXAnimation, forKey:"Untitled Animation_ScaleXRoll")
        
        let textElementScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        textElementScaleYAnimation.duration = 0.600
        textElementScaleYAnimation.values = [2.0, 1.0] as [Float]
        textElementScaleYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementScaleYAnimation.timingFunctions = [linearTiming]
        textElementScaleYAnimation.beginTime = beginTime
        textElementScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.TextCounterRemote.layer.add(textElementScaleYAnimation, forKey:"Untitled Animation_ScaleYRoll")
        
        let textElementTranslationXAnimation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        textElementTranslationXAnimation.duration = 0.600
        textElementTranslationXAnimation.values = [MaxXText,0.000] as [Float]
        textElementTranslationXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementTranslationXAnimation.timingFunctions = [linearTiming]
        textElementTranslationXAnimation.beginTime = beginTime
        textElementTranslationXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementTranslationXAnimation.isRemovedOnCompletion = removedOnCompletion
        self.TextCounterRemote.layer.add(textElementTranslationXAnimation, forKey:"Untitled Animation_TranslationXRoll")
        let textElementTranslationYAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        textElementTranslationYAnimation.duration = 0.600
        textElementTranslationYAnimation.values = [MaxYText,0.000] as [Float]
        textElementTranslationYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        textElementTranslationYAnimation.timingFunctions = [linearTiming]
        textElementTranslationYAnimation.beginTime = beginTime
        textElementTranslationYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        textElementTranslationYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.TextCounterRemote.layer.add(textElementTranslationYAnimation, forKey:"Untitled Animation_TranslationYRoll")
        UIView.transition(with: self.TextCounterLocal, duration: 0.6, options: .transitionCrossDissolve, animations: {
            self.TextCounterRemote.font = UIFont(name: "FingerPaint-Regular", size: 17)
        }) { finished in
            
        }
      
    }
    func removeUntitledAnimation() {
        self.LaughImageView.layer.removeAnimation(forKey: "LaughAnimation")
        self.LaughImageView.layer.removeAnimation(forKey: "LaughAnimationRoll")
        self.TextCounterRemote.layer.removeAnimation(forKey: "TextAnimation")
        self.TextCounterRemote.layer.removeAnimation(forKey: "TextAnimationRoll")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_ScaleX")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_ScaleY")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_ScaleXRoll")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_ScaleYRoll")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_TranslationX")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_TranslationY")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_TranslationXRoll")
        self.LaughImageView.layer.removeAnimation(forKey: "Untitled Animation_TranslationYRoll")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_ScaleX")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_ScaleY")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_ScaleXRoll")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_ScaleYRoll")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_TranslationX")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_TranslationY")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_TranslationXRoll")
        self.TextCounterRemote.layer.removeAnimation(forKey: "Untitled Animation_TranslationYRoll")
    }
    
    // MARK: CAAnimationDelegate
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let completion = self.animationCompletions[anim] {
            self.animationCompletions.removeValue(forKey: anim)
            completion(flag)
        }
    }
    
    func removeAllAnimations() {
        for subview in viewsByName.values {
            subview.layer.removeAllAnimations()
        }
        //self.layer.removeAnimation(forKey: "UntitledAnimation")
    }
}
extension VideoGameRework {
    func addClockRotationAnimation(completion: ((Bool) -> Void)?) {
        addClockRotationAnimation(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addClockRotationAnimationRemote(completion: ((Bool) -> Void)?) {
    addClockRotationAnimationRemote(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addClockRotationAnimationRemote(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?){
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if self.ScorpionWise != nil {
        self.ScorpionWise.layer.anchorPoint = CGPoint(x:0.5, y:0.84)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.keyRemote")
            representativeAnimation.duration = 10.000
            representativeAnimation.delegate = self
            
            self.ScorpionWise.layer.add(representativeAnimation, forKey: "UntitledAnimationClockRemote")
            self.animationCompletions[self.ScorpionWise.layer.animation(forKey: "UntitledAnimationClockRemote")!] = complete
        }
        
        let scorpionClockRotationAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        scorpionClockRotationAnimation.duration = 10.000
        scorpionClockRotationAnimation.values = [0.000, 6.283] as [Float]
        scorpionClockRotationAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        scorpionClockRotationAnimation.timingFunctions = [linearTiming]
        scorpionClockRotationAnimation.beginTime = beginTime
        scorpionClockRotationAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        scorpionClockRotationAnimation.isRemovedOnCompletion = removedOnCompletion
        self.ScorpionWise.layer.add(scorpionClockRotationAnimation, forKey:"Untitled Animation_RotationClockRemote")
        }
    }
    func addClockRotationAnimation(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        self.ScorpionWiseLocal.layer.anchorPoint = CGPoint(x:0.5, y:0.84)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.key")
            representativeAnimation.duration = 7.200
            representativeAnimation.delegate = self
            
            self.ScorpionWiseLocal.layer.add(representativeAnimation, forKey: "UntitledAnimationClock")
            self.animationCompletions[self.ScorpionWiseLocal.layer.animation(forKey: "UntitledAnimationClock")!] = complete
        }
        
        let scorpionClockRotationAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        scorpionClockRotationAnimation.duration = 7.200
        scorpionClockRotationAnimation.values = [0.000, 6.283] as [Float]
        scorpionClockRotationAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        scorpionClockRotationAnimation.timingFunctions = [linearTiming]
        scorpionClockRotationAnimation.beginTime = beginTime
        scorpionClockRotationAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        scorpionClockRotationAnimation.isRemovedOnCompletion = removedOnCompletion
        self.ScorpionWiseLocal.layer.add(scorpionClockRotationAnimation, forKey:"Untitled Animation_RotationClock")
    }
    func removeClockRotationAnimation() {
        self.ScorpionWiseLocal.layer.removeAnimation(forKey: "UntitledAnimationClock")
       self.ScorpionWiseLocal.layer.removeAnimation(forKey: "Untitled Animation_Rotation")
    }
    func removeClockRotationRemoteAnimation(){
        self.ScorpionWise.layer.removeAnimation(forKey: "UntitledAnimationClockRemote")
        self.ScorpionWise.layer.removeAnimation(forKey: "Untitled Animation_RotationRemote")
    }
    func removeAllAnimationsClock() {
        if self.ScorpionWiseLocal != nil {
        self.ScorpionWiseLocal.layer.removeAllAnimations()
        self.ClockWithWiseLocal.layer.removeAllAnimations()
        }
    }
    
}
extension VideoGameRework {
    func addClockScaleAnimation(completion: ((Bool) -> Void)?) {
        addClockScaleAnimation(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addClockScaleAnimation(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.key")
            representativeAnimation.duration = 0.600
            representativeAnimation.delegate = self
            
           self.ClockWithWiseLocal.layer.add(representativeAnimation, forKey: "UntitledAnimationClockScale")
            self.animationCompletions[self.ClockWithWiseLocal.layer.animation(forKey: "UntitledAnimationClockScale")!] = complete
        }
        
        let untitledScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        untitledScaleXAnimation.duration = 0.600
        untitledScaleXAnimation.values = [1.000, 3.5] as [Float]
        untitledScaleXAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        untitledScaleXAnimation.timingFunctions = [linearTiming]
        untitledScaleXAnimation.beginTime = beginTime
        untitledScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        self.ClockWithWiseLocal.layer.add(untitledScaleXAnimation, forKey:"Untitled Animation_ScaleXScale")
        
        let untitledScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        untitledScaleYAnimation.duration = 0.600
        untitledScaleYAnimation.values = [1.000, 3.5] as [Float]
        untitledScaleYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        untitledScaleYAnimation.timingFunctions = [linearTiming]
        untitledScaleYAnimation.beginTime = beginTime
        untitledScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        self.ClockWithWiseLocal.layer.add(untitledScaleYAnimation, forKey:"Untitled Animation_ScaleYScale")
        
        let untitledTranslationYAnimation = CAKeyframeAnimation(keyPath: "transform.translation.y")
        untitledTranslationYAnimation.duration = 0.600
        untitledTranslationYAnimation.values = [0.000, 100.000] as [Float]
        untitledTranslationYAnimation.keyTimes = [0.000, 1.000] as [NSNumber]
        untitledTranslationYAnimation.timingFunctions = [linearTiming]
        untitledTranslationYAnimation.beginTime = beginTime
        untitledTranslationYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledTranslationYAnimation.isRemovedOnCompletion = removedOnCompletion
       self.ClockWithWiseLocal.layer.add(untitledTranslationYAnimation, forKey:"Untitled Animation_TranslationYScale")
    }
    func addViewScaleAnimation(view: UIView,completion: ((Bool) -> Void)?) {
        addViewScaleAnimation(view:view,beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addViewScaleAnimation(view:UIView,beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.keyView")
            representativeAnimation.duration = 0.600
            representativeAnimation.delegate = self
            
            view.layer.add(representativeAnimation, forKey: "UntitledAnimationViewScale")
            self.animationCompletions[view.layer.animation(forKey: "UntitledAnimationViewScale")!] = complete
        }
        
        let untitledScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        untitledScaleXAnimation.duration = 0.600
        untitledScaleXAnimation.values = [1.000, 1.200,1.200,1.000] as [Float]
        untitledScaleXAnimation.keyTimes = [0.000, 0.497, 0.500, 1.000] as [NSNumber]
        untitledScaleXAnimation.timingFunctions = [linearTiming]
        untitledScaleXAnimation.beginTime = beginTime
        untitledScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        view.layer.add(untitledScaleXAnimation, forKey:"Untitled AnimationView_ScaleXScale")
        
        let untitledScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        untitledScaleYAnimation.duration = 0.600
        untitledScaleYAnimation.values = [1.000, 1.200,1.200,1.000] as [Float]
        untitledScaleYAnimation.keyTimes = [0.000, 0.497, 0.500, 1.000] as [NSNumber]
        untitledScaleYAnimation.timingFunctions = [linearTiming]
        untitledScaleYAnimation.beginTime = beginTime
        untitledScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        view.layer.add(untitledScaleYAnimation, forKey:"Untitled AnimationView_ScaleYScale")
        
    }
    func removeViewAnimation(view:UIView){
        view.layer.removeAllAnimations()
    }
    func addLabelScaleAnimation(label: UILabel,completion: ((Bool) -> Void)?) {
        addLabelScaleAnimation(label:label,beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addLabelScaleAnimation(label:UILabel,beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.keyLabel")
            representativeAnimation.duration = 0.600
            representativeAnimation.delegate = self
            
            label.layer.add(representativeAnimation, forKey: "UntitledAnimationLabelScale")
            self.animationCompletions[label.layer.animation(forKey: "UntitledAnimationLabelScale")!] = complete
        }
        
        let untitledScaleXAnimation = CAKeyframeAnimation(keyPath: "transform.scale.x")
        untitledScaleXAnimation.duration = 0.600
        untitledScaleXAnimation.values = [1.000, 2.400,2.400,1.000] as [Float]
        untitledScaleXAnimation.keyTimes = [0.000, 0.497, 0.500, 1.000] as [NSNumber]
        untitledScaleXAnimation.timingFunctions = [linearTiming]
        untitledScaleXAnimation.beginTime = beginTime
        untitledScaleXAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleXAnimation.isRemovedOnCompletion = removedOnCompletion
        label.layer.add(untitledScaleXAnimation, forKey:"Untitled AnimationLabel_ScaleXScale")
        
        let untitledScaleYAnimation = CAKeyframeAnimation(keyPath: "transform.scale.y")
        untitledScaleYAnimation.duration = 0.600
        untitledScaleYAnimation.values = [1.000, 2.400,2.400,1.000] as [Float]
        untitledScaleYAnimation.keyTimes = [0.000, 0.497, 0.500, 1.000] as [NSNumber]
        untitledScaleYAnimation.timingFunctions = [linearTiming]
        untitledScaleYAnimation.beginTime = beginTime
        untitledScaleYAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledScaleYAnimation.isRemovedOnCompletion = removedOnCompletion
        label.layer.add(untitledScaleYAnimation, forKey:"Untitled AnimationLabel_ScaleYScale")
        
    }
    func removeLabelAnimation(label:UILabel){
        label.layer.removeAllAnimations()
    }
    
}
extension VideoGameRework {
    
    func addClockAlarmAnimation(completion: ((Bool) -> Void)?) {
        addClockAlarmAnimation(beginTime: 0, fillMode: CAMediaTimingFillMode.both.rawValue, removedOnCompletion: false, completion: completion)
    }
    func addClockAlarmAnimation(beginTime: CFTimeInterval, fillMode: String, removedOnCompletion: Bool, completion: ((Bool) -> Void)?) {
        let linearTiming = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        if let complete = completion {
            let representativeAnimation = CABasicAnimation(keyPath: "not.a.real.key")
            representativeAnimation.duration = 2.200
            representativeAnimation.delegate = self
             self.ClockWithWiseLocal.layer.add(representativeAnimation, forKey: "UntitledAnimationClockAlarm")
            self.animationCompletions[ self.ClockWithWiseLocal.layer.animation(forKey: "UntitledAnimationClockAlarm")!] = complete
        }
        
        let untitledRotationAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        untitledRotationAnimation.duration = 2.200
        untitledRotationAnimation.values = [0.000, 0.000, 0.611, 0.000, -0.611, 0.000, 0.611, 0.000, -0.611, 0.000] as [Float]
        untitledRotationAnimation.keyTimes = [0.000, 0.273, 0.364, 0.455, 0.545, 0.636, 0.727, 0.818, 0.909, 1.000] as [NSNumber]
        untitledRotationAnimation.timingFunctions = [linearTiming, linearTiming, linearTiming, linearTiming, linearTiming, linearTiming, linearTiming, linearTiming, linearTiming]
        untitledRotationAnimation.beginTime = beginTime
        untitledRotationAnimation.fillMode = CAMediaTimingFillMode(rawValue: fillMode)
        untitledRotationAnimation.isRemovedOnCompletion = removedOnCompletion
        self.ClockWithWiseLocal.layer.add(untitledRotationAnimation, forKey:"Untitled Animation_Rotation")
    }
}
extension AVAudioSession {
    static var isHeadphonesConnected :Bool {
        return sharedInstance().isHeadphonesConnected
    }
    var isHeadphonesConnected : Bool {
        return !currentRoute.outputs.filter{ $0.isHeadphones}.isEmpty
    }
}
extension AVAudioSessionPortDescription {
    var isHeadphones : Bool {
        return portType == AVAudioSession.Port.headphones
    }
}
extension VideoGameRework {
    func showNoFace(){
        
        
        //print("Show No Face")
        
        if self.containerNoFace.isHidden == true {
            DispatchQueue.main.async {
                self.containerNoFace.isHidden = false
                 
                  //print("Will Show No Face")
                 self.NofaceNewLBL.text = Localization("NoFaceNewMessage")
                
                 self.NoFaceNewDesign.isHidden = false
            }
        
        }
     
    }
    func hideNoFace(){
      
        
        if containerNoFace.isHidden == false {
            //print("Hide No Face")
            DispatchQueue.main.async {
                //self.NoFaceNewDesign.layer.removeAllAnimations()
                       UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                           self.NoFaceNewDesign.alpha = 0
                       }) { (verif) in
                           self.NoFaceNewDesign.isHidden = true
                           self.containerNoFace.isHidden = true
                           self.NoFaceNewDesign.alpha = 1
                       }
            }
       
        }
    }
  
    func animateNofaceDetected(){
        self.hideNoFace()
        UIView.animate(withDuration: 0.5, delay: 0.75, options: [.curveEaseInOut], animations: {
            self.NoFaceNewDesign.alpha = 0
        }) { (verify) in
            UIView.animate(withDuration: 0.5, delay: 0.75, options: [.curveEaseInOut], animations: {
                self.NoFaceNewDesign.alpha = 1
            }, completion: { (complete) in
                if self.stopWorking == false {
                self.animateNofaceDetected()
                }
            })
        }
        
    }
    func openLeaveGame(){
        self.ContainerStart.isHidden = false
        self.leaveMSGLBL.text = Localization("LeaveMSG")
        
        self.containerLeave.alpha = 0
        self.containerLeave.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerLeave.alpha = 1
            self.ContainerStart.alpha = 1
        }) { (complete) in
            
        }
    }
    func closeLeaveGame(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerLeave.alpha = 0
        }) { (complete) in
             self.ContainerStart.isHidden = true
        }
    }
   
    func openDisconnected(userDecision : Bool){
        if inDisconnected == false {
            inDisconnected = true
        DispatchQueue.main.async {
            
      
        self.ContainerStart.isHidden = false
        self.stopWorking = true
        SocketIOManager.sharedInstance.closeLifeChangedNotifier()
            
            self.disconnectedMSGLBL.text = userDecision == false ? Localization("DisconnectedMSG") : Localization("DiscconectedDecision")
           
        self.closeFaceDisconnected()
        self.containerDisconnected.alpha = 0
        self.containerDisconnected.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerDisconnected.alpha = 1
               self.ContainerStart.alpha = 1
        }) { (complete) in
            var i = 0
            for key in self.peerConnectionDict.keys {
                var jc: JanusConnection? = nil
                
                jc = self.peerConnectionDict[key] as? JanusConnection
                if i == 0 && self.Owner == "master" {
                    
                    self.onLeaving(jc?.handleId)
                    
                }else if i == 1 && self.Owner == "slave" {
                    self.onLeaving(jc?.handleId)
                }
                i = i + 1
            }
            self.publisherPeerConnection?.close()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
                for v in self.view.subviews {
                    v.removeFromSuperview()
                }
                if self.detectionFaceSleeper != nil {
                self.detectionFaceSleeper.invalidate()
                self.detectionFaceSleeper = nil
                }
                self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
                if self.navigationController?.topViewController == self {
               // self.dismiss(animated: true, completion: nil)
                     SocketIOManager.sharedInstance.reactivateOnFinishGame()
                    if self.timerQ != nil {
                        self.timerQ?.invalidate()
                        self.timerQ = nil
                    }
                    self.soundPlayer.stop()
                    self.navigationController?.popViewController(animated: true)
                }
                //self.navigation?.popToRootViewController(animated: true)
            })
            
        }
              }
        }
    }
    func closeDisconnected(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerDisconnected.alpha = 0
        }) { (complete) in
            self.ContainerStart.isHidden = true
               self.ContainerStart.alpha = 0
        }
    }
 
    func openFaceDisconnected() {
        self.hideNoFace()
        self.ContainerStart.isHidden = false
    
        self.faceDisconnectedLBL.text = Localization("DisconnectedFaceMSG")
        self.faceDisconnectedLBL.lineBreakMode = .byWordWrapping
        self.NoFaceDetectedLBL.text = Localization("NoFaceMSG")
        self.containerFaceDisconnected.alpha = 0
        self.containerFaceDisconnected.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerFaceDisconnected.alpha = 1
           self.ContainerStart.alpha = 1
        }) { (complete) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
             
                    let a =  JSON(data: dataFromString!)
                if self.isFaceDisconnectedWorking {
                    SocketIOManager.sharedInstance.leaveGame(userMaster: a["_id"].stringValue, userSlave: self.SecondUserId,pathVideo: self.PathMaser)
                self.timerProba?.invalidate()
                self.timerProba = nil
                 SocketIOManager.sharedInstance.removeFromMatchMaking(roomId: self.roomId.stringValue)
                  self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
                var i = 0
                for key in self.peerConnectionDict.keys {
                    var jc: JanusConnection? = nil
                    
                    jc = self.peerConnectionDict[key] as? JanusConnection
                    if i == 0 && self.Owner == "master" {
                        
                        self.onLeaving(jc?.handleId)
                        
                    }else if i == 1 && self.Owner == "slave" {
                        self.onLeaving(jc?.handleId)
                    }
                    i = i + 1
                }
                    if self.isFaceDisconnectedWorking {
                self.publisherPeerConnection?.close()
                    
                self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
                        if self.detectionFaceSleeper != nil {
                self.detectionFaceSleeper.invalidate()
                self.detectionFaceSleeper = nil
                        }
                        if self.navigationController?.topViewController == self {
                // self.dismiss(animated: true, completion: nil)
                             SocketIOManager.sharedInstance.reactivateOnFinishGame()
                            if self.timerQ != nil {
                                self.timerQ?.invalidate()
                                self.timerQ = nil
                            }
                            self.soundPlayer.stop()
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
               //  self.navigation?.popToRootViewController(animated: true)
            })
        }
    }
    func closeFaceDisconnected(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.containerFaceDisconnected.alpha = 0
        }) { (complete) in
            self.ContainerStart.isHidden = true
               self.ContainerStart.alpha = 0
        }
    }
   
}

extension VideoGameRework : AVCaptureVideoDataOutputSampleBufferDelegate {
    func renderToJanus(sampleBuffer: CMSampleBuffer){
        
        let pixelBuffer:CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!;
        var videoFrame:RTCVideoFrame?;
        let timestamp = NSDate().timeIntervalSince1970 * 1000
        videoFrame = RTCVideoFrame(pixelBuffer: pixelBuffer, rotation: RTCVideoRotation._0, timeStampNs: Int64(timestamp))
        // connect the video frames to the WebRTC
        
        localVideoTrack!.source.capturer(RTCVideoCapturer(), didCapture: videoFrame!)

        
        
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
         i = i + 1
        
        externalVideoBufferDelegate?.captureOutput!(output, didOutput: sampleBuffer, from: connection)
        if true {
            //
         if  i % 45 == 0 && isControllerWorking && MessageFinished{
            var detected = false
                    let visionImage = VisionImage(buffer: sampleBuffer)
                   let metadata = VisionImageMetadata()
                       //metadata.orientation = .rightTop
                      metadata.orientation = .rightTop
                      visionImage.metadata = metadata
            
                self.faceDetector.process(visionImage) { features, error in
                  
                 guard let faces = features else {
                    self.smileProba = -3.0
                         return
                       }
            
            for feature in faces {
                self.tracked = true
                 var FaceHandler = 0
                if FaceHandler == 0 {
                    FaceHandler = 1
           
            self.FaceFrameStart = self.recordTime
        if self.recordTime >= 50.00  && self.recordTime <= 60.00 &&  self.tracked {
            
            self.FaceUserX = 0
            self.FaceUserY = 0
            self.FaceFrameDetection = self.recordTime
            self.FaceUserXStart = 0
            self.FaceUserYStart = 0
            self.FaceFrameStart = self.recordTime
           
                    }
           
                                        
                                     
            }
            if feature.hasSmilingProbability {
               detected = true
               self.smileProba = Float(feature.smilingProbability)
                            
            }
                        
                         //self.addContours(forFace: feature, transform: transform)
            }
                    if faces.isEmpty  {
                        print("Scan Failed: Found nothing to scan :")
                       
                        self.smileProba = -3.0
                    }else if detected == false{
                       
                        print("No Faces Detected")
                        self.smileProba = -2.0
                    }else{
                        print("detected")
                    }
                   }
            }
           // }else{
           //     print("convertion failed for samplBuffer")
          //  }
        
       // }
        //self.renderToJanus(sampleBuffer: sampleBuffer)
        }
    }
    func imageFromSampleBuffer(sampleBuffer : CMSampleBuffer) -> UIImage
     {
       // Get a CMSampleBuffer's Core Video image buffer for the media data
       let  imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        
       // Lock the base address of the pixel buffer
       CVPixelBufferLockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly);


       // Get the number of bytes per row for the pixel buffer
       let baseAddress = CVPixelBufferGetBaseAddress(imageBuffer!);

       // Get the number of bytes per row for the pixel buffer
       let bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer!);
       // Get the pixel buffer width and height
       let width = CVPixelBufferGetWidth(imageBuffer!);
       let height = CVPixelBufferGetHeight(imageBuffer!);

       // Create a device-dependent RGB color space
       let colorSpace = CGColorSpaceCreateDeviceRGB();

       // Create a bitmap graphics context with the sample buffer data
       var bitmapInfo: UInt32 = CGBitmapInfo.byteOrder32Little.rawValue
       bitmapInfo |= CGImageAlphaInfo.premultipliedFirst.rawValue & CGBitmapInfo.alphaInfoMask.rawValue
       //let bitmapInfo: UInt32 = CGBitmapInfo.alphaInfoMask.rawValue
       let context = CGContext.init(data: baseAddress, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo)
       // Create a Quartz image from the pixel data in the bitmap graphics context
       let quartzImage = context?.makeImage();
       // Unlock the pixel buffer
       CVPixelBufferUnlockBaseAddress(imageBuffer!, CVPixelBufferLockFlags.readOnly);

       // Create an image object from the Quartz image
       let image = UIImage.init(cgImage: quartzImage!);

       return (image);
     }
    
    

    func imageFromSampleBufferFullRange(sampleBuffer:CMSampleBuffer) -> UIImage? {
       // let utility = UtilityImage()
        let converted = UtilityImage.image(fromSampleBufferWebRTC: sampleBuffer)
        
        return converted
    }
  
}
extension UIViewController {

    func delay(delayTime: Double, completionHandler handler:@escaping () -> ()) {
            let newDelay = DispatchTime.now() + delayTime
            DispatchQueue.main.asyncAfter(deadline: newDelay, execute: handler)
        }
}
extension VideoGameRework : RTCVideoCapturerDelegate {
    func mergeSmileSampleBuffer(_ sample: CMSampleBuffer) -> CMSampleBuffer {
        
       // let desc = CMSampleBufferGetFormatDescription(sample)!
       // let bufferFrame = CMVideoFormatDescriptionGetCleanAperture(desc, false)
        
             
        return sample
    }
    func capturer(_ capturer: RTCVideoCapturer, didCapture frame: RTCVideoFrame) {
       
           
            
         
    }
    
    
}
