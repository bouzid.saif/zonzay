//
//  SettingsReworked.swift
//  YLYL
//
//  Created by macbook on 3/25/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import OneSignal
import ChameleonFramework
import Alamofire
import SwiftyJSON
import StoreKit
import SnapKit
import Hero
class SettingsReworked: NavigationProfile,DoubleSliderValueChanged ,UITableViewDelegate,UITableViewDataSource,SKProductsRequestDelegate{
    //VIP SECTION
    let ZonzWeek = "com.regystone.zonzay.PlusWeek"
    let ZonzMonth = "com.regystone.zonzay.PlusMonth"
    let ZonzYear = "com.regystone.zonzay.PlusYear"
    var products : [SKProduct] = []
    @IBOutlet weak var ZonzayPoints: UILabel!
    
    @IBOutlet weak var tableV: UITableView!
    
    @IBOutlet weak var VIPLabel : UILabel!
    
    @IBOutlet weak var ViewAgeRange: UIView!
    
    @IBOutlet weak var ViewGender: UIView!
    var animateView = true
    @IBOutlet weak var backButtonImage : UIButton!
    
    @IBOutlet weak var ViewContainerWorldLaugh: UIView!
    
    @IBOutlet weak var VIPBuyContainer: UIView!

    @IBOutlet weak var BuyBlurred: UIButton!
    
    @IBOutlet weak var viewContainerVolume: UIView!
    
    @IBOutlet weak var masterVolumeSwitch: UISwitch!
    
    @IBOutlet weak var masterLBL: UILabel!
    
    @IBOutlet weak var sfxLBL: UILabel!
    
    @IBOutlet weak var sfxVolumeSwitch: UISwitch!
    func valueChanged(forMin value: Double) {
        print("MIN",value)
        minAge.text = String(Int(value))
        
    }
    func valueChanged(forMax value: Double) {
        print("MAX",value)
        maxAge.text = String(Int(value))
    }
    @IBOutlet weak var ageSlider: DoubleSlider!
    
    @IBOutlet weak var minAge: UILabel!
    
    @IBOutlet weak var maxAge: UILabel!
    
    
    @IBOutlet weak var GenderSlider: VerticalSlider!
    
    @IBOutlet weak var HeightContainer: NSLayoutConstraint!
    @IBOutlet weak var VIPContainer : UIView!
    
    @IBOutlet weak var HeightVIPBuy: NSLayoutConstraint!
    @IBOutlet weak var VIPBackgroundContainer : UIView!
    
    @IBOutlet weak var ZonzContainer: UIView!
    
    @IBOutlet weak var weekContainer: UIView!
    
    @IBOutlet weak var monthContainer: UIView!
    
    @IBOutlet weak var yearContainer: UIView!
    
    @IBOutlet weak var visualBlurThing: UIVisualEffectView!
    

    @IBOutlet weak var plusBTN: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        self.getZonz()
    }
    @IBAction func plusButtonJokes(_ sender: UIButton) {
        self.VIPBuyContainer.isHidden = false
        self.VIPBuyContainer.layoutIfNeeded()
        self.HeightVIPBuy.constant = 340
        animateView = false
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [.curveEaseIn], animations: {
            self.VIPBuyContainer.alpha = 1
            self.HeightContainer.constant = 340
            //self.HeightVIPBuy.constant = 340
            self.VIPBuyContainer.layoutIfNeeded()
            //self.BuyContainer.alpha = 0.92
        }) { _ in
            
        }
    }
    var notifEnabled = false
    var privateEnabled = false
    
    @IBOutlet weak var MaleLBL: EdgeInsetLabel!
    
    @IBOutlet weak var FemaleLBL: EdgeInsetLabel!
    
    @IBOutlet weak var OtherLBL: EdgeInsetLabel!
    
    @IBOutlet weak var LineToAnimate: UIImageView!
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
    func configureZonzContainer(){
        self.ZonzContainer.layer.cornerRadius = 7.5
        self.ZonzContainer.layer.borderColor = UIColor.white.cgColor
        self.ZonzContainer.layer.borderWidth = 1.0
        self.ZonzContainer.layer.masksToBounds = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ZonzayPoints.text = "0"
        
       self.masterLBL.text = Localization("masterSound")
       self.sfxLBL.text = Localization("sfxSound")
        //MaleLBL.layer.borderWidth = 2
        //MaleLBL.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        
        PKIAPHandler.shared.setProductIds(ids: [ZonzWeek,ZonzMonth,ZonzYear])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([ZonzWeek,ZonzMonth,ZonzYear]))
        
        //PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            
            for product in skProducts {
                print(product)
            }
        }
        configureGestures()
        self.tableV.delegate = self
      
        configureZonzContainer()
        configureBlur()
        self.tableV.dataSource = self
        
        self.HeightContainer.constant = 286
        self.VIPBuyContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        self.HeightVIPBuy.constant = 0
        self.visualBlurThing.isHidden = true
        self.VIPBackgroundContainer.isHidden = true
        self.ageSlider.isUserInteractionEnabled = false
        self.ageSlider.valueChangedSlider = self
        self.GenderSlider.isUserInteractionEnabled = false
        self.GenderSlider.slider.addTarget(self, action: #selector(self.sliderDidChange(_:)), for: [.touchUpInside,.touchUpOutside, .valueChanged, .touchCancel])
        self.GenderSlider.isContinuous = true
        
        guard let ab = UserDefaults.standard.value(forKey: "Subscription") as? String else {
            return
        }
        print("Subscription " , ab)
        if ab == "true"{
            
            self.animateView = false
            self.configureGenderLabels()
            print(UserDefaults.standard.value(forKey: "MatchChoice"))
            if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                //GenderBTN.setTitle(UserDefaults.standard.value(forKey: "MatchChoice") as? String, for: .normal)
                switch UserDefaults.standard.value(forKey: "MatchChoice") as? String {
                case "Male" :
                    
                  self.markLabel(label: MaleLBL)
                case "Female" :
                    self.markLabel(label: FemaleLBL)
                case "Other" :
                    self.markLabel(label: OtherLBL)
                default :
                    break
                }
                minAge.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxAge.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                ageSlider.lowerValue =  Double(minAge.text!)!
                ageSlider.upperValue = Double(maxAge.text!)!
                self.permuteColorObjects()
                //hideContainers(false)
            }else{
                let ac = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ac.data(using: String.Encoding.utf8, allowLossyConversion: false)
                let a = JSON(data: dataFromString!)
                    
                UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                UserDefaults.standard.setValue("18", forKey: "minChoice")
                UserDefaults.standard.setValue("75", forKey: "maxChoice")
                UserDefaults.standard.synchronize()
                switch UserDefaults.standard.value(forKey: "MatchChoice") as? String {
                case "Male" :
                    
                    
                    self.markLabel(label: MaleLBL)
                case "Female" :
                    self.markLabel(label: FemaleLBL)
                case "Other" :
                    self.markLabel(label: OtherLBL)
                default :
                    break
                }
                minAge.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxAge.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                ageSlider.lowerValue =  Double(minAge.text!)!
                ageSlider.upperValue = Double(maxAge.text!)!
                self.permuteColorObjects()
                
            }
            
        }else{
          self.animateView = true
        }
    }
    @objc func sliderDidChange(_ slider: UISlider) {
        print("hello")
        print(slider.value)
        if Float(0)...Float(0.25) ~= slider.value   {
            slider.value = 0.08
            self.unmarkLabels()
            self.markLabel(label: self.OtherLBL)
        }
        if Float(0.26)...Float(0.75) ~= slider.value   {
            slider.value = 0.5
            self.unmarkLabels()
            self.markLabel(label: self.FemaleLBL)
        }
        if Float(0.76)...Float(1) ~= slider.value   {
            slider.value = 0.92
            self.unmarkLabels()
            self.markLabel(label: self.MaleLBL)
        }
    }
    func configureGenderLabels(){
        let tapGestureMale = UITapGestureRecognizer(target: self, action: #selector(self.MaleLBLTapped(_:)))
         let tapGestureFemale = UITapGestureRecognizer(target: self, action: #selector(self.FemaleLBLTapped(_:)))
         let tapGestureOther = UITapGestureRecognizer(target: self, action: #selector(self.OtherLBLTapped(_:)))
        MaleLBL.isUserInteractionEnabled = true
        FemaleLBL.isUserInteractionEnabled = true
        OtherLBL.isUserInteractionEnabled = true
        MaleLBL.addGestureRecognizer(tapGestureMale)
        FemaleLBL.addGestureRecognizer(tapGestureFemale)
        OtherLBL.addGestureRecognizer(tapGestureOther)

    }
    @objc func MaleLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: MaleLBL)
    }
    @objc func FemaleLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: FemaleLBL)
    }
    @objc func OtherLBLTapped(_ sender: UITapGestureRecognizer) {
        self.unmarkLabels()
        self.markLabel(label: OtherLBL)
    }
    func markLabel(label:EdgeInsetLabel) {
        label.layer.cornerRadius = 4.25
        //label.backgroundColor = .black
        label.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
        label.layer.masksToBounds = true
        label.textColor = UIColor.white
        label.leftTextInset = 10
        label.rightTextInset = 10
        label.topTextInset = 5
        label.bottomTextInset = 5
       // label.layoutIfNeeded()
        switch label {
        case MaleLBL:
            GenderSlider.value = 0.92
            
            self.LineToAnimate.snp.remakeConstraints { (const) in
                const.centerY.equalTo(MaleLBL)
                const.right.equalTo(MaleLBL.snp.left).offset(-1.5)
                const.width.equalTo(28)
                const.height.equalTo(4)
                    }
           // self.MaleLBL.size = CGSize(width: self.MaleLBL.size.width, height: 20)
            case FemaleLBL:
                GenderSlider.value = 0.5
                //self.LineToAnimate.frame = CGRect(x: self.LineToAnimate.frame.origin.x, y: self.FemaleLBL.centerY, width: self.LineToAnimate.frame.width, height: self.LineToAnimate.frame.height)

                self.LineToAnimate.snp.remakeConstraints { (const) in
                    const.centerY.equalTo(FemaleLBL)
                    const.right.equalTo(FemaleLBL.snp.left).offset(-1.5)
                    const.width.equalTo(28)
                    const.height.equalTo(4)
            }
            case OtherLBL:
                GenderSlider.value = 0.08
               
                self.LineToAnimate.snp.remakeConstraints { (const) in
                    const.centerY.equalTo(OtherLBL)
                     const.right.equalTo(OtherLBL.snp.left).offset(-1.5)
                    const.width.equalTo(28)
                    const.height.equalTo(4)
            }
        default:
            break
        }
    }
    func unmarkLabels(){
        MaleLBL.backgroundColor = UIColor.white
        MaleLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        MaleLBL.leftTextInset = 0
        MaleLBL.rightTextInset = 0
        MaleLBL.topTextInset = 0
        MaleLBL.bottomTextInset = 0
        /// ** //
        FemaleLBL.backgroundColor = UIColor.white
        FemaleLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        FemaleLBL.leftTextInset = 0
        FemaleLBL.rightTextInset = 0
        FemaleLBL.topTextInset = 0
        FemaleLBL.bottomTextInset = 0
        /// ** //
        OtherLBL.backgroundColor = UIColor.white
        OtherLBL.textColor = UIColor(red: 112/255, green: 112/255, blue: 112/255, alpha: 1)
        OtherLBL.leftTextInset = 0
        OtherLBL.rightTextInset = 0
        OtherLBL.topTextInset = 0
        OtherLBL.bottomTextInset = 0
        
    }
    func configureGestures(){
        let tapgestureWeek = UITapGestureRecognizer(target: self, action: #selector(self.weekAnimate(_:)))
        tapgestureWeek.numberOfTapsRequired = 1
        self.weekContainer.addGestureRecognizer(tapgestureWeek)
        self.weekContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.weekContainer.layer.borderWidth = 1
        self.weekContainer.isUserInteractionEnabled = true
        self.weekContainer.tag = 0
        self.monthContainer.tag = 1
        self.yearContainer.tag = 2
        let tapgestureMonth = UITapGestureRecognizer(target: self, action: #selector(self.monthAnimate(_:)))
        tapgestureMonth.numberOfTapsRequired = 1
        self.monthContainer.addGestureRecognizer(tapgestureMonth)
        self.monthContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.monthContainer.layer.borderWidth = 1
        self.monthContainer.isUserInteractionEnabled = true
        let tapgestureYear = UITapGestureRecognizer(target: self, action: #selector(self.yearAnimate(_:)))
        tapgestureYear.numberOfTapsRequired = 1
        self.yearContainer.addGestureRecognizer(tapgestureYear)
        self.yearContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        self.yearContainer.layer.borderWidth = 1
        self.yearContainer.isUserInteractionEnabled = true
    }
    func configureBlur(){
        self.ViewContainerWorldLaugh.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.ViewContainerWorldLaugh.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
        self.BuyBlurred.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.BuyBlurred.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.95))
        self.view.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.view.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
    }
    @objc func weekAnimate(_ sender: UITapGestureRecognizer) {
       
        if weekContainer.tag == 0 {
        deAnimateContainers()
        weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
        
    }
    @IBAction func weekButton (_ sender: UIButton){
        if weekContainer.tag == 0 {
            deAnimateContainers()
            weekContainer.tag = 3
            weekContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.weekContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.weekContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.weekContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    
    func deAnimateContainers(){
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
            self.weekContainer.transform = CGAffineTransform.identity
            self.weekContainer.backgroundColor = UIColor.white
            self.weekContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.weekContainer.tag = 0
             self.monthContainer.transform = CGAffineTransform.identity
            self.monthContainer.backgroundColor = UIColor.white
            self.monthContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.monthContainer.tag = 1
             self.yearContainer.transform = CGAffineTransform.identity
            self.yearContainer.backgroundColor = UIColor.white
            self.yearContainer.subviews.forEach({ vi in
                
                if vi .isKind(of: UILabel.self) {
                    (vi as! UILabel).textColor = UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1)
                    
                }
                if vi .isKind(of: UIButton.self) {
                    (vi as! UIButton).backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    
                    (vi as! UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
            })
            self.yearContainer.tag = 2
        }) { _ in
            
        }
    }
    @objc func monthAnimate(_ sender: UITapGestureRecognizer) {
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @IBAction func monthButton (_ sender: UIButton){
        if monthContainer.tag == 1 {
            deAnimateContainers()
            monthContainer.tag = 4
            monthContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.monthContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.monthContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.monthContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }
        }else{
            
        }
    }
    @objc func yearAnimate(_ sender: UITapGestureRecognizer) {
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
    @IBAction func yearButton (_ sender: UIButton){
        if yearContainer.tag == 2 {
            deAnimateContainers()
            yearContainer.tag = 5
            yearContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseIn], animations: {
                self.yearContainer.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                self.yearContainer.subviews.forEach({ vi in
                    
                    if vi .isKind(of: UILabel.self) {
                        (vi as! UILabel).textColor = UIColor.white
                        
                    }
                    if vi .isKind(of: UIButton.self) {
                        (vi as! UIButton).backgroundColor = UIColor.white
                        
                        (vi as! UIButton).setTitleColor(UIColor(red: 120/255, green: 120/255, blue: 120/255, alpha: 1), for: .normal)
                        
                    }
                })
                self.yearContainer.transform = CGAffineTransform(scaleX: 1.14, y: 1.14)
            }) { _ in
                
            }        }else{
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellNotif", for: indexPath)
            let NotifSwitch = cell.viewWithTag(1) as! UISwitch
            NotifSwitch.addTarget(self, action: #selector(self.NotifcationAction(_:)), for: .valueChanged)
            
            NotifSwitch.isOn = notifEnabled
            if notifEnabled {
             NotifSwitch.onTintColor = UIColor(red: 37/255, green: 230/255, blue: 211/255,alpha: 1.0)
                
            }else{
                
            }
            return cell
        case 1:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellBlocked", for: indexPath)
             
             
            return cell
        case 2:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellPrivate", for: indexPath)
             let PrivateSwitch = cell.viewWithTag(1) as! UISwitch
             let privateLBL = cell.viewWithTag(2) as! UILabel
             PrivateSwitch.addTarget(self, action: #selector(self.PrivateAction(_:)), for: .valueChanged)
             
             PrivateSwitch.isOn = privateEnabled
             if privateEnabled {
                PrivateSwitch.thumbTintColor = UIColor.white
                PrivateSwitch.onTintColor = UIColor(red: 37/255, green: 230/255, blue: 211/255,alpha: 1.0)
                privateLBL.text = Localization("Private")
             }else{
                privateLBL.text = Localization("Public")
             }
             return cell
        case 3:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellVIP", for: indexPath)
             let vipText = cell.viewWithTag(1) as! UILabel
             let vipDate = cell.viewWithTag(2) as! UILabel
             if (UserDefaults.standard.value(forKey: "Subscription") as! String) == "true" {
            let date = UserDefaults.standard.value(forKey: "DateSub") as! String
                
             vipText.text = Localization("VIPA")
             vipDate.text = Localization("UNTIL") + date
             }else{
             vipText.text = Localization("VIPA")
             vipDate.text = Localization("NACTIVE")
             }
             return cell
      /*  case 4:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellDelete", for: indexPath)
             return cell */
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellVolume", for: indexPath)
            return cell
        case 5:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellLogout", for: indexPath)
             return cell
        default:
             let cell = tableView.dequeueReusableCell(withIdentifier: "CellVersion", for: indexPath)
             let version = cell.viewWithTag(1) as! UILabel
             //First get the nsObject by defining as an optional anyObject
             let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
             
             //Then just cast the object as a String, but be careful, you may want to double check for nil
             let versions = nsObject as! String
             version.text = "Version : " + versions
             return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
          /*  self.VIPLabel.isHidden = true
            self.backButtonImage.isHidden = true
            self.plusBTN.isHidden = true
            self.VIPContainer.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.VIPContainer.transform = CGAffineTransform(scaleX: 0, y: 0)
           // self.VIPContainer.size = CGSize.zero
            VIPContainer.alpha = 0
            self.visualBlurThing.isHidden = false
            VIPContainer.isHidden = false
            UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
                self.VIPBackgroundContainer.isHidden = false
                self.VIPContainer.alpha = 1.0
                //self.VIPContainer.size = CGSize(width: width, height: heigh)
                self.VIPContainer.transform = CGAffineTransform.identity
            }) { _ in
                self.VIPLabel.isHidden = false
                self.backButtonImage.isHidden = false
                self.plusBTN.isHidden = false

            } */
        let vc = UIStoryboard(name: "VIPAccess", bundle: nil).instantiateViewController(withIdentifier: "VIPAccessController") as! VIPAccessController
            vc.hero.modalAnimationType = .zoom
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
            
            //hero.replaceViewController(with: vc)
        }else if indexPath.row == 5 {
            self.disconnectAction()
        }else if indexPath.row == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BlockedUsersController") as! BlockedUsersController
            vc.navigation = self.navigationController
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 4 {
            self.viewContainerVolume.alpha = 0
            self.viewContainerVolume.isHidden = false
            if SoundBackGround.enabled {
                self.masterVolumeSwitch.isOn = true
            }else{
                self.masterVolumeSwitch.isOn = false
            }
              if UserDefaults.standard.object(forKey: "SfxVolumeZonzay") != nil {
                let ab = UserDefaults.standard.value(forKey: "SfxVolumeZonzay") as! String
                if ab == "true" {
                    self.sfxVolumeSwitch.isOn = true
                }else{
                    self.sfxVolumeSwitch.isOn = false
                }
              }else{
                self.sfxVolumeSwitch.isOn = true
            }
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.viewContainerVolume.alpha = 1
               UserDefaults.standard.setValue("true", forKey: "SfxVolumeZonzay")
                UserDefaults.standard.synchronize()
            }, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func permuteColorObjects(){
        self.ViewGender.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ViewAgeRange.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.thumbTintColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.GenderSlider.thumbTintColor = UIColor(red: 31/255, green: 165/255, blue: 194/255,alpha : 1.0)
        self.ageSlider.isUserInteractionEnabled = true
        self.GenderSlider.isUserInteractionEnabled = true
    }
    @IBAction func ShowVIPBuy(_ sender: UITapGestureRecognizer) {
        if animateView {
          //self.BuyContainer.alpha  = 0
        //self.BuyContainer.isHidden = false
            //self.VIPBuyContainer.alpha = 0
            self.VIPBuyContainer.isHidden = false
            self.VIPBuyContainer.layoutIfNeeded()
            self.HeightVIPBuy.constant = 340
      animateView = false
        UIView.animate(withDuration: 0.4, delay: 0.1, options: [.curveEaseIn], animations: {
            self.VIPBuyContainer.alpha = 1
            self.HeightContainer.constant = 340
            //self.HeightVIPBuy.constant = 340
            self.VIPBuyContainer.layoutIfNeeded()
            //self.BuyContainer.alpha = 0.92
        }) { _ in
            
        }
        }
    }
    @IBAction func BackVIPAction(_ sender: UIButton) {
        if (UserDefaults.standard.value(forKey: "Subscription") as! String) == "true" {
            if MaleLBL.textColor == UIColor.white {
                UserDefaults.standard.setValue("Male", forKey: "MatchChoice")
            }else if FemaleLBL.textColor == UIColor.white {
                 UserDefaults.standard.setValue("Female", forKey: "MatchChoice")
            }else if OtherLBL.textColor == UIColor.white {
                 UserDefaults.standard.setValue("Other", forKey: "MatchChoice")
            }
            //UserDefaults.standard.setValue(self.GenderBTN.title(for: .normal)!, forKey: "MatchChoice")
            UserDefaults.standard.setValue(self.minAge.text!, forKey: "minChoice")
            UserDefaults.standard.setValue(self.maxAge.text!, forKey: "maxChoice")
            UserDefaults.standard.synchronize()
        }
        UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
            //self.BuyContainer.alpha  = 0
            self.VIPContainer.alpha = 0
            
        }) { _ in
            //self.BuyContainer.isHidden = true
            self.VIPContainer.isHidden = true
            self.visualBlurThing.isHidden = true
            self.VIPBackgroundContainer.isHidden = true
            self.VIPBuyContainer.isHidden = true
            self.VIPBuyContainer.alpha = 0
            self.HeightContainer.constant = 286
            self.animateView = true
        }
    }
    func determineType() -> PaymentType {
        if weekContainer.backgroundColor == UIColor.white {
            if monthContainer.backgroundColor == UIColor.white {
                if yearContainer.backgroundColor == UIColor.white {
                    return PaymentType.None
                }else{
                    return PaymentType.Year
                }
            }else{
                return PaymentType.Month
            }
        }else{
            return PaymentType.Week
        }
    }
    func determineIndex () -> Int {
        if weekContainer.backgroundColor == UIColor.white {
            if monthContainer.backgroundColor == UIColor.white {
                if yearContainer.backgroundColor == UIColor.white {
                    return -1
                }else{
                    return 2
                }
            }else{
                return 1
            }
        }else{
            return 0
        }
    }
    @IBAction func BuyAction(_ sender: UIButton) {
        if determineType() != PaymentType.None {
           // PKIAPHandler.shared.purchase(product: products[determineIndex()]) { (purchaseAlertType, product, transaction) in
              //  if transaction != nil {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "duration" : self.determineType().rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.filterPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                if data != JSON.null {
                    
                    let yesAction = UIAlertAction(title: Localization("OK"), style: .destructive) { (alert) in
                        //do the work
                        self.permuteColorObjects()
                        UIView.animate(withDuration: 0.6, delay: 0.1, options: [.curveEaseInOut], animations: {
                            self.VIPBuyContainer.alpha = 0
                            self.HeightContainer.constant = 286
                        }) { _ in
                            self.VIPBuyContainer.isHidden = true
                        }
                       
                    }
                    let dateFormatterNow = DateFormatter()
                    dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                    dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                    dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                    let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                    let dateFormatterFinal = DateFormatter()
                    dateFormatterFinal.dateFormat = "dd-MM-yyy"
                    let date = dateFormatterFinal.string(from: dateNow!)
                    let alert = UIAlertController(title: "Zonzay Plus", message: Localization("PaymentVU") + date, preferredStyle: .alert)
                    alert.addAction(yesAction)
                    UserDefaults.standard.setValue("true", forKey: "Subscription")
                    if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                         UserDefaults.standard.setValue(date, forKey: "DateSub")
                    }else{
                        UserDefaults.standard.setValue(a["userGender"].stringValue == "Male" ? "Female" : "Male" , forKey: "MatchChoice")
                        UserDefaults.standard.setValue(date, forKey: "DateSub")
                        UserDefaults.standard.setValue("18", forKey: "minChoice")
                        UserDefaults.standard.setValue("75", forKey: "maxChoice")
                    }
                    UserDefaults.standard.synchronize()
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
                    
                }
                
                
           // }
            
       // }else{
            
       // }
    }
    func disconnectAction() {
        
        let yesAction = UIAlertAction(title: Localization("Yes"), style: .destructive) { (alert) in
            //do the work
            self.signOut()
            
        }
        let noAction = UIAlertAction(title: Localization("No"), style: .cancel, handler: nil)
        let alert = UIAlertController(title: Localization("Disconnect"), message: Localization("AYS"), preferredStyle: .alert)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    func signOut(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let root =  storyboard.instantiateViewController(withIdentifier: "login")
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
        //q.popToRootViewController(animated: true)
        configureMasterSoundON()
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
        self.navigationController?.isNavigationBarHidden = true
        OneSignal.setSubscription(false)
        for viewC in self.navigationController!.viewControllers {
            print("View : ",viewC)
        }
        //let delegate = UIApplication.shared.delegate as! AppDelegate
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
        print(self.navigationController?.viewControllers)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            print("Logout Called")
            NotificationCenter.default.post(name: NSNotification.Name.init("LogoutCalled"), object: nil)
        }
        
        //self.navigationController?.pushViewController(root, animated: true)
    }
    func configureMasterSoundON(){
        if SoundBackGround.enabled {
            if let url = SoundBackGround.soundsBundle.url(forResource: "background_music", withExtension: "mp3") {
                if let playerXX = SoundBackGround.sounds[url] {
                    playerXX.pause()
                }
            }
        }
    }
    @objc func NotifcationAction(_ sender: UISwitch) {
        if sender.isOn == false {
            sender.tintColor = UIColor(red: 25/255, green: 127/255, blue: 149/255, alpha: 1)
             OneSignal.setSubscription(false)
        }else{
            sender.onTintColor =  UIColor(red: 37/255, green: 230/255, blue: 211/255,alpha: 1.0)
            OneSignal.setSubscription(true)
        }
    }
    
    @objc func PrivateAction(_ sender: UISwitch) {
        if sender.isOn == false {
            sender.tintColor = UIColor(red: 25/255, green: 127/255, blue: 149/255, alpha: 1)
            self.privateEnabled = false
            updateUser()
            self.tableV.beginUpdates()
            self.tableV.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            self.tableV.endUpdates()
        }else{
            sender.onTintColor =  UIColor(red: 37/255, green: 230/255, blue: 211/255,alpha: 1.0)
            self.privateEnabled = true
            updateUser()
            self.tableV.beginUpdates()
            self.tableV.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            self.tableV.endUpdates()
           
        }
        
    }
    func updateUser(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "privacyAccount" : privateEnabled ? "true" : "false"
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updatePrivacyUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("updateUser : ",data)
                if data != JSON.null {
                UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                UserDefaults.standard.synchronize()
                }
            }
            
        }catch {
            
        }
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                if zonz != "" {
                self.ZonzayPoints.text = zonz
                    
                }else{
                    self.ZonzayPoints.text = "0"
                }
                if data["pushEnabled"].stringValue == "1" {
                    self.notifEnabled = true
                    
                }else{
                     self.notifEnabled = false
                }
                if data["privacyAccount"].boolValue {
                    self.privateEnabled = true
                }else{
                    self.privateEnabled = false
                }
                self.tableV.beginUpdates()
                self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 3, section: 0)], with: .fade)
                self.tableV.endUpdates()
            }
            
        }catch {
            
        }
    }
    @IBAction func BackAction(_ sender : UIButton) {
        /*UserDefaults.standard.setValue(self.GenderBTN.title(for: .normal)!, forKey: "MatchChoice")
        UserDefaults.standard.setValue(self.minAge.text!, forKey: "minChoice")
        UserDefaults.standard.setValue(self.maxAge.text!, forKey: "maxChoice")
        UserDefaults.standard.synchronize() */
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func masterVolumeAction(_ sender: UISwitch) {
        if sender.isOn {
        SoundBackGround.enabled = true
             SoundBackGround.play(file: "background_music", fileExtension: "mp3", numberOfLoops: -1)
        }else{
        SoundBackGround.enabled = false
             SoundBackGround.stopAll()
        }
        
    }
    
    @IBAction func sfxVolumeAction(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.setValue("true", forKey: "SfxVolumeZonzay")
            UserDefaults.standard.synchronize()
        }else{
            UserDefaults.standard.setValue("false", forKey: "SfxVolumeZonzay")
            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func closeVolumeContainer(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.viewContainerVolume.alpha = 0
        }) { (complete) in
            self.viewContainerVolume.isHidden = true
            self.viewContainerVolume.alpha = 1
        }
    }
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
    //Create our background gradient layer
    let backgroundGradientLayer = CAGradientLayer()

    //Set the frame to our object's bounds
    backgroundGradientLayer.frame = frame
    backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
    //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
    var cgColors: [AnyHashable] = []
    for color in colors as? [UIColor] ?? [] {
        let cg = color.cgColor
            cgColors.append(cg)
        
    }
    switch gradientStyle {
        
    case UIGradientStyle.leftToRight:

            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors

            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)

            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

           // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:

            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors

            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

           // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
    }
}
  
}
