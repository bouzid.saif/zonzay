//
//  JokesController.swift
//  YLYL
//
//  Created by macbook on 1/8/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
class JokesController: ServerUpdateDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var Jokes : [String] = ["Q:What's Homer Simpson's least favorite style of beer?\nA: Flanders Red Ale."]
    var JokesJSON : JSON = []
    /*
     * Mark: DataSource
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Jokes.count
    }
    /*
     * Mark: DataSource
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let ImageJoke = cell.viewWithTag(1) as! UIImageView
        ImageJoke.image = #imageLiteral(resourceName: "joke.png")
        let QuestionLBL = cell.viewWithTag(2) as! UILabel
        QuestionLBL.text = Jokes[indexPath.row]
        return cell
    }
    /*
     * Mark : Delegate
     */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let jokeText = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 96, height: 24))
       
        jokeText.font =  UIFont.systemFont(ofSize: 17)
        
        jokeText.text = self.Jokes[indexPath.row]
        let count = offSetTableViewIfNeeded(textView: jokeText)
        print("countttttttttttt:",count)
        if count > 4 {
        return   CGFloat(103 + (19 * (count - 4)))
        }else{
            
            return 80
        }
    }
    func offSetTableViewIfNeeded(textView:UITextView) -> Int {
        let numberOfGlyphs = textView.layoutManager.numberOfGlyphs
        var index : Int = 0
        var lineRange = NSRange(location: NSNotFound, length: 0)
        var currentNumOfLines : Int = 0
        var numberOfParagraphJump : Int = 0
        
        while index < numberOfGlyphs {
            textView.layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            currentNumOfLines += 1
            
            // Observing whether user went to line and if it's the first such line break, accounting for it.
            if textView.text.last == "\n", numberOfParagraphJump == 0 {
                numberOfParagraphJump = 1
            }
        }
        
        currentNumOfLines += numberOfParagraphJump
        print("Number of lines is:", currentNumOfLines)
        return currentNumOfLines
        
    }
    /*
     * Mark : Delegate
     */
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  103
    }
    /*
     * The tableview that contains our jokes
     */
    @IBOutlet weak var tableView: UITableView!
    /*
     * The Done button
     */
    @IBOutlet weak var DoneBTN: UIButton!
    /*
     * the Floating add Button
     */
    @IBOutlet weak var AddBTN: UIButton!
    /*
     * The view that create a focus alpha on the AddContainer
     */
    @IBOutlet weak var AddView: UIView!
    /*
     * It contains our Joke text and the sava&cancel buttons
     */
    @IBOutlet weak var AddContainer: UIView!
    /*
     * Where the user will write his joke
     */
    @IBOutlet weak var JokeTextView: PlaceholderTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureConnectButton(button: DoneBTN)
        self.navigationController?.isNavigationBarHidden = true
        self.JokesJSON =  self.constructJson(json: self.Jokes, joke: "")
        
    }
   
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
        button.layer.cornerRadius = 9.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.DoneAction(_:)), for: .touchUpInside)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * Save button Action
     */
    @IBAction func SaveJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = true
       
        if self.JokeTextView.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")) != "" {
            //self.Jokes.append(JokeTextView.text!)
            self.saveJokesIntoDataBase(joke: JokeTextView.text!)
           // self.tableView.reloadData()
        }
         self.JokeTextView.text = ""
    }
    /*
     * Cancel button Action
     */
    @IBAction func CancelJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = true
        self.JokeTextView.text = ""
    }
    /*
     * Add Joke Action
     */
    @IBAction func AddJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = false
        self.JokeTextView.becomeFirstResponder()
    }
    @objc func DoneAction(_ sender: UIButton) {
        UserDefaults.standard.set(Jokes, forKey: "Jokes")
        
        DispatchQueue.main.async(execute: {
        self.performSegue(withIdentifier: "go_to_home", sender: self)
        })
    }
    func saveJokesIntoDataBase(joke:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        self.tableView.isHidden = false
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let res = self.constructJson(json: self.Jokes, joke: joke)
            self.JokesJSON = res
            self.Jokes.append(joke)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.tableView.reloadData()
                }else{
                    self.tableView.reloadData()
                    //self.defaultMessage.isHidden = false
                    self.tableView.isHidden = true
                }
                
            }
            
        }catch {
            
        }
    }
    func constructJson(json:[String],joke:String) -> JSON{
        var result = "["
        for j in json {
            result = result + "{\"jokeId\":\"\",\"joke\":\"" + j + "\",\"userId\":\"\"},"
        }
        if joke != "" {
        result = result + "{\"jokeId\":\"\",\"joke\":\"" + joke + "\",\"userId\":\"\"}]"
        }else{
            result = String(result.dropLast())
             result = result + "]"
        }
        return JSON(stringLiteral: result)
    }
}
