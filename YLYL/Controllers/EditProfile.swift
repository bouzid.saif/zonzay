//
//  EditProfile.swift
//  YLYL
//
//  Created by macbook on 2/15/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import DKImagePickerController
import SwiftyJSON
import MapleBacon
import Alamofire
import NotificationBannerSwift
class EditProfile: FixUITableController,UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.salutations.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return salutations[row]
    }
    
    @IBOutlet weak var navigationItemCustom : UINavigationItem!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var UserNameCell : UITableViewCell!
    @IBOutlet weak var FirstNameCell : UITableViewCell!
    @IBOutlet weak var EmailCell : UITableViewCell!
    @IBOutlet weak var LastNameCell : UITableViewCell!
    @IBOutlet weak var PictureCell : UITableViewCell!
    @IBOutlet weak var GenderCell : UITableViewCell!
    @IBOutlet weak var BirthDayCell : UITableViewCell!
    @IBOutlet weak var VideoJokesCell : UITableViewCell!
    var countryChoosed = "Afghanistan"
    var countryAlphaCode = ""
    var navigation : UINavigationController!
    var actuality: [VideoYLYL] = []
    var FirstLogin = false
    let color = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
    let salutations = ["", "Male", "Female","Other"]
    let picker  = UIPickerView()
    let datePicker = UIDatePicker()
    let _notificationToast = CustomToast.shared
    func verifyData() -> Bool {
        let username = UserNameCell.viewWithTag(1) as! UITextField
           let firstName = FirstNameCell.viewWithTag(1) as! UITextField
         let lastName = LastNameCell.viewWithTag(1) as! UITextField
         let Email = EmailCell.viewWithTag(1) as! UITextField
        let Birth = BirthDayCell.viewWithTag(1) as! UITextField
        let GenderTF = self.GenderCell.viewWithTag(1) as! TextFieldPhone
        let phoneTF = self.VideoJokesCell.viewWithTag(2) as! TextFieldPhone
        
        if username.text != "" && firstName.text != "" && lastName.text != "" && Email.text != "" && Birth.text != "" && GenderTF.text != "" && phoneTF.text != "" {
            return true
        }
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if FirstLogin {
            self.navigation?.isNavigationBarHidden = false
            self.title = Localization("ProfileSS")
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.hidesBackButton = true
            let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
            self.navigation?.navigationBar.titleTextAttributes = textAttributes
        }
        self.tableView.backgroundColor = UIColor(red: 243/255, green: 243/255, blue: 243/255, alpha: 1)
        if FirstLogin == false {
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.changeState))
        done.tintColor = UIColor.white
        self.navigationItemCustom.leftBarButtonItem = done
        
        let cancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.NothingAction))
         cancel.tintColor = UIColor.white
        self.navigationItemCustom.rightBarButtonItem = cancel
        }else{
            let finish = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.NothingAction))
            finish.tintColor = UIColor.white
            self.navigationItem.setRightBarButton(finish, animated: false)
           // self.navigationItemCustom.rightBarButtonItem = finish
        }
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            if a["userName"].exists() {
            let username = UserNameCell.viewWithTag(1) as! UITextField
            username.text = a["userName"].stringValue
                
            }
            if a["userFirstName"].exists(){
                let firstName = FirstNameCell.viewWithTag(1) as! UITextField
                firstName.text = a["userFirstName"].stringValue
            }
            if a["userLastName"].exists(){
                let lastName = LastNameCell.viewWithTag(1) as! UITextField
            lastName.text = a["userLastName"].stringValue
            }
            if a["userEmail"].exists(){
            let Email = EmailCell.viewWithTag(1) as! UITextField
               Email.text = a["userEmail"].stringValue
            }
            if a["userAge"].exists(){
               let Birth = BirthDayCell.viewWithTag(1) as! UITextField
                Birth.text = a["userAge"].stringValue
            }
            print(a)
            if a["userImageURL"].exists(){
                print("true")
                let Picture = PictureCell.viewWithTag(1) as! RoundedUIImageView
                Picture.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "profileVideo_hamid"), transformer: nil, progress: nil, completion: nil)
        
            }
            
           configureGenderTF()
            configureBirthdayTF()
            if a["userGender"].exists() {
                let GenderTF = self.GenderCell.viewWithTag(1) as! TextFieldPhone
                switch a["userGender"].stringValue {
                case salutations[1]:
                   GenderTF.text = salutations[1]
                    picker.selectRow(1, inComponent: 0, animated: false)
                case salutations[2]:
                    GenderTF.text = salutations[2]
                    picker.selectRow(2, inComponent: 0, animated: false)
                case salutations[3]:
                    GenderTF.text = salutations[3]
                    picker.selectRow(3, inComponent: 0, animated: false)
                default:
                    break
                }
            }
            
            let changePicture = PictureCell.viewWithTag(3) as! UIButton
           changePicture.addTarget(self, action: #selector(self.ChangePictureAction(_:)), for: .touchUpInside)
           // if a[]
        }catch{
            
        }
      
        if FirstLogin {
            UserNameCell.enable(on: true)
            if (EmailCell.viewWithTag(1) as! UITextField).text != "" {
            EmailCell.enable(on: false)
            }
            //VideoJokesCell.isHidden  = true
        }else{
            UserNameCell.enable(on: false)
            EmailCell.enable(on: false)
        }
    }
    func configurePhoneTF(){
        let image = UIImage(named: "down_arrow_saif")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        let PhoneTF = self.BirthDayCell.viewWithTag(2) as! TextFieldPhone
        PhoneTF.rightView = imageView
        PhoneTF.rightView?.frame.size = CGSize(width: image!.size.width , height: image!.size.height)
        PhoneTF.rightViewMode = .always
        let countryLocale = NSLocale.current
        let countryCode = countryLocale.regionCode
        //let country = (countryLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        //print(countryCode)
        for i in 0...(AppDelegate.countryArray.count - 1) {
            if AppDelegate.countryArray[i]["alpha2Code"].stringValue == countryCode {
                PhoneTF.text = "+" + AppDelegate.countryArray[i]["callingCodes"][0].stringValue
                countryChoosed = AppDelegate.countryArray[i]["name"].stringValue
                countryAlphaCode = AppDelegate.countryArray[i]["alpha2Code"].stringValue
            }
        }
    }
    /*
     * Protocol used to get the Country code choosed
     */
    func getCountryCode(data: String,country : String, alpha: String) {
        print("Country: ", data)
        let PhoneTF = self.VideoJokesCell.viewWithTag(2) as! TextFieldPhone
        PhoneTF.text = "+" + data
        countryChoosed = country
        countryAlphaCode = alpha
    }
    func configureBirthdayTF(){
        let BirthdayTF = self.BirthDayCell.viewWithTag(1) as! TextFieldPhone
        let image = UIImage(named: "down_arrow_saif")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        BirthdayTF.rightView = imageView
        BirthdayTF.rightView?.frame.size = CGSize(width: image!.size.width , height: image!.size.height)
        BirthdayTF.rightView?.frame.origin.x = (BirthdayTF.rightView?.frame.origin.x)! - 3
        BirthdayTF.rightViewMode = .always
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let language = Bundle.main.preferredLocalizations.first! as NSString
        datePicker.locale = language == "fr" ? NSLocale(localeIdentifier: "fr_FR") as Locale : NSLocale(localeIdentifier: "en_EN") as Locale
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        let doneButton = UIBarButtonItem(title: language == "fr" ? "Terminé" : "Done", style: .done, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cancelButton = UIBarButtonItem(title: language == "fr" ? "Annuler" : "Cancel", style: .plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        BirthdayTF.inputAccessoryView = toolbar
        BirthdayTF.inputView = datePicker
        
    }
    func configureGenderTF(){
        let GenderTF = self.GenderCell.viewWithTag(1) as! TextFieldPhone
        let image = UIImage(named: "down_arrow_saif")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        GenderTF.rightView = imageView
        GenderTF.rightView?.frame.size = CGSize(width: image!.size.width , height: image!.size.height)
        GenderTF.rightView?.frame.origin.x = (GenderTF.rightView?.frame.origin.x)! - 3
        GenderTF.rightViewMode = .always
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let language = Bundle.main.preferredLocalizations.first! as NSString
        
        let doneButton = UIBarButtonItem(title: language == "fr" ? "Terminé" : "Done", style: .done, target: self, action: #selector(self.doneGenderPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cancelButton = UIBarButtonItem(title: language == "fr" ? "Annuler" : "Cancel", style: .plain, target: self, action: #selector(self.cancelGenderPicker))
        picker.dataSource = self
        picker .delegate = self
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        GenderTF.inputAccessoryView = toolbar
        GenderTF.inputView = picker
    }
    @objc func doneGenderPicker(){
        let GenderTF = self.GenderCell.viewWithTag(1) as! TextFieldPhone
        GenderTF.text = salutations[picker.selectedRow(inComponent: 0)]
        self.view.endEditing(true)
    }
    @objc func cancelGenderPicker(){
        self.view.endEditing(true)
    }
    @objc func donedatePicker(){
        let BirthdayTF = self.BirthDayCell.viewWithTag(1) as! TextFieldPhone
        let age = calculateAge()
        
        if age > 7 {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            BirthdayTF.text = formatter.string(from: datePicker.date)
        //BirthdayTF.text = String(age)
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = Localization("AgeMBV")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
        self.view.endEditing(true)
    }
    func calculateAge() -> Int{
        let now = Date()
        let birthDay = datePicker.date
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthDay,to: now)
        return  ageComponents.year!
    }
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    @objc func goVideo(){
        do {
        try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [.mixWithOthers])
           
        }catch{
            print("AVAudioSession : ",error.localizedDescription)
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        }catch{
            print("setActive: ",error.localizedDescription )
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
        }catch{
            print("overrideOutputAudioPort: ",error.localizedDescription )
        }
        let alert = UIAlertController(style: .actionSheet)
        alert.addPhotoLibraryPicker(flow: .vertical, paging: false, selection: .multiple(action: { assetes in
            NSLog("ok")
            if assetes.count != 0 {
                self.deleteSomeVideos(videos: assetes)
            }
        }), actuality: actuality)
        
        alert.addAction(title: Localization("Cancel"), style: .cancel)
        alert.show(navigation: self.navigationController!)
    }
    func deleteSomeVideos(videos:[VideoYLYL]) {
        var  dict =  "["
        for vid in videos {
            dict = dict + "{\"videoId\" : \"\(vid.getVideoId())\"},"
        }
        dict.removeLast()
        dict = dict + "]"
        
        print("Dict :",dict)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "videos" : dict
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.deleteVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("deletation  : ",data)
            }
        }catch {
        }
    }
    @objc func ChangePictureAction(_ button: UIButton){
        let pickerController = DKImagePickerController()
        pickerController.singleSelect = true
        pickerController.autoCloseOnSingleSelect = false
        pickerController.maxSelectableCount = 1
        pickerController.assetType = .allPhotos
        pickerController.allowMultipleTypes = false
        pickerController.allowsLandscape = false
        pickerController.showsCancelButton = true
        pickerController.isModalInPopover = false
        pickerController.didSelectAssets = {(assets:[DKAsset]) in
            let asset = assets.first
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            asset?.fetchOriginalImage(completeBlock: { (image, info) in
                (self.PictureCell.viewWithTag(1) as! UIImageView).image = image
                self.uploadToServer()
            })
        }
        pickerController.didCancel = {
            print("Not Now")
        /*    DispatchQueue.main.async {
                self.view.frame = frame
            } */
            self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            print("After : ",self.view.frame)
        }
        print("Before : ",view.frame)
        self.present(pickerController, animated: true) {
            print("When : ",self.view.frame)
        }
       // self.present(pickerController, animated: true, completion: nil)
    }
    @objc func changeState(){
       
        if verifyData() {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                let settings : Parameters = [
                    "userId" : a["_id"].stringValue,
                    "firstName" : (self.FirstNameCell.viewWithTag(1) as! UITextField).text!,
                    "lastName" : (self.LastNameCell.viewWithTag(1) as! UITextField).text!,
                    "age" : datePicker.date,
                    "userName" : (self.UserNameCell.viewWithTag(1) as! UITextField).text!
                ]
                let headers : HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                print(settings)
                print(headers)
                Alamofire.request(ScriptBase.sharedInstance.updateUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    let data = JSON(response.data ?? Data())
                     print("updateUser : ",data)
                    UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                  self.dismiss(animated: true, completion: nil)
                    
                }
                
            }catch {
                
            }
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = Localization("FillBlanks")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
    }
    func uploadToServer(){
       // SwiftSpinner.show("Uploading Picture...")
        let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"
        let contentType = "multipart/form-data; boundary=" + boundaryConstant
        
        let mimeType = "image/jpeg"
        
       
        let uploadScriptUrl = URL(string:ScriptBase.sharedInstance.uploadImage)
        let fileData : Data? = (self.PictureCell.viewWithTag(1) as! UIImageView).image!.jpegData(compressionQuality: 1)
        let requestBodyData : NSMutableData = NSMutableData()
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
       
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let key = "userId"
        requestBodyData.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
        requestBodyData.appendString(string: "\(a["_id"].stringValue)\r\n")
        }catch{
            
        }
        requestBodyData.append(("--\(boundaryConstant)\r\n").data(using: String.Encoding.utf8)!)
        do {
            let a = try JSON(data: dataFromString!)
             let fieldName = "picture"
            let filename = "zonz" + a["_id"].stringValue + ".jpg"
        requestBodyData.append(( "Content-Disposition: form-data; name=\"\(fieldName)\"; filename=\"\(filename)\"\r\n").data(using: String.Encoding.utf8)!)
        }catch{
            
        }
        requestBodyData.append(( "Content-Type: \(mimeType)\r\n\r\n").data(using: String.Encoding.utf8)!)
        
        //dataString += String(contentsOfFile: SongToSave.path, encoding: NSUTF8StringEncoding, error: &error)!
        requestBodyData.append(fileData!)
        // dataString += try! String(contentsOfFile: SongToSave.path, encoding: String.Encoding.utf8)
        requestBodyData.append(("\r\n").data(using: String.Encoding.utf8)!)
        requestBodyData.append(("--\(boundaryConstant)--\r\n").data(using: String.Encoding.utf8)!)
        var request = URLRequest(url: uploadScriptUrl!)
        
        
        request.httpMethod = "POST"
        request.httpBody = requestBodyData as Data
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // You can print out response object
            print("******* response = \(String(describing: response))")
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("****** response data = \(responseString!)")
           // SwiftSpinner.hide()
            
            let b = JSON(data!)
            print(b)
            if b["error"].exists() == false {
          print("upload finished")
                do {
                    var a = try JSON(data: dataFromString!)
                    a["userImageURL"].stringValue = b["userImageURL"].stringValue
                    UserDefaults.standard.setValue(a.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                     MapleBacon.shared.cache.clearDisk()
                }catch{
                    
                }
            }else{
               print("upload failed")
            }
            
        }
        
        task.resume()
    }
    @objc func NothingAction(){
        //self.dismiss(animated: true, completion: nil)
         let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
         let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if FirstLogin == false {
        self.navigationController?.popViewController(animated: true)
        }else{
    if verifyData() {
        
        
            do {
                let a = try JSON(data: dataFromString!)
                print("Verify: ",verifyData())
                let settings : Parameters = [
                    "userId" : a["_id"].stringValue,
                    "firstName" : (self.FirstNameCell.viewWithTag(1) as! UITextField).text!,
                    "lastName" : (self.LastNameCell.viewWithTag(1) as! UITextField).text!,
                    "userBirthday" : (self.BirthDayCell.viewWithTag(1) as! UITextField).text!,
                    "userName" : (self.UserNameCell.viewWithTag(1) as! UITextField).text!,
                    "userGender" : (self.GenderCell.viewWithTag(1) as! UITextField).text!,
                    "paysUser" : countryChoosed,
                    "alphaCode" : countryAlphaCode
                ]
                let headers : HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                print(settings)
                print(headers)
                Alamofire.request(ScriptBase.sharedInstance.updateUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    let data = JSON(response.data ?? Data())
                    print("updateUser : ",data)
                    UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                   
                    self.navigation.popToRootViewController(animated: true)
                     NotificationCenter.default.post(name: NSNotification.Name.init("FromEditToHome"), object: nil)
                    
                }
            }catch {
                print("JSON error:",error.localizedDescription)
            }
        }else{
        let banner = NotificationBanner(customView: self._notificationToast)
        _notificationToast.LabelN.text = Localization("FillBlanks")
        banner.customBannerHeight = 80
        banner.show(queuePosition: .front, bannerPosition: .top)
        
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
class FixUIController :  ServerUpdateDelegate {
    

    
    var alert : UIAlertController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.revengeCallback(_:)), name: NSNotification.Name.init("RevengeSlave"), object: nil)
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
             appDelegate.navigation = self.navigationController ?? UINavigationController()
    }
   
    @objc func revengeCallback(_ notification :Notification) {
        print("revenge")
        print(self)
        let tab = notification.object as! [String : Any]
        //secondUser: String,room: NSNumber,Type:GameType
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let secondUser = tab["secondUser"] as! String
        let room  = tab["room"] as! NSNumber
        let Type = tab["Type"] as! GameType
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            var FirstAttempt = 0
            SocketIOManager.sharedInstance.videoRoomRequestAccepted(completionHandler: { json in
                SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                print("Game accepted")
               
                print(json)
                if FirstAttempt == 0 {
                    FirstAttempt = 1
                    var i = 0
                    self.alert.dismiss(animated: true, completion: nil)
                    // let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                    //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                    let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                    vc.willAppear = true
                    vc.Owner = "master"
                    vc.FriendOrMatch = false
                    vc.roomId = NSNumber(value: Int(json[0])!)
                    vc.SecondUserId = secondUser
                    vc.selectedJokes = SocketIOManager.sharedInstance.selectedJokes
                    //vc.navigation = self.navigationController
                    let viewUnknown =  SocketIOManager.sharedInstance.presentStartViewController()
                    if viewUnknown.isKind(of: AuthentificationController.self) {
                        vc.navigation = SocketIOManager.sharedInstance.currentPresentedViewController.navigationController
                        SocketIOManager.sharedInstance.currentPresentedViewController.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        vc.navigation = (viewUnknown as! UINavigationController)
                        (viewUnknown as! UINavigationController).pushViewController(vc, animated: true)
                    }
                    //MARK : This Master leads to VideoGameCOntroller
                    //self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                
            })
            SocketIOManager.sharedInstance.videoRoomRequestRefused(completionHandler: { json in
                print("Game Refused")
                SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                let alertR = UIAlertController(title: Localization("Zonzay Challenge"), message: Localization("ZonzayFriendCancel"), preferredStyle: .alert)
                self.navigationController?.dismiss(animated: true, completion: nil)
                let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                   
                })
                alertR.addAction(ok)
                self.present(alertR, animated: true, completion: nil)
                
            })
            SocketIOManager.sharedInstance.joinVideoRoomRequest(userId: a["_id"].stringValue, userName: a["userName"].stringValue, SecondUserId: secondUser, VideoRoom: room.stringValue, gameType: Type)
            self.alert = UIAlertController(title: nil, message: Localization("PleaseWait"), preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: Localization("Cancel"), style: .cancel , handler: { (alert) in
                //self.navigationController?.dismiss(animated: true, completion: nil)
            })
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            self.alert.addAction(cancelAction)
            self.alert.view.addSubview(loadingIndicator)
            //let nav = UINavigationController(rootViewController: self.alert)
             let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.navigation.present(self.alert, animated: true, completion: nil)
            //nav.present(self.alert, animated: true, completion: nil)
        }catch{
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
    
    }
    
    }
class FixUITableController : UITableViewController{
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        
    }
}
extension NSMutableData {
        
        func appendString(string: String) {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            append(data!)
        }
}
extension UITableViewCell {
    func enable(on: Bool) {
        self.isUserInteractionEnabled = on
        for view in contentView.subviews {
            self.isUserInteractionEnabled = on
            view.alpha = on ? 1 : 0.5
        }
    }
}
