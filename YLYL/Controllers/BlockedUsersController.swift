//
//  BlockedUsersController.swift
//  YLYL
//
//  Created by macbook on 4/12/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
import Alamofire
import SwiftyJSON
class BlockedUsersController: ServerUpdateDelegate,UITableViewDelegate,UITableViewDataSource {
    var usersBlocked : JSON = []
    var usersBlockedModel : [User] = []
    var indexSelected : IndexPath!
    var test = true
    var navigation : UINavigationController!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersBlockedModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableV.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let viewBackground = cell.viewWithTag(4)
        viewBackground?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.36)
        let unblockBTN = cell.viewWithTag(2) as! CustomButton
        unblockBTN.index = indexPath.row
        unblockBTN.addTarget(self, action: #selector(self.didUnblockSomeOne(_:)), for: .touchUpInside)
        let FullName = cell.viewWithTag(5) as! UILabel
        FullName.text = self.usersBlockedModel[indexPath.row].getFirstName() + " " + self.usersBlockedModel[indexPath.row].getLastName()
        let passions = cell.viewWithTag(6) as! UILabel
        if self.usersBlockedModel[indexPath.row].getPassionList() != "" {
            var resString = ""
            let resIntersts = (self.usersBlockedModel[indexPath.row].getPassionList()).components(separatedBy: ";")
            for res in resIntersts {
                resString  = resString + "#" + res + " "
            }
            resString.removeLast()
            passions.text = resString
        }else{
            passions.isHidden = true
        }
        return cell
    }
    
    @IBOutlet weak var tableV : UITableView!
    
    @IBOutlet weak var blurBackground: UIVisualEffectView!
    
    @IBOutlet weak var confirmContainer: UIView!
    
    @IBOutlet weak var confirmLBL: UILabel!
    
    @IBOutlet weak var YesBTN: UIButton!
    
    @IBOutlet weak var NoBTN: UIButton!
    @IBOutlet weak var defaultLBL : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.view.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
        self.tableV.dataSource = self
        self.tableV.delegate = self
        self.tableV.tableFooterView = UIView(frame: CGRect.zero)
        getUsersBlocked()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
    }
    @objc func didUnblockSomeOne(_ sender: CustomButton) {
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveLinear], animations: {
            sender.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }) { (complete) in
            UIView.animate(withDuration: 0.1, delay: 0.1, options: [.curveLinear], animations: {
                sender.transform = CGAffineTransform.identity
            }){ (completeTwo) in
                guard let cell = self.tableV.cellForRow(at: IndexPath(row: sender.index, section: 0)) else {
                    return
                }
                let viewBack = cell.viewWithTag(4)
                self.indexSelected = IndexPath(row: sender.index, section: 0)
                UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                 viewBack?.alpha = 0
                    
                }, completion: { (completeThree) in
                    self.showHideConfirmContainer(false, completionHandler: { (okay) in
                        
                    })
                })
              
                
            }
        
    }
    }
    func showHideConfirmContainer(_ hide:Bool,completionHandler : @escaping (_ finish : Bool) -> Void) {
        if hide {
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                self.confirmContainer.alpha = 0
            }) { (complete) in
                self.confirmContainer.isHidden = true
                self.confirmContainer.alpha = 1
                UIView.animate(withDuration: 0.1, delay: 0.1, options: [.curveEaseOut], animations: {
                    self.blurBackground.alpha = 0
                    
                }, completion: { (completeTwo) in
                    self.blurBackground.isHidden = true
                    self.blurBackground.alpha = 1
                     completionHandler(true)
                })
            }
        }else{
            
            self.blurBackground.alpha = 0
            self.blurBackground.isHidden = false
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
               
                 self.blurBackground.alpha = 1
            }) { (complete) in
                  self.confirmContainer.alpha = 0
                self.confirmContainer.isHidden = false
              
                UIView.animate(withDuration: 0.1, delay: 0.1, options: [.curveEaseOut], animations: {
                    self.confirmContainer.alpha = 1
                    
                }, completion: { (completeTwo) in
                  completionHandler(true)
                })
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getUsersBlocked(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.blockedUserList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("blockedUserList : ",data)
                if data != JSON.null {
                    self.usersBlocked = data
                    if data["message"].exists() == false {
                        if self.usersBlocked.arrayObject?.count != 0 {
                            for i in 0...((self.usersBlocked.arrayObject?.count)! - 1) {
                                let user = User(id: self.usersBlocked[i]["_id"].stringValue, userFirstName: self.usersBlocked[i]["userFirstName"].stringValue, userLastName: self.usersBlocked[i]["userLastName"].stringValue, imageProfileUrl: self.usersBlocked[i]["userImageURL"].stringValue, username: self.usersBlocked[i]["userName"].stringValue, listPassion: self.usersBlocked[i]["listInterests"].stringValue)
                                self.usersBlockedModel.append(user)
                            }
                             self.defaultLBL.isHidden = true
                            self.tableV.reloadData()
                        }else{
                         self.defaultLBL.isHidden = false
                        }
                    }
                }
            }
        }catch {
            
        }
    }
    @IBAction func YesBTNAction(_ sender: UIButton) {
        self.unblockUser { (verif) in
            if verif {
                self.showHideConfirmContainer(true) { (complete) in
                    print("hello")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2 , execute: {
                        self.test = false
                        self.usersBlockedModel.remove(at: self.indexSelected.row)
                        self.tableV.beginUpdates()
                        self.tableV.deleteRows(at: [self.indexSelected], with: .fade)
                        self.tableV.endUpdates()
                    })
                    
                }
            }
        }
        
    }
    func unblockUser(completionHandler : @escaping (_ finish : Bool) -> Void){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let dateForm = DateFormatter()
            dateForm.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "userBlockedId" : self.usersBlockedModel[indexSelected.row].getId()
            ]
            
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.unBlockUserRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    print(response.data)
                    let data = JSON(response.data ?? Data())
                    print("unBlockRequest : ",data)
                    if data["message"].exists() {
                        // self.sendRequestButton.setTitle(Localization("Cancel_Request"), for: .normal)
                        print("error")
                        completionHandler(false)
                        
                    }else{
                        let alert = UIAlertController(title: Localization("BlockedUsers"), message: Localization("FriendUnblocked"), preferredStyle: .alert)
                        let alertAction = UIAlertAction(title: Localization("OK"), style: .default, handler: {(okay) in
                            completionHandler(true)
                        })
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    //self.jokes = data
                    
                }else{
                   self.defaultLBL.isHidden = false
                    
                }
                
            }
            
        }catch {
            
        }
    }
    @IBAction func BackButton(_ sender: UIButton) {
        self.navigation.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
    }
    @IBAction func NoBTNAction(_ sender: UIButton) {
        self.showHideConfirmContainer(true) { (complete) in
            let cell = self.tableV.cellForRow(at: self.indexSelected)
            let viewBack = cell?.viewWithTag(4)
            UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
                viewBack?.alpha = 1
                
            }, completion: { (completeThree) in
            })
        }
    }
}
extension BlockedUsersController {
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
    
}
