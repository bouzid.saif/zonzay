//
//  JokesFragmentController.swift
//  YLYL
//
//  Created by macbook on 1/24/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
protocol JokesFragmentDelegate {
    func getJokesFromFragment(jokes : [String])
}

class JokesFragmentController: ServerUpdateDelegate,UITableViewDelegate,UITableViewDataSource {
    var CellHeights : [IndexPath : CGFloat] = [:]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jokes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let jokeText = UITextView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 96, height: 24))
        
        jokeText.font =  UIFont.systemFont(ofSize: 17)
        
        jokeText.text = self.jokes[indexPath.row]
        let count = offSetTableViewIfNeeded(textView: jokeText)
        print("countttttttttttt:",count)
        if count > 4 {
            return   CGFloat(138 + (20 * (count - 4)))
        }else{
            return 138
        }
    }
    func offSetTableViewIfNeeded(textView:UITextView) -> Int {
        let numberOfGlyphs = textView.layoutManager.numberOfGlyphs
        var index : Int = 0
        var lineRange = NSRange(location: NSNotFound, length: 0)
        var currentNumOfLines : Int = 0
        var numberOfParagraphJump : Int = 0
        
        while index < numberOfGlyphs {
            textView.layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            currentNumOfLines += 1
            
            // Observing whether user went to line and if it's the first such line break, accounting for it.
            if textView.text.last == "\n", numberOfParagraphJump == 0 {
                numberOfParagraphJump = 1
            }
        }
        
        currentNumOfLines += numberOfParagraphJump
        print("Number of lines is:", currentNumOfLines)
        return currentNumOfLines
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         print("cellForRowAt")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell.selectionStyle = .none
        let ImageJoke = cell.viewWithTag(1) as! UIImageView
        if self.selectedCells.contains(where: { (key, value) -> Bool in
            return value == indexPath
        }) {
            ImageJoke.isHidden = true
            let index =  selectdJokes.indexes(of: jokes[indexPath.row])
            print("IndexJoke : ",index)
            (cell.viewWithTag(4) as! UILabel).text = String(index[0] + 1)
            cell.viewWithTag(3)?.isHidden = false
        }else{
        ImageJoke.image = #imageLiteral(resourceName: "joke.png")
        }
        let QuestionLBL = cell.viewWithTag(2) as! UILabel
        QuestionLBL.text = jokes[indexPath.row]
        let numberOfLines = QuestionLBL.calculateMaxLines()
        if numberOfLines <= 4 {
        CellHeights[indexPath] = 138
        }else{
            CellHeights[indexPath] =  CGFloat(138 + ( 17 * (numberOfLines - 4)))
        }
        
    
        return cell
    }
    
    /*
     * Mark : Delegate
     */
    /*
     * Mark : Delegate
     */
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        print("estimatedHeight")
        if let height =  self.CellHeights[indexPath] {
            return height
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if cell?.viewWithTag(3)!.isHidden == false {
            cell?.viewWithTag(3)!.isHidden = true
            cell?.viewWithTag(1)?.isHidden = false
           let index =  selectdJokes.indexes(of: jokes[indexPath.row])
            selectdJokes.removeString(text: jokes[indexPath.row])
             self.selectedCells.removeValue(forKey: (index[0] + 1))
            if index[0] == 1 {
                if self.selectedCells[3] != nil {
                    self.selectedCells.changeKey(from: 3, to: 2)
                    self.tableV.beginUpdates()
                    self.tableV.reloadRows(at: [self.selectedCells[2]!], with: .none)
                    self.tableV.endUpdates()
                }
            }
            if index[0] == 0 {
                if self.selectedCells[2] != nil {
                    self.selectedCells.changeKey(from: 2, to: 1)
                    self.tableV.beginUpdates()
                    self.tableV.reloadRows(at: [self.selectedCells[1]!], with: .none)
                    self.tableV.endUpdates()
                }
                if self.selectedCells[3] != nil {
                    self.selectedCells.changeKey(from: 3, to: 2)
                    self.tableV.beginUpdates()
                    self.tableV.reloadRows(at: [self.selectedCells[2]!], with: .none)
                    self.tableV.endUpdates()
                }
            }
        }else{
             if selectdJokes.count < 3 {
                selectdJokes.append(jokes[indexPath.row])

                (cell?.viewWithTag(4) as! UILabel).text = String(selectdJokes.count)
                var index = selectdJokes.count
                if index == 0 {
                    index = 1
                }
                self.selectedCells[index] = indexPath
                cell?.viewWithTag(3)!.isHidden = false
                cell?.viewWithTag(1)?.isHidden = true
                //cell?.setSelected(false, animated: true)
            }
        }
        print(selectdJokes)
    }
    
    var jokes : [String] = []
    var selectdJokes : [String] = []
    var selectedCells : [Int : IndexPath] = [:]
    var JokesJSON : JSON = []
    var delegate: JokesFragmentDelegate?
    var WhatInterface = ""
    @IBOutlet weak var defaultMessage : UILabel!
    @IBOutlet weak var tableV : UITableView!
    /*
     * The view that create a focus alpha on the AddContainer
     */
    @IBOutlet weak var AddView: UIView!
    /*
     * It contains our Joke text and the sava&cancel buttons
     */
    @IBOutlet weak var AddContainer: UIView!
    /*
     * Where the user will write his joke
     */
    @IBOutlet weak var JokeTextView: PlaceholderTextView!
    /*
     * The Done button
     */
    @IBOutlet weak var DoneBTN: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableV.delegate = self
        self.tableV.dataSource = self
        self.configureConnectButton(button: DoneBTN)
        self.getJokesFromDataBase()
        NotificationCenter.default.addObserver(self, selector: #selector(self.listenFromResult(_:)), name: Notification.Name.init("DissmissJoke"), object: nil)
       self.tableV.rowHeight = UITableView.automaticDimension
    }
    @objc func listenFromResult(_ notification :Notification){
        
        self.navigationController?.popViewController(animated: false)
    }
    /*
     * configure the button style
     */
    func configureConnectButton(button:UIButton) {
        button.layer.cornerRadius = 9.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.DoneAction(_:)), for: .touchUpInside)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getJokesFromDataBase(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        self.tableV.isHidden = false
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseString { response in
                if response.data != nil {
                    var stringToParse = response.result.value!
                    if stringToParse != "" {
                        print("stringToParse : ",stringToParse)
                        stringToParse.removeFirst()
                        stringToParse.removeLast()
                        stringToParse = stringToParse.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        let data = JSON(stringLiteral: response.result.value!)
                        
                        print("JokesResponse : ",data)
                        if data != JSON.null {
                            
                            let parsedString = stringToParse.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            do{
                                
                               var JokesJSON = try JSONDecoder().decode([Jokes].self, from: parsedString!)
                                print(self.JokesJSON.count)
                                if JokesJSON.count != 0 {
                                    for i in 0...((JokesJSON.count) - 1) {
                                        self.jokes.append(JokesJSON[i].joke)
                                    }
                                    self.tableV.reloadData()
                                }
                            }catch{
                                print("error converting",error.localizedDescription)
                            }
                        }
                        
                    }
                //self.jokes = data
               
                }else{
                    self.defaultMessage.isHidden = false
                    self.tableV.isHidden = true
                }
                
            }
            
        }catch {
            
        }
    }
    
    /*
     * Add Joke Action
     */
    @IBAction func AddJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = false
        self.JokeTextView.becomeFirstResponder()
    }
    /*
     * Save button Action
     */
    @IBAction func SaveJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = true
        
        if self.JokeTextView.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")) != "" {
            
            //self.jokes.append(JokeTextView.text!)
            self.saveJokesIntoDataBase(joke : (JokeTextView.text?.replacingOccurrences(of: "\n", with: " ", options: NSString.CompareOptions.literal, range: nil))!)
            //self.tableView.reloadData()
        }
        self.JokeTextView.text = ""
    }
    @objc func DoneAction(_ sender: UIButton) {
        if self.selectdJokes.count == 3 {
        self.navigationController?.popViewController(animated: true)
        delegate?.getJokesFromFragment(jokes: self.selectdJokes)
        }else{
            let alert = UIAlertController(title: Localization("JokesSelection"), message: Localization("JokesThree"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func saveJokesIntoDataBase(joke:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        self.tableV.isHidden = false
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let res = self.constructJson(json: self.jokes, joke: joke)
            self.JokesJSON = res
            self.jokes.append(joke)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.tableV.reloadData()
                }else{
                    self.tableV.reloadData()
                    self.defaultMessage.isHidden = false
                    self.tableV.isHidden = true
                }
                
            }
            
        }catch {
            
        }
    }
    /*
     * Cancel button Action
     */
    @IBAction func CancelJokeAction(_ sender: UIButton) {
        self.AddView.isHidden = true
        self.JokeTextView.text = ""
        
    }
    func constructJson(json:[String],joke:String) -> JSON{
        var result = "["
        for j in json {
            result = result + "{\"jokeId\":\"\",\"joke\":\"" + j + "\",\"userId\":\"\"},"
        }
        result = result + "{\"jokeId\":\"\",\"joke\":\"" + joke + "\",\"userId\":\"\"}]"
        return JSON(stringLiteral: result)
    }
    
}
extension UILabel {
    func calculateMaxLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
}
extension Dictionary {
    mutating func changeKey(from: Key, to: Key) {
        self[to] = self[from]
        self.removeValue(forKey: from)
    }
}
