//
//  UITextFields+Padding.swift
//  YLYL
//
//  Created by macbook on 1/7/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
class TextField: UITextField {
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 30, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left:  30, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 30, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
}
class TextFieldPhone: UITextField {
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left:  15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
}
class TextFieldCode: UITextField {
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left:  15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: 3, left: 15, bottom: 3, right: 0)
        return  bounds.inset(by: padding)
    }
}
extension TextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get{
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor : newValue!])
        }
    }
    
}
extension TextFieldPhone {
    @IBInspectable var placeHolderColor: UIColor? {
        get{
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor : newValue!])
        }
    }
    
}
extension TextFieldCode {
    @IBInspectable var placeHolderColor: UIColor? {
        get{
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "", attributes: [NSAttributedString.Key.foregroundColor : newValue!])
        }
    }
    
}
