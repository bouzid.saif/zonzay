//
//  StartViewController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 07/01/2019.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftyJSON
import QuartzCore
import Alamofire
import StoreKit
import ChameleonFramework
import Photos
import OneSignal
import GoogleMobileAds
class StartViewController: NavigationProfile,JokesFragmentDelegate,FloatyDelegate,UITableViewDelegate,UITableViewDataSource,AVCaptureVideoDataOutputSampleBufferDelegate,AVCaptureMetadataOutputObjectsDelegate,SlideToControlDelegate {
    
    //Tutorial & Gifts
    
    @IBOutlet weak var visualEffectBlur: UIVisualEffectView!
    
    //Gift Steps
    @IBOutlet weak var watchVideoContainer: UIView!
    
    @IBOutlet weak var watchVideoLBL: UILabel!
    
    @IBOutlet weak var winFiveLBL: UILabel!
    
    @IBOutlet weak var watchBTN: UIButton!
    
    
    @IBOutlet weak var fiveGiftsZonzContainer: UIView!
    
    @IBOutlet weak var gifFiveZzonzIMG: UIImageView!
    
    @IBOutlet weak var videoTutoView: UIView!
    
    @IBOutlet weak var closeVideoTutoBTN: UIButton!
    
    @IBOutlet weak var backFriendBTN: UIButton!
   
    @IBAction func backFriendAction(_ button: UIButton) {
        DispatchQueue.main.async {
        self.tableV.isHidden = true
        self.backFriendBTN.isHidden = true
        self.containerAlphaColor.isHidden = true
        self.OptionsButton.isHidden = false
              //self.soundBTN.isHidden = false
        self.TutoButton.isHidden = false
        self.giftButton.isHidden = false
        }
      
    }
    @IBAction func closeVideoTutoAction(_ sender: UIButton) {
        self.moviePlayer.pause()
        self.moviePlayerLayer.removeFromSuperlayer()
        self.statusBarIsHidden = false
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.visualEffectBlur.alpha = 0
            self.videoTutrialContainer.alpha = 0
              self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
        }, completion: {(verif) in
          
        })
    }
    @IBAction func watchAction(_ sender: UIButton) {
        self.videoTutrialContainer.isHidden = false
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.watchTutorialContainer.alpha = 0
            self.videoTutrialContainer.alpha = 1
        }, completion: {(verif) in
            self.watchTutorialContainer.isHidden = true
          self.moviePlayer.play()
           
        })
    
       
       
    }
    
    //Tutorial Steps
    @IBOutlet weak var watchTutorialContainer: UIView!
    
    @IBOutlet weak var videoTutrialContainer: UIView!
    
    @IBOutlet weak var doYouWantWatchLBL: UILabel!
    
    @IBOutlet weak var yesWatchBTN: UIButton!
    
    @IBOutlet weak var noWatchBTN: UIButton!
    var statusBarIsHidden = false
    @IBAction func yesWatchAction(_ sender: UIButton) {
        self.videoTutrialContainer.isHidden = false
        initVideoTuto()
        statusBarIsHidden = true
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
           self.watchTutorialContainer.alpha = 0
            self.videoTutrialContainer.alpha = 1
        }, completion: {(verif) in
           self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
           self.moviePlayerLayer.frame = self.videoTutoView.bounds
            self.moviePlayer.play()
        })
    }
    override var prefersStatusBarHidden: Bool {
        return statusBarIsHidden
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.moviePlayerLayer.frame = self.videoTutoView.bounds
    }
    @IBAction func noWatchAction(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.watchTutorialContainer.alpha = 0
            self.visualEffectBlur.alpha = 0
        }, completion: {(verif) in
            self.watchTutorialContainer.isHidden = true
              self.visualEffectBlur.isHidden = true
            
        })
    }
    
    
    //
    @IBOutlet weak var soundBTN: UIButton!
    
    @IBOutlet weak var giftButton: UIButton!
    @IBAction func GiftAction(_ sender: UIButton) {
      
        if UserDefaults.standard.value(forKey: "ZonzayAds") == nil {
            self.visualEffectBlur.isHidden = false
            self.watchVideoContainer.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
             self.visualEffectBlur.alpha = 1
             self.watchVideoContainer.alpha = 1
        }, completion: {(verif) in
            
            UserDefaults.standard.setValue("true", forKey: "ZonzayAds")
            UserDefaults.standard.synchronize()
        })
        }else{
            if GADRewardBasedVideoAd.sharedInstance().isReady == true {
                GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
            }
            
        }
    }
    
    @IBOutlet weak var TutoButton : UIButton!
    
    @IBAction func Tuto(_ sender: UIButton) {
        self.visualEffectBlur.isHidden = false
        self.watchTutorialContainer.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.visualEffectBlur.alpha = 1
            self.watchTutorialContainer.alpha = 1
        }, completion: {(verif) in
            
        })
    }
    func initAdMob(){
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(), withAdUnitID: "ca-app-pub-3940256099942544/1712485313")
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        
    }
    var moviePlayer : AVPlayer = AVPlayer()
    var moviePlayerLayer : AVPlayerLayer = AVPlayerLayer()
    var moviePlayerAd : AVPlayer = AVPlayer()
    var moviePlayerLayerAd : AVPlayerLayer = AVPlayerLayer()
    func initRewardAd(){
        let videoBundleURL = Bundle.main.path(forResource: "add_FiveZonz", ofType: "mp4")
        if let videoBundleURL = videoBundleURL {
            let videoURL = NSURL.fileURL(withPath: videoBundleURL)
            self.gifFiveZzonzIMG.backgroundColor = .black
            self.moviePlayerAd = AVPlayer(url: videoURL)
            self.moviePlayerAd.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
            self.moviePlayerLayerAd = AVPlayerLayer(player: self.moviePlayerAd)
            self.moviePlayerLayerAd.frame = self.gifFiveZzonzIMG.bounds
            self.moviePlayerLayerAd.videoGravity = AVLayerVideoGravity.resize
            self.gifFiveZzonzIMG.layer.addSublayer(self.moviePlayerLayerAd)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlayingAd(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.moviePlayerAd.currentItem)
            
        }
    }
    func initVideoTuto(){
        let videoBundleURL = Bundle.main.path(forResource: "tuto_play", ofType: "mp4")
        if let videoBundleURL = videoBundleURL {
            let videoURL = NSURL.fileURL(withPath: videoBundleURL)
           self.videoTutrialContainer.backgroundColor = .black
            self.moviePlayer = AVPlayer(url: videoURL)
            self.moviePlayer.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
            self.moviePlayerLayer = AVPlayerLayer(player: self.moviePlayer)
            self.moviePlayerLayer.frame = self.videoTutoView.bounds
            self.moviePlayerLayer.videoGravity = AVLayerVideoGravity.resize
            self.videoTutoView.layer.addSublayer(self.moviePlayerLayer)
            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.moviePlayer.currentItem)
            
        }
    }
    @objc func playerDidFinishPlayingt(note: Notification) {
        self.statusBarIsHidden = false
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.videoTutrialContainer.alpha = 0
            self.visualEffectBlur.alpha = 0
        }, completion: {(verif) in
            self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
            self.videoTutrialContainer.isHidden = true
            self.visualEffectBlur.isHidden = true
            
        })
    }
    @objc func playerDidFinishPlayingAd(note: Notification) {
        addZonz()
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.fiveGiftsZonzContainer.alpha = 0
            self.visualEffectBlur.alpha = 0
        }, completion: {(verif) in
           
            self.fiveGiftsZonzContainer.isHidden = true
            self.visualEffectBlur.isHidden = true
            
        })
    }
    func addZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.registerSocket(Name: a["_id"].stringValue)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addZonzAD, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("addZonz : ",data)
                self.getZonz()
                
                
            }
            
        }catch {
            
        }
    }
    var isFriendsOrMatchMaking = true
    var currentFrame : CMSampleBuffer? = nil
    //static let shared = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
    @objc func revengeCallback(_ notification :Notification) {
            print("revenge")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
       
        let tab = notification.object as! [String : Any]
        //secondUser: String,room: NSNumber,Type:GameType
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let secondUser = tab["secondUser"] as! String
        let room  = tab["room"] as! NSNumber
        let Type = tab["Type"] as! GameType
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if appDelegate.ConnectedUser.contains(secondUser) {
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            var FirstAttempt = 0
        SocketIOManager.sharedInstance.videoRoomRequestAccepted(completionHandler: { json in
            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                self.tableV.isHidden = true
            self.backFriendBTN.isHidden = true
                self.containerAlphaColor.isHidden = true
                self.OptionsButton.isHidden = false
                self.TutoButton.isHidden = false
                //self.soundBTN.isHidden = false
                print("Game accepted")
                print(json)
                if FirstAttempt == 0 {
                    FirstAttempt = 1
                    var i = 0
                    for view in (self.navigationController?.viewControllers)! {
                        if view .isKind(of: VideoGameRework.self) || view .isKind(of: VideoGameResultController.self) {
                            self.navigationController?.viewControllers.remove(at: i)
                        }
                        i = i + 1
                    }
                    self.alert.dismiss(animated: true, completion: nil)
                   // let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                    //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                    let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                    vc.willAppear = true
                    vc.Owner = "master"
                    vc.FriendOrMatch = false
                    vc.roomId = NSNumber(value: Int(json[0])!)
                    vc.SecondUserId = secondUser
                     vc.selectedJokes = self.selectedJokes
                    vc.navigation = self.navigationController
                    //MARK : This Master leads to VideoGameCOntroller
                    vc.hero.modalAnimationType = .zoom
                    vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
                    //self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            })
            SocketIOManager.sharedInstance.videoRoomRequestRefused(completionHandler: { json in
                print("Game Refused")
                SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                 self.tableV.isHidden = true
                self.backFriendBTN.isHidden = true
                self.containerAlphaColor.isHidden = true
                self.OptionsButton.isHidden = false
                self.TutoButton.isHidden = false
                  //self.soundBTN.isHidden = false
                let alertR = UIAlertController(title: Localization("Zonzay Challenge"), message: Localization("ZonzayFriendCancel"), preferredStyle: .alert)
                self.navigationController?.dismiss(animated: true, completion: nil)
                let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                     self.tableV.isHidden = true
                    self.backFriendBTN.isHidden = true
                    self.containerAlphaColor.isHidden = true
                })
                alertR.addAction(ok)
                self.present(alertR, animated: true, completion: nil)
                
            })
            SocketIOManager.sharedInstance.joinVideoRoomRequest(userId: a["_id"].stringValue, userName: a["userName"].stringValue, SecondUserId: secondUser, VideoRoom: room.stringValue, gameType: Type)
            self.alert = UIAlertController(title: nil, message: Localization("PleaseWait"), preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: Localization("Cancel"), style: .cancel , handler: { (alert) in
                 self.tableV.isHidden = true
                self.backFriendBTN.isHidden = true
                self.containerAlphaColor.isHidden = true
                self.OptionsButton.isHidden = false
                self.TutoButton.isHidden = false
                  //self.soundBTN.isHidden = false
                //self.navigationController?.dismiss(animated: true, completion: nil)
            })
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating();
            self.alert.addAction(cancelAction)
            self.alert.view.addSubview(loadingIndicator)
            self.navigationController!.present( self.alert, animated: true, completion: nil)
        }catch{
            
        }
        }else{
            //TODO: User is disconnected so alert it
            }
            
        }
    }
    func getJokesFromFragment(jokes: [String]) {
        if jokes.count != 0 {
        self.selectedJokes = jokes
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        }
       
        print("delegate:",self.selectedJokes)
        //self.view.bringSubviewToFront(self.containerAlphaColor)
        if isFriendsOrMatchMaking {
         self.tableV.isHidden = false
            self.backFriendBTN.isHidden = false
        self.containerAlphaColor.isHidden = false
       
        }else{
            SocketIOManager.sharedInstance.getOppents(completionHandler: { (tab) in
                print("Opponent Found : ",tab)
                self.gifPlayer.stopAnimatingGif()
                self.gifPlayer.setGifImage(UIImage(gifName: "MatchMaking2.gif"))
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                    self.showLoaderMatchMaking(hide: true, orientation: "left")
                    
                    if self.SocketMatchMaking == 0 {
                        self.SocketMatchMaking = 1
                        if tab.count != 0 {
                            if tab[2] as! String == "M" {
                                let janus = VideoRoomController()
                                janus.attachRoomToJanus(roomId: tab[1] as! NSNumber, completionHandler: { owner in
                                    print(owner)
                                    //self.navigationController?.dismiss(animated: true, completion: nil)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                                        //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                                        //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                                        let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                                        
                                        vc.willAppear  = true
                                        vc.Owner = "master"
                                        vc.FriendOrMatch = true
                                        vc.roomId = tab[1] as? NSNumber
                                        vc.SecondUserId = tab[0] as? String
                                        vc.navigation = self.navigationController
                                        vc.jokes = self.selectedJokes
                                        vc.selectedJokes = self.selectedJokes
                                        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                                        vc.hero.modalAnimationType = .zoom
                                        vc.modalPresentationStyle = .fullScreen
                                        self.present(vc, animated: true, completion: nil)
                                        //self.navigationController?.present(vc, animated: true, completion: nil)
                                        
                                       // self.navigationController?.pushViewController(vc, animated: true)
                                    })
                                    
                                    //self.jokes = data
                                })
                            }else{
                                //  self.navigationController?.dismiss(animated: true, completion: nil)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                                    //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                                    //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                                    let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                                    vc.willAppear  = true
                                    vc.Owner = "slave"
                                    vc.FriendOrMatch = true
                                    vc.roomId = tab[1] as? NSNumber
                                    vc.SecondUserId = tab[0] as? String
                                    vc.jokes = self.selectedJokes
                                    vc.selectedJokes = self.selectedJokes
                                    vc.navigation = self.navigationController
                                    //self.navigationController?.present(vc, animated: true, completion: nil)
                                    self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                                    vc.hero.modalAnimationType = .zoom
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                    //self.navigationController?.pushViewController(vc, animated: true)
                                })
                            }
                        }else{
                            self.navigationController?.dismiss(animated: true, completion: nil)
                        }
                    }
                    
                })
            })
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                
                let aX = UserDefaults.standard.value(forKey: "Subscription") as! String
                if aX == "true"{
                    if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                        let gender = UserDefaults.standard.value(forKey: "MatchChoice") as? String
                        let min = UserDefaults.standard.value(forKey: "minChoice") as? String
                        let max = UserDefaults.standard.value(forKey: "maxChoice") as? String
                        
                        SocketIOManager.sharedInstance.sendMatchMakingPremium(userId: a["_id"].stringValue, minAge: min!, maxAge: max!, Gender: gender!)
                        
                    }else{
                        SocketIOManager.sharedInstance.sendMatchMakingPremium(userId: a["_id"].stringValue, minAge: "18", maxAge: "75", Gender: a["userGender"].stringValue == "Male" ? "Feminin" : "Male")
                    }
                    
                }else{
                    SocketIOManager.sharedInstance.sendMatchMaking(userId: a["_id"].stringValue)
                }
                self.showLoaderMatchMaking(hide: false, orientation: "none")
                
            }catch{
                
                
            }
        }
        
        }
    }
    
    @IBAction func closeChooseFriends(_ sender: UITapGestureRecognizer) {
         self.tableV.isHidden = true
        self.containerAlphaColor.isHidden = true
        self.backFriendBTN.isHidden = true
    }
    //Views
    @IBOutlet weak var profileImageView: ExtensionProfile!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var zonzayMoneyLabel: UILabel!
    @IBOutlet  var cameraview: UIView!
    @IBOutlet weak var OptionsButton: Floaty!
    @IBOutlet weak var tableV : UITableView! {
        didSet {
            tableV.tableFooterView = UIView(frame: .zero)
        }
    }
    @IBOutlet weak var containerMatchmaking: UIView!
    
    @IBAction func showVIPAction(_ sender: UIButton) {
        let vc = UIStoryboard(name: "VIPAccess", bundle: nil).instantiateViewController(withIdentifier: "VIPAccessController") as! VIPAccessController
        vc.hero.modalAnimationType = .zoom
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        // showVIPContainer()
    }
    @IBOutlet weak var containerSound: UIView!
    @IBOutlet  var GLUIView: UIView!
    
    @IBOutlet weak var masterSoundLBL: UILabel!
    @IBOutlet weak var masterSoundIcon : UIImageView!
    @IBOutlet weak var CalqueSound: UIView!
    @IBOutlet weak var masterSoundONOFFLBL: UILabel!
    @IBAction func soundBTNAction(_ sender: UIButton) {
        if SoundBackGround.enabled {
            SoundBackGround.enabled = false
            self.masterSoundONOFFLBL.text = "OFF"
            soundBTN.setImage(UIImage(named: "SoundOffIcon"), for: .normal)
            self.masterSoundIcon.alpha = 0.75
            self.soundBTN.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                self.soundBTN.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            }) { (verif) in
                UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                    self.soundBTN.transform = CGAffineTransform.identity
                }) { (verifTwo) in
                    
                }
            }
            self.CalqueSound.isHidden = false
            self.containerSound.alpha = 0
            self.containerSound.isHidden = false
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.containerSound.alpha =     1
            }) { (verif) in
                UIView.animate(withDuration: 0.3, delay: 0.5, options: [.curveEaseIn], animations: {
                     self.containerSound.alpha =     0
                }) { (verifTwo) in
                     self.containerSound.isHidden = true
                    self.CalqueSound.isHidden = true
                }
            }
        }else{
           SoundBackGround.enabled = true
           self.masterSoundONOFFLBL.text = "ON"
           soundBTN.setImage(UIImage(named: "SoundOnIcon"), for: .normal)
            self.masterSoundIcon.alpha = 1
           SoundBackGround.play(file: "background_music", fileExtension: "mp3", numberOfLoops: -1)
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                self.soundBTN.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            }) { (verif) in
                UIView.animate(withDuration: 0.15, delay: 0, options: [.curveEaseIn], animations: {
                    self.soundBTN.transform = CGAffineTransform.identity
                }) { (verifTwo) in
                    
                }
            }
            self.CalqueSound.isHidden = false
            self.containerSound.alpha = 0
            self.containerSound.isHidden = false
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
                self.containerSound.alpha =     1
            }) { (verif) in
                UIView.animate(withDuration: 0.3, delay: 0.5, options: [.curveEaseIn], animations: {
                    self.containerSound.alpha =     0
                }) { (verifTwo) in
                    self.containerSound.isHidden = true
                    self.CalqueSound.isHidden = true
                }
            }
        }
    }
    
    @IBOutlet weak var gifPlayer: UIImageView!
    
    @IBOutlet weak var pleaseWaitLBL: UILabel!
    
    @IBOutlet weak var navBarTop: UIView!
    @IBOutlet weak var SliderCancelMatchM: SlideToControl!
    fileprivate var didSetupConstraints: Bool = false
    
    @IBOutlet weak var viewUPFilter: UIView!
    
    @IBOutlet weak var showVIPBTN: UIButton!
    
    @IBOutlet weak var viewMinusFilter: UIView!
    
    @IBOutlet weak var viewClaqueSaved: UIView!
    
    
    @IBOutlet weak var viewContainerSaved: UIView!
    
    @IBOutlet weak var SavedImageView: UIImageView!
    
    @IBOutlet weak var SavedTextLBL: UILabel!
    var tapgestureShow : UITapGestureRecognizer!
    var indexFilter : Int = 0
    var items = ["noneImage","leopard3_512", "icon 2","Test","test2","test3","Pipeandglassesmask","Pipeandglassesmasksizefit","Blackpanthernoeyes","Blackpantherwitheyes"]
    var imageViewSnap : UIImageView! = UIImageView()
    var stackView : UIStackView! = UIStackView()
    var scrollview : UIScrollView! = UIScrollView()
    fileprivate lazy var filterCollection: HFSwipeView = {
        let view = HFSwipeView.newAutoLayout()
        view.autoAlignEnabled = true
        view.circulating = true        // true: circulating mode
        view.delegate = self
        view.dataSource = self
        view.pageControlHidden = true
        view.currentPage = 0
        view.magnifyCenter = true
        view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        view.preferredMagnifyBonusRatio = 1.5
        return view
    }()
    
    
    @IBOutlet weak var filterIndicator: InstagramActivityIndicator!
    @IBOutlet weak var filterPickerDefault: UIImageView!
    let captureSession = AVCaptureSession()
    let stillImageOutput = AVCapturePhotoOutput()
    let output = AVCaptureVideoDataOutput()
    let metaOutput = AVCaptureMetadataOutput()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var captureDevice : AVCaptureDevice?
    var selectedJokes : [String] = []
    var friends : JSON = []
    var alert : UIAlertController!
    var SocketMatchMaking = 0
    var observerCount = 0
    //Filter vars
    var isFaceDetectionOff:Bool = false
    var glView:OGLView!
    let wrapper = DlibWrapper()
    
    var currentMetadata: [Any] = []
    
    var lastFaceDetectedTime:Double = 0
    
    let FDTimeThreshold:Double = 20.0
    var layer = AVSampleBufferDisplayLayer()
    let sampleQueue = DispatchQueue(label: "com.zweigraf.DisplayLiveSamples.sampleQueue", attributes: [])
    let faceQueue = DispatchQueue(label: "com.zweigraf.DisplayLiveSamples.faceQueue", attributes: [])
    var faceMaskArray = NSMutableArray()
    var collectionArray:NSMutableArray?
    let landmarkParser = LandmaskXMLParser()
    var isGLViewAdded = false
    var selectedCell:UICollectionViewCell?
    //
    @IBOutlet weak var gifButton: UIButton!
    
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var collectionFilter: UICollectionView!
    @IBOutlet weak var cameraFilterButton: UIButton!
    
    
    @IBAction func go_to_gifs(_ sender: UIButton) {
        detectDownloadFinished { (verif) in
            if verif {
               let vc = UIStoryboard(name: "GifStore", bundle: nil).instantiateViewController(withIdentifier: "GifBuyController") as! GifBuyController
                
                self.present(vc, animated: true, completion: nil)
            }else{
               let alert = UIAlertController(title: "Zonzay Additional", message: "Please wait a few sec, we are downloading new additionl updates", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    override func updateViewConstraints() {
        if !didSetupConstraints {
            filterCollection.autoSetDimension(.height, toSize: 100)
            filterCollection.autoPinEdge(toSuperviewEdge: .leading)
            filterCollection.autoPinEdge(toSuperviewEdge: .trailing)
            filterCollection.autoPinEdge(toSuperviewEdge: .bottom)
            didSetupConstraints = true
        }
        super.updateViewConstraints()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
       self.doViewWillAppearWork = true
        //appD.generateLocalNotification()
        if let navigationController = self.navigationController, navigationController.isBeingPresented {
            // being presented
            print("Being presented")
        }else{
            // being pushed
              print("Being pushed")
        }
        print("Navigation stacks: ",self.navigationController!.viewControllers)
        for view in self.navigationController!.viewControllers {
            if view .isKind(of: VideoGameRework.self) || view .isKind(of: VideoGameResultController.self) {
                view.removeFromParent()
                view.removeFromParent()
            }
        }
    print("Navigation stacks: ",self.navigationController!.viewControllers)
        showVideoTutoView()
    }
    func showVideoTutoView(){
        if UserDefaults.standard.value(forKey: "WatchTutoZonzay") == nil {
            
      UserDefaults.standard.set("True", forKey: "WatchTutoZonzay")
            UserDefaults.standard.synchronize()
        self.visualEffectBlur.isHidden = false
        self.watchTutorialContainer.isHidden = false
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            self.visualEffectBlur.alpha = 1
            self.watchTutorialContainer.alpha = 1
        }, completion: {(verif) in
            
        })
        }
    }
    func sliderCameToEnd() {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.cancelChallenge(userId: a["_id"].stringValue)
        }catch{
            
        }
        showLoaderMatchMaking(hide: true, orientation: "right")
    }
    func showLoaderMatchMaking(hide: Bool,orientation:String){
        if hide{
            self.gifPlayer.stopAnimatingGif()
            UIView.animate(withDuration: 0.3, delay: 0.4, options: [.curveLinear], animations: {
                if orientation == "right" {
                self.containerMatchmaking.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                self.gifPlayer.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                self.SliderCancelMatchM.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
                }else{
                    self.containerMatchmaking.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
                    self.gifPlayer.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
                    self.SliderCancelMatchM.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
                }
            }) { (bool) in
                self.containerMatchmaking.isHidden = true
                self.gifPlayer.isHidden = true
                self.SliderCancelMatchM.isHidden = true
                self.containerMatchmaking.transform = CGAffineTransform.identity
                self.gifPlayer.transform = CGAffineTransform.identity
                self.SliderCancelMatchM.transform = CGAffineTransform.identity
                self.SliderCancelMatchM.resetSlider()
                 self.OptionsButton.isHidden = false
                  //self.soundBTN.isHidden = false
                self.TutoButton.isHidden = false
                self.giftButton.isHidden = false
                
            }
        }else{
            self.containerMatchmaking.alpha = 0
            self.gifPlayer.alpha = 0
            self.SliderCancelMatchM.alpha = 0
            self.containerMatchmaking.isHidden = false
            self.gifPlayer.isHidden = false
            self.SliderCancelMatchM.isHidden = false
            self.OptionsButton.isHidden = true
             // self.soundBTN.isHidden = true
            self.TutoButton.isHidden = true
            self.giftButton.isHidden = true
            UIView.animate(withDuration: 0.6, delay: 0, options: [.curveEaseInOut], animations: {
                self.containerMatchmaking.alpha = 1
                self.gifPlayer.alpha = 1
                self.SliderCancelMatchM.alpha = 1
            }) { (boo) in
                let gif = UIImage(gifName: "MatchMaking1.gif")
                self.gifPlayer.setGifImage(gif)
                self.gifPlayer.startAnimatingGif()
            }
        }
    }
    func configureFilterGesture(){
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.hideFilter(_:)))
         tapgestureShow = UITapGestureRecognizer(target: self, action: #selector(self.showFilter(_:)))
        self.viewMinusFilter.isUserInteractionEnabled = true
        self.viewUPFilter.isUserInteractionEnabled = true
        self.viewMinusFilter.addGestureRecognizer(tapgesture)
        self.viewUPFilter.addGestureRecognizer(tapgestureShow)
    }
    @objc func showFilter(_ sender: UITapGestureRecognizer) {
        let yOld = (self.filterCollection.origin.y + 25 ) - 100
        let ySnap = yOld - 55
        DispatchQueue.main.async {
            self.filterIndicator.animationDuration = 2
            self.filterIndicator.rotationDuration = 2
            self.filterIndicator.lineWidth = 3
            self.filterIndicator.hidesWhenStopped = true
            self.filterIndicator.startAnimating()
            self.OptionsButton.isHidden = true
              //self.soundBTN.isHidden = true
            self.TutoButton.isHidden = true
            self.giftButton.isHidden = true
            self.cameraButton.isHidden = true
            self.gifButton.isHidden = true
            self.filterCollection.origin.y = self.view.height
            self.filterCollection.layoutIfNeeded()
            self.filterCollection.isHidden = false
            self.filterIndicator.isHidden = false
            self.navBarTop.isHidden = true
            self.viewUPFilter.isHidden = true
            
            //self.collectionFilter.alpha = 0
            //self.collectionFilter.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                self.filterCollection.origin.y = yOld
                self.filterCollection.collectionView.alpha = 1
                 self.takeSnapshotView.origin.y = ySnap
                self.filterCollection.layoutIfNeeded()
                self.takeSnapshotView.layoutIfNeeded()
            }) { (verif) in
                self.filterPickerDefault.isHidden = false
                self.viewMinusFilter.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                    self.filterIndicator.stopAnimating()
                })
            }
        }
    }
    @objc func hideFilter(_ sender: UITapGestureRecognizer) {
         let yOld = self.filterCollection.origin.y
       let ySnap = self.takeSnapshotView.origin.y
       
            self.viewMinusFilter.isHidden = true
            self.filterPickerDefault.isHidden = true
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                self.filterCollection.origin.y = (yOld + 100 ) - 25
                 self.takeSnapshotView.origin.y = (ySnap + 100 ) - 25
                self.filterCollection.layoutIfNeeded()
                 self.takeSnapshotView.layoutIfNeeded()
                self.filterCollection.collectionView.alpha = 0
            }) { (verif2) in
                self.viewUPFilter.isHidden = false
            }
        
    }
    
   
       
   
   @objc func changesOnFriends(_ notif:Notification) {
      
       print("Friends Changed")
       self.tableV.reloadSections([0], with: .fade)
   }
    @objc func handleReload(notification: NSNotification) {
           print("Reload Conversation")
          self.tableV.reloadSections([0], with: .fade)
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        initAdMob()
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changesOnFriends(_:)), name: NSNotification.Name.init("changesOnFriends"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
         self.OptionsButton.buttonImage = UIImage(gifName: "challenge-gif.gif")
        //SwiftSpinner.show("Loading...")
        let viewInit = UIView()
        viewInit.backgroundColor = .white
        viewInit.layer.cornerRadius = 9
        viewInit.layer.masksToBounds = true
        let label = UILabel()
        label.text = "Initialisation\nPlease Wait"
        label.font = UIFont(name: "Roboto-Regular", size: 15)
        label.numberOfLines = 2
        label.textColor = .gray
        label.textAlignment = .center
        viewInit.addSubview(label)
        self.view.addSubview(viewInit)
        getGifs()
        viewInit.snp.makeConstraints { (const) in
            const.center.equalToSuperview()
            const.width.equalTo(120)
            const.height.equalTo(60)
        }
        label.snp.makeConstraints { (const) in
            const.center.equalToSuperview()
            const.left.equalToSuperview().offset(4)
            const.right.equalToSuperview().offset(4)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                viewInit.alpha = 0
            }, completion: { (done) in
                viewInit.isHidden = true
                viewInit.removeFromSuperview()
            })
        }
        configureFilterGesture()
        configureSound()
        self.filterCollection.isHidden = true
        self.view.addSubview(filterCollection)
        self.view.bringSubviewToFront(filterPickerDefault)
        self.filterCollection.collectionView.contentInsetAdjustmentBehavior = .never
        if observerCount == 0 {
            NotificationCenter.default.addObserver(self, selector: #selector(self.revengeCallback(_:)), name: NSNotification.Name.init("Revenge"), object: nil)
        }
        tableV.delegate = self
        tableV.dataSource = self
        self.tableV.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1.0)
        profileImageView.image = UIImage(named: "profile_hamid")
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.borderColor = UIColor.white.cgColor
        configureOptionsButtons()
        
            self.configureFilter()
        
      
       
        self.SliderCancelMatchM.delegate = self
         self.tableV.isHidden = true
        self.backFriendBTN.isHidden = true
        self.containerAlphaColor.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadZonzons(_:)), name: NSNotification.Name.init("ReloadZonz"), object: nil)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
  
            let a =  JSON(data: dataFromString!)
       

    }
    
    @objc func reloadZonzons(_ notif:Notification){
        self.getZonz()
    }
    func configureSound(){
        
        if SoundBackGround.enabled {
        SoundBackGround.play(file: "background_music", fileExtension: "mp3", numberOfLoops: -1)

        }else{
        soundBTN.setImage(UIImage(named: "SoundOffIcon"), for: .normal)
        }
    }
    @IBOutlet weak var takeSnapshotView: UIView!
    
    @IBOutlet weak var closeSnapshotView: UIView!
    
    @IBAction func closeSnapshot(_ sender: UITapGestureRecognizer){
        print("CloseSnapshot")
        closeSnapshotView.isHidden = true
        if self.viewMinusFilter.isHidden == true {
            self.showFilter(self.tapgestureShow)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute:
            
         {
            
            self.filterCollection.isHidden = true
            self.filterCollection.delegate?.swipeView!(self.filterCollection, didFinishScrollAtIndexPath: IndexPath(row: 0, section: 0))
            self.filterCollection.collectionView.delegate?.collectionView!(self.filterCollection.collectionView, didSelectItemAt: IndexPath(row: 0, section: 0))
            //self.filterCollection.delegate?.swipeView!(self.filterCollection, didSelectItemAtPath: IndexPath(row: 0, section: 0))
            self.OptionsButton.isHidden = false
              //self.soundBTN.isHidden = false
            self.TutoButton.isHidden = false
            self.giftButton.isHidden = false
            self.gifButton.isHidden = false
            self.filterIndicator.isHidden = true
            self.navBarTop.isHidden = false
            self.filterPickerDefault.isHidden = true
            self.viewMinusFilter.isHidden = true
            self.takeSnapshotView.isHidden = true
            self.showVIPBTN.isHidden = false
            self.viewUPFilter.isHidden = true
            self.showVIPBTN.isHidden = false
            self.cameraFilterButton.isHidden = false
            
            
        })
    }
    func reInitFilter(){
        if self.filterCollection.isHidden == false {
            closeSnapshotView.isHidden = true
            if self.viewMinusFilter.isHidden == true {
                self.showFilter(self.tapgestureShow)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute:
                
                {
                    
                    self.filterCollection.isHidden = true
                    self.filterCollection.delegate?.swipeView!(self.filterCollection, didFinishScrollAtIndexPath: IndexPath(row: 0, section: 0))
                    self.filterCollection.collectionView.delegate?.collectionView!(self.filterCollection.collectionView, didSelectItemAt: IndexPath(row: 0, section: 0))
                    //self.filterCollection.delegate?.swipeView!(self.filterCollection, didSelectItemAtPath: IndexPath(row: 0, section: 0))
                    self.OptionsButton.isHidden = false
                   // self.soundBTN.isHidden = false
                    self.TutoButton.isHidden = false
                    self.giftButton.isHidden = false
                    self.gifButton.isHidden = false
                    self.filterIndicator.isHidden = true
                    self.navBarTop.isHidden = false
                    self.filterPickerDefault.isHidden = true
                    self.viewMinusFilter.isHidden = true
                    self.takeSnapshotView.isHidden = true
                    self.showVIPBTN.isHidden = false
                    self.viewMinusFilter.isHidden = true
                    self.viewUPFilter.isHidden = true
                    self.showVIPBTN.isHidden = false
                    self.cameraFilterButton.isHidden = false
                    
            })
        }
    }
    @IBAction func takeSnapshot(_ sender: UITapGestureRecognizer){
        
        //self.isFaceDetectionOff = true
        ///
        
        if self.currentFrame != nil {
            if self.viewMinusFilter.isHidden == true {
                self.showFilter(tapgestureShow)
            }
         self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
            print("CameraView : ",self.cameraview.subviews)
            

            let currentFilter = self.GLUIView.takeScreenshot()
        let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(self.currentFrame!)!
        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
          //ciimage =  ciimage.oriented(forExifOrientation: 6)
    let flippedImage = ciimage.transformed(by: CGAffineTransform(scaleX: -1, y: 1))
        let imageFace : UIImage = self.convert(cmage: flippedImage)
            
            self.imageViewSnap = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height))
            // print("Size : Filter : ",(self.glView.layer as! CAEAGLLayer).drawableProperties)
            //())
           // imageView.image = self.glView.renderToImage()
            //imageView.image = currentFilter
         
            
            imageViewSnap.image = self.mergeTwoImages(imageFace: imageFace, imageFilter: currentFilter)
            scrollview = UIScrollView.init(frame: self.view.bounds)
            scrollview.minimumZoomScale = 1
            scrollview.bouncesZoom = false
            scrollview.maximumZoomScale = 4.0
            scrollview.zoomScale = 1.0
            scrollview.delegate = self
            scrollview.bounces = false
            scrollview.addSubview(imageViewSnap)
            
            //self.view.addSubview(imageView)
            self.view.addSubview(scrollview)
            scrollview.snp.makeConstraints { (const) in
                const.top.equalTo(self.view)
                const.bottom.equalTo(self.view)
                const.left.equalTo(self.view)
                const.right.equalTo(self.view)
            }
            let deleteButton = UIButton.init(frame: CGRect.zero)
            deleteButton.size = CGSize(width: 50, height: 50)
            deleteButton.setTitle("", for: .normal)
            deleteButton.setImage(UIImage(named: "deleteSnap"), for: .normal)
            deleteButton.addTarget(self, action: #selector( self.deleteSnapAction(_:)), for: .touchUpInside)
            let saveButton = UIButton.init(frame: CGRect.zero)
            saveButton.size = CGSize(width: 50, height: 50)
            saveButton.setTitle("", for: .normal)
            saveButton.setImage(UIImage(named: "saveSnap"), for: .normal)
            saveButton.addTarget(self, action: #selector( self.saveSnapAction(_:)), for: .touchUpInside)
            let shareButton = UIButton.init(frame: CGRect.zero)
            shareButton.size = CGSize(width: 50, height: 50)
            shareButton.setTitle("", for: .normal)
            shareButton.setImage(UIImage(named: "shareSnap"), for: .normal)
             shareButton.addTarget(self, action: #selector( self.shareSnapAction(_:)), for: .touchUpInside)
            stackView = UIStackView(frame: CGRect.zero)
            stackView.size = CGSize(width: (self.view.width * 2) / 3, height: 50)
            stackView.addSubview(deleteButton)
            stackView.addSubview(saveButton)
            stackView.addSubview(shareButton)
            stackView.alignment = .fill
            stackView.distribution = .fill
            stackView.axis = .horizontal
            stackView.spacing = 25
            deleteButton.snp.makeConstraints { (const) in
                const.width.equalTo(50)
                const.height.equalTo(50)
                const.left.equalToSuperview().offset(0)
                 const.centerY.equalTo(stackView)
            }
            saveButton.snp.makeConstraints { (const) in
                const.width.equalTo(50)
                const.height.equalTo(50)
                const.centerX.equalTo(stackView)
                const.centerY.equalTo(stackView)
            }
            shareButton.snp.makeConstraints { (const) in
                const.width.equalTo(50)
                const.height.equalTo(50)
                const.centerY.equalTo(stackView)
                 const.right.equalToSuperview().offset(0)
            }
            self.view.addSubview(stackView)
            stackView.snp.makeConstraints { (const) in
                const.width.equalTo((self.view.width * 2) / 3)
                const.centerX.equalToSuperview()
                const.bottom.equalToSuperview().offset(-30)
                const.height.equalTo(50)
            }
            self.view.bringSubviewToFront(stackView)
            imageViewSnap.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
            
            self.isFaceDetectionOff = true
        }
}
    @objc func deleteSnapAction(_ sender: UIButton) {
           self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        let bottomLeft = CGPoint(x: (self.imageViewSnap.frame.width / 8) , y: self.imageViewSnap.frame.height)
        self.imageViewSnap.layer.anchorPoint = CGPoint(x: (self.imageViewSnap.frame.width / 8) / self.imageViewSnap.frame.width, y: 1)
        self.imageViewSnap.layer.position = bottomLeft
        print(bottomLeft)
        print(self.imageViewSnap.layer.anchorPoint)
        UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
            self.imageViewSnap.transform = CGAffineTransform(scaleX: 1/8, y: 1/8)
        }) { (finished) in
            UIView.performWithoutAnimation {
                self.imageViewSnap.removeFromSuperview()
                self.stackView.removeFromSuperview()
                self.scrollview.removeFromSuperview()
                if self.indexFilter != 0 {
                    self.isFaceDetectionOff = false
                }
            }
            
        }
        UIView.animate(withDuration: 0.1, delay: 0.2, options: [.curveEaseInOut], animations: {
            //self.imageViewSnap.alpha = 0
            //self.scrollview.alpha = 0
            //self.stackView.alpha = 0
        }){ (finished) in
          
        }
    }
    @objc func shareSnapAction(_ sender:UIButton) {
        let vc = UIActivityViewController(activityItems: [self.imageViewSnap.image!], applicationActivities: [])
        present(vc, animated: true, completion: nil)
    }
    @objc func saveSnapAction(_ sender : UIButton) {
        
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized :
                
                
                DispatchQueue.main.async(execute: {
                    self.viewClaqueSaved.isHidden = false
                    self.view.bringSubviewToFront(self.viewClaqueSaved)
                 UIImageWriteToSavedPhotosAlbum(self.imageViewSnap.image!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                })
            case .denied:
                let ac = UIAlertController(title: "Zonzay", message: "Save feature denied, please enable it in Settings -> Zonzay", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
            case .notDetermined:
                let ac = UIAlertController(title: "Zonzay", message: "Could not get authorization", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
            case .restricted:
                let ac = UIAlertController(title: "Zonzay", message: "Sauthorization restricted", preferredStyle: .alert)
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(ac, animated: true)
            }
        }
        
    }
    @objc func image(_ image:UIImage,didFinishSavingWithError error:Error?,contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            self.viewClaqueSaved.isHidden = true
        } else {
            self.view.bringSubviewToFront(self.viewContainerSaved)
            self.viewContainerSaved.alpha = 0
            self.viewContainerSaved.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                self.viewContainerSaved.alpha = 1
            }) { complete in
                UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveLinear], animations: {
                    self.SavedImageView.origin.y -= 37
                }, completion: { (completeTwo) in
                    UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveLinear], animations: {
                        self.SavedImageView.origin.y += 37
                    }, completion: { (completeThree) in
                        UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveLinear], animations: {
                            self.SavedImageView.origin.y -= 37
                        }, completion: { (completeTwoAnex) in
                            UIView.animate(withDuration: 0.2, delay: 0.1, options: [.curveLinear], animations: {
                                self.SavedImageView.origin.y += 37
                            }, completion: { (completeTwoAnex) in
                                self.SavedImageView.image = UIImage(named: "SavedDoneIcon")
                                UIView.animate(withDuration: 0.7, delay: 1.3, options: [.curveEaseInOut], animations: {
                                    self.viewContainerSaved.alpha = 0
                                }, completion: ({ (completeFour) in
                                    self.SavedImageView.image = UIImage(named: "SavedIcon")
                                    self.viewContainerSaved.isHidden = true
                                    self.viewClaqueSaved.isHidden = true
                                    self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
                                    let bottomLeft = CGPoint(x: (self.imageViewSnap.frame.width / 2) , y: self.imageViewSnap.frame.height)
                                    self.imageViewSnap.layer.anchorPoint = CGPoint(x: (self.imageViewSnap.frame.width / 2) / self.imageViewSnap.frame.width, y: 1)
                                    self.imageViewSnap.layer.position = bottomLeft
                                    print(bottomLeft)
                                    print(self.imageViewSnap.layer.anchorPoint)
                                    UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                                        self.imageViewSnap.transform = CGAffineTransform(scaleX: 1/8, y: 1/8)
                                        
                                    }) { (finished) in
                                        self.imageViewSnap.removeFromSuperview()
                                        self.stackView.removeFromSuperview()
                                        self.scrollview.removeFromSuperview()
                                        
                                        if self.indexFilter != 0 {
                                            self.isFaceDetectionOff = false
                                        }
                                    }
                                    UIView.animate(withDuration: 0.1, delay: 0.2, options: [.curveEaseInOut], animations: {
                                        //self.imageViewSnap.alpha = 0
                                        //self.scrollview.alpha = 0
                                        //self.stackView.alpha = 0
                                    }){ (finished) in
                                        
                                    }
                                }))
                            })
                        })
                      
                    })
                })
            }
        }
    }
    func mergeTwoImages(imageFace:UIImage,imageFilter:UIImage) -> UIImage{
        if self.isGLViewAdded {
           
        return imageFace.overlayWith(image: imageFilter, posX: 0, posY: 0)
            
        }
        return imageFace
    }
    // Convert CIImage to CGImage
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    func configureFilter(){
       
       
        self.currentMetadata = []
        self.lastFaceDetectedTime = 0
        let xmlparser = XmlParser()
        xmlparser.loadRssFeed("FaceMask")
        self.faceMaskArray = (xmlparser.faceMaskArray.firstObject as! FaceMaskCategories).faceMaskItemArray
        print("ConfigureFilters: ",self.faceMaskArray)
            self.collectionArray = self.faceMaskArray
       // let item = collectionArray?.firstObject as? FaceMaskItem
      //  DispatchQueue.main.async {
            
        self.landmarkParser.parseDelegate = self
       
           //  }
        //landmarkParser.loadRssFeed(item?.landmakrFile)
    }
  var gifItems : JSON = []
    var allGifs : [GifsBuy] = []
   
    func getGifs(){
        self.gifItems = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
                
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getAllGifs, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getGifs For Download : ",data)
                if data != JSON.null {
                    if !data["message"].exists() {
                        self.gifItems = data
                    }
                    self.allGifs = []
                    if self.gifItems.arrayObject?.count != 0 {
                        for i in 0...((self.gifItems.arrayObject?.count)! - 1) {
                            self.allGifs.append(GifsBuy(priceGif: self.gifItems[i]["priceGif"].stringValue, nomGif: self.gifItems[i]["nomGif"].stringValue, id: self.gifItems[i]["_id"].stringValue, pathGif: self.gifItems[i]["pathGif"].stringValue, thumbGif: self.gifItems[i]["thumbGif"].stringValue, category: self.gifItems[i]["category"].stringValue, externalP: self.gifItems[i]["pathGif"].stringValue))
                          
                        }
                    }
                    for a in self.allGifs {
                        
                        var names =  a.getPathGifOriginal().components(separatedBy: "/")
                       
                        var namesTwo =  a.getThumbGifOriginal().components(separatedBy: "/")
                        self.detectPathExsits(name: names[names.count - 1], completionHandler: { (verif) in
                            if verif == false {
                            let key = SDDownloadManager.shared.downloadFile(withRequest: URLRequest(url: URL(string: a.getPathGifOriginal())!), inDirectory: "Gifs", withName: nil, shouldDownloadInBackground: true, onProgress: { (float) in
                                
                            }, onCompletion: { (error, url) in
                                print("fileURL: ",url)
                                print("error : ",error)
                            })
                                    print("downloadKey: ",key)
                            }
                        })
                        self.detectPathExsits(name: namesTwo[namesTwo.count - 1], completionHandler: { (verif) in
                            if verif == false {
                                let keyTwo = SDDownloadManager.shared.downloadFile(withRequest: URLRequest(url: URL(string: a.getThumbGifOriginal())!), inDirectory: "Gifs", withName: nil, shouldDownloadInBackground: true, onProgress: { (float) in
                                    
                                }, onCompletion: { (error, url) in
                                    print("fileURL: ",url)
                                    print("error : ",error)
                                })
                                 print("downloadkeyTwo : ",keyTwo)
                            }
                        })
                        
                    
                       
                    }
                    
                    print(self.gifItems.arrayObject?.count)
                    
                }else{
                    
                    
                }
            }
            
        }catch {
            
        }
    }
    func detectPathExsits(name:String,completionHandler : @escaping ((Bool) -> Void)) {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent("Gifs")?.appendingPathComponent(name) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            print("filePathToTest : ",filePath)
            if fileManager.fileExists(atPath: filePath) {
            completionHandler(true)
            } else {
                completionHandler(false)
            }
        } else {
            completionHandler(false)
        }
    }
    func detectDownloadFinished(completionHandler : @escaping ((Bool) -> Void)){
       
        if allGifs.count != 0 {
        if SDDownloadManager.shared.currentDownloads().count != 0 {
        completionHandler(false)
        }else{
            completionHandler(true)
        }
        }else{
            getGifs()
            completionHandler(false)
        }
    }
    var UIimage = UIImageView()
    var doViewWillAppearWork = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if !self.UIimage.isDescendant(of: self.OptionsButton) {
        self.OptionsButton.addSubview(UIimage)
        UIimage.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        UIimage.setGifImage(UIImage(gifName: "challenge-gif.gif"))
        UIimage.layer.cornerRadius =  UIimage.frame.width / 2
        UIimage.layer.masksToBounds = true
        UIimage.startAnimatingGif()
         self.OptionsButton.bringSubviewToFront(UIimage)
        }
       // self.OptionsButton.buttonImageView.setGifImage(UIImage(gifName: "startPlay.gif"))
       // self.OptionsButton.buttonImageView.startAnimatingGif()
        if self.selectedJokes.count == 0 {
            self.OptionsButton.isHidden = false
            self.TutoButton.isHidden = false
        }
        if doViewWillAppearWork {
            getZonz()
        if SoundBackGround.enabled {
            soundBTN.setImage(UIImage(named: "SoundOnIcon"), for: .normal)
        }else{
            soundBTN.setImage(UIImage(named: "SoundOffIcon"), for: .normal)
        }
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            self.profileImageView.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "profile_hamid"), transformer: nil, progress: nil, completion: nil)
            self.profileNameLabel.text = a["userName"].stringValue
            
            
        }catch{
            
        }
        reInitFilter()
       // self.isGLViewAdded = false
        //VIP ACCESS
        
        self.cameraview.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        if let layers = self.cameraview.layer.sublayers {
            layers.forEach { (layer) in
                layer.removeFromSuperlayer()
            }
        }
        ////////
        SocketIOManager.sharedInstance.currentPresentedViewController = self
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        self.SocketMatchMaking = 0
        getFriends()
        #if !targetEnvironment(simulator)
        //this is Real device running
        self.startRunningVideoSession()
        print("SubViews : ",self.cameraview.subviews)
        print("SubLayers : ",self.cameraview.layer.sublayers)
        if observerCount == 0 {
            doItOneTime()
            observerCount += 1
        }
        #endif
        }
    }

    func doItOneTime(){
        DispatchQueue.global(qos: .default).async {
            
        }
        DispatchQueue.global(qos: .default).async {
             self.wrapper?.prepare()
            
        }
      /*  DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
            
        } */
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        #if !targetEnvironment(simulator)
        //this is Real device running
       
        #endif
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(false)
        // self.stopRunningVideo()
    }
    func configureOptionsButtons(){
        OptionsButton.addItem(icon: #imageLiteral(resourceName: "playfri_saif.png")) { (item) in
            self.isFriendsOrMatchMaking = true
            self.selectedJokes = []
            if item.iconTintColor == UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1) {
            let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
            vc.delegate = self
                vc.zonzMoney = self.zonzayMoneyLabel.text ?? "0"
            vc.isPlayingOrProfile = true
            //self.containerAlphaColor.isHidden = false
            self.OptionsButton.isHidden = true
            //self.soundBTN.isHidden = true
                self.TutoButton.isHidden = true
                self.giftButton.isHidden = true
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                self.doViewWillAppearWork = false
            self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let alertR = UIAlertController(title: Localization("Zonzay Challenge"), message: Localization("NoFriendPlaying"), preferredStyle: .alert)
                let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                   
                })
                alertR.addAction(ok)
                self.present(alertR, animated: true, completion: nil)
            }
        }
        OptionsButton.addItem(icon: #imageLiteral(resourceName: "random_saif.png")) { (item) in
            self.isFriendsOrMatchMaking = false
            self.selectedJokes = []
            let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
            vc.delegate = self
            vc.isPlayingOrProfile = true
            //self.containerAlphaColor.isHidden = false
            self.OptionsButton.isHidden = true
            //self.soundBTN.isHidden = true
            self.TutoButton.isHidden = true
            self.giftButton.isHidden = true
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            self.doViewWillAppearWork = false
            self.navigationController?.pushViewController(vc, animated: true)
          
        }
        OptionsButton.buttonColor = UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1)
        OptionsButton.openAnimationType = .pop
        OptionsButton.plusColor = .white
        OptionsButton.autoCloseOnTap = true
        OptionsButton.fabDelegate = self
    }
    /*
     *Start the camera Process
     */
    func startRunningVideoSession(){
        captureSession.sessionPreset = AVCaptureSession.Preset.hd1280x720
        
        let devices  = cameraWithPosition(position: AVCaptureDevice.Position.front )
        
        
        
        captureDevice = devices
        
        if captureDevice != nil
        {
            print("Capture device found")
            beginSession()
        }else{
            print("No Capture device found")
        }
    }
    /*
     *Stop running the camera if needed
     */
    func stopRunningVideo(){
        print("StopRunningVideo")
        if captureSession.isRunning {
        captureSession.stopRunning()
        
        do {
          
            //captureSession.removeOutput(stillImageOutput)
            output.setSampleBufferDelegate(nil, queue: sampleQueue)
            metaOutput.setMetadataObjectsDelegate(nil, queue: faceQueue)
            captureSession.removeOutput(output)
            captureSession.removeOutput(metaOutput)
            try captureSession.removeInput(AVCaptureDeviceInput(device: captureDevice!))
            self.cameraview.layer.sublayers?.forEach({ (a) in
                a.removeFromSuperlayer()
            })
          // self.glView?.removeFromSuperview()
          //  self.isGLViewAdded = false
        }catch{
            print("errorRemovingCamera: \(error.localizedDescription)")
        }
           
    }
    }
    /*
     *Detect all the camera devices
     */
    func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                           mediaType: AVMediaType.video,
                                                                           position: position)
        
        for device in deviceDescoverySession.devices {
            if device.position == position {
                return device
            }
            
        }
        
        return nil
    }
    /*
     *Begin the camera session
     */
    func beginSession()
    {
        
        do
        {
            if captureSession.canAddInput(try! AVCaptureDeviceInput(device: captureDevice!)) {
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
                
              /*  if captureSession.canAddOutput(stillImageOutput)
                {
                    captureSession.addOutput(stillImageOutput)
                } */
            }
        }
        catch
        {
            print("error: \(error.localizedDescription)")
        }
       
        output.setSampleBufferDelegate(self, queue: sampleQueue)
        
     
        metaOutput.setMetadataObjectsDelegate(self, queue: faceQueue)
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
            let settings: [AnyHashable: Any] = [kCVPixelBufferPixelFormatTypeKey as AnyHashable: Int(kCVPixelFormatType_32BGRA)]
            output.videoSettings = settings as? [String : Any]
           
            let conn:AVCaptureConnection = output.connection(with: AVMediaType.video)!
            conn.videoOrientation = .portrait
            
        }
        if captureSession.canAddOutput(metaOutput) {
            captureSession.addOutput(metaOutput)
        }
        
       /* previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill */
        metaOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.face]
        
        //print("cameraView:",self.cameraview.bounds)
       // print("cameraViewLayer:",self.cameraview.layer.bounds)
       // self.cameraview.layer.addSublayer(previewLayer!)
       // previewLayer?.frame = self.cameraview.layer.bounds
        captureSession.startRunning()
        let width = UIScreen.main.bounds.width;
        let height = UIScreen.main.bounds.height
        
        layer.frame = CGRect(x: 0, y: 0, width: width, height: height)
        let scale = CGAffineTransform(scaleX: -1, y: 1)
        layer.name = "VisualLayer"
        layer.setAffineTransform( scale)
        
        self.cameraview.layer.addSublayer(layer)
      
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("TOUCH BEGIN")
        let point = touches.first?.location(in: self.cameraview)
        if self.collectionFilter.hitTest(point!, with: event) != nil || self.tableV.hitTest(point!, with: event) != nil {
            print("THIS COLLECTION")
            if  self.tableV.hitTest(point!, with: event) != nil {
                guard let index = self.tableV.indexPathForRow(at: point!) else{
                    return
                }
                self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
            }
        }else{
          print("THIS CAMERA VIEW")
            if self.collectionFilter.isHidden == false {
                self.isFaceDetectionOff = true
                DispatchQueue.main.async {
                    
                    //sender.isHidden = true
                   
                  
                   
                  
                    UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: {
                        self.collectionFilter.alpha = 0
                    }) { (verif) in
                        self.OptionsButton.isHidden = false
                         // self.soundBTN.isHidden = false
                        self.TutoButton.isHidden = false
                        self.giftButton.isHidden = false
                         self.cameraButton.isHidden = false
                          self.gifButton.isHidden = false
                        self.cameraFilterButton.isHidden = false
                          self.collectionFilter.isHidden = true
                        self.selectedCell = nil
                        if let indexes = self.collectionFilter.indexPathsForSelectedItems {
                            for index in indexes {
                                self.collectionFilter.deselectItem(at: index, animated: false)
                                
                            }
                        }
                    }
                }
            }
        }
        
      super.touchesBegan(touches, with: event)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("NumberFirst:",number)
    
       
        //previewLayer?.frame = self.cameraview.layer.bounds
      /*  number = (self.friends.arrayObject?.count)!
        if number % 3 != 0 {
            print("number : ",(Double(number / 3)))
            self.PopBarHeight.constant = self.PopBarHeight.constant + CGFloat((81 * (Double(number / 3))))
            
        } */
        
        
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.friends.arrayObject?.count)!
    }
    var number = 3
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableV.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let imageUser = cell.viewWithTag(1) as! UIImageView
        let userName = cell.viewWithTag(2) as! UILabel
        let interestes = cell.viewWithTag(3) as! UILabel
        let connectedFriend = cell.viewWithTag(8) as! UIImageView
        //userName.text = self.friends[indexPath.row]["userFirstName"].stringValue + " " +  self.friends[indexPath.row]["userLastName"].stringValue
        userName.text = self.friends[indexPath.row]["userName"].stringValue
        if self.friends[indexPath.row]["userImageURL"].exists() {
        imageUser.setImage(with: URL(string: self.friends[indexPath.row]["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
        }else{
          imageUser.image  = UIImage(named: "artist")
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let friendConnected = appDelegate.ConnectedUser
         if friendConnected.contains(self.friends[indexPath.row]["_id"].stringValue) {
        if self.friends[indexPath.row]["listInterests"].stringValue != "" {
            var resString = ""
            let resIntersts = (self.friends[indexPath.row]["listInterests"].stringValue).components(separatedBy: ";")
            for res in resIntersts {
                resString  = resString + "#" + res
            }
    
            interestes.text = resString
        }else{
            interestes.isHidden = true
        }
        }else{
           interestes.text = Localization("offline")
        
        }
        if friendConnected.contains(self.friends[indexPath.row]["_id"].stringValue) {
          connectedFriend.image = UIImage(named: "ConnectedIcon")
          
            cell.enableGame(on: true)
        }else{
         connectedFriend.image = UIImage(named: "Disconnectedicon")
            cell.isUserInteractionEnabled = false
            cell.enableGame(on: false)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected ROW")
        SocketIOManager.sharedInstance.deactivateDuringGame()
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "firstUserId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            var FirstAttempt = 0
            Alamofire.request(ScriptBase.sharedInstance.createRoom, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let janus = VideoRoomController()
                    
                    let data = JSON(response.data ?? Data())
                    janus.attachRoomToJanus(roomId: data["roomNumber"].numberValue, completionHandler: { json in
                        
                        let room = Room(id: data["roomId"].stringValue, roomNumber: data["roomNumber"].stringValue, firstUserId: data["roomFirstUserId"].stringValue, secondUserId: "")
                        SocketIOManager.sharedInstance.videoRoomRequestAccepted(completionHandler: { json in
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                             self.tableV.isHidden = true
                            self.backFriendBTN.isHidden = true
                            self.containerAlphaColor.isHidden = true
                            self.OptionsButton.isHidden = false
                              //self.soundBTN.isHidden = false
                            self.TutoButton.isHidden = false
                            self.giftButton.isHidden = false
                            print("Game accepted")
                            print(json)
                            if FirstAttempt == 0 {
                                FirstAttempt = 1
                                self.alert.dismiss(animated: true, completion: nil)
                                var i = 0
                                for view in (self.navigationController?.viewControllers)! {
                                    if view .isKind(of: VideoGameRework.self) || view .isKind(of: VideoGameResultController.self) {
                                        self.navigationController?.viewControllers.remove(at: i)
                                    }
                                    i = i + 1
                                }
                                //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                                //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                                let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                                vc.willAppear = true
                                vc.Owner = "master"
                                vc.FriendOrMatch = false
                                vc.selectedJokes = self.selectedJokes
                                vc.roomId = NSNumber(value: Int(json[0])!)
                                vc.SecondUserId = self.friends[indexPath.row]["_id"].stringValue
                                vc.navigation = self.navigationController
                                //self.navigationController?.present(vc, animated: true, completion: nil)
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                
                                //self.navigationController?.dismiss(animated: true, completion: nil)
                            }
                        })
                        SocketIOManager.sharedInstance.videoRoomRequestRefused(completionHandler: { json in
                            print("Game Refused")
                            SocketIOManager.sharedInstance.reactivateOnFinishGame()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                             self.tableV.isHidden = true
                            self.containerAlphaColor.isHidden = true
                            self.OptionsButton.isHidden = false
                              //self.soundBTN.isHidden = false
                            self.TutoButton.isHidden = false
                            self.giftButton.isHidden = false
                            self.backFriendBTN.isHidden = true
                            let alertR = UIAlertController(title: Localization("Zonzay Challenge"), message: Localization("ZonzayFriendCancel"), preferredStyle: .alert)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                            let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                                 self.tableV.isHidden = true
                                self.backFriendBTN.isHidden = true
                                self.containerAlphaColor.isHidden = true
                            })
                            alertR.addAction(ok)
                            self.present(alertR, animated: true, completion: nil)
                            
                        })
                        SocketIOManager.sharedInstance.joinVideoRoomRequest(userId: a["_id"].stringValue, userName: a["userName"].stringValue, SecondUserId: self.friends[indexPath.row]["_id"].stringValue, VideoRoom: room.getRoomNumber(), gameType: GameType.Friend)
                        self.alert = UIAlertController(title: nil, message: Localization("PleaseWait"), preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: Localization("Cancel"), style: .cancel , handler: { (alert) in
                            SocketIOManager.sharedInstance.reactivateOnFinishGame()
                             self.tableV.isHidden = true
                            self.backFriendBTN.isHidden = true
                            self.containerAlphaColor.isHidden = true
                            self.OptionsButton.isHidden = false
                              //self.soundBTN.isHidden = false
                            self.TutoButton.isHidden = false
                            self.giftButton.isHidden = false
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            //self.navigationController?.dismiss(animated: true, completion: nil)
                        })
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        self.alert.addAction(cancelAction)
                        self.alert.view.addSubview(loadingIndicator)
                        self.navigationController!.present( self.alert, animated: true, completion: nil)
                        print("REQU1 : ",data)
                        //self.jokes = data
                    })
                    
                    
                }else{
                    //TODO: print error
                }
                
            }
            
        }catch {
            
        }
    }
    
    @IBAction func shareApp(_ sender: UITapGestureRecognizer) {
        let shareLink = ["http://zonzay.com/download"]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    func getFriends(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
       
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.friendList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    self.friends = []
                    let data = JSON(response.data ?? Data())
                    if data["users"].arrayObject != nil {
                        self.friends  = data["users"]
                        self.tableV.reloadData()
                    }
                    print("Friends : ",self.friends)
                    //self.jokes = data
                    
                }else{
                    self.tableV.reloadData()
                    self.tableV.isHidden = true
                }
                if self.friends.arrayObject != nil {
                    if self.friends.arrayObject?.count == 0 {
                        print("ArrAyObject == 0")
                        let optionOne = self.OptionsButton.items[0]
                        optionOne.iconTintColor = UIColor.lightGray
                        self.OptionsButton.setNeedsDisplay()
                    }else{
                        print("You Have Friends")
                        let optionOne = self.OptionsButton.items[0]
                        optionOne.iconTintColor = UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1)
                        let optionTwo = self.OptionsButton.items[1]
                        optionTwo.iconTintColor = UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1)
                        self.OptionsButton.setNeedsDisplay()
                    }
                }else{
                    print("ArrAyObject == nil")
                    let optionOne = self.OptionsButton.items[0]
                    optionOne.iconTintColor = UIColor.lightGray
                    self.OptionsButton.setNeedsDisplay()
                }
                
            }
            
        }catch {
            
        }
    }
    
   
    @IBOutlet weak var containerAlphaColor: UIVisualEffectView!
    @IBAction func CancelGameAction(_ sender: UITapGestureRecognizer) {
        
        self.containerAlphaColor.isHidden = true
        self.OptionsButton.isHidden = false
          //self.soundBTN.isHidden = false
        self.TutoButton.isHidden = false
        self.giftButton.isHidden = false
    }
    

    @IBAction func MoreFilterAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.filterIndicator.animationDuration = 2
            self.filterIndicator.rotationDuration = 2
            self.filterIndicator.lineWidth = 3
            self.filterIndicator.hidesWhenStopped = true
            self.filterIndicator.startAnimating()
            self.OptionsButton.isHidden = true
           // self.soundBTN.isHidden = true
            self.TutoButton.isHidden = true
            self.giftButton.isHidden = true
            sender.isHidden = true
           self.showVIPBTN.isHidden = true
            self.cameraButton.isHidden = true
            self.gifButton.isHidden = true
            let yOld = self.filterCollection.origin.y
            self.filterCollection.origin.y = self.view.height
            self.filterCollection.layoutIfNeeded()
            self.filterCollection.isHidden = false
            self.filterIndicator.isHidden = false
            self.navBarTop.isHidden = true
            //self.collectionFilter.alpha = 0
            //self.collectionFilter.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveLinear], animations: {
                self.filterCollection.origin.y = yOld
                self.filterCollection.layoutIfNeeded()
            }) { (verif) in
                 self.filterPickerDefault.isHidden = false
                self.viewMinusFilter.isHidden = false
                 self.takeSnapshotView.isHidden = false
                self.closeSnapshotView.isHidden = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                       self.filterIndicator.stopAnimating()
                })
        }
        }
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
             SocketIOManager.sharedInstance.registerSocket(Name: a["_id"].stringValue)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                
                self.zonzayMoneyLabel.text = zonz
                self.getSubscriptionActivation(videoLimit: data["videoLimit"].stringValue)
            }
            
        }catch {
            
        }
    }
    func floatyClosed(_ floaty: Floaty) {
        self.UIimage.startAnimatingGif()
    }
    func floatyOpened(_ floaty: Floaty, subscription: Bool) {
        self.UIimage.stopAnimatingGif()
        if subscription == false {
            
            let alertR = UIAlertController(title: Localization("Zonzay Challenge"), message: Localization("ZonzayVideoMax"), preferredStyle: .alert)
            self.navigationController?.dismiss(animated: true, completion: nil)
            let ok = UIAlertAction(title: Localization("Subscribe"), style: .default, handler: { alert in
         
                let vc = UIStoryboard(name: "VIPAccess", bundle: nil).instantiateViewController(withIdentifier: "VIPAccessController") as! VIPAccessController
                vc.hero.modalAnimationType = .zoom
                vc.modalPresentationStyle = .fullScreen
                self.present(vc, animated: true, completion: nil)
            })
            let cancel = UIAlertAction(title: Localization("Cancel"), style: .cancel, handler: nil)
            alertR.addAction(ok)
            alertR.addAction(cancel)
            self.present(alertR, animated: true, completion: nil)
        }
    }
    func getSubscriptionActivation(videoLimit:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getUserSubscription, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                print("Subscription :",data)
                if data != JSON.null  {
                    
                    if data["message"].stringValue == "true" {
                        let dateFormatterNow = DateFormatter()
                        let dateNow = dateFormatterNow.date(fromSwapiString: data["zonzExpirationDate"].stringValue)
                        let dateFormatterFinal = DateFormatter()
                        dateFormatterFinal.dateFormat = "dd-MM-yyy"
                        let date = dateFormatterFinal.string(from: dateNow!)
                        UserDefaults.standard.setValue(date, forKey: "DateSub")
                        UserDefaults.standard.setValue("true", forKey: "Subscription")
                        UserDefaults.standard.synchronize()
                        self.OptionsButton.buttonColor = UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1)
                        self.OptionsButton.isSubscribed = true
                         self.OptionsButton.setNeedsDisplay()
                    }else if data["message"].stringValue == "false"{
                        UserDefaults.standard.setValue("false", forKey: "Subscription")
                        UserDefaults.standard.synchronize()
                        if videoLimit != "" {
                        if Int(videoLimit)! >= 5 {
                         self.OptionsButton.isSubscribed = false
                        self.OptionsButton.buttonColor = UIColor.lightGray
                            self.OptionsButton.setNeedsDisplay()
                        }else{
                            self.OptionsButton.isSubscribed = true
                            self.OptionsButton.buttonColor = UIColor(red: 31/255.0, green: 165/255.0, blue: 194/255.0, alpha: 1)
                             self.OptionsButton.setNeedsDisplay()
                        }
                    }
                        
                    }
                }
                
            }
            
        }catch {
            
        }
    }
    // VIP ACCOUNT
    @IBOutlet weak var visualBlurThing: UIVisualEffectView!
    
}
extension CMSampleBuffer {
    func image(orientation: UIImage.Orientation = .up,
               scale: CGFloat = 1.0) -> UIImage? {
        if let buffer = CMSampleBufferGetImageBuffer(self) {
            let ciImage = CIImage(cvPixelBuffer: buffer)
            
            return UIImage(ciImage: ciImage,
                           scale: scale,
                           orientation: orientation)
            
        }
        return nil
    }
}
public extension UIImage {

/// Extension to fix orientation of an UIImage without EXIF
func fixOrientation() -> UIImage {

    guard let cgImage = cgImage else { return self }

    if imageOrientation == .up { return self }

    var transform = CGAffineTransform.identity

    switch imageOrientation {

    case .down, .downMirrored:
        transform = transform.translatedBy(x: size.width, y: size.height)
        transform = transform.rotated(by: CGFloat(M_PI))

    case .left, .leftMirrored:
        transform = transform.translatedBy(x: size.width, y: 0)
        transform = transform.rotated(by: CGFloat(M_PI_2))

    case .right, .rightMirrored:
        transform = transform.translatedBy(x: 0, y: size.height)
        transform = transform.rotated(by: CGFloat(-M_PI_2))

    case .up, .upMirrored:
        break
    }

    switch imageOrientation {

    case .upMirrored, .downMirrored:
        transform.translatedBy(x: size.width, y: 0)
        transform.scaledBy(x: -1, y: 1)

    case .leftMirrored, .rightMirrored:
        transform.translatedBy(x: size.height, y: 0)
        transform.scaledBy(x: -1, y: 1)

    case .up, .down, .left, .right:
        break
    }

    if let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: cgImage.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) {

        ctx.concatenate(transform)

        switch imageOrientation {

        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))

        default:
            ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }

        if let finalImage = ctx.makeImage() {
            return (UIImage(cgImage: finalImage))
        }
    }

    // something failed -- return original
    return self
}
}
extension StartViewController {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
       
       
        
       
        if !currentMetadata.isEmpty {
            if self.glView != nil {
                DispatchQueue.main.async(execute: {
            if self.glView.isHidden {
                
                    self.glView.isHidden = false
               
                
            }
                     })
            }
            let currentFaceDetectedTime = Date().timeIntervalSince1970 * 1000
            let diff = abs(Double(currentFaceDetectedTime - lastFaceDetectedTime))
            if diff > FDTimeThreshold {
                if isGLViewAdded {
                    
                wrapper?.detectFace(from: sampleBuffer, in: currentMetadata.compactMap { $0 as? AVMetadataFaceObject }.map{ $0.bounds }.first!,  in:glView)
                    
                lastFaceDetectedTime = currentFaceDetectedTime
                }
            }
            
            
        }else{
            if glView != nil {
                
                //                print(glView)
                  DispatchQueue.main.async(execute: {
                if !self.glView.isHidden {
                  
                        self.glView.isHidden = true
                   
                }
                 })
            }
        }
        self.currentFrame = sampleBuffer
        if layer.status == .failed {
            layer.flush()
        }
        layer.enqueue(sampleBuffer)
    }
    func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
         let emptyArray:[Any]! = []
        currentMetadata = isFaceDetectionOff ? emptyArray : metadataObjects
    }
}
extension StartViewController : XMLParserDelegate, ParsingDidEndDelegate {
    func didEndLandmarksParsing(_ faceLandmarks: FaceLandmarks!) {
        DispatchQueue.main.async {
            let screenBounds = UIScreen.main.bounds
            //            print(faceLandmarks.maskImageName)
            if self.isGLViewAdded
            {
                self.glView?.setupVBOs(faceLandmarks.maskImageName, withLandmaskArray: faceLandmarks.landmarksArray)
               // self.cameraview.bringSubviewToFront(self.glView!)
            }
            else
            {
                print("RedrawGLView")
                self.isGLViewAdded = true
              
                if self.glView == nil {
                self.glView = OGLView(frame:screenBounds, imageName:
                    faceLandmarks.maskImageName, landmarkArray:  faceLandmarks.landmarksArray)
                let scale = CGAffineTransform(scaleX: -1, y: 1)
                self.glView?.layer.setAffineTransform(scale)
                self.GLUIView.addSubview(self.glView!)
                self.glView.enableSetNeedsDisplay = true
                self.glView.setNeedsDisplay()
                }
                //self.cameraview.addSubview(self.glView!)
                //self.cameraview.bringSubviewToFront(self.glView!)
                //return image
            }
            
            self.isFaceDetectionOff = false
           // self.view.bringSubviewToFront(self.collectionFilter)
        }
    }
}
extension StartViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (collectionArray?.count)!
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let item:FaceMaskItem = (collectionArray?.object(at: indexPath.row))! as! FaceMaskItem
        let imageView = cell.viewWithTag(1) as! UIImageView
         imageView.image = UIImage(named: item.imageName)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.isFaceDetectionOff = true
        
        if selectedCell != nil {
            selectedCell?.layer.borderColor = UIColor.clear.cgColor
            selectedCell?.layer.borderWidth = 0
        }
        selectedCell = collectionView .cellForItem(at: indexPath)
        selectedCell?.layer.borderColor = UIColor.black.cgColor
        selectedCell?.layer.borderWidth = 3
        let item:FaceMaskItem = (collectionArray?.object(at: indexPath.row))! as! FaceMaskItem
        //        print(item.landmakrFile)
        landmarkParser.loadRssFeed(item.landmakrFile)
    }
}
extension StartViewController {
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
}
// MARK: - HFSwipeViewDelegate
extension StartViewController: HFSwipeViewDelegate {
    func swipeView(_ swipeView: HFSwipeView, didFinishScrollAtIndexPath indexPath: IndexPath) {
       // log("HFSwipeView(\(swipeView.tag)) -> \(indexPath.row)")
        indexFilter = indexPath.row
        print("didFinishScrollAtIndexPath : ",indexPath)
        if indexPath.row != 0 {
            self.isFaceDetectionOff = true
            let item:FaceMaskItem = (collectionArray?.object(at: indexPath.row - 1))! as! FaceMaskItem
            //        print(item.landmakrFile)
            landmarkParser.loadRssFeed(item.landmakrFile)
        }else{
            self.isFaceDetectionOff = true
        }
    }
    
    func swipeView(_ swipeView: HFSwipeView, didSelectItemAtPath indexPath: IndexPath) {
       // log("HFSwipeView(\(swipeView.tag)) -> \(indexPath.row)")
         print("didSelectItemAtPath")
        indexFilter = indexPath.row
        if indexPath.row != 0 {
            self.isFaceDetectionOff = true
            let item:FaceMaskItem = (collectionArray?.object(at: indexPath.row - 1))! as! FaceMaskItem
            //        print(item.landmakrFile)
            landmarkParser.loadRssFeed(item.landmakrFile)
        }else{
            self.isFaceDetectionOff = true
        }
    }
    
    func swipeView(_ swipeView: HFSwipeView, didChangeIndexPath indexPath: IndexPath, changedView view: UIView) {
       // log("HFSwipeView(\(swipeView.tag)) -> \(indexPath.row)")
         print("didChangeIndexPath")
    }
}

// MARK: - HFSwipeViewDataSource
extension StartViewController: HFSwipeViewDataSource {
    func swipeViewItemDistance(_ swipeView: HFSwipeView) -> CGFloat {
        return 30   // left pad 15 + right pad 15
    }
    func swipeViewItemSize(_ swipeView: HFSwipeView) -> CGSize {
        // view [pad 15 + width 70 + pad 15] -> displays 100 width of cell
        return CGSize(width: 50, height: 100)
    }
    func swipeViewItemCount(_ swipeView: HFSwipeView) -> Int {
        return items.count
    }
    func swipeView(_ swipeView: HFSwipeView, viewForIndexPath indexPath: IndexPath) -> UIView {
        //let contentLabel = UILabel(frame: CGRect(origin: CGPoint(x: 0, y: 15), size: CGSize(width: 70, height: 70)))
        let contentImageView  = UIImageView(frame: CGRect(x: 0, y: 25, width: 50, height: 50))
        
        contentImageView.image = UIImage(named: items[indexPath.row])
        contentImageView.layer.cornerRadius = contentImageView.frame.width / 2
        contentImageView.layer.masksToBounds = true
        //contentLabel.textAlignment = .center
        //contentLabel.layer.cornerRadius = 35
        //contentLabel.layer.masksToBounds = true
        return contentImageView
    }
    func swipeView(_ swipeView: HFSwipeView, needUpdateViewForIndexPath indexPath: IndexPath, view: UIView) {
        updateCellView(view, indexPath: indexPath, isCurrent: false)
    }
    func swipeView(_ swipeView: HFSwipeView, needUpdateCurrentViewForIndexPath indexPath: IndexPath, view: UIView) {
        updateCellView(view, indexPath: indexPath, isCurrent: true)
    }
    func updateCellView(_ view: UIView, indexPath: IndexPath, isCurrent: Bool) {
        
        if let label = view as? UIImageView {
          /*  if isCurrent {
             
            } */
          
            label.image = UIImage(named: items[indexPath.row])
            
        } else {
            assertionFailure("failed to retrieve UILabel for index: \(indexPath.row)")
        }
}
}
extension UIImage {
    func overlayWith(image: UIImage, posX: CGFloat, posY: CGFloat) -> UIImage {
        let newWidth = posX < 0 ? abs(posX) + max(self.size.width, image.size.width) :
            size.width < posX + image.size.width ? posX + image.size.width : size.width
        let newHeight = posY < 0 ? abs(posY) + max(size.height, image.size.height) :
            size.height < posY + image.size.height ? posY + image.size.height : size.height
        let newSize = CGSize(width: self.size.width, height: self.size.height)
       
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0.0)
        let originalPoint = CGPoint(x: 0, y: 0)
        self.draw(in: CGRect(origin: originalPoint, size: self.size))
        print("Face Size : ",self.size)
        let overLayPoint = CGPoint(x: 0 , y: 0)
        image.draw(in: CGRect(origin: overLayPoint, size: self.size))
        print("Position Filter: ",posX, posY)
        print("Filter Size : ",image.size)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
}
extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), false, UIScreen.main.scale)
        self.backgroundColor = .clear
        // Draw view in that context
        drawHierarchy(in: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
extension StartViewController : UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return  imageViewSnap
    }
}
extension StartViewController : GADRewardBasedVideoAdDelegate {
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        print("Reward based video ad is received.")
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidCompletePlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad has completed.")
        
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad is closed.")
        self.fiveGiftsZonzContainer.isHidden = false
        self.visualEffectBlur.isHidden = false
        initRewardAd()
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.fiveGiftsZonzContainer.alpha = 1
              self.visualEffectBlur.alpha = 1
        }) { (verif) in
            self.moviePlayerAd.play()
        }
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
                                                    withAdUnitID: "ca-app-pub-3940256099942544/1712485313")
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
    }
    
    
}
public extension String {
    func indexInt(of char: Character) -> Int? {
        return firstIndex(of: char)?.utf16Offset(in: self)
    }
}
extension UITableViewCell {
    func enableGame(on: Bool) {
        self.isUserInteractionEnabled = on
        for view in contentView.subviews {
            self.isUserInteractionEnabled = on
            view.alpha = on ? 1 : 0.75
        }
    }
}
