//
//  SplashScreenController.swift
//  YLYL
//
//  Created by macbook on 1/4/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AVKit
import OneSignal
import Alamofire
import Hero
class SplashScreenController: UIViewController,OSSubscriptionObserver {
    /*
     * The Video Container
     */
    @IBOutlet weak var videoView : UIView!
    /*
     * The Player Engine of the Animation
     */
    var moviePlayer = AVPlayer()
    /*
     * The layer screen of the video
     */
    var moviePlayerLayer = AVPlayerLayer()
    
    
    @objc func LogoutCalled(_ notifcication : Notification) {
        print("Logout Received")
        if self.determineEligibality() {
                           print(UserDefaults.standard.object(forKey: "UserZonzay"))
                           let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                           DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                               // SocketIOManager.sharedInstance.detectDisconnect()
                           }
                           
                           let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                           do {
                               let a = try JSON(data: dataFromString!)
                               
                               //let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                              // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                               
                               SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                               self.ConnectTopush()
                               self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                               
                           }catch{
                               
                               print("UserDefaultsPlayer: ",error.localizedDescription)
                           }
                           
                       }else{
                          // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                          // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                           SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                           self.performSegue(withIdentifier: "go_to_main", sender: self)
                       }
    }
    
    /*
     * Init
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.LogoutCalled(_:)), name: NSNotification.Name.init("LogoutCalled"), object: nil)
       
        if let currentLanguage = Locale.currentLanguage {
            print(currentLanguage.rawValue)
            if currentLanguage == .en {
               _ = SetLanguage("English_en")
            }else if currentLanguage == .fr {
                _ = SetLanguage("French_fr")
                
            }else if currentLanguage == .es {
                _ = SetLanguage("Spanish_es")
            }else if currentLanguage == .pt{
                 _ = SetLanguage("Portugese_pg")
            }else if currentLanguage == .it {
                _ = SetLanguage("English_en")
            }else{
                _ = SetLanguage("Italien_it")
            }
            // Your code here.
        }
        Config.doneButtonTitle = Localization("Done")
        print("SplashScreen")
            let mode = (UIApplication.shared.delegate as! AppDelegate).NormalMode
            let index = (UIApplication.shared.delegate as! AppDelegate).indexToShow
        
            if mode {
                print("SplashScreen working : ",mode)
            let videoBundleURL = Bundle.main.path(forResource: "logoanimations_saif", ofType: "mp4")
            if let videoBundleURL = videoBundleURL {
                let videoURL = NSURL.fileURL(withPath: videoBundleURL)
                //let videoURL = URL(string: "http://zonzay.com/ioshKZsSq4I201905281349120000_android5ced389b9046be298b0962415ced389b9046be298b0962414760.mp4")
                self.moviePlayer = AVPlayer(url: videoURL)
                self.moviePlayer.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
                self.moviePlayerLayer = AVPlayerLayer(player: self.moviePlayer)
                self.moviePlayerLayer.frame = self.videoView.bounds
                self.moviePlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
                self.videoView.layer.addSublayer(self.moviePlayerLayer)
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlayingt(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.moviePlayer.currentItem)
                
            }
            }else{
                print("SplashScreen kicked : ",mode)
                if self.determineEligibality() {
                    print(UserDefaults.standard.object(forKey: "UserZonzay"))
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        // SocketIOManager.sharedInstance.detectDisconnect()
                    }
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        
                        //let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                       // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                        
                        SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                        self.ConnectTopush()
                        self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                        
                    }catch{
                        
                        print("UserDefaultsPlayer: ",error.localizedDescription)
                    }
                    
                }else{
                   // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                   // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                    SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                    self.performSegue(withIdentifier: "go_to_main", sender: self)
                }
            }
        
    
        
        
    }
    /*
     * When the animation finish, we go to the next Controller
     */
    var oneTime = false
    @objc func playerDidFinishPlayingt(note: Notification) {
        print("VIdeo Finished")
        
     /*   let pinghelper = PingHelper()
        pinghelper.host = "www.google.com"
        pinghelper.hostForCheck = "apple.com"
        pinghelper.ping { (isPingable) in
            print("ping succefully : ",isPingable)
        } */
        let settings : Parameters = [:]
        let headers : HTTPHeaders = [:]
        print(settings)
        print(headers)
        
        Alamofire.request(ScriptBase.sharedInstance.checkConncetion, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).response { response in
            print("response code : ",response.response?.statusCode )
            if response.response?.statusCode != nil {
                VersionCheck.shared.isUpdateAvailable() { (hasUpdates) in
                    print("is update available: \(hasUpdates)")
                    if self.oneTime == false {
                        self.oneTime = true
                        if hasUpdates == false {
                            if SocketIOManager.sharedInstance.connectedMiddleWar == false {
                                if self.determineEligibality() {
                                    print(UserDefaults.standard.object(forKey: "UserZonzay"))
                                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                                    
                                    
                                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                                    do {
                                        let a = try JSON(data: dataFromString!)
                                        
                                        // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                                        
                                        // navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                                        
                                        SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                                        self.ConnectTopush()
                                        self.videoView.removeFromSuperview()
                                        /*var testTab : [Int] = []
                                                                           for i in 1...59 {
                                                                               testTab.append(i)
                                                                           }
                                                                           SocketIOManager.sharedInstance.testBougiNumbers(tab: testTab) */
                                        self.performSegue(withIdentifier: "go_to_homeLogin", sender: self)
                                        
                                    }catch{
                                        
                                        print("UserDefaultsPlayer: ",error.localizedDescription)
                                    }
                                    
                                }else{
                                    // let navigation = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! UINavigationController)
                                    //  navigation.viewControllers.append((UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "login") as! AuthentificationController))
                                    if self.determinePhoto() {
                                        
                                    }else{
                                        
                                    }
                                    SocketIOManager.sharedInstance.rootNavigationController = self.navigationController
                                   
                                    self.performSegue(withIdentifier: "go_to_main", sender: self)
                                }
                            }else{
                                // Connected To Update
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServerUpdateController") as! ServerUpdateController
                                vc.hero.modalAnimationType = .zoom
                                vc.modalPresentationStyle = .overCurrentContext
                                self.present(vc, animated: true, completion: nil)
                            }
                        }else{
                            //Update Available
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AppUpdateController") as! AppUpdateController
                            vc.hero.modalAnimationType = .zoom
                            vc.modalPresentationStyle = .overCurrentContext
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
            }else{
                let alert = UIAlertController(title: "Zonzay", message: Localization("CANTSERVER"), preferredStyle: .alert)
                let action = UIAlertAction(title: Localization("Retry"), style: .default) { (alertt) in
                    let appDeleg = UIApplication.shared.delegate as! AppDelegate
                    appDeleg.restartApplication()
                }
                
                alert.addAction(action)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
      
    }
    func determineEligibality() -> Bool{
         if UserDefaults.standard.object(forKey: "UserZonzay") != nil {
            
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                let a =  JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.updateSocketId(userId: a["_id"].stringValue)
            print ("UserZonzay",a)
        if (a["userEmail"].exists() == false ) || (a["userBirthday"].exists() == false) || (a["userGender"].exists() == false) || (a["userName"].exists() == false) || (a["alphaCode"].exists() == false) {
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                return false
            }else {
                
                return true
            }
         }else{
            return false
        }
    }
    func determinePhoto() -> Bool {
         if UserDefaults.standard.object(forKey: "UserZonzay") != nil {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            let a =  JSON(data: dataFromString!)
            if (a["userImageURL"].exists() == false ) {
                return false
            }else{
                return true
            }
         }else{
            return false
        }
    }
    /*
     * To play to animation
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         let mode = (UIApplication.shared.delegate as! AppDelegate).NormalMode
        if mode {
        moviePlayer.playImmediately(atRate: 2.0)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    let params : Parameters = [
                        "userId" : a["_id"].stringValue ,
                        "playerId" : stateChanges.to.userId,
                        "type" : "ios"
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.setIosPlayerId , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            //      LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            print(b)
                            UserDefaults.standard.setValue(b.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
                }catch{
                    print("UserDefaultsError: ",error.localizedDescription)
                }
            }
            
        }
    }
}
extension Locale {
    
    static var enLocale: Locale {
        
        return Locale(identifier: "en-EN")
    } // to use in **currentLanguage** to get the localizedString in English
    
    static var currentLanguage: Language? {
        
        guard let code = preferredLanguages.first?.components(separatedBy: "-").first else {
            
            print("could not detect language code")
            
            return nil
        }
        print("Code Language : ",code)
        if ((code != "en") && (code != "fr") && (code != "es") && (code != "pt") && (code != "it")) {
            return Language(rawValue: "en")
        }else{
            return Language(rawValue: code)
        }
       
    }
}
enum Language: String {
    
    case none = ""
    case en = "en"
    case fr = "fr"
    case it = "it"
    case es = "es"
    case pt = "pt"
    
}
class VersionCheck {
    
    public static let shared = VersionCheck()
    let isDebuggerAttached: Bool = {
        var debuggerIsAttached = false
        
        var name: [Int32] = [CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid()]
        var info: kinfo_proc = kinfo_proc()
        var info_size = MemoryLayout<kinfo_proc>.size
        
        let success = name.withUnsafeMutableBytes { (nameBytePtr: UnsafeMutableRawBufferPointer) -> Bool in
            guard let nameBytesBlindMemory = nameBytePtr.bindMemory(to: Int32.self).baseAddress else { return false }
            return -1 != sysctl(nameBytesBlindMemory, 4, &info/*UnsafeMutableRawPointer!*/, &info_size/*UnsafeMutablePointer<Int>!*/, nil, 0)
        }
        
        // The original HockeyApp code checks for this; you could just as well remove these lines:
        if !success {
            debuggerIsAttached = false
        }
        
        if !debuggerIsAttached && (info.kp_proc.p_flag & P_TRACED) != 0 {
            debuggerIsAttached = true
        }
        
        return debuggerIsAttached
    }()
    func isUpdateAvailable(callback: @escaping (Bool)->Void) {
        if isDebuggerAttached == true {
           
        let bundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        if detectAppStoreInstallation() == false { Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(bundleId)").responseJSON { response in
            
            if let json = response.result.value as? NSDictionary, let results = json["results"] as? NSArray, let entry = results.firstObject as? NSDictionary, let appStoreVersion = entry["version"] as? String, let installedVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                print("json : ", Float(installedVersion))
                print("json : ", Float(appStoreVersion))
               
                if let appstoreFloat = Float(appStoreVersion), let installedVersionFloat = Float(installedVersion) {
                     print(installedVersionFloat < appstoreFloat)
                    callback(installedVersionFloat < appstoreFloat)
                }else{
                    callback(false)
                }
               
            }else{
                callback(false)
            }
        }
        }else{
            callback(false)
            }
        }else{
            callback(false)
        }
    }
    
    
}
func detectAppStoreInstallation() -> Bool{
    if let provisionPath = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") {
    if (!(FileManager.default.fileExists(atPath: provisionPath))) {
        	return true
    }
    }else{
       return true
    }
    return false
}
