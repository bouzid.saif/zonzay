//
//  SettingsViewController.swift
//  YLYL
//
//  Created by macbook on 1/16/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
class SettingsViewController: FixUIController,DoubleSliderValueChanged {
    func valueChanged(forMin value: Double) {
        print("MIN",value)
        minRangeLBL.text = String(Int(value))
        
    }
    func valueChanged(forMax value: Double) {
        print("MAX",value)
        maxRangeLBL.text = String(Int(value))
    }
    @IBOutlet weak var slider : DoubleSlider!
    @IBOutlet weak var minRangeLBL :UILabel!
    @IBOutlet weak var maxRangeLBL : UILabel!
    @IBOutlet weak var containerSlider : UIView!
    @IBOutlet weak var containerRange : UIView!
    @IBOutlet weak var containerMatchMaking : UIView!
    @IBOutlet weak var containerVIP : UIView!
    @IBOutlet weak var VIPLabel : UILabel!
    @IBOutlet weak var GenderBTN: UIButton!
    @IBOutlet weak var containerMatchMakingSettings : UIView!
    func hideContainers(_ verif:Bool) {
        slider.isHidden = verif
           minRangeLBL.isHidden = verif
           maxRangeLBL.isHidden = verif
           containerSlider.isHidden = verif
           containerRange.isHidden = verif
           containerMatchMaking.isHidden = verif
        containerMatchMakingSettings.isHidden = verif
        if verif {
            VIPLabel.text = "VIP ACCESS (not active)"
            VIPLabel.textColor = UIColor.lightGray
        }else{
             VIPLabel.text = "VIP ACCESS"
            VIPLabel.textColor = UIColor.black
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let button = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneAction(_:)))
        self.navigationItem.setLeftBarButton(button, animated: true)
        self.title = Localization("Settings")
        slider.valueChangedSlider = self
        hideContainers(true)
        let ab = UserDefaults.standard.value(forKey: "Subscription") as! String
        if ab == "true"{
            if UserDefaults.standard.value(forKey: "MatchChoice") != nil {
                GenderBTN.setTitle(UserDefaults.standard.value(forKey: "MatchChoice") as? String, for: .normal)
                minRangeLBL.text = UserDefaults.standard.value(forKey: "minChoice") as? String
                maxRangeLBL.text = UserDefaults.standard.value(forKey: "maxChoice") as? String
                slider.lowerValue =  Double(minRangeLBL.text!)!
                slider.upperValue = Double(maxRangeLBL.text!)!
                hideContainers(false)
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func doneAction(_ sender: UIBarButtonItem){
    UserDefaults.standard.setValue(self.GenderBTN.title(for: .normal)!, forKey: "MatchChoice")
         UserDefaults.standard.setValue(self.minRangeLBL.text!, forKey: "minChoice")
         UserDefaults.standard.setValue(self.maxRangeLBL.text!, forKey: "maxChoice")
        UserDefaults.standard.synchronize()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func disconnectAction(_ sender: UITapGestureRecognizer) {
        
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (alert) in
            //do the work
            self.signOut()
        }
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let alert = UIAlertController(title: Localization("Disconnect"), message: Localization("AYS"), preferredStyle: .alert)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.present(alert, animated: true, completion: nil)
    }
    func signOut(){
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let root =  storyboard.instantiateViewController(withIdentifier: "login")
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.navigationController?.view.window?.layer.add(transition, forKey: kCATransition)
        //q.popToRootViewController(animated: true)
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        
        self.navigationController?.pushViewController(root, animated: true)
    }
     private var selectedRow: Int = 0
    @IBAction func GanderPicker(_ sender: UIButton) {
        let displayStringFor:((String?)->String?)? = { string in
            if let s = string {
                switch(s){
                case "value 1":
                    return "Male"
                case "value 2":
                    return "Female"
                case "value 3":
                    return "Other"
                default:
                    return s
                }
            }
            return nil
        }
        /// Create StringPickerPopover:
        let p = StringPickerPopover(title: "Gender Match", choices: ["Male","Female","Other"])
            .setDisplayStringFor(displayStringFor)
            .setValueChange(action: { _, _, selectedString in
                print("current string: \(selectedString)")
            })
            .setFontSize(16)
            .setDoneButton(
                font: UIFont.boldSystemFont(ofSize: 16),
                color: UIColor.orange,
                action: { popover, selectedRow, selectedString in
                    print("done row \(selectedRow) \(selectedString)")
                    self.selectedRow = selectedRow
                    sender.setTitle(selectedString, for: .normal)
                    
            })
            .setCancelButton(action: {_, _, _ in
                print("cancel") })
            .setSelectedRow(selectedRow)
        p.appear(originView: sender, baseViewController: self)
        //p.disappearAutomatically(after: 3.0, completion: { print("automatically hidden")} )
        
    }
}
