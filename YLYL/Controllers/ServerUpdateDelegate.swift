//
//  ServerUpdateDelegate.swift
//  YLYL
//
//  Created by macbook on 2019-05-20.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

import Foundation
import UIKit

class ServerUpdateDelegate: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPoPup(notification:)), name: NSNotification.Name.init("closeAppUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPoPup(notification:)), name: NSNotification.Name.init("closeUpdate"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.RoomListener(_:)), name: NSNotification.Name.init("VideoRoomListener"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.dissmissIfPossible(_:)), name: NSNotification.Name.init("DissmissAll"), object: nil)
        
    }
    @objc func dissmissIfPossible(_ notification: Notification) {
        if let navigationController = self.navigationController, navigationController.isBeingPresented {
            // being presented
            print("Being presented")
            self.dismiss(animated: false, completion: nil)
        }else{
            // being pushed
            print("Being pushed")
        }
    }
       var couldWork = 1
    @objc func RoomListener(_ notification :Notification) {
        //Video Room Received
        //NotificationCenter.default.post(name: NSNotification.Name.init("VideoRoomReceived"), object: notification.object)
        print("Room Received :",self)
       
        if  SocketIOManager.sharedInstance.currentPresentedViewController == self{
            if couldWork % 1 == 0 {
                couldWork += 1
        let selectedJokes = SocketIOManager.sharedInstance.selectedJokes
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            var gameType = GameType.Friend
            switch SocketIOManager.sharedInstance.gameInfo[3] {
            case GameType.Friend.rawValue:
                gameType = GameType.Friend
                
            case GameType.ReplayFriend.rawValue :
                gameType = GameType.ReplayFriend
                
            case GameType.ReplayMatch.rawValue :
                gameType = GameType.ReplayMatch
                
            case GameType.RevengeFriend.rawValue :
                gameType = GameType.RevengeFriend
                
            case GameType.RevengeMatch.rawValue :
                gameType = GameType.RevengeMatch
                
            default:
                break
            }
            
            //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
            //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
            let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
            vc.willAppear = true
            vc.Owner = "slave"
            vc.roomId = NSNumber(value: Int(SocketIOManager.sharedInstance.gameInfo[2])!)
            vc.SecondUserId = SocketIOManager.sharedInstance.gameInfo[0]
            vc.selectedJokes = selectedJokes
            //let currentVc: UIViewController? = (UIApplication.shared.delegate?.window?!.rootViewController as? UINavigationController)?.topViewController
            //let viewUnknown =  presentStartViewController()
            SocketIOManager.sharedInstance.acceptVideoRoomRequest(userId: a["_id"].stringValue, senderId: SocketIOManager.sharedInstance.gameInfo[0], senderName: SocketIOManager.sharedInstance.gameInfo[1], roomId: SocketIOManager.sharedInstance.gameInfo[2], gameType: gameType)
            
            print("ToPresentFromJokes : ", self)
            self.navigationController?.pushViewController(vc, animated: true)
            /*if let topViewControler =  UIApplication.shared.keyWindow?.visibleViewController() {
                vc.navigation = UINavigationController()
                topViewControler.navigationController?.pushViewController(vc, animated: true)
            } */
            /*  if viewUnknown.isKind(of: AuthentificationController.self) {
             vc.navigation = self.currentPresentedViewController.navigationController
             self.currentPresentedViewController.navigationController?.pushViewController(vc, animated: true)
             }else{
             if viewUnknown.isKind(of: UINavigationController.self) {
             vc.navigation = (viewUnknown as! UINavigationController)
             (viewUnknown as! UINavigationController).pushViewController(vc, animated: true)
             }else{
             viewUnknown.navigationController?.pushViewController(vc, animated: true)
             }
             } */
            
            // DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            
            // currentVc.navigationController?.pushViewController(vc, animated: true)
            
            //}
            
        }catch{
            
        }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    @objc func showPoPup(notification:NSNotification) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.restartApplication()
    }
}
