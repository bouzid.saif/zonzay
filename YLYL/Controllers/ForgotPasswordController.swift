//
//  ForgotPasswordController.swift
//  YLYL
//
//  Created by macbook on 1/7/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import NotificationBannerSwift
class ForgotPasswordController: ServerUpdateDelegate {
    /*
     * Email of user
     */
    @IBOutlet weak var EmailTF:TextField!
  let _notificationToast = CustomToast.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.configureEmailTF()
    }
    /*
     * Configure of the Email textField style
     */
    func configureEmailTF(){
        EmailTF.layer.cornerRadius = EmailTF.frame.height / 2
        EmailTF.layer.borderWidth = 1.0
        EmailTF.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        EmailTF.layer.masksToBounds = true
       
            let image = UIImage(named: "path_642_saif")
            let imageView = UIImageView(image:image )
            imageView.contentMode = .center
            EmailTF.leftView = imageView
            EmailTF.leftView?.frame.size = CGSize(width: image!.size.width + 20, height: image!.size.height )
            
            EmailTF.leftViewMode = .always
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func forgotPasswordAction(_ sender: UIButton){
        if (EmailTF.text?.isValidEmail())! {
      
       
    
            let settings : Parameters = [
                "userEmail" :   self.EmailTF.text!
            ]
            
            print(settings)
            Alamofire.request(ScriptBase.sharedInstance.forgotPassword, method: .post, parameters: settings, encoding: JSONEncoding.default).responseString { response in
                if response.data != nil {
                    
                   
                    let res = String(data: response.data ?? Data(), encoding: .utf8)!
                     print("REQU1 : ",res)
                    //self.jokes = data
                    if res == "invalid" {
                        let banner = NotificationBanner(customView: self._notificationToast)
                        self._notificationToast.LabelN.text = "This Email is wrong or dosen't exist."
                        banner.customBannerHeight = 80
                        banner.show(queuePosition: .front, bannerPosition: .top)
                    }else{
                        let alert = UIAlertController(title: "Forgot password", message: "An Email containing instructions has been sent to you.", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .cancel, handler: { alert in
                            self.navigationController?.popViewController(animated: true)
                        })
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    let banner = NotificationBanner(customView: self._notificationToast)
                    self._notificationToast.LabelN.text = "Please fill all in the blanks."
                    banner.customBannerHeight = 80
                    banner.show(queuePosition: .front, bannerPosition: .top)
                    //self.defaultMessage.isHidden = false
                  
                }
                
            }
            
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = "Please fill a valid email address."
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
    }
}
