//
//  HomeViewController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 07/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapleBacon
class HomeViewController: NavigationProfile,UITabBarControllerDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //Views
    @IBOutlet weak var profileImageView: ExtensionProfile!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var zonzayMoneyLabel: UILabel!
    
    @IBOutlet weak var videosTableView: UITableView!
    
    @IBOutlet weak var noVideoContainerAlpha: UIView!
    
    @IBAction func shareAppAction(_ sender: UITapGestureRecognizer) {
        let shareLink = ["http://zonzay.com/download"]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    @IBOutlet weak var noVideoLBL: UILabel!
    @IBOutlet weak var noVideoImage: UIImageView!
    @IBOutlet weak var videoCollectionView : UICollectionView!
    
    
    @IBOutlet weak var laughOverVidLBL: UILabel!
    var Acutality : [VideoYLYL]  = []
     var AcutalityTemp : [VideoYLYL]  = []
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showVideoFromNotif(_:)), name: NSNotification.Name.init("ShowVideoFromNotif"), object: nil)
        //Create a circle image profile
        profileImageView.image = UIImage(named: "profile_hamid")
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.borderColor = UIColor.white.cgColor
        self.videoCollectionView.delegate = self
        self.videoCollectionView.dataSource = self
        self.noVideoImage.image = self.noVideoImage.image?.maskWithColor(color: UIColor.white.withAlphaComponent(0.64))
        self.noVideoLBL.textColor = UIColor.white.withAlphaComponent(0.64)
        self.noVideoImage.isHidden = true
        self.noVideoLBL.isHidden = true
        self.laughOverVidLBL.text = Localization("LaughOverVidJokers")
       
    }
    @objc func showVideoFromNotif(_ notif : Notification) {
        if let data = notif.object as? JSON {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeNext") as! HomeNextViewController
            
            
            let tempVid = VideoYLYL(id: data["_id"].stringValue, videoFirstUserId: User(id: data["videoFirstUserId"]["_id"].stringValue, userFirstName: data["videoFirstUserId"]["userFirstName"].stringValue, userLastName: data["videoFirstUserId"]["userLastName"].stringValue, userAge: data["videoFirstUserId"]["userAge"].stringValue, userEmail: data["videoFirstUserId"]["userEmail"].stringValue, token: data["videoFirstUserId"]["userRefreshToken"].stringValue,photo:data["videoFirstUserId"]["userImageURL"].stringValue,username: data["videoFirstUserId"]["userName"].stringValue ), videoSecondUserId: User(id: data["videoSecondUserId"]["_id"].stringValue, userFirstName: data["videoSecondUserId"]["userFirstName"].stringValue, userLastName: data["videoSecondUserId"]["userLastName"].stringValue, userAge: data["videoSecondUserId"]["userAge"].stringValue, userEmail: data["videoSecondUserId"]["userEmail"].stringValue, token: data["videoSecondUserId"]["userRefreshToken"].stringValue,photo:data["videoSecondUserId"]["userImageURL"].stringValue ,username: data["videoSecondUserId"]["userName"].stringValue ), videoroomId: Room(id: "", roomNumber: "", firstUserId: "", secondUserId: ""), videoDate: data["videoDate"].stringValue, pathVideo: data["pathVideo"].stringValue, commentList: [],listLikes: data["likes"],seen: data["seen"].stringValue,listComments: data["comments"])
            vc.videoURL = tempVid.getPathVideo()
            vc.videoObject = tempVid
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewwillappear")
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            self.profileImageView.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "profile_hamid"), transformer: nil, progress: nil, completion: nil)
            self.profileNameLabel.text = a["userName"].stringValue
            
            
        }catch{
            
        }
        SocketIOManager.sharedInstance.currentPresentedViewController = self
         self.configureSockets()
        if  self.reloadView {
            self.videoCollectionView.isUserInteractionEnabled = false
            self.getZonz(reload: false)
        }else{
            self.videoCollectionView.isUserInteractionEnabled = false
            self.getZonz(reload: true)
            self.reloadView = true
            
        }
        self.getSubscriptionActivation()
        
    }
   @objc func goToProfile(_ sender: Any){
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
    //let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! ProfileTest
    //MARK:StopedHere
    let vc = storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
    
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let a =  JSON(data: dataFromString!)
        vc.userTarget = a
    
    self.navigationController?.pushViewController(vc, animated: true)
    }
    func configureSockets(){
        do {
            print("socketConfigured")
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            
            
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
                let a = try JSON(data: dataFromString!)
            
                self.profileImageView.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "profile_hamid"), transformer: nil, progress: nil, completion: nil)
                self.profileNameLabel.text = a["userName"].stringValue
          
            //SocketIOManager.sharedInstance.test(userId: a["_id"].stringValue)
        
        }catch{
            
        }
       
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Acutality.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.Acutality.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width - 54, height: ((self.view.frame.width - 74) - 65) + 63)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTwo", for: indexPath)
            
            let viewToRound = videoCell.viewWithTag(5)
            viewToRound?.layer.borderWidth = 2.0
            viewToRound?.layer.borderColor = UIColor.white.withAlphaComponent(0.25).cgColor
            let imageUserOne = videoCell.viewWithTag(1) as! UIImageViewWithMask
             let imageUserTwo = videoCell.viewWithTag(2) as! UIImageViewWithMask
            
            // Users Images
        print("indexPath : ",indexPath.row)
            imageUserOne.setImage(with: URL(string: Acutality[indexPath.row].getVideoFirstUser().getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            imageUserTwo.setImage(with: URL(string: Acutality[indexPath.row].getVideoSecondUser().getProfileImage()), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            //Share Section
            let shareButton = videoCell.viewWithTag(3)
            let shareLabel = videoCell.viewWithTag(18) as! UILabel
            shareLabel.text = Localization("share")
            shareButton!.isUserInteractionEnabled = true
            shareLabel.isUserInteractionEnabled = true
            let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.shareObject(_:)))
            shareButton!.addGestureRecognizer(tapgesture)
            shareLabel.addGestureRecognizer(tapgesture)
            //Likes Section
            let likeLabel = videoCell.viewWithTag(10) as! UILabel
            likeLabel.text = "\(Acutality[indexPath.row].getLikes().count)"
            let likeButton = videoCell.viewWithTag(7)
            likeButton!.isUserInteractionEnabled = true
            let tapgestureLike = UITapGestureRecognizer(target: self, action: #selector(self.LikeObject(_:)))
            likeButton?.addGestureRecognizer(tapgestureLike)
            let likeHeart = videoCell.viewWithTag(11) as! UIButton
            likeHeart.addGestureRecognizer(tapgestureLike)
        
        if determineContains(Acutality[indexPath.row].getLikes(), self.ownedLikes) {
            likeHeart.setImage(UIImage(named: "HeartFull")?.maskWithColor(color: UIColor(red: 255 / 255, green: 61 / 255, blue: 61 / 255, alpha: 1)), for: .normal)
        }else{
            likeHeart.setImage(UIImage(named: "HeartFull"), for: .normal)
        }
        //Seen Section
        let seenLabel = videoCell.viewWithTag(12) as! UILabel
       
            seenLabel.text = "\(Acutality[indexPath.row].getSeen()) " + Localization("seenVideo")
        
        
        //Video Details Section
        
        let ZonzayLogo = videoCell.viewWithTag(15) as! UIImageView
        let gestureZonzay = UITapGestureRecognizer(target: self, action: #selector(self.didSelectVideoDetails(_:)))
        ZonzayLogo.isUserInteractionEnabled = true
        ZonzayLogo.addGestureRecognizer(gestureZonzay)
        
        // Comments Action
        let commentsLabel = videoCell.viewWithTag(16) as! UILabel
        commentsLabel.text = "\(Acutality[indexPath.row].getComments().count) " + Localization("comments")
        //next 17
        let commentsViewAll = videoCell.viewWithTag(17) as! UILabel
        let gestureComments = UITapGestureRecognizer(target: self, action: #selector(self.didSelectVideoDetailsFormComments(_:)))
        commentsViewAll.isUserInteractionEnabled = true
        commentsLabel.isUserInteractionEnabled = true
        commentsViewAll.addGestureRecognizer(gestureComments)
        commentsLabel.addGestureRecognizer(gestureComments)
        if Acutality[indexPath.row].getComments().count > 0 {
            commentsViewAll.text = Localization("ViewAll")
        }else{
            commentsViewAll.alpha = 0
        }
       // let imageToWrite = UIImage(named: "background_profile*")
       // backgroundImage.image  = imageToWrite?.imageWithBorder(width: 4, color: .white)
           return videoCell
       
    }
    func determineContains(_ tab:[String], _ tabTwo:[String]) -> Bool{
        for t in tab {
            for ti in tabTwo {
                if ti == t {
                    return true
                }
            }
        }
        return false
    }
    @objc func didSelectVideoDetailsFormComments(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: self.videoCollectionView)
        if  point != CGPoint.zero  {
            if let index = self.videoCollectionView.indexPathForItem(at: point)  {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeNext") as! HomeNextViewController
                vc.videoURL = self.Acutality[index.row].getPathVideo()
                vc.videoObject = self.Acutality[index.row]
                self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
    }
    var reloadView = false
    @objc func didSelectVideoDetails(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: self.videoCollectionView)
        if  point != CGPoint.zero  {
            if let index = self.videoCollectionView.indexPathForItem(at: point)  {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeNext") as! HomeNextViewController
                if self.Acutality.item(at: index.row) != nil {
                vc.ownedLikes = self.ownedLikes
                vc.videoURL = self.Acutality[index.row].getPathVideo()
                vc.videoObject = self.Acutality[index.row]
                    self.reloadView = false
                 self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
                self.navigationController?.pushViewController(vc, animated: true)
                
                
            }
            }
        }
        
    }
    
    @objc func shareObject(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: self.videoCollectionView)
        if  point != CGPoint.zero  {
            if let index = self.videoCollectionView.indexPathForItem(at: point)  {
                let actu = Acutality[index.row]
                let shareLink = ["http://zonzay.com/video/" + actu.getVideoId()]
                let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        
        
        
    }
    @objc func LikeObject(_ sender: UITapGestureRecognizer) {
       /* let point = sender.location(in: self.videoCollectionView)
        if  point != CGPoint.zero  {
            
            if let index = self.videoCollectionView.indexPathForItem(at: point)  {
                let actu = Acutality[index.row]
                
               if determineContains(Acutality[index.row].getLikes(), self.ownedLikes) {
                
                //dislike
                self.disLikeVideo(index: actu.getVideoId())
               }else{
                //like
                self.likeVideo(index: actu.getVideoId())
                }
            }
        } */
        
        
        
    }
    func likeVideo(index:String){
         let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
         let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
           let a = JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "videoId" : index
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.likeVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.data != nil {
                let data = JSON(response.data ?? Data())
                print("REQU1 : ",data)
                if data["like"]["_id"].exists() {
                    self.ownedLikes.append(data["like"]["_id"].stringValue)
                    self.getActualityFromDataBase(animated: false,reload:false)
                }
            }
        }
        
    }
    func disLikeVideo(index:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "videoId" : index
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
        print(settings)
        print(headers)
        Alamofire.request(ScriptBase.sharedInstance.disLikeVideo, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            if response.data != nil {
                let data = JSON(response.data ?? Data())
                print("REQU1 : ",data)
                if data["like"]["_id"].exists() {
                    self.ownedLikes.removeAll(data["like"]["_id"].stringValue)
                    self.getActualityFromDataBase(animated: false,reload:false)
                }
            }
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let videoCell = tableView.dequeueReusableCell(withIdentifier: "video", for: indexPath) as! HomeCustomCell
        videoCell.setVideo(name: "hamido hamido")
        print(Acutality.count)
        print(indexPath.row)
        if Acutality.count != 0 {
        videoCell.configureCell(nameVideo: Acutality[indexPath.row].getVideoFirstUser().getFirstName() + " " + Acutality[indexPath.row].getVideoSecondUser().getLastName(), videoDate: Acutality[indexPath.row].getVideoDate(), firstUserName: Acutality[indexPath.row].getVideoFirstUser().getFirstName() + " " + Acutality[indexPath.row].getVideoFirstUser().getLastName(), secondUserName: Acutality[indexPath.row].getVideoSecondUser().getFirstName() + " " + Acutality[indexPath.row].getVideoSecondUser().getLastName(), pathVideo: Acutality[indexPath.row].getPathVideo())
        videoCell.firstUserName.textAlignment = .center
        videoCell.secondUserName.textAlignment = .center
        }
        return videoCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
   
    func getActualityFromDataBase(animated:Bool,reload:Bool){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        if animated == true {
            self.Acutality = []
            self.AcutalityTemp = []
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getActuality, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    print("REQU1 : ",data)
                    if data.arrayObject != nil {
                       // self.JokesJSON = data
                        
                        if data.arrayObject?.count != 0 {
                            self.AcutalityTemp = []
                        for i in 0...((data.arrayObject?.count)! - 1) {
                            self.AcutalityTemp.append(VideoYLYL(id: data[i]["_id"].stringValue, videoFirstUserId: User(id: data[i]["videoFirstUserId"]["_id"].stringValue, userFirstName: data[i]["videoFirstUserId"]["userFirstName"].stringValue, userLastName: data[i]["videoFirstUserId"]["userLastName"].stringValue, userAge: data[i]["videoFirstUserId"]["userAge"].stringValue, userEmail: data[i]["videoFirstUserId"]["userEmail"].stringValue, token: data[i]["videoFirstUserId"]["userRefreshToken"].stringValue,photo:data[i]["videoFirstUserId"]["userImageURL"].stringValue,username: data[i]["videoFirstUserId"]["userName"].stringValue ), videoSecondUserId: User(id: data[i]["videoSecondUserId"]["_id"].stringValue, userFirstName: data[i]["videoSecondUserId"]["userFirstName"].stringValue, userLastName: data[i]["videoSecondUserId"]["userLastName"].stringValue, userAge: data[i]["videoSecondUserId"]["userAge"].stringValue, userEmail: data[i]["videoSecondUserId"]["userEmail"].stringValue, token: data[i]["videoSecondUserId"]["userRefreshToken"].stringValue,photo:data[i]["videoSecondUserId"]["userImageURL"].stringValue ,username: data[i]["videoSecondUserId"]["userName"].stringValue ), videoroomId: Room(id: "", roomNumber: "", firstUserId: "", secondUserId: ""), videoDate: data[i]["videoDate"].stringValue, pathVideo: data[i]["pathVideo"].stringValue, commentList: [],listLikes: data[i]["likes"],seen: data[i]["seen"].stringValue,listComments: data[i]["comments"]))
                            self.AcutalityTemp[i].getVideoFirstUser().setUserName(data[i]["videoFirstUserId"]["userName"].stringValue)
                            self.AcutalityTemp[i].getVideoSecondUser().setUserName(data[i]["videoSecondUserId"]["userName"].stringValue)
                         
                            }
                           /* let dateFormatter = DateFormatter()
                            
                            let dateFormatterNow = DateFormatter()
                            dateFormatter.timeZone = NSTimeZone.system
                            dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
                            dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
                            dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
                            
                            self.AcutalityTemp.sort { (v1, v2) -> Bool in
                                let dateFirst = dateFormatter.date(fromSwapiString: v1.getVideoDate())
                                let dateSecond = dateFormatter.date(fromSwapiString: v2.getVideoDate())
                                 //let dateNow = dateFormatterNow.date(fromSwapiString: dateFormatter.string(from: Date()))
                                switch dateFirst!.compare(dateSecond!) {
                                case .orderedAscending :
                                    return false
                                case .orderedDescending :
                                    return true
                                case .orderedSame:
                                    return false
                                }
                               
                                
                            } */
                            if reload  {
                            if animated {
                                self.Acutality = self.AcutalityTemp
                             self.videoCollectionView.reloadData()
                            }else{
                                if self.Acutality.count != self.AcutalityTemp.count {
                    let imageReload = UIImage(named: "refreshHome")
                    let buttonReload = UIButton(type: .custom)
                    buttonReload.setImage(imageReload, for: .normal)
                    buttonReload.addTarget(self, action: #selector(self.reloadHome(_:)), for: .touchUpInside)
                    self.view.addSubview(buttonReload)
                    buttonReload.frame = CGRect(x: (self.view.frame.width / 2) - 25, y: self.laughOverVidLBL.centerY + 25, width: 50, height: 50)
                    self.view.layoutIfNeeded()
                                }
                        }
                        }
                        else{
                                    let indexes = self.videoCollectionView.indexPathsForVisibleItems
                                     self.Acutality = self.AcutalityTemp
                                    self.videoCollectionView.performBatchUpdates({
                                        self.videoCollectionView.reloadItems(at: indexes)
                                    }, completion: nil)
                        }
                               
                                
                            
                        }else{
                             self.Acutality = []
                            self.AcutalityTemp = []
                           // self.videosTableView.reloadData()
                            self.videoCollectionView.isHidden = true
                            self.noVideoImage.isHidden = false
                            self.noVideoLBL.isHidden = false

                        }
                    }else{
                            self.Acutality = []
                            //self.videosTableView.reloadData()
                          self.videoCollectionView.isHidden = true
                        self.noVideoImage.isHidden = false
                        self.noVideoLBL.isHidden = false

                    }
                    //self.jokes = data
                   
                }else{
                   // self.defaultMessage.isHidden = false
                    self.videoCollectionView.isHidden = true
                    self.noVideoImage.isHidden = false
                    self.noVideoLBL.isHidden = false

                }
                
            }
            
        }catch {
            
        }
        
        self.videoCollectionView.isUserInteractionEnabled = true
    }
    @objc func reloadHome(_ sender: UIButton ) {
        self.Acutality = self.AcutalityTemp
        self.videoCollectionView.reloadData()
        /*let indexes = self.videoCollectionView.indexPathsForVisibleItems
                                        self.Acutality = self.AcutalityTemp
                                       self.videoCollectionView.performBatchUpdates({
                                           self.videoCollectionView.reloadItems(at: indexes)
                                       }, completion: nil) */
        self.videoCollectionView.scrollToTop(animated: true)
        DispatchQueue.main.async {
            sender.removeFromSuperview()
        }
    }
    func getZonz(reload: Bool){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                if data != JSON.null  {
                    if data["auth"].boolValue == true {
                  
                    }else{
                        self.ownedLikes = []
                        if !data["message"].exists() {
                        if data["likes"].arrayObject?.count != 0 {
                            for i in 0...((data["likes"].arrayObject?.count)! - 1) {
                                self.ownedLikes.append(data["likes"][i].stringValue)
                                
                            }
                        }
                        UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                        UserDefaults.standard.synchronize()
                        }
                    }
                }
                if zonz != "" {
                    self.zonzayMoneyLabel.text = zonz
                    
                }else{
                    self.zonzayMoneyLabel.text = "0"
                }
                if reload == false && self.reloadView == true {
                    self.getActualityFromDataBase(animated: false,reload:!reload)

                }else{
                    self.getActualityFromDataBase(animated: reload,reload:reload)

                }
                
            }
            
        }catch {
            
        }
    }
    var ownedLikes : [String] = []
    func getSubscriptionActivation(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getUserSubscription, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data)
                print("Subscription :",data)
                if data != JSON.null  {
                    
                    if data["message"].stringValue == "true" {
                        
                        UserDefaults.standard.setValue("true", forKey: "Subscription")
                        UserDefaults.standard.synchronize()
                        
                    }else if data["message"].stringValue == "false"{
                        
                            UserDefaults.standard.setValue("false", forKey: "Subscription")
                            UserDefaults.standard.synchronize()
                        
                        
                    }
                }
               
            }
            
        }catch {
            
        }
    }
}
class CustomButton : UIButton {
    var index : Int!
}


