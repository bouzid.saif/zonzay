//
//  PassionController.swift
//  YLYL
//
//  Created by macbook on 1/8/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import ChameleonFramework
class PassionController: ServerUpdateDelegate,UICollectionViewDelegate,UICollectionViewDataSource {
    var isFirstTime = false
    var navigation : UINavigationController? = nil
   // var Interests : [String] = ["#Musique","#Movies","#Sports","#Camping","#Gaming","#Traveling","#Dancing","#Photography"]
    /*
     * MARK: DataSource
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    /*
     * MARK: DataSource
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as UICollectionViewCell
        let viewContainer = cell.viewWithTag(1)!
        let imageV = cell.viewWithTag(2) as! UIImageView
        let textLBL = cell.viewWithTag(3) as! UILabel
        
        viewContainer.layer.cornerRadius = viewContainer.frame.width / 8
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        viewContainer.clipsToBounds = true
        switch indexPath.row {
        case 0:
            imageV.image = #imageLiteral(resourceName: "musicunclick_saif")
           // imageV.contentMode = .scaleAspectFill
           // imageV.clipsToBounds = true
            textLBL.text = "#Musique"
            
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Musique") {
                self.selectCell(cell: cell)
            }
        case 1:
            imageV.image = #imageLiteral(resourceName: "moviesuncliked_saif")
            textLBL.text = "#Movies"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Movies") {
                self.selectCell(cell: cell)
            }
        case 2:
            imageV.image = #imageLiteral(resourceName: "sportsunclick")
            textLBL.text = "#Sports"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Sports") {
                self.selectCell(cell: cell)
            }
        case 3:
            imageV.image = #imageLiteral(resourceName: "campingunclick_saif")
            textLBL.text = "#Camping"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Camping") {
                self.selectCell(cell: cell)
            }
        case 4:
            imageV.image = #imageLiteral(resourceName: "gamingunclick_saif")
            textLBL.text = "#Gaming"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Gaming") {
                self.selectCell(cell: cell)
            }
        case 5:
            imageV.image = #imageLiteral(resourceName: "travelingunclick_saif")
            textLBL.text = "#Traveling"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Traveling") {
                self.selectCell(cell: cell)
            }
        case 6:
            imageV.image = #imageLiteral(resourceName: "dancingunclick_saif")
            textLBL.text = "#Dancing"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Dancing") {
                self.selectCell(cell: cell)
            }
        case 7:
            imageV.image = #imageLiteral(resourceName: "photographyunclick_saif")
            textLBL.text = "#Photography"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Photography") {
                self.selectCell(cell: cell)
            }
        case 8 :
            imageV.image = #imageLiteral(resourceName: "ReadingIcon.png")
            textLBL.text = "#Reading"
            textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
            if Choosed.contains("#Reading") {
                self.selectCell(cell: cell)
            }
        default:
            break
        }
       
        return cell
    }
    
    func selectCell(cell:UICollectionViewCell) {
        let imageV = cell.viewWithTag(2) as! UIImageView
        let textLBL = cell.viewWithTag(3) as! UILabel
        let view = cell.viewWithTag(1)
        imageV.image = imageV.image?.maskWithColor(color: .white)
        view!.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
        textLBL.textColor = .white
    }
    /*
     * MARK: Delegate
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) else {
            return
        }
        let view = cell.viewWithTag(1)!
        if view.backgroundColor == UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1){
             let textLBL = cell.viewWithTag(3) as! UILabel
             self.Choosed.removeString(text: textLBL.text!)
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveLinear], animations: {
                cell.contentView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }) { _ in
                UIView.animate(withDuration: 0.15, delay: 0, options: [.curveLinear], animations: {
                    cell.contentView.transform = CGAffineTransform.identity
                }) { _ in
                    view.backgroundColor = UIColor.white
                    let image = cell.viewWithTag(2) as! UIImageView
                    image.image = image.image?.maskWithColor(color: UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1))
                   
                    textLBL.textColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    self.Choosed.removeString(text: textLBL.text!)
                    print("Choosed:",self.Choosed)
                }
            }
            
        }else{
             let textLBL = cell.viewWithTag(3) as! UILabel
             self.Choosed.append(textLBL.text!)
            UIView.animate(withDuration: 0.15, delay: 0, options: [.curveLinear], animations: {
            cell.contentView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }) { _ in
                
                UIView.animate(withDuration: 0.15, delay: 0, options: [.curveLinear], animations: {
                    cell.contentView.transform = CGAffineTransform.identity
                }) { _ in
                    view.backgroundColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1)
                    let image = cell.viewWithTag(2) as! UIImageView
                    image.image = image.image?.maskWithColor(color: UIColor.white)
                   
                    textLBL.textColor = UIColor.white
                    //self.Choosed.append(textLBL.text!)
                    print("Choosed:",self.Choosed)
                }
            }
            
           
        }
        
    }
    /*
     * The collection view that contains our passions
     */
    @IBOutlet weak var collectionV : UICollectionView!
    /*
     * Done Button
     */
    @IBOutlet weak var DoneBTN : UIButton!
    
    @IBOutlet weak var pleasechooseLBL : UILabel!
    /*
     * Contains the Passions choosed
     */
    var Choosed : [String]  = []
        var ViewBack = ""
    /*
     * Init our view
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        pleasechooseLBL.text = Localization("Interests0")
        self.view.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.view.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.3))
        if isFirstTime {
            
        }else{
             getPassion()
        }
       
        self.configureConnectButton(button: DoneBTN)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
       // button.layer.cornerRadius = 9.0
       //button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.DoneAction(_:)), for: .touchUpInside)
    }
    @objc func DoneAction(_ sender: UIButton) {
        if Choosed.count != 0 {
           
        UserDefaults.standard.set(determineUserChoice(), forKey: "Passion")
        updatePassion()
           
        }else{
            //0
            let alert = UIAlertController(title: Localization("Interests"), message: Localization("Interests0"), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel ,handler: { _ in
                //self.getGifs(firstTime: false)
            }))
        
            self.present(alert, animated: true, completion: nil)
        }
        /* DispatchQueue.main.async(execute: {
        self.performSegue(withIdentifier: "go_to_jokes", sender: self)
        }) */
        
    }
    func determineUserChoice() -> String {
        var result = ""
        if Choosed.count == 0 {
        return ""
        }else{
            for var ch in Choosed {
                ch.removeFirst()
                result = result + ch + ";"
            }
            result.removeLast()
            return result
        }
    }
    func updatePassion(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listInterests" : determineUserChoice()
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateInterests, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                var data = JSON(response.data ?? Data())
                
                print("updateInterests : ",data)
                if data != JSON.null  {
                    data = data["user"]
                    UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                    UserDefaults.standard.synchronize()
                   
                    if self.ViewBack == "" {
                    print("A")
                    if self.isFirstTime{
                        print("B")
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "AddingPhotoController") as! AddingPhotoController
                        self.navigationController?.pushViewController(vc, animated: true)
                        //self.navigation?.pushViewController(vc, animated: true)
                        //self.performSegue(withIdentifier: "go_to_homeP", sender: self)
                    }else{
                        print("C")
                        self.navigation?.popViewController(animated: true)
                    }
                    }else{
                        print("D")
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }

            }
            
        }catch {
            
        }
    }
    func getPassion(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print("Saif :" ,headers)
            Alamofire.request(ScriptBase.sharedInstance.getInterests, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseString { response in
                //let data = JSON(response.data ?? Data())
                print("Response : ",response.value)
                if 200..<300 ~= (response.response?.statusCode)! {
           
                var result = String(data: response.data!, encoding: .utf8)!
                    print("Passion : ",result)
                    //result.remove
                    if result.first == "\"" {
                        result.removeFirst()
                    }
                    if result.last == "\"" {
                        result.removeLast()
                    }
                    print("ResultTab : ",result)
                    if result != ""{
                    let resultTab = result.components(separatedBy: ";")
                    
                    self.Choosed = []
                    for res in resultTab {
                        print(res)
                        self.Choosed.append("#" + res)
                    }
                    }
                    print(self.Choosed)
                    self.collectionV.reloadData()
                }
              /*  if data != JSON.null  {
                        UserDefaults.standard.setValue(data.rawString(), forKey: "UserZonzay")
                        UserDefaults.standard.synchronize()
                    
                } */
                
            }
            
        }catch {
            
        }
    }
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }

}
/*
 * Extension to let us delete a string object from an array
 */
extension Array {
    mutating func removeString(text: String) {
        for i in 0...self.count - 1 {
            if (self[i] as! String) == text {
                remove(at: i)
                break
            }
        }
    }
}
extension UIImage {
    
    func scaled(with scale: CGFloat) -> UIImage? {
        // size has to be integer, otherwise it could get white lines
        let size = CGSize(width: floor(self.size.width * scale), height: floor(self.size.height * scale))
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
