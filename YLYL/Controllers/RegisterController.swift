//
//  RegisterController.swift
//  YLYL
//
//  Created by macbook on 1/7/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import libPhoneNumber_iOS
import SwiftyJSON
import Alamofire
import NotificationBannerSwift
import OneSignal
class RegisterController: ServerUpdateDelegate,PassCountryDelegate,UIPickerViewDelegate,UIPickerViewDataSource,OSSubscriptionObserver,UITextFieldDelegate {
   
    
    
    /*
     * The Logo ImageView
     */
    @IBOutlet weak var LogoView : UIImageView!
    /*
     * Full Name Textfield
     */
    @IBOutlet weak var FullNameTF : TextField!
    /*
     * User Name Textfield
     */
    @IBOutlet weak var UserName : TextField!
    /*
     * Email Textfield
     */
    @IBOutlet weak var EmailTF: TextField!
    /*
     * Password Textfield
     */
    @IBOutlet weak var PasswordTF : PasswordTextField!
    /*
     * Phone Code text field choice
     */
    @IBOutlet weak var PhoneTF : TextFieldCode!
    /*
     * Password confirmation Textfield
     */
    @IBOutlet weak var ConfirmPasswordTF : TextField!
    @IBOutlet weak var BirthdayTF : TextFieldPhone!
    @IBOutlet weak var GenderTF : TextFieldPhone!
    
    
    @IBOutlet weak var ChoiceOne: DLRadioButton!
    
    @IBOutlet weak var ChoiceTwo: DLRadioButton!
    @IBOutlet weak var ChoiceThree: DLRadioButton!
    @IBOutlet weak var BlurEffect: UIVisualEffectView!
    
    @IBOutlet weak var CustomValidation: UIView!
    
    @IBOutlet weak var validationOKBTN: UIButton!
    @IBOutlet weak var smileyEmail: UIImageView!
    @IBOutlet weak var scrollView : UIScrollView!
    /*
     * Birtdhay picker date
     */
    @IBOutlet weak var PhoneSelectionView : UIView!
    /*
     * Utility of Birtdhay picker date
     */
    let datePicker = UIDatePicker()
    /*
     * Check whether the user has checked the terms or not
     */
    var verifiedTerms : Bool = false
    /*
     * Accept Terms checkbox
     */
    @IBOutlet weak var checkBox: Checkbox!
    /*
     * SignUp Button
     */
    @IBOutlet weak var signUpBTN: UIButton!
    
    @IBOutlet weak var PhoneNumberLBL : TextFieldPhone!
    @IBOutlet weak var byRegistringTerms: UILabel!
    let _notificationToast = CustomToast.shared
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        FullNameTF.delegate = self
        UserName.delegate = self
        EmailTF.delegate = self
        PhoneNumberLBL.delegate = self
        PasswordTF.delegate = self
        ConfirmPasswordTF.delegate = self
        self.navigationController?.isNavigationBarHidden = true
       // self.configureImageView()
        self.configurePasswordTF()
        self.configureTextFields(textFields: [FullNameTF,EmailTF,ConfirmPasswordTF])
        self.configurePhoneSelectioView()
        self.configurePhoneTF()
        self.configureBirthdayTF()
        //self.configureGenderTF()
        self.configureCheckBox()
        self.configureConnectButton(button: signUpBTN)
        ChoiceOne.isSelected = true
        initTerms()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
  /*  func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.PasswordTF {
            textField.keyboardType = .default
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.PasswordTF {
            textField.keyboardType = .numberPad
        }
    } */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    func initTerms(){
        let privacy = byRegistringTerms.text!
        let attributedString : NSMutableAttributedString = NSMutableAttributedString(string: privacy)
        
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: privacy.count - 1))
        byRegistringTerms.attributedText = attributedString
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.go_to_terms(_:)))
        byRegistringTerms.isUserInteractionEnabled = true
        byRegistringTerms.addGestureRecognizer(tapgesture)
    }
    @objc func go_to_terms(_ sender: UITapGestureRecognizer) {
        let vc = UIStoryboard(name: "TermsAndService", bundle: nil).instantiateViewController(withIdentifier: "TermsAService") as! TermsController
        self.present(vc, animated: true, completion: nil)
    }
    func doValidationAnimation(){
        self.scrollView.scrollToBottom(animated: true)
        self.scrollView.isScrollEnabled = false
        self.BlurEffect.isHidden = false
        self.CustomValidation.alpha = 0
        self.CustomValidation.isHidden = false
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomValidation.alpha = 1
        }) { (okay) in
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.curveLinear], animations: {
                self.smileyEmail.frame.origin.y =  self.smileyEmail.frame.origin.y - 24
                self.smileyEmail.layoutIfNeeded()
            }, completion: { (okayTwo) in
                self.validationOKBTN.isHidden = false
            })
        }
    }
    /*
     * Configure the style and action of the checkbox
     */
    func configureCheckBox(){
        checkBox.borderStyle = .square
        checkBox.checkmarkStyle = .tick
        checkBox.checkmarkSize = 0.7
        checkBox.uncheckedBorderColor = UIColor.gray
        checkBox.checkedBorderColor = UIColor(red: 23/255, green: 118/255, blue: 174/255, alpha: 1)
        checkBox.valueChanged = { (value) in
            print("square checkbox value change: \(value)")
            self.verifiedTerms = value
        }
       
        
    }
    /*
     * configure the login button style
     */
    func configureConnectButton(button:UIButton) {
        button.layer.cornerRadius = 9.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(self.RegisterWithDataBase), for: .touchUpInside)
    }
    let salutations = ["", Localization("Male"), Localization("Female"),Localization("Other")]
    let defaultSalutations = ["","Male","Female","Other"]
    var finalGender = ""
     let picker  = UIPickerView()
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return salutations.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return salutations[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("yellow")
    }
    func configureGenderOptions(){
        ChoiceOne.frame.size = CGSize(width: ChoiceOne.frame.size.width + 20, height: ChoiceOne.frame.size.height)
         ChoiceTwo.frame.size = CGSize(width: ChoiceTwo.frame.size.width + 20, height: ChoiceTwo.frame.size.height)
         ChoiceThree.frame.size = CGSize(width: ChoiceThree.frame.size.width + 20, height: ChoiceThree.frame.size.height)
        
    }
    func configureGenderTF(){
        GenderTF.layer.cornerRadius = PhoneSelectionView.frame.height / 2
        GenderTF.layer.borderWidth = 1.0
        GenderTF.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        GenderTF.layer.masksToBounds = true
        
        UserName.layer.cornerRadius = PhoneSelectionView.frame.height / 2
        UserName.layer.borderWidth = 1.0
        UserName.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        UserName.layer.masksToBounds = true
        
        let image = UIImage(named: "down_arrow_saif")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        GenderTF.rightView = imageView
        GenderTF.rightView?.frame.size = CGSize(width: image!.size.width , height: image!.size.height)
        GenderTF.rightView?.frame.origin.x = (GenderTF.rightView?.frame.origin.x)! - 3
        GenderTF.rightViewMode = .always
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let language = Bundle.main.preferredLocalizations.first! as NSString
        
        let doneButton = UIBarButtonItem(title: language == "fr" ? "Terminé" : "Done", style: .done, target: self, action: #selector(self.doneGenderPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cancelButton = UIBarButtonItem(title: language == "fr" ? "Annuler" : "Cancel", style: .plain, target: self, action: #selector(self.cancelGenderPicker))
       
        picker.delegate = self
        picker.dataSource = self
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        GenderTF.inputAccessoryView = toolbar
        GenderTF.inputView = picker
    }
    /*
     * Configure the Birtdhay Textfield style and init the picker
     */
    func configureBirthdayTF(){
        BirthdayTF.layer.cornerRadius = 7
        BirthdayTF.layer.masksToBounds = true
        let image = UIImage(named: "down_arrow_saif")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .center
        BirthdayTF.rightView = imageView
        BirthdayTF.rightView?.frame.size = CGSize(width: image!.size.width + 10 , height: image!.size.height + 5)
        BirthdayTF.rightViewMode = .always
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let language = Bundle.main.preferredLocalizations.first! as NSString
        datePicker.locale = language == "fr" ? NSLocale(localeIdentifier: "fr_FR") as Locale : NSLocale(localeIdentifier: "en_EN") as Locale
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date() - 18.years
        let doneButton = UIBarButtonItem(title: language == "fr" ? "Terminé" : "Done", style: .done, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cancelButton = UIBarButtonItem(title: language == "fr" ? "Annuler" : "Cancel", style: .plain, target: self, action: #selector(self.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        BirthdayTF.inputAccessoryView = toolbar
        BirthdayTF.inputView = datePicker
        
    }
    @objc func doneGenderPicker(){
        GenderTF.text = salutations[picker.selectedRow(inComponent: 0)]
        self.finalGender = defaultSalutations[picker.selectedRow(inComponent: 0)]
        self.view.endEditing(true)
    }
    /*
     * the done bar button of Birthday picker
     */
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat =  "yyyy-MM-dd"
        BirthdayTF.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    @objc func cancelGenderPicker(){
        self.view.endEditing(true)
    }
    /*
     * The cancel bar button of birthday picker
     */
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    /*
     * init the rounded logo view style
     */
    func configureImageView(){
        LogoView.layer.cornerRadius = LogoView.frame.width / 8
        LogoView.layer.borderWidth = 1
        LogoView.layer.borderColor = UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1).cgColor
        LogoView.clipsToBounds = true
    }
    /*
     * configure the view containing the Phone Code + Phone number view
     */
    func configurePhoneSelectioView(){
        PhoneSelectionView.layer.cornerRadius = 7
        
        PhoneSelectionView.layer.masksToBounds = true
        
    }
    func configurePasswordTF(){
        PasswordTF.layer.cornerRadius = 7
        
        PasswordTF.layer.masksToBounds = true
        let image = UIImage(named: "PasswordTextField")
        let imageView = customTextfieldImageView(image:image )
        imageView.contentMode = .center
        PasswordTF.leftView = imageView
        //PasswordTF.leftView?.frame.size = CGSize(width: image!.size.width + 10, height: image!.size.height)
        PasswordTF.leftViewMode = .always
        PasswordTF.textContentType = .oneTimeCode
    }
    /*
     * Configure the styles of the others textFields
     */
    func configureTextFields(textFields : [TextField]){
        for textField in textFields {
        textField.layer.cornerRadius = 7
        
        textField.layer.masksToBounds = true
        switch textField.tag {
        case 0:
            let image = UIImage(named: "user_saif")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            //textField.leftView?.frame.size = CGSize(width: image!.size.width + 10, height: image!.size.height )
            
            textField.leftViewMode = .always
        case 1:
            print("Email_saif")
            let image = UIImage(named: "EmailTextField")
            let imageView = customTextfieldImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            //textField.leftView?.frame.size = CGSize(width: image!.size.width + 10, height: image!.size.height)
              textField.leftViewMode = .always
        case 3:
            break
           /* let image = UIImage(named: "PasswordTextField")
            let imageView = UIImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            textField.leftView?.frame.size = CGSize(width: image!.size.width + 10, height: image!.size.height)
               textField.leftViewMode = .always */
        case 4:
          /*  let image = UIImage(named: "confirm_password_saif")
            let imageView = UIImageView(image:image )
            imageView.contentMode = .center
            textField.leftView = imageView
            textField.leftView?.frame.size = CGSize(width: image!.size.width + 10, height: image!.size.height)
               textField.leftViewMode = .always */
            break
        default:
            break
        }
        }
    }
    /*
     * configure the Phone Code Textfield
     */
    func configurePhoneTF(){
        let image = UIImage(named: "down_arrow_saif")
        let imageView = customTextfieldImageView(image: image)
        imageView.contentMode = .center
        PhoneTF.rightView = imageView
        //PhoneTF.rightView?.frame.size = CGSize(width: image!.size.width + 12, height: image!.size.height + 5)
        PhoneTF.rightViewMode = .always
        let countryLocale = NSLocale.current
        let countryCode = countryLocale.regionCode
        //let country = (countryLocale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
        //print(countryCode)
        for i in 0...(AppDelegate.countryArray.count - 1) {
            if AppDelegate.countryArray[i]["alpha2Code"].stringValue == countryCode {
                PhoneTF.text = "+" + AppDelegate.countryArray[i]["callingCodes"][0].stringValue
                countryChoosed = AppDelegate.countryArray[i]["name"].stringValue
                countryAlphaCode = AppDelegate.countryArray[i]["alpha2Code"].stringValue
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*
     * Protocol used to get the Country code choosed
     */
    func getCountryCode(data: String,country : String, alpha:String) {
        print("Country: ", data)
         PhoneTF.text = "+" + data
        countryChoosed = country
        countryAlphaCode = alpha
        
    }
    var countryChoosed = "Afghanistan"
    var countryAlphaCode = ""
    /*
     * preparing the destination viewController
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "show_country" {
            let vc = segue.destination as! CountryController
            
            vc.delegate = self
        }
        if segue.identifier == "go_to_passion" {
            let vc = segue.destination as! PassionController
            vc.isFirstTime = true
        }
    }
    /*
     * a Tapgesture installed on the Phone Code textfield
     */
    @IBAction func showCountrySectionAction(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "show_country", sender: self)
    }
    func calculateAge() -> Int{
        let now = Date()
        let birthDay = datePicker.date
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthDay,to: now)
        return  ageComponents.year!
    }
    @objc func RegisterWithDataBase(){
        if FullNameTF.text != "" && EmailTF.text != "" && PasswordTF.text != "" && ConfirmPasswordTF.text != "" && PhoneTF.text != "" && BirthdayTF.text != "" && UserName.text != ""  {
            if checkBox.isChecked == false{
                print ("accept check please")
                let banner = NotificationBanner(customView: self._notificationToast)
              _notificationToast.LabelN.text = Localization("TermsAccept")
                banner.customBannerHeight = 80
                banner.show(queuePosition: .front, bannerPosition: .top)
            }else{
                if PasswordTF.text != ConfirmPasswordTF.text {
                    print ("Passwords dosen't match")
                    let banner = NotificationBanner(customView: self._notificationToast)
                    _notificationToast.LabelN.text = Localization("PasswordsMatches")
                    banner.customBannerHeight = 80
                    banner.show(queuePosition: .front, bannerPosition: .top)
                  
                }else {//if ((FullNameTF.text?.components(separatedBy: " ").count)! < 2) {
                    //print ("Passwords dosen't match")
                    //let banner = NotificationBanner(customView: self._notificationToast)
                    //_notificationToast.LabelN.text = Localization("FullNameMatch")
                    //banner.customBannerHeight = 80
                    //banner.show(queuePosition: .front, bannerPosition: .top)
                
                //}else{
                
                    var lastName = FullNameTF.text!
                    lastName = lastName.replacingOccurrences(of: (FullNameTF.text?.components(separatedBy: " ").first)! + " ", with: "")
                    let settings : Parameters = [
                        //"firstName" : (FullNameTF.text?.components(separatedBy: " ").first)!.capitalized,
                        "firstName" : FullNameTF.text!.capitalized,
                        "lastName" : "",
                        "password" : PasswordTF.text!,
                        "userBirthday" : self.BirthdayTF.text!,
                        "userGender" : verifGender(),
                        "paysUser" : countryChoosed,
                        "alphaCode" : countryAlphaCode,
                        "email" : EmailTF.text!,
                        "userName" : UserName.text!,
                        "userPhoneNumber" : PhoneNumberLBL.text!
                    
                    ]
                    print(settings)
                    Alamofire.request(ScriptBase.sharedInstance.registerUser, method: .post, parameters: settings,encoding: JSONEncoding.default).responseJSON { response in
                        if response.error == nil {
                        let resp = JSON(response.data)
                            print(resp)
                            if resp["message"].exists() {
                                if resp["message"].stringValue == Localization("UserExist") {
                                    print("User already exist")
                                    let banner = NotificationBanner(customView: self._notificationToast)
                                    self._notificationToast.LabelN.text = Localization("UserExist") + "!"
                                    banner.customBannerHeight = 80
                                    banner.show(queuePosition: .front, bannerPosition: .top)
                                }else if resp["message"].stringValue == "Thanks for registering with us, Please Validate Your Email" {
                                   // let user = User(id: resp["_id"].stringValue, userFirstName: resp["userFirstName"].stringValue, userLastName: resp["userLastName"].stringValue, userAge: resp["userAge"].stringValue, userEmail: resp["userEmail"].stringValue, token: resp["token"].stringValue)
                                    self.doValidationAnimation()
                                
                                    
                                }else if resp["message"].stringValue == "There is some error sending the validation Email, please try again !" {
                                    ///dont forget
                                }
                            }else{
                                  /// next Step
                                
                                //UserDefaults.standard.setValue(resp.rawString(), forKey: "UserZonzay")
                                let alert = UIAlertController(title: Localization("Register"), message: Localization("EmailSent"), preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: Localization("OK"), style: .default, handler: { adc in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                               // self.present(alert, animated: true, completion: nil)
                               
                            }
                        }else{
                            print("there was an error")
                            let banner = NotificationBanner(customView: self._notificationToast)
                            self._notificationToast.LabelN.text = Localization("Error")
                            banner.customBannerHeight = 80
                            banner.show(queuePosition: .front, bannerPosition: .top)
                        }
                    }
                }
            }
        }else{
            let banner = NotificationBanner(customView: self._notificationToast)
            _notificationToast.LabelN.text = Localization("FillBlanks")
            banner.customBannerHeight = 80
            banner.show(queuePosition: .front, bannerPosition: .top)
        }
    }
    func ConnectTopush(){
        
        OneSignal.add(self as OSSubscriptionObserver)
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.setSubscription(false)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
            OneSignal.setSubscription(true)
            let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
            let userID = status.subscriptionStatus.userId
            print("userID = \(userID)")
            let pushToken = status.subscriptionStatus.pushToken
            print("pushToken = \(pushToken)")
        })
        
    }
func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            // get player ID
            print("userId:" ,  stateChanges.to.userId)
            if stateChanges.to.userId != nil {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    let params : Parameters = [
                        "userId" : a["_id"].stringValue ,
                        "playerId" : stateChanges.to.userId,
                        "type" : "ios"
                    ]
                    let header: HTTPHeaders = [
                        "Content-Type" : "application/json",
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                        
                    ]
                    Alamofire.request(ScriptBase.sharedInstance.setIosPlayerId , method: .post, parameters: params, encoding: JSONEncoding.default,headers : header)
                        .responseJSON { response in
                            //      LoaderAlert.shared.dismiss()
                            
                            let b = JSON(response.data!)
                            print(b)
                            UserDefaults.standard.setValue(b.rawString(), forKey: "UserZonzay")
                            UserDefaults.standard.synchronize()
                            
                    }
                    //  else {
                    //      _ = SweetAlert().showAlert("Conexion", subTitle: "Veuillez vous connecter", style: AlertStyle.error)
                    //}
                }catch{
                    
                }
            }
            
        }
    }
    
    @IBAction func ValidationOKAction(_ sender: UIButton) {
        self.validationOKBTN.isHidden = true
        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseInOut], animations: {
            self.CustomValidation.alpha = 0
            
        }) { (okay) in
            self.CustomValidation.isHidden = true
            self.BlurEffect.isHidden = true
            self.CustomValidation.alpha = 1
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func BackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension RegisterController {
    func verifGender() -> String{
        switch self.ChoiceOne.selected() {
        case ChoiceOne:
            return "Male"
        case ChoiceTwo:
            return "Female"
        case ChoiceThree:
            return "Other"
        default:
            return ""
        }
    }
    
}
extension RegisterController {
    @objc func keyboardWillShow(_ notification: Notification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue
        let keyboardSize = keyboardInfo.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height + 16.0, right: 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
}
