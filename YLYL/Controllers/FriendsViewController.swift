//
//  FriendsViewController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 07/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ChameleonFramework
class FriendsViewController:NavigationProfile,UITabBarControllerDelegate,
UITableViewDelegate,UITableViewDataSource,FriendRequestCustomCellDelegate,FriendMessageCustomCellDelegate,JokesFragmentDelegate{
  static let shared = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "FriendsViewController") as! FriendsViewController
  
    //Views   //Views
    @IBOutlet weak var friendsTableView: UITableView!
    @IBOutlet weak var ViewSelector : MXSegmentedControl!
    @IBOutlet weak var searchViewDefault : UIView!
    @IBOutlet weak var defaultMessage : UILabel!
    
    var FriendsL : [User] = []
    var selectedTab = 0
    var FriendsList : [FriendRequest] = []
    
    var FriendsListJson : JSON = []
    var ConversatioNRoom : [JSON] = []
    var FriendsListJsonTemp : JSON = []
    var chatRoomList : [ChatRoom] = []
    var userConnected : User!
    var FirstRequest = false
    var SecondRequest = false
    var searchController: AZSearchViewController!
    var resultFriends : JSON = []
    var IndexFriends : [IndexPath] = []
    var IndexInvitations: [IndexPath] = []
    var friendConnected : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("FriendsConnected: ",appDelegate.ConnectedUser)
        self.configureSearchBar()
        self.configureGradient()
        self.configureSegmentedControl()
        self.friendsTableView.tableFooterView = UIView(frame: CGRect.zero)
        //Create a circle image profile
      NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
        
        //self.getDataForFriendsRequest()
        NotificationCenter.default.addObserver(self, selector: #selector(self.didHideSearch(_:)), name: NSNotification.Name.init("DidHide"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.changesOnFriends(_:)), name: NSNotification.Name.init("changesOnFriends"), object: nil)
        
    }
    @objc func changesOnFriends(_ notif:Notification) {
        
        self.friendsTableView.reloadSections([0], with: .fade)
    }
    @objc func handleReload(notification: NSNotification) {
        print("Reload Conversation")
        if ViewSelector.selectedIndex == 1 || ViewSelector.selectedIndex == 0 {
       // self.friendsTableView.reloadSections([0], with: .none)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loadData()
            self.loadData()
        }
     
    }
    func configureGradient(){
       // let gradient = CAGradientLayer()
       // gradient.colors = [UIColor(red: 31/255, green: 165/255, blue: 194/255).cgColor,UIColor(red: 6/255, green: 59/255, blue: 70/255).cgColor]
        //gradient.frame = self.view.bounds
       // self.view.layer.addSublayer(gradient)
        self.view.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.view.bounds, andColors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1.0),UIColor(red: 6/255, green: 59/255, blue: 70/255, alpha: 1.0)])
       
    }
    @objc func didHideSearch(_ notif:Notification) {
        self.searchViewDefault.isHidden = false
    }
    func configureSegmentedControl(){
        ViewSelector.append(title: Localization("Friends"))
        ViewSelector.append(title: Localization("Chat"))
        ViewSelector.append(title: Localization("Invitations"))
        ViewSelector.addTarget(self, action: #selector(self.changeIndex(segmentedControl:)), for: .valueChanged)
    }
    @objc func changeIndex(segmentedControl: MXSegmentedControl) {
        print("\(segmentedControl.selectedIndex)")
        switch segmentedControl.selectedIndex {
        case 0:
            break
        case 1:
            
            self.ViewSelector.badgeViewTwo.isHidden = true
            self.ViewSelector.badgeValueTwo.text = "0"
            DispatchQueue.main.async {
                 (UIApplication.shared.delegate as! AppDelegate).NotifTwo = -1
            }
           
        case 2:
            self.ViewSelector.badgeViewThree.isHidden = true
            self.ViewSelector.badgeValueThree.text = "0"
            DispatchQueue.main.async {
                (UIApplication.shared.delegate as! AppDelegate).NotifThree = -1
            }
        default:
            break
        }
        if selectedTab != segmentedControl.selectedIndex {
            selectedTab = segmentedControl.selectedIndex
        if let segment = segmentedControl.segment(at: segmentedControl.selectedIndex) {
            segmentedControl.indicator.boxView.backgroundColor = segment.titleColor(for: .selected)
            segmentedControl.indicator.lineView.backgroundColor = segment.titleColor(for: .selected)
        }
        self.friendsTableView.beginUpdates()
        if segmentedControl.selectedIndex == 0 || segmentedControl.selectedIndex == 1 {
       
            self.friendsTableView.reloadSections([0], with: .fade)
        }else{
       self.friendsTableView.reloadSections([0], with: .fade)
        }
          self.friendsTableView.endUpdates()
         self.loadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SocketIOManager.sharedInstance.currentPresentedViewController = self
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.loadData()
         // self.friendConnected = SocketIOManager.sharedInstance.friendsConnected
           self.getZonz()
        //self.getDataForFriendsRequest(firstTime: true)
        self.loadData()
        detectNotification()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        countNotificationNotSeen()
    }
    func countNotificationNotSeen(){
        print("countNotificationNotSeen")
        let NotifTwo = ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo
        let NotifThree = ((UIApplication.shared.delegate) as! AppDelegate).NotifThree
        var total = 0
        if NotifTwo != -1 {
            total += NotifTwo
        }
        if NotifThree != -1 {
            total += NotifThree
        }
        print("countNotificationNotSeen : ", total)
        if total != 0 {
            NotificationCenter.default.post(name: NSNotification.Name.init("addNotSee"), object: total)
        }
    }
    func detectNotification(){
        print("Notification Detected")
        let NotifTwo = ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo
        let NotifThree = ((UIApplication.shared.delegate) as! AppDelegate).NotifThree
        print("Notification Added One : ",NotifTwo)
         print("Notification Added Two : ",NotifThree)
        if NotifTwo == -1 && NotifThree == -1 {
            return
        }else{
        print("Notification Added : ",NotifTwo)
            if NotifTwo > 0 {
                
                self.ViewSelector.badgeValueTwo.text = "\(NotifTwo)"
                self.ViewSelector.badgeViewTwo.isHidden = false
            }else{
                self.ViewSelector.badgeValueTwo.text = "0"
                
                self.ViewSelector.badgeViewTwo.isHidden = true
            }
            if NotifThree > 0 {
                self.ViewSelector.badgeValueThree.text = "\(NotifThree)"
                self.ViewSelector.badgeViewThree.isHidden = false
            }else{
                self.ViewSelector.badgeValueThree.text = "0"
                self.ViewSelector.badgeViewThree.isHidden = true
            }
        }
    }
    func configureSearchBar(){
        self.searchController = AZSearchViewController()
        self.searchController.definesPresentationContext = true
        self.searchController.delegate = self
        self.searchController.dataSource = self
        self.searchController.statusBarStyle = .default
        self.searchController.keyboardAppearnce = .default
        if #available(iOS 13.0, *) {
            self.searchController.searchBar.searchTextField.textColor = .black
        }
       
        self.searchController.searchBarPlaceHolder = Localization("SearchFriends")
        self.searchController.navigationBarClosure = { bar in
            bar.barTintColor = UIColor(hexString: "1FA5C2")
            bar.tintColor = UIColor.black
        }
        let item = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(self.close(sender:)))
        item.tintColor = .white
        self.searchController.navigationItem.rightBarButtonItem = item
    }
    @objc func close(sender: AnyObject?) {
        searchController.searchBar.text = ""
        //free the DataSource
        self.resultFriends = []
        searchController.reloadData()
        searchController.dismiss(animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Count: ",self.FriendsList.count)
        if ViewSelector.selectedIndex == 2 {
            if self.FriendsListJson["friendsRequest"].arrayObject != nil {
        if (self.FriendsListJson["friendsRequest"].arrayObject?.count)! != 0{
                for i in 0...((self.FriendsListJson["friendsRequest"].arrayObject?.count)! - 1){
                    self.IndexInvitations.append(IndexPath(row: i, section: 0))
                }
            self.defaultMessage.isHidden = true
            return (self.FriendsListJson["friendsRequest"].arrayObject?.count)!
        }else{
            self.IndexInvitations = []
            self.defaultMessage.text = Localization("No new Notifications")
            self.defaultMessage.isHidden = false
            return 0
            }
            
            }else{
                self.IndexInvitations = []
                self.defaultMessage.text = Localization("No new Notifications")
                self.defaultMessage.isHidden = false
                return 0
            }
            
        }else if ViewSelector.selectedIndex == 0{
            if self.FriendsListJson["users"].arrayObject != nil {
                if (self.FriendsListJson["users"].arrayObject?.count)! != 0 {
    for i in 0...((self.FriendsListJson["users"].arrayObject?.count)! - 1){
                        self.IndexFriends.append(IndexPath(row: i, section: 0))
                    }
                    self.defaultMessage.isHidden = true
                     return (self.FriendsListJson["users"].arrayObject?.count)!
                }else{
                    self.IndexFriends = []
                     self.defaultMessage.text = Localization("No friends")
                    self.defaultMessage.isHidden = false
                    return 0
                }
               
            }else{
                self.IndexFriends = []
                 self.defaultMessage.text = Localization("No friends")
                self.defaultMessage.isHidden = false
                return 0
            }
        }else {
            if self.FriendsListJson["users"].arrayObject != nil {
                           if (self.FriendsListJson["users"].arrayObject?.count)! != 0 {
               for i in 0...((self.FriendsListJson["users"].arrayObject?.count)! - 1){
                                   self.IndexFriends.append(IndexPath(row: i, section: 0))
                               }
                               self.defaultMessage.isHidden = true
                                return (self.FriendsListJson["users"].arrayObject?.count)!
                           }else{
                               self.IndexFriends = []
                                self.defaultMessage.text = Localization("No friends")
                               self.defaultMessage.isHidden = false
                               return 0
                           }
                          
                       }else{
                           self.IndexFriends = []
                            self.defaultMessage.text = Localization("No friends")
                           self.defaultMessage.isHidden = false
                           return 0
                       }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if ViewSelector.selectedIndex == 0 {
            
            let friendCell = tableView.dequeueReusableCell(withIdentifier: ViewSelector.selectedIndex == 0 ? "cellFriends" : "cellChat", for: indexPath) as! FriendCustomCell
            friendCell.isCellFriends = ViewSelector.selectedIndex == 0
            friendCell.configureView()
            
           // friendCell.friendNameLabel.text = self.FriendsListJson["users"][indexPath.row]["userFirstName"].stringValue + " " + self.FriendsListJson["users"][indexPath.row]["userLastName"].stringValue
            friendCell.friendNameLabel.text = self.FriendsListJson["users"][indexPath.row]["userName"].stringValue
            if self.FriendsListJson["users"][indexPath.row]["userImageURL"].exists() {
            friendCell.friendProfileImageView.setImage(with: URL(string: self.FriendsListJson["users"][indexPath.row]["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            }else{
               friendCell.friendProfileImageView.image = UIImage(named: "artist")
            }
            
             let passions = friendCell.Interestes
            if self.FriendsListJson["users"][indexPath.row]["listInterests"].stringValue != "" {
                var resString = ""
                let resIntersts = (self.FriendsListJson["users"][indexPath.row]["listInterests"].stringValue).components(separatedBy: ";")
                var i = 0
                for res in resIntersts {
                    if i < 4 {
                    resString  = resString + "#" + res
                    }
                    i+=1
                }
                passions!.text = resString
            }else{
                passions!.isHidden = true
            }
            friendCell.delegate = self
            friendCell.indexPath = indexPath
        
            return friendCell
        }else if ViewSelector.selectedIndex == 2 {
            let friendRequestCell = tableView.dequeueReusableCell(withIdentifier: "cellInvitation", for: indexPath) as! FriendRequestCustomCell
            friendRequestCell.configureView()
           // friendRequestCell.friendNameLabel.text  =  self.FriendsListJson["friendsRequest"][indexPath.row]["sender"]["userFirstName"].stringValue + " " + self.FriendsListJson["friendsRequest"][indexPath.row]["sender"]["userLastName"].stringValue
            friendRequestCell.friendNameLabel.text = self.FriendsListJson["friendsRequest"][indexPath.row]["sender"]["userName"].stringValue
            if self.FriendsListJson["friendsRequest"][indexPath.row]["sender"]["userImageURL"].exists(){
             friendRequestCell.friendProfileImageView.setImage(with: URL(string: self.FriendsListJson["friendsRequest"][indexPath.row]["sender"]["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            }else{
                friendRequestCell.friendProfileImageView.image = UIImage(named: "artist")
            }
            friendRequestCell.delegate = self
            return friendRequestCell
        }else{
            let friendCell = tableView.dequeueReusableCell(withIdentifier: ViewSelector.selectedIndex == 0 ? "cellFriends" : "cellChat", for: indexPath) as! FriendCustomCell
            friendCell.isCellFriends = ViewSelector.selectedIndex == 0
            friendCell.configureView()
            var who = ""
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            let a =  JSON(data: dataFromString!)
            if self.ConversatioNRoom[indexPath.row]["convFirstUserId"]["_id"].stringValue == a["_id"].stringValue {
                who = "convSecondUserId"
            }else{
                 who = "convFirstUserId"
            }
            //friendCell.friendNameLabel.text = self.ConversatioNRoom[indexPath.row][who]["userFirstName"].stringValue + " " + self.ConversatioNRoom[indexPath.row][who]["userLastName"].stringValue
            friendCell.idUser = self.FriendsListJson["users"][indexPath.row]["_id"].stringValue
            friendCell.friendNameLabel.text = self.FriendsListJson["users"][indexPath.row]["userName"].stringValue
            if self.FriendsListJson["users"][indexPath.row]["userImageURL"].exists() {
                friendCell.friendProfileImageView.setImage(with: URL(string: self.FriendsListJson["users"][indexPath.row]["userImageURL"].stringValue), placeholder: UIImage(named: "artist"), transformer: nil, progress: nil, completion: nil)
            }else{
                friendCell.friendProfileImageView.image = UIImage(named: "artist")
            }
            if ViewSelector.selectedIndex == 1 {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                self.friendConnected = appDelegate.ConnectedUser
                let converstationNotif = appDelegate.convRemoteNumbers
                if  let convNotif = converstationNotif[self.FriendsListJson["users"][indexPath.row]["_id"].stringValue]  {
                  
                    friendCell.NotifNBLBL.text = "\(convNotif)"
                    
                }else{
                     friendCell.NotifNBLBL.text = "0"
                    friendCell.NotifNBLBL.isHidden = true
                }
     
                friendCell.friendConnectView.image =  self.friendConnected.contains(self.FriendsListJson["users"][indexPath.row]["_id"].stringValue) ?  UIImage(named: "ConnectedIcon") : UIImage(named: "Disconnectedicon")
                
                if friendCell.friendConnectView.image == UIImage(named:"Disconnectedicon") {
                   friendCell.challengeBTN.isEnabled = false
                   friendCell.offlineLBL.text = Localization("offline")
                   friendCell.offlineLBL.isHidden = false
                   friendCell.friendProfileImageView.alpha = 0.89
                }else{
                   friendCell.challengeBTN.isEnabled = true
                   friendCell.offlineLBL.isHidden = true
                   
                }
                if let numberNotif = Int(friendCell.NotifNBLBL.text!) {
                    if numberNotif > 0 {
                        friendCell.NotifView.isHidden = false
                    }else{
                        friendCell.NotifView.isHidden = true
                    }
                }else{
                    friendCell.NotifView.isHidden = true
                }
                
                
                
            }
            let passions = friendCell.Interestes
            
            if self.FriendsListJson["users"][indexPath.row]["listInterests"].stringValue != "" {
                var resString = ""
                let resIntersts = (self.FriendsListJson["users"][indexPath.row]["listInterests"].stringValue).components(separatedBy: ";")
                for res in resIntersts {
                    resString  = resString + "#" + res
                }
                passions!.text = resString
            }else{
                passions!.isHidden = true
            }
            friendCell.delegate = self
            friendCell.indexPath = indexPath
            
            return friendCell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if ViewSelector.selectedIndex == 0 {
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
            let navigation = UINavigationController(rootViewController: vc )
            vc.userTarget = self.FriendsListJson["users"][indexPath.row]
            vc.ViewType = "Friend"
            appDelegate.navigation.present(navigation, animated: true, completion: nil)
        }
        if indexPath.section == 5
        {
            
           
        }else{
            
        }
    }
    
    var videoLimit = ""
    var alert : UIAlertController!
    var IndexUser : IndexPath!
    func cellDidTapPlay(_ sender: FriendCustomCell) {
        let subscription = UserDefaults.standard.value(forKey: "Subscription") as! String
        if subscription == "false" && (Int(videoLimit) ?? 10 >= 10) {
            let alertR = UIAlertController(title: Localization("ZonzayChallenge"), message: Localization("ZonzayVideoMax"), preferredStyle: .alert)
            self.navigationController?.dismiss(animated: true, completion: nil)
            let ok = UIAlertAction(title: Localization("Subscribe"), style: .default, handler: { alert in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ZonzayPlusPayment") as! ZonzayPlusPayment
                vc.navigation = self.navigationController!
                vc.tabbar = self.tabBarController!
                self.navigationController?.pushViewController(vc, animated: true)
            })
            let cancel = UIAlertAction(title: Localization("Cancel"), style: .cancel, handler: nil)
            alertR.addAction(ok)
            alertR.addAction(cancel)
            self.present(alertR, animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
            vc.delegate = self
            vc.isPlayingOrProfile = true
            self.IndexUser = sender.indexPath
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func getJokesFromFragment(jokes: [String]) {
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "firstUserId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            var FirstAttempt = 0
            Alamofire.request(ScriptBase.sharedInstance.createRoom, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let janus = VideoRoomController()
                    
                    let data = JSON(response.data ?? Data())
                    janus.attachRoomToJanus(roomId: data["roomNumber"].numberValue, completionHandler: { json in
                        
                        let room = Room(id: data["roomId"].stringValue, roomNumber: data["roomNumber"].stringValue, firstUserId: data["roomFirstUserId"].stringValue, secondUserId: "")
                        SocketIOManager.sharedInstance.videoRoomRequestAccepted(completionHandler: { json in
        SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
    SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            print("Game accepted")
                            print(json)
                            if FirstAttempt == 0 {
                                FirstAttempt = 1
                self.alert.dismiss(animated: true, completion: nil)
                                var i = 0
                                for view in (self.navigationController?.viewControllers)! {
                                    if view .isKind(of: VideoGameController.self) || view .isKind(of: VideoGameResultController.self) {
                                        self.navigationController?.viewControllers.remove(at: i)
                                    }
                                    i = i + 1
                                }
                                //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                                //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                                let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                                vc.willAppear = true
                                vc.Owner = "master"
                                vc.FriendOrMatch = false
                                vc.roomId = NSNumber(value: Int(json[0])!)
                                vc.SecondUserId = self.FriendsListJson["users"][self.IndexUser.row]["_id"].stringValue
                                vc.navigation = self.navigationController
                                //self.navigationController?.present(vc, animated: true, completion: nil)
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                
                                //self.navigationController?.dismiss(animated: true, completion: nil)
                            }
                        })
                        SocketIOManager.sharedInstance.videoRoomRequestRefused(completionHandler: { json in
                            print("Game Refused")
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            let alertR = UIAlertController(title: Localization("ZonzayChallenge"), message: Localization("ZonzayFriendCancel"), preferredStyle: .alert)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                            let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                              
                            })
                            alertR.addAction(ok)
                            self.present(alertR, animated: true, completion: nil)
                            
                        })
                        SocketIOManager.sharedInstance.joinVideoRoomRequest(userId: a["_id"].stringValue, userName: a["userName"].stringValue, SecondUserId: self.FriendsListJson["users"][self.IndexUser.row]["_id"].stringValue, VideoRoom: room.getRoomNumber(), gameType: GameType.Friend)
                        self.alert = UIAlertController(title: nil, message: Localization("PleaseWait"), preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: Localization("Cancel"), style: .cancel , handler: { (alert) in
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            //self.navigationController?.dismiss(animated: true, completion: nil)
                        })
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        self.alert.addAction(cancelAction)
                        self.alert.view.addSubview(loadingIndicator)
                        self.navigationController!.present( self.alert, animated: true, completion: nil)
                        print("REQU1 : ",data)
                        //self.jokes = data
                    })
                    
                    
                }else{
                }
                
            }
            
        }catch {
            
        }
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("getZonz : ",data)
                self.videoLimit =  data["videoLimit"].stringValue
            }
            
        }catch {
            
        }
    }
    func cellDidTapMessage(_ sender: FriendCustomCell) {
       let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var converstationNu = appDelegate.convRemoteNumbers
      
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatFriend") as! ChatFriendController
        
        vc.user = self.userConnected
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a = JSON(data: dataFromString!)
        for i in 0...(self.ConversatioNRoom.count - 1) {
            if (self.ConversatioNRoom[i]["convFirstUserId"]["_id"].stringValue == a["_id"].stringValue &&  self.ConversatioNRoom[i]["convSecondUserId"]["_id"].stringValue == sender.idUser)  {
                vc.RoomQ = ChatRoom(id: self.ConversatioNRoom[i]["_id"].stringValue, receiverId: "", userId: "")
                vc.contact = User(id: self.ConversatioNRoom[i]["convSecondUserId"]["_id"].stringValue, userFirstName: self.ConversatioNRoom[i]["convSecondUserId"]["userFirstName"].stringValue, userLastName: self.ConversatioNRoom[i]["convSecondUserId"]["userLastName"].stringValue, imageProfileUrl: self.ConversatioNRoom[i]["convSecondUserId"]["userImageURL"].stringValue, username: self.ConversatioNRoom[i]["convSecondUserId"]["userName"].stringValue, listPassion: self.ConversatioNRoom[i]["convSecondUserId"]["listInterests"].stringValue)
                 vc.Conversation = self.ConversatioNRoom[i]
                converstationNu[self.ConversatioNRoom[i]["_id"].stringValue] = 0
                appDelegate.convRemoteNumbers = converstationNu
            }else if (self.ConversatioNRoom[i]["convSecondUserId"]["_id"].stringValue == a["_id"].stringValue &&  self.ConversatioNRoom[i]["convFirstUserId"]["_id"].stringValue == sender.idUser) {
                vc.RoomQ = ChatRoom(id: self.ConversatioNRoom[i]["_id"].stringValue, receiverId: "", userId: "")
                vc.contact = User(id: self.ConversatioNRoom[i]["convFirstUserId"]["_id"].stringValue, userFirstName: self.ConversatioNRoom[i]["convFirstUserId"]["userFirstName"].stringValue, userLastName: self.ConversatioNRoom[i]["convFirstUserId"]["userLastName"].stringValue, imageProfileUrl: self.ConversatioNRoom[i]["convFirstUserId"]["userImageURL"].stringValue, username: self.ConversatioNRoom[i]["convFirstUserId"]["userName"].stringValue, listPassion: self.ConversatioNRoom[i]["convFirstUserId"]["listInterests"].stringValue)
                 vc.Conversation = self.ConversatioNRoom[i]
                converstationNu[self.ConversatioNRoom[i]["_id"].stringValue] = 0
                appDelegate.convRemoteNumbers = converstationNu
            }
        }
       
        //vc.RoomQ = ChatRoom(id: self.ConversatioNRoom[idConv]["_id"].stringValue, receiverId: "", userId: "")
        //vc.contact = User(id: self.ConversatioNRoom[indexPath.row][who]["_id"].stringValue, userFirstName: self.ConversatioNRoom[indexPath.row][who]["userFirstName"].stringValue, userLastName: self.ConversatioNRoom[indexPath.row][who]["userLastName"].stringValue, imageProfileUrl: self.ConversatioNRoom[indexPath.row][who]["userImageURL"].stringValue, username: self.ConversatioNRoom[indexPath.row][who]["userName"].stringValue, listPassion: self.ConversatioNRoom[indexPath.row][who]["listInterests"].stringValue)
        
         //vc.Conversation = self.ConversatioNRoom[indexPath.row]
        //vc.Room = roomToPass
       
        self.navigationController?.pushViewController(vc, animated: true)
        
      /*  SocketIOManager.sharedInstance.getAllChatRooms { rooms in
            SocketIOManager.sharedInstance.StopgetAllChatRooms()
            if rooms != JSON.null {
                let roomsNew = rooms[0]
                if roomsNew.arrayObject?.count != 0 {
                    for i in 0 ... ((roomsNew.arrayObject?.count)! - 1) {
                        if (roomsNew[i]["userId"].stringValue == self.FriendsListJson["users"][indexPath.row]["_id"].stringValue || roomsNew[i]["receiverId"].stringValue == self.FriendsListJson["users"][indexPath.row]["_id"].stringValue) {
                            haveChatRoom = true
                            roomToPassAvailable = ChatRoom(id: roomsNew[i]["_id"].stringValue, receiverId: roomsNew[i]["receiverId"].stringValue, userId: roomsNew[i]["userId"].stringValue, soketId: roomsNew[i]["receiverSoketId"].stringValue)
                            break
                        }
                    }
                }
            }
            if haveChatRoom {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatFriend") as! ChatFriendController
                vc.user = self.userConnected
                vc.contact = User(id: self.FriendsListJson["users"][indexPath.row]["_id"].stringValue, userFirstName: self.FriendsListJson["users"][indexPath.row]["userFirstName"].stringValue, userLastName: self.FriendsListJson["users"][indexPath.row]["userLastName"].stringValue, userAge: self.FriendsListJson["users"][indexPath.row]["userAge"].stringValue, userEmail: self.FriendsListJson["users"][indexPath.row]["userEmail"].stringValue, token: self.FriendsListJson["users"][indexPath.row]["userToken"].stringValue)
                vc.Room = roomToPassAvailable
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                SocketIOManager.sharedInstance.getCreatedChatRoom { (roomChat) in
                    SocketIOManager.sharedInstance.StopCreatedChatRoom()
                    DispatchQueue.main.async(execute: {
                        let tempRoom = roomChat[0]
                        let roomToPass : ChatRoom = ChatRoom(id: tempRoom["_id"].stringValue, receiverId: tempRoom["receiverId"].stringValue, userId: tempRoom["userId"].stringValue, receiverFirstName: tempRoom["receiverFirstName"].stringValue, receiverLastName: tempRoom["receiverLastName"].stringValue, receiverImageURL: tempRoom["receiverImageURL"].stringValue, soketId: tempRoom["SoketId"].stringValue)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "chatFriend") as! ChatFriendController
                        vc.user = self.userConnected
                        vc.contact = User(id: self.FriendsListJson["users"][indexPath.row]["_id"].stringValue, userFirstName: self.FriendsListJson["users"][indexPath.row]["userFirstName"].stringValue, userLastName: self.FriendsListJson["users"][indexPath.row]["userLastName"].stringValue, userAge: self.FriendsListJson["users"][indexPath.row]["userAge"].stringValue, userEmail: self.FriendsListJson["users"][indexPath.row]["userEmail"].stringValue, token: self.FriendsListJson["users"][indexPath.row]["userToken"].stringValue)
                        vc.Room = roomToPass
                        self.navigationController?.pushViewController(vc, animated: true)
                    })
                }
                SocketIOManager.sharedInstance.addRoom(userId: self.userConnected.getId(), FriendId: self.FriendsListJson["users"][indexPath.row]["_id"].stringValue)
            }
        }
        // self.performSegue(withIdentifier: "chatFriend", sender: self)
        SocketIOManager.sharedInstance.startNotifyChatRooms(userId: self.userConnected.getId()) */
        
    }
   
    func cellDidTapAccept(_ sender: FriendRequestCustomCell) {
        guard let tappedIndexPath = friendsTableView.indexPath(for: sender) else { return }
        print("accecpt", tappedIndexPath.row)
        
        print("accecpt")
        sender.isUserInteractionEnabled = false
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
        let a = try JSON(data: dataFromString!)
        let settings : Parameters = [
            "friendRequestId" : self.FriendsListJson["friendsRequest"][tappedIndexPath.row]["friendRequest"]["_id"].stringValue
        ]
            let settingsTwo : Parameters = [
                "userId" : a["_id"].stringValue,
                "userOneId" : a["_id"].stringValue,
                "userTwoId" : self.FriendsListJson["friendsRequest"][tappedIndexPath.row]["friendRequest"]["senderId"]["_id"].stringValue
            ]
        let headers : HTTPHeaders =  [
        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
            Alamofire.request(ScriptBase.sharedInstance.acceptFriendRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print(data)
                Alamofire.request(ScriptBase.sharedInstance.addConversation, method: .post, parameters: settingsTwo, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                    let dataConv = JSON(response.data ?? Data())
                    if dataConv != JSON.null {
                        
                    let app = UIApplication.shared.delegate as! AppDelegate
                        app.loadData()
                       
                    }
                    sender.isUserInteractionEnabled = true
                
                self.loadData()
                    
                }
                
            }
        }catch{
        }
    }
    func cellDidTapDecline(_ sender: FriendRequestCustomCell) {
        sender.isUserInteractionEnabled = false
        guard let tappedIndexPath = friendsTableView.indexPath(for: sender) else { return }
        print("decline", tappedIndexPath.row)
        print("decline")
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "senderId" : a["_id"].stringValue,
                "receiverId" : self.userConnected.getId()
            
                
            ]
            let headers : HTTPHeaders =  [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.declineFriendRequest, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                sender.isUserInteractionEnabled = true

                let data = JSON(response.data ?? Data())
                print(data)
                
                //self.getDataForFriendsRequest(firstTime: true)
                self.loadData()
            }
            
        }catch{
            
        }
        
    }
    func getDataForFriendsRequest(firstTime:Bool){
        self.FriendsList = []
        self.FriendsL = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            self.userConnected = User(id: a["_id"].stringValue, userFirstName: a["userFirstName"].stringValue, userLastName:a["userLastName"].stringValue, userAge: a["userAge"].stringValue, userEmail: a["userEmail"].stringValue, token: a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue)
        let settings : Parameters = [
            "userId" : a["_id"].stringValue
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.friendList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                var data = JSON(response.data ?? Data())
                
                print("REQU1 : ",data)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let userConnected = appDelegate.ConnectedUser
                if firstTime {
                    print("Before Ordered : ",data["users"])

                    var temp = data["users"].arrayValue
                    temp.sort(by: { (j1, j2) -> Bool in
                        return  ((userConnected.contains(j1["_id"].stringValue) && (j1["_id"].stringValue != a["_id"].stringValue)) || (userConnected.contains(j2["_id"].stringValue) && (j2["_id"].stringValue != a["_id"].stringValue)))
                    })
                    data["users"] = JSON(temp)
                    print("Ordered : ",data["users"])
                self.FriendsListJson = data
                
                self.friendsTableView.reloadData()
                }else{
                  self.FriendsListJsonTemp = data
                  //  if self.FriendsListJson != self.FriendsListJsonTemp {
                        var temp = self.FriendsListJsonTemp["users"].arrayValue
                        temp.sort(by: { (j1, j2) -> Bool in
                            //return  !((userConnected.contains(j1["convSecondUserId"]["_id"].stringValue)))
                            return ((userConnected.contains(j1["convSecondUserId"]["_id"].stringValue) && (j1["convSecondUserId"]["_id"].stringValue != a["_id"].stringValue)) || (userConnected.contains(j2["convSecondUserId"]["_id"].stringValue) && (j2["convSecondUserId"]["_id"].stringValue != a["_id"].stringValue)))
                        })
                        self.FriendsListJsonTemp["users"] = JSON(temp)
                        self.FriendsListJson = self.FriendsListJsonTemp
                          self.friendsTableView.reloadData()
                    //}
                }
            }
            
        }catch {
            
        }
       
    }
    
    @IBAction func searchFriendsAction(_ sender: Any) {
        searchController.show(in: self)
        self.searchViewDefault.isHidden = true
    }
    var userTarget : JSON = []
    
    func loadData(){
        print("SectionCritique: ")
        
            if (UserDefaults.standard.value(forKey: "UserZonzay") != nil ) {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let settings : Parameters = [
                    "userId" : a["_id"].stringValue
                ]
                let header: HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                print(settings)
                Alamofire.request(ScriptBase.sharedInstance.getAllConversations , method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                        
                        // print("sent : ",response.request?.httpBody.)
                        let b = JSON(response.data)
                       
                   
                        if b != JSON.null {
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let userConnected = appDelegate.ConnectedUser
                            var results = b.arrayValue
                           /* results.sort(by: { (j1, j2) -> Bool in
                            //return  (userConnected.contains(j1["convSecondUserId"]["_id"].stringValue) || (userConnected.contains(j1["convFirstUserId"]["_id"].stringValue)))
                                 return ((userConnected.contains(j1["convSecondUserId"]["_id"].stringValue) && (j1["convSecondUserId"]["_id"].stringValue != a["_id"].stringValue)) || (userConnected.contains(j2["convSecondUserId"]["_id"].stringValue) && (j2["convSecondUserId"]["_id"].stringValue != a["_id"].stringValue)))
                            }) */
                           
                            
                            self.ConversatioNRoom = results
                        self.getDataForFriendsRequest(firstTime: true)
                            print("Conversation:",b.arrayObject?.count)
                       
                        }
                }
            }
        }
}
extension FriendsViewController : AZSearchViewDelegate {
    func searchView(_ searchView: AZSearchViewController, didSearchForText text: String) {
        searchView.dismiss(animated: false, completion: nil)
        self.searchViewDefault.isHidden = false
    }
    func searchView(_ searchView: AZSearchViewController, didDismissWithText text: String) {
        self.searchViewDefault.isHidden = false
    }
    func searchView(_ searchView: AZSearchViewController, didTextChangeTo text: String, textLength: Int) {
        /// Here we call our Socket for realTime searching and reload the data
        if text == ""{
            self.resultFriends = []
            searchView.reloadData()
        }else {
        self.resultFriends = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "word" : text
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.searchUser, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.response != nil {
                let data = JSON(response.data ?? Data())
                print(data)
                if data.arrayObject?.count != 0 {
                    self.resultFriends = data
                
                    for i in 0...((self.resultFriends.arrayObject?.count)! - 1){
                        if self.resultFriends[i]["_id"].stringValue == self.userConnected.getId() {
                            print("Friend TRUE")
                            self.resultFriends.arrayObject?.remove(at: i)
                            break
                        }
                    }
                    searchView.reloadData()
                }else{
                    self.resultFriends = []
                    searchView.reloadData()
                }
               
            }
            }
        }catch {
            
        }
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_to_profile" {
            let vc = segue.destination
            print("Profile: ",vc)
            guard let vcDest = vc.children[0] as? ProfileTestTable else{
                return
            }
            vcDest.userTarget = self.userTarget
            if let tab = self.userTarget["listFriends"].arrayObject as? [String] {
                if tab.contains(self.userConnected.getId()){
                    vcDest.ViewType = "Friend"
                }else{
                    vcDest.ViewType = "Visitor"
                }
            }else{
            vcDest.ViewType = "Visitor"
            }
        }
    }
    
    func searchView(_ searchView: AZSearchViewController, didSelectResultAt index: Int, text: JSON) {
        ///Here we handle the results click
     
        userTarget = self.resultFriends[index]
        //performSegue(withIdentifier: "go_to_profile2", sender: self)
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileTestTable") as! ProfileTestTable
        let navigation = UINavigationController(rootViewController: vc )
        vc.userTarget = userTarget
        if let tab = self.userTarget["listFriends"].arrayObject as? [String] {
            if tab.contains(self.userConnected.getId()){
                vc.ViewType = "Friend"
            }else{
                vc.ViewType = "Visitor"
            }
        }else{
            vc.ViewType = "Visitor"
        }
        appDelegate.navigation.present(navigation, animated: true, completion: nil)
    }
    
}
extension FriendsViewController : AZSearchViewDataSource {
    func results() -> JSON {
       return self.resultFriends
    }
    func searchView(_ searchView: AZSearchViewController, tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: searchView.cellIdentifier)
        //cell?.textLabel?.text = self.resultFriends[indexPath.row]["userFirstName"].stringValue + " " +  self.resultFriends[indexPath.row]["userLastName"].stringValue
        cell?.textLabel?.text = self.resultFriends[indexPath.row]["userName"].stringValue
        cell?.textLabel?.textColor = .black
        if self.resultFriends[indexPath.row]["userImageURL"].exists() {
            cell?.imageView?.layer.cornerRadius = (cell?.imageView?.frame.width)! / 2
            cell?.imageView?.layer.masksToBounds = true
          cell?.imageView?.setImage(with: URL(string: self.resultFriends[indexPath.row]["userImageURL"].stringValue), placeholder: UIImage(named: "profileVideo_hamid"), transformer: nil, progress: nil, completion: nil)
            
        }else{
            cell?.imageView?.image = UIImage(named: "profileVideo_hamid")
        }
        
        cell?.contentView.backgroundColor = .white
        return cell!
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}
