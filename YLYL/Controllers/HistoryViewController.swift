//
//  HostoryViewController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 07/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class HistoryViewController: NavigationProfile, TGLParallaxCarouselDelegate,UITableViewDelegate,UITableViewDataSource{
    //Views
    @IBOutlet weak var profileImageView: ExtensionProfile!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var zonzayMoneyLabel: UILabel!
    @IBOutlet weak var defaultMessage : UILabel!
    var history: JSON = []
    var friends : JSON = []
    var userId = ""
    @IBOutlet weak var carouselView: TGLParallaxCarousel!
    @IBOutlet weak var tableView : UITableView!
    
    @IBAction func shareAppAction(_ sender: UITapGestureRecognizer) {
        let shareLink = ["http://zonzay.com/download"]
        let activityVC = UIActivityViewController(activityItems: shareLink, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultMessage.text = Localization("No history")
        //Create a circle image profile
        profileImageView.image = UIImage(named: "profile_hamid")
        profileImageView.layer.borderWidth = 1.0
        profileImageView.layer.borderColor = UIColor.white.cgColor
        
        tableView.delegate = self
        
        tableView.dataSource = self
        //setupCarousel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        do {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            let a = try JSON(data: dataFromString!)
            self.profileImageView.setImage(with: URL(string: a["userImageURL"].stringValue), placeholder: UIImage(named: "profile_hamid"), transformer: nil, progress: nil, completion: nil)
            self.userId = a["_id"].stringValue
            self.profileNameLabel.text = a["userName"].stringValue
        }catch{
            
        }
        SocketIOManager.sharedInstance.currentPresentedViewController = self
        self.getZonz()
        self.getHistory()
    }
    fileprivate func setupCarousel(){
        carouselView.delegate = self
        carouselView.margin = 10
        carouselView.selectedIndex = 0
        carouselView.type = .threeDimensional
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.history.arrayObject?.count)! != 0 ? ((self.history.arrayObject?.count)! + 1) : 0
    }
    func numberOfItemsInCarouselView(_ carouselView: TGLParallaxCarousel) -> Int {
        return  (self.history.arrayObject?.count)!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 43
        }else{
            return 70
        }
    }
    func determineScoreText(scoreOne: String,scoreTwo : String) -> String{
        let scoreOneInt = Int(scoreOne)!
        let scoreTwoInt = Int(scoreTwo)!
        print("ScoreONe : ",scoreOneInt)
         print("ScoreTwo : ",scoreTwoInt)
        if scoreOneInt > scoreTwoInt {
            return Localization("YOUW")
        }else if scoreOneInt < scoreTwoInt {
            return Localization("YOUL")
        }else{
            return Localization("YOUD")
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let imageCell = cell.viewWithTag(1) as! UIImageView
        let userCell = cell.viewWithTag(2) as! UILabel
        let flagCell = cell.viewWithTag(3) as! UIImageView
        let dateCell = cell.viewWithTag(4) as! UILabel
            let ScoresUserScoreCell =  cell.viewWithTag(5) as! UILabel
             let WinnerUserScoreCell =  cell.viewWithTag(6) as! UILabel
          WinnerUserScoreCell.adjustsFontSizeToFitWidth = true
        let interestCell = cell.viewWithTag(13) as! UILabel
        dateCell.text = (self.history[indexPath.row - 1]["videoDate"].stringValue).components(separatedBy: "T")[0]
        let isfirstUser = self.history[indexPath.row - 1]["videoFirstUserId"]["_id"].stringValue == self.userId ? false : true
         if isfirstUser {
            ScoresUserScoreCell.text = self.history[indexPath.row - 1]["scoreSecondUser"].stringValue + " / " + self.history[indexPath.row - 1]["scoreFirstUser"].stringValue
            WinnerUserScoreCell.text = determineScoreText(scoreOne: self.history[indexPath.row - 1]["scoreSecondUser"].stringValue, scoreTwo: self.history[indexPath.row - 1]["scoreFirstUser"].stringValue)
            if self.history[indexPath.row - 1]["videoFirstUserId"]["userImageURL"].exists() {
            imageCell.setImage(with: URL(string: self.history[indexPath.row - 1]["videoFirstUserId"]["userImageURL"].stringValue), placeholder: UIImage(named: "playfri_saif"), transformer: nil, progress: nil, completion: nil)
            }else{
             imageCell.image = UIImage(named: "playfri_saif")
            }
            userCell.text = self.history[indexPath.row - 1]["videoFirstUserId"]["userName"].stringValue
            if self.history[indexPath.row - 1]["videoFirstUserId"]["alphaCode"].exists() {
                
                let alpha = self.history[indexPath.row - 1]["videoFirstUserId"]["alphaCode"].stringValue
                    flagCell.image = UIImage(named: alpha)
                
            }else{
                flagCell.isHidden = true
            }
            if self.history[indexPath.row - 1]["videoFirstUserId"]["listInterests"].stringValue != "" {
                var resString = ""
                let resIntersts = (self.history[indexPath.row - 1]["videoFirstUserId"]["listInterests"].stringValue).components(separatedBy: ";")
                for res in resIntersts {
                    resString  =  resString + "#" + res
                }
               interestCell.text = resString
            }else{
                interestCell.isHidden = true
            }
         }else{
            ScoresUserScoreCell.text = self.history[indexPath.row - 1]["scoreFirstUser"].stringValue + " / " + self.history[indexPath.row - 1]["scoreSecondUser"].stringValue
            WinnerUserScoreCell.text = determineScoreText(scoreOne: self.history[indexPath.row - 1]["scoreFirstUser"].stringValue, scoreTwo: self.history[indexPath.row - 1]["scoreSecondUser"].stringValue)
             if self.history[indexPath.row - 1]["videoSecondUserId"]["userImageURL"].exists() {
           imageCell.setImage(with: URL(string: self.history[indexPath.row - 1]["videoSecondUserId"]["userImageURL"].stringValue), placeholder: UIImage(named: "playfri_saif"), transformer: nil, progress: nil, completion: nil)
             }else{
                imageCell.image = UIImage(named: "playfri_saif")
            }
            userCell.text = self.history[indexPath.row - 1]["videoSecondUserId"]["userName"].stringValue
            if self.history[indexPath.row - 1]["videoSecondUserId"]["alphaCode"].exists() {
                let alpha = self.history[indexPath.row - 1]["videoSecondUserId"]["alphaCode"].stringValue
                    flagCell.image = UIImage(named: alpha)
               
            }else{
                flagCell.isHidden = true
            }
            print("Interests")
            if self.history[indexPath.row - 1]["videoSecondUserId"]["listInterests"].stringValue != "" {
                print("Interests not empty")

                var resString = ""
                let resIntersts = (self.history[indexPath.row - 1]["videoSecondUserId"]["listInterests"].stringValue).components(separatedBy: ";")
                for res in resIntersts {	
                    resString  =  resString + "#" + res
                }
                 interestCell.text = resString
            }else{
                 interestCell.isHidden = true
            }
        }
              return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDefault", for: indexPath)
            cell.separatorInset = UIEdgeInsets(top: 0, left: cell.bounds.size.width, bottom: 0, right: 0)
           return cell
        }
      
    }
    func carouselView(_ carouselView: TGLParallaxCarousel, itemForRowAtIndex index: Int) -> TGLParallaxCarouselItem {
        let view = CustomView(frame: CGRect(x: 0, y: 0, width: 300, height: 300), number: index)
        let isfirstUser = self.history[index]["videoFirstUserId"]["_id"].stringValue == self.userId ? false : true
        view.index = index
        if isfirstUser {
            view.userFullName.text = self.history[index]["videoFirstUserId"]["userName"].stringValue
            view.userImage.setImage(with: URL(string: self.history[index]["videoFirstUserId"]["userImageURL"].stringValue), placeholder: UIImage(named: "playfri_saif"), transformer: nil, progress: nil, completion: nil)
            var isFriend = false
            if self.friends.arrayObject?.count != 0 {
                for i in 0...((self.friends.arrayObject?.count)! - 1) {
                    if self.friends[i]["_id"].stringValue == self.history[index]["videoFirstUserId"]["_id"].stringValue {
                        isFriend = true
                        break
                    }
                }
            }
            if isFriend == false {
            view.friendButton.text = "Add as Friend"
            }else{
                view.friendButton.text = "Friends"
            }
            
        }else{
            view.userFullName.text = self.history[index]["videoSecondUserId"]["userName"].stringValue
            view.userImage.setImage(with: URL(string: self.history[index]["videoSecondUserId"]["userImageURL"].stringValue), placeholder: UIImage(named: "playfri_saif"), transformer: nil, progress: nil, completion: nil)
            var isFriend = false
            if self.friends.arrayObject?.count != 0 {
                for i in 0...((self.friends.arrayObject?.count)! - 1) {
                    if self.friends[i]["_id"].stringValue == self.history[index]["videoSecondUserId"]["_id"].stringValue {
                        isFriend = true
                        break
                    }
                }
                
            }
            if isFriend == false {
                view.friendButton.text = "Add as Friend"
            }else{
                view.friendButton.text = "Friends"
            }
        }
        return view
    }
    func carouselView(_ carouselView: TGLParallaxCarousel, didSelectItemAtIndex index: Int) {
        if carouselView.items[index].viewWithTag(1) != nil {
            carouselView.removeExpandableView(index: index)
        }else{
            carouselView.addExpandableView(index: index)
        }
        
    }
    func carouselView(_ carouselView: TGLParallaxCarousel, willDisplayItem item: TGLParallaxCarouselItem, forIndex index: Int) {
        
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                if zonz != "" {
                    self.zonzayMoneyLabel.text = zonz
                    
                }else{
                    self.zonzayMoneyLabel.text = "0"
                }
               
            }
            
        }catch {
            
        }
    }
    
    func getHistory(){
        self.tableView.isHidden = true
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.historyList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.history = []
                let data = JSON(response.data ?? Data())
                
                print("History : ",data)
                if data != JSON.null {
                    if !data["message"].exists() {
                    self.history = data
                    if self.history.arrayObject?.count != 0 {
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                  // self.getFriends()
                    }else{
                        self.tableView.isHidden = true
                        self.defaultMessage.isHidden = false
                        }
                    }else{
                        self.tableView.isHidden = true
                        self.defaultMessage.isHidden = false
                    }
                }else{
                    self.tableView.isHidden = true
                    self.defaultMessage.isHidden = false
                }
            }
            
        }catch {
            
        }
    }
    func getFriends(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.friendList, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                self.friends = []
                let data = JSON(response.data ?? Data())
                
                print("History : ",data)
                if data != JSON.null {
                    self.friends = data["users"]
                    //self.carouselView.reloadData()
                    //self.tableView.reloadData()
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    
}
