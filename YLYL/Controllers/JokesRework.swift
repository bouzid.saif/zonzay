//
//  JokesRework.swift
//  YLYL
//
//  Created by macbook on 4/2/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
import Alamofire
import SwiftyJSON
struct Joke {
    var name : String?
    var jokeTxt : String?
}
class JokesRework: ServerUpdateDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate {
   
    @IBOutlet weak var zonzMoneyCount:UILabel!
    @IBOutlet weak var bottomContainer :UIView!
    @IBOutlet weak var tableV :UITableView!
    var jokesRandom : [Joke] = []
    var myJokes : [Joke] = []
    var selectedJokes : [Joke] = []
    var isAnimation = false
    var isRandomOrMy = true
    var isPlayingOrProfile = true
    var delegate: JokesFragmentDelegate?
    var zonzMoney = ""
    @IBOutlet weak var myJokesImage: UIImageView!
    
    @IBOutlet weak var rollDiceImage: UIImageView!
    
    @IBOutlet weak var containerBlur: UIVisualEffectView!
    
    @IBOutlet weak var ThreeJokesGameLBL: UILabel!
    @IBOutlet weak var validateJokesImage: UIImageView!
    @IBOutlet weak var containerTutorial: UIView!
    
    @IBOutlet weak var tutorialLBL: UILabel!
    
    @IBOutlet weak var tutorialLBLTwo: UILabel!
    
    @IBOutlet weak var tutorialLBLThree: UILabel!
    @IBOutlet weak var trianglePrimary: UIImageView!
    
    @IBOutlet weak var triangleRight: UIImageView!
    
    @IBOutlet weak var triangleLeft: UIImageView!
    @IBOutlet weak var defaultMessage: UILabel!
    
    @IBOutlet weak var plusButtonJokes: UIImageView!
    
    @IBOutlet weak var containerAddJokes: UIView!
    
    @IBOutlet weak var jokeTitleTF: UITextField!
    
    @IBOutlet weak var jokeTitleMaxLBL: UILabel!
    @IBOutlet weak var jokeCoreMaxLBL: UILabel!

    @IBOutlet weak var jokeCoreTV: PlaceholderTextView!
    
    var pushedOrModal = true
    @IBOutlet weak var backBTN: UIButton!
    @IBOutlet weak var backBTNBig: UIButton!
    
    @IBAction func backAction(_ sender: UIButton) {
        if pushedOrModal {
            if isPlayingOrProfile {
                self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: false)
            }
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureBottomContainer()
        getJokesFromDataBase()
        self.zonzMoneyCount.text = self.zonzMoney
         self.containerTutorial.isHidden = true
        self.trianglePrimary.isHidden = true
        let view = UIView(frame: CGRect.zero)
        view.size = CGSize(width: self.tableV.frame.width, height: 40)
        view.backgroundColor = .clear
        self.tableV.tableHeaderView = view
        print("3AAAA")
        self.hideAddJokes(hide: true)
        self.jokeTitleTF.delegate = self
        self.jokeCoreTV.delegate = self
        self.configureView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.listenFromResult(_:)), name: Notification.Name.init("DissmissJoke"), object: nil)
      
    }
    @objc func listenFromResult(_ notification :Notification){
        
        self.navigationController?.popViewController(animated: false)
    }
    func configureView(){
        if isPlayingOrProfile == false{
            
            self.rollDiceImage.image = UIImage(named: "PlusButtonJokes")
            self.validateJokesImage.image = UIImage(named: "DeleteJokes")
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.jokeTitleMaxLBL.text = String(textField.text!.count) + "/40"
        return range.location < 40
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return range.location < 164
    }
    func textViewDidChange(_ textView: UITextView) {
         self.jokeCoreMaxLBL.text = String(textView.text!.count) + "/164"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isPlayingOrProfile {
         animateContainerThreeJokes()
        }else{
            self.isRandomOrMy = false
        }
    }
    func hideAddJokes(hide: Bool) {
        if hide {
            
            self.jokeTitleTF.text = ""
            self.jokeCoreTV.text = ""
            
        }
        self.rollDiceImage.isUserInteractionEnabled = hide
        self.validateJokesImage.isUserInteractionEnabled = hide
        self.myJokesImage.isUserInteractionEnabled = hide
        self.tableV.isHidden = !hide
        self.containerAddJokes.isHidden = hide
    }
    func blockUI(block:Bool){
        self.rollDiceImage.isUserInteractionEnabled = !block
        self.myJokesImage.isUserInteractionEnabled = !block
        self.validateJokesImage.isUserInteractionEnabled = !block
    }
    func animateContainerThreeJokes(){
        self.blockUI(block: true)
        if UserDefaults.standard.value(forKey: "ZonzayFirstTime") == nil {
        self.containerBlur.alpha = 0
        self.containerBlur.isHidden = false
        self.rollDiceImage.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 0.3, delay: 0.8, options: [.curveEaseIn], animations: {
            self.rollDiceImage.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
        }) { _ in
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [.curveEaseOut], animations: {
                self.rollDiceImage.transform = CGAffineTransform.identity
            }, completion: { _ in
                UIView.animate(withDuration: 0.6, delay: 0.8, options: [.curveEaseInOut], animations: {
                    self.containerBlur.alpha = 1
                }) { _ in
                    UIView.animate(withDuration: 0.6, delay: 1.7, options: [.curveEaseInOut], animations: {
                        self.containerBlur.alpha = 0
                    }, completion: { _ in
                        self.containerBlur.isHidden = true
                        self.containerBlur.alpha = 1
                        if UserDefaults.standard.value(forKey: "ZonzayFirstTime") != nil {
                            self.validateJokesImage.image = UIImage(named: "ValidateJokeDisabled")
                        }else{
                             self.animateJokesTutorial()
                            
                        }
                       
                    })
                }
            })
        }
        }else{
            self.rollDiceImage.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, delay: 0.8, options: [.curveEaseIn], animations: {
                     self.rollDiceImage.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
                 }) { _ in
                     UIView.animate(withDuration: 0.3, delay: 0.3, options: [.curveEaseOut], animations: {
                         self.rollDiceImage.transform = CGAffineTransform.identity
                     }, completion: { _ in
                        self.validateJokesImage.image = UIImage(named: "ValidateJokeDisabled")
                        self.blockUI(block: false)
                     })
            }
          
        }
    }
    func animateJokesTutorial(){
        self.containerTutorial.alpha = 0
        self.containerTutorial.isHidden = false
        self.trianglePrimary.alpha = 0
        self.trianglePrimary.isHidden = false
        self.triangleLeft.isHidden = true
        //
        self.rollDiceImage.isUserInteractionEnabled = false
        self.validateJokesImage.isUserInteractionEnabled = false
        self.myJokesImage.isUserInteractionEnabled = false
        //
        UIView.animate(withDuration: 0.6, delay: 0.3, options: [.curveEaseInOut], animations: {
            self.containerTutorial.alpha = 1
            self.trianglePrimary.alpha = 1
        }) { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                self.tutorialLBL.isHidden = true
                
                self.tutorialLBLTwo.alpha = 0
                self.tutorialLBLTwo.isHidden = false
                UIView.animate(withDuration: 0.6, delay: 0, options: [.curveLinear], animations: {
                    print("translate TO X")
                self.trianglePrimary.origin.x = self.triangleLeft.origin.x
                self.tutorialLBLTwo.alpha = 1
                }, completion : { _ in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                        self.tutorialLBLTwo.isHidden = true
                        self.tutorialLBLTwo.alpha = 1
                        self.tutorialLBLThree.alpha = 0
                        self.tutorialLBLThree.isHidden = false
                        UIView.animate(withDuration: 0.6, delay: 0, options: [.curveLinear], animations: {
                            print("translate TO X")
                            self.trianglePrimary.origin.x = self.triangleRight.origin.x
                            self.tutorialLBLThree.alpha = 1
                        }, completion : { _ in
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                                UIView.animate(withDuration: 0.6, delay: 0, options: [.curveEaseInOut], animations: {
                                    self.containerTutorial.alpha = 0
                                    self.trianglePrimary.alpha = 0
                                }, completion: { _ in
                                    self.containerTutorial.isHidden = true
                                    self.containerTutorial.alpha = 1
                                    self.trianglePrimary.isHidden = true
                                    self.trianglePrimary.alpha = 1
                                    UserDefaults.standard.set("true", forKey: "ZonzayFirstTime")
                                    //
                                    self.rollDiceImage.isUserInteractionEnabled = true
                                    self.validateJokesImage.isUserInteractionEnabled = true
                                    self.myJokesImage.isUserInteractionEnabled = true
                                    //
                                    if let currentLanguage = Locale.currentLanguage {
                                        print("LNG :",currentLanguage.rawValue)
                                        if currentLanguage == .fr {
                                            self.createRandomFRJokes()
                                        }else if currentLanguage == .en {
                                            self.createRandomENJokes()
                                        }else if currentLanguage == .pt {
                                            self.createRandomPORTJokes()
                                        }else if currentLanguage == .es {
                                            self.createRandomESPJokes()
                                        }else if currentLanguage == .it
                                        {
                                            self.createRandomITAJokes()
                                        }else{
                                         self.createRandomENJokes()
                                        }
                                        self.blockUI(block: false)
                                        // Your code here.
                                    }
                                })
                                
                            })
                        })
                    })
                })
            })
        }
    }
    func createRandomFRJokes(){
        if self.jokesRandom.count < 3 {
            let randomInt = Int.random(in: 0..<45)
            let joke = Joke(name: AppDelegate.frenchJokes[randomInt]["name"].stringValue, jokeTxt: AppDelegate.frenchJokes[randomInt]["joke"].stringValue)
            if self.jokesRandom.contains(where: { jj -> Bool in
                return ((jj.name == joke.name) &&  (jj.jokeTxt == joke.jokeTxt))
            }) {
                createRandomFRJokes()
            }else{
                self.jokesRandom.append(joke)
                createRandomFRJokes()
            }
        }else{
            self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            self.selectedJokes = []
            self.selectedJokes = self.jokesRandom
            if isAnimation == false {
            self.isAnimation = true
            self.tableV.reloadData()
            }else{
                if self.tableV.indexPathsForSelectedRows != nil {
                    for index in  self.tableV.indexPathsForSelectedRows! {
                        self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
                    }
                }
                self.tableV.beginUpdates()
                self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 0, section: 1),IndexPath(row: 0, section: 2)], with: .automatic)
                self.tableV.endUpdates()
                self.isAnimation = false
               
            }
        }
    }
    func createRandomENJokes(){
        if self.jokesRandom.count < 3 {
            let randomInt = Int.random(in: 0..<16)
            let joke = Joke(name: AppDelegate.englishJokes[randomInt]["name"].stringValue, jokeTxt: AppDelegate.englishJokes[randomInt]["joke"].stringValue)
            if self.jokesRandom.contains(where: { jj -> Bool in
                return ((jj.name == joke.name) &&  (jj.jokeTxt == joke.jokeTxt))
            }) {
                createRandomENJokes()
            }else{
                self.jokesRandom.append(joke)
                createRandomENJokes()
            }
        }else{
             self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            self.selectedJokes = []
            self.selectedJokes = self.jokesRandom
            if isAnimation == false {
                 self.isAnimation = true
           self.tableV.reloadData()
            }else{
                if self.tableV.indexPathsForSelectedRows != nil {
                for index in  self.tableV.indexPathsForSelectedRows! {
                    self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
                }
                }
                if self.tableV.indexPathsForVisibleRows?.count == 3 {
                    self.tableV.beginUpdates()
                    self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 0, section: 1),IndexPath(row: 0, section: 2)], with: .automatic)
                    self.tableV.endUpdates()
                }else{
                    self.tableV.reloadData()
                }
                
                self.isAnimation = false
            }
        }
    }
    func createRandomESPJokes(){
        if self.jokesRandom.count < 3 {
            let randomInt = Int.random(in: 0..<9)
            let joke = Joke(name: AppDelegate.espagnolJokes[randomInt]["name"].stringValue, jokeTxt: AppDelegate.espagnolJokes[randomInt]["joke"].stringValue)
            if self.jokesRandom.contains(where: { jj -> Bool in
                return ((jj.name == joke.name) &&  (jj.jokeTxt == joke.jokeTxt))
            }) {
                createRandomESPJokes()
            }else{
                self.jokesRandom.append(joke)
                createRandomESPJokes()
            }
        }else{
            self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            self.selectedJokes = []
            self.selectedJokes = self.jokesRandom
            if isAnimation == false {
                self.isAnimation = true
                self.tableV.reloadData()
            }else{
                if self.tableV.indexPathsForSelectedRows != nil {
                    for index in  self.tableV.indexPathsForSelectedRows! {
                        self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
                    }
                }
                self.tableV.beginUpdates()
                self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 0, section: 1),IndexPath(row: 0, section: 2)], with: .automatic)
                self.tableV.endUpdates()
                self.isAnimation = false
            }
        }
    }
    func createRandomPORTJokes(){
        if self.jokesRandom.count < 3 {
            let randomInt = Int.random(in: 0..<6)
            let joke = Joke(name: AppDelegate.portugalJokes[randomInt]["name"].stringValue, jokeTxt: AppDelegate.portugalJokes[randomInt]["joke"].stringValue)
            if self.jokesRandom.contains(where: { jj -> Bool in
                return ((jj.name == joke.name) &&  (jj.jokeTxt == joke.jokeTxt))
            }) {
                createRandomPORTJokes()
            }else{
                self.jokesRandom.append(joke)
                createRandomPORTJokes()
            }
        }else{
            self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            self.selectedJokes = []
            self.selectedJokes = self.jokesRandom
            if isAnimation == false {
                self.isAnimation = true
                self.tableV.reloadData()
            }else{
                if self.tableV.indexPathsForSelectedRows != nil {
                    for index in  self.tableV.indexPathsForSelectedRows! {
                        self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
                    }
                }
                self.tableV.beginUpdates()
                self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 0, section: 1),IndexPath(row: 0, section: 2)], with: .automatic)
                self.tableV.endUpdates()
                self.isAnimation = false
            }
        }
    }
    func createRandomITAJokes(){
        if self.jokesRandom.count < 3 {
            let randomInt = Int.random(in: 0..<10)
            let joke = Joke(name: AppDelegate.italianJokes[randomInt]["name"].stringValue, jokeTxt: AppDelegate.italianJokes[randomInt]["joke"].stringValue)
            if self.jokesRandom.contains(where: { jj -> Bool in
                return ((jj.name == joke.name) &&  (jj.jokeTxt == joke.jokeTxt))
            }) {
                createRandomITAJokes()
            }else{
                self.jokesRandom.append(joke)
                createRandomITAJokes()
            }
        }else{
            self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            self.selectedJokes = []
            self.selectedJokes = self.jokesRandom
            if isAnimation == false {
                self.isAnimation = true
                self.tableV.reloadData()
            }else{
                if self.tableV.indexPathsForSelectedRows != nil {
                    for index in  self.tableV.indexPathsForSelectedRows! {
                        self.tableV.delegate?.tableView!(self.tableV, didSelectRowAt: index)
                    }
                }
                self.tableV.beginUpdates()
                self.tableV.reloadRows(at: [IndexPath(row: 0, section: 0),IndexPath(row: 0, section: 1),IndexPath(row: 0, section: 2)], with: .automatic)
                self.tableV.endUpdates()
                self.isAnimation = false
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.jokesRandom.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func configureBottomContainer(){
          bottomContainer.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.bottomContainer.frame, colors: [UIColor(red: 45/255, green: 157/255, blue: 181/255, alpha: 1), UIColor(red: 23/255, green: 79/255, blue: 91/255, alpha: 1)], limit: NSNumber(value: 0.8))
        let maskPathAll = UIBezierPath(roundedRect: bottomContainer.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 22, height: 10))
        let shapeLayerAll = CAShapeLayer()
        shapeLayerAll.frame = bottomContainer.bounds
        shapeLayerAll.path = maskPathAll.cgPath
        bottomContainer.layer.mask = shapeLayerAll
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
        }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerGen = UIView()
        let header = UIView(frame: CGRect.zero)
        header.size = CGSize(width: tableView.frame.width, height: 25)
        let label = UILabel()
        let headerBefore = UIView(frame: CGRect.zero)
        headerBefore.size = CGSize(width: tableView.frame.width, height: 10)
        headerBefore.backgroundColor = .clear

        let headerAfter = UIView(frame: CGRect.zero)
        headerAfter.size = CGSize(width: tableView.frame.width, height: 5)
        headerAfter.backgroundColor = .clear
        label.textColor = .black
        label.text = Localization("Name") + ": " + self.jokesRandom[section].name!
        label.font = UIFont(name: "Roboto-Regular", size: 14)
        header.addSubview(label)
        label.snp.makeConstraints { (const) in
            const.left.equalTo(header).offset(10)
            const.right.equalTo(header).offset(0)
            const.top.equalTo(header).offset(1)
            const.bottom.equalTo(header).offset(2)
        }
        headerGen.size = CGSize(width: tableView.frame.width, height: 40)
        header.backgroundColor = UIColor.white
        
        headerGen.addSubview(headerBefore)
        headerGen.addSubview(header)
        headerGen.addSubview(headerAfter)
        headerBefore.snp.makeConstraints { (const) in
            const.top.equalTo(headerGen).offset(0)
            const.left.equalTo(headerGen).offset(0)
            const.right.equalTo(headerGen).offset(0)
            const.height.equalTo(10)
        }
        headerAfter.snp.makeConstraints { (const) in
            const.bottom.equalTo(headerGen).offset(0)
            const.left.equalTo(headerGen).offset(0)
            const.right.equalTo(headerGen).offset(0)
            const.height.equalTo(10)
        }
        header.roundCorners(corners: [.topLeft, .topRight], radius: 6.5)
        header.snp.makeConstraints { (const) in
            const.bottom.equalTo(headerAfter).offset(0)
            const.left.equalTo(headerGen).offset(0)
            const.right.equalTo(headerGen).offset(0)
            const.height.equalTo(25)
        }
        return headerGen
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let jokeText = (cell.viewWithTag(1) as! UILabel)
        jokeText.text = self.jokesRandom[indexPath.section].jokeTxt
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.isRandomOrMy == false {
            if self.selectedJokes.count < 3 {
                
        guard let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        if cell.backgroundColor == UIColor.white {
            cell.backgroundColor = UIColor(red: 31 / 255, green: 165 / 255, blue: 194 / 255, alpha: 1)
            (cell.viewWithTag(1) as! UILabel).textColor = .white
            self.selectedJokes.append(self.jokesRandom[indexPath.section])
            if self.selectedJokes.count == 3 {
                self.validateJokesImage.image = UIImage(named: "ValidateJoke")
            }
        }else{
            cell.backgroundColor = .white
            (cell.viewWithTag(1) as! UILabel).textColor = .black
            self.selectedJokes.removeAll { (joke) -> Bool in
                if (joke.name == self.jokesRandom[indexPath.section].name) && (joke.jokeTxt == self.jokesRandom[indexPath.section].jokeTxt ) {
                    return true
                }else{
                    return false
                }
            }
        }
                print(selectedJokes)
            }else{
                guard let cell = tableView.cellForRow(at: indexPath) else {
                    return
                }
                 if cell.backgroundColor != UIColor.white {
                    cell.backgroundColor = .white
                    (cell.viewWithTag(1) as! UILabel).textColor = .black
                    self.selectedJokes.removeAll { (joke) -> Bool in
                        if (joke.name == self.jokesRandom[indexPath.section].name) && (joke.jokeTxt == self.jokesRandom[indexPath.section].jokeTxt ) {
                            return true
                        }else{
                            return false
                        }
                    }
                 }else{
                    self.tableV.deselectRow(at: indexPath, animated: false)
                }
            }
        }
            
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        print("deselection")
        if isPlayingOrProfile {
        if cell.backgroundColor != UIColor.white {
            cell.backgroundColor = .white
            (cell.viewWithTag(1) as! UILabel).textColor = .black
            self.selectedJokes.removeAll { (joke) -> Bool in
                if (joke.name == self.jokesRandom[indexPath.section].name) && (joke.jokeTxt == self.jokesRandom[indexPath.section].jokeTxt ) {
                    return true
                }else{
                    return false
                }
            }
              self.validateJokesImage.image = UIImage(named: "ValidateJokeDisabled")
        
            // self.selectedJokes.append(self.jokesRandom[indexPath.section])
        }
        }else{
            if cell.backgroundColor != UIColor.white {
                cell.backgroundColor = .white
                (cell.viewWithTag(1) as! UILabel).textColor = .black
                self.selectedJokes.removeAll { (joke) -> Bool in
                    if (joke.name == self.jokesRandom[indexPath.section].name) && (joke.jokeTxt == self.jokesRandom[indexPath.section].jokeTxt ) {
                        return true
                    }else{
                        return false
                    }
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let maskPathAll = UIBezierPath(roundedRect: cell.bounds, byRoundingCorners: [ .bottomRight, .bottomLeft], cornerRadii: CGSize(width: 6.5, height: 5.0))
        let shapeLayerAll = CAShapeLayer()
        shapeLayerAll.frame = cell.bounds
        shapeLayerAll.path = maskPathAll.cgPath
        cell.layer.mask = shapeLayerAll
    }
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
    
    @IBAction func RollDiceAction(_ sender: UITapGestureRecognizer) {
        if isPlayingOrProfile {
        self.isRandomOrMy = true
        self.defaultMessage.isHidden = true
            //self.plusButtonJokes.isHidden = true
        self.jokesRandom = []
        if let currentLanguage = Locale.currentLanguage {
            print("LNG :",currentLanguage.rawValue)
            if currentLanguage == .fr {
               
                createRandomFRJokes()
            }else if currentLanguage == .en {
             
                createRandomENJokes()
            }else if currentLanguage == .es {
                createRandomESPJokes()
            } else if currentLanguage == .it {
                createRandomITAJokes()
            } else if currentLanguage == .pt {
                createRandomPORTJokes()
            } else {
                createRandomENJokes()
            }
            // Your code here.
        }
        }else{
             isModifying = false
            self.hideAddJokes(hide: false)
        }
    }
    
    @IBAction func MyJokesACtion(_ sender: UITapGestureRecognizer) {
        if isPlayingOrProfile {
        if isRandomOrMy {
            isRandomOrMy = false
            //self.plusButtonJokes.isHidden = false
        validateJokesImage.image = UIImage(named: "ValidateJokeDisabled")
            self.jokesRandom = []
            self.jokesRandom = self.myJokes
            print(self.myJokes.count)
            print(self.myJokes)
            self.tableV.reloadData()
            if self.jokesRandom.count == 0 {
                self.defaultMessage.isHidden = false
            }
            if self.jokesRandom.count != 0 {
            animateSelectJokes()
            }
        }else{
            
        }
        }else{
            guard let indexs = self.tableV.indexPathsForSelectedRows else {
                let alert = UIAlertController(title: "Jokes", message: "Please select a joke", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("OK"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            if indexs.count == 1 {
                isModifying = true
                self.hideAddJokes(hide: false)
                self.jokeTitleTF.text = self.jokesRandom[indexs[0].section].name
                self.jokeCoreTV.text = self.jokesRandom[indexs[0].section].jokeTxt
                self.tableV.delegate?.tableView!(self.tableV, didDeselectRowAt: indexs[0])
            }else{
                let alert = UIAlertController(title: "Jokes", message: "Please select only one joke", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("OK"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    func animateSelectJokes(){
        self.containerBlur.alpha = 0
        self.containerBlur.isHidden = false
                self.ThreeJokesGameLBL.text = "Please select 3 jokes!"
                UIView.animate(withDuration: 0.4, delay: 0.8, options: [.curveEaseInOut], animations: {
                    self.containerBlur.alpha = 1
                }) { _ in
                    UIView.animate(withDuration: 0.4, delay: 1.7, options: [.curveEaseInOut], animations: {
                        self.containerBlur.alpha = 0
                    }, completion: { _ in
                        self.containerBlur.isHidden = true
                        self.containerBlur.alpha = 1
                        
                    })
                }
        
        
    }
    @IBAction func ValidateJokesACtion(_ sender: UITapGestureRecognizer) {
        if isPlayingOrProfile {
        if validateJokesImage.image != UIImage(named: "ValidateJokeDisabled") {
            if pushedOrModal {
            self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
            var selectedJokeToSend : [String] = []
            self.selectedJokes.forEach { (joke) in
                selectedJokeToSend.append(joke.jokeTxt!)
            }
            delegate?.getJokesFromFragment(jokes: selectedJokeToSend)
        }
        }else{
            guard let indexs = self.tableV.indexPathsForSelectedRows else {
                let alert = UIAlertController(title: "Jokes", message: "Please select one or more joke to delete", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: Localization("OK"), style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            print(indexs)
           for index in indexs.reversed() {
                self.tableV.delegate?.tableView!(self.tableV, didDeselectRowAt: index)
            }
            for index in indexs.reversed() {
                self.jokesRandom.removeAll { (joke) -> Bool in
                    return (joke.name == self.myJokes[index.section].name ) && (joke.jokeTxt == self.myJokes[index.section].jokeTxt)
                }
            }
               self.saveJokesIntoDataBase()
            
        }
    }
    
    @IBAction func AddJokesAction(_ sender: UITapGestureRecognizer) {
        
    }
    var isModifying = false
    @IBAction func saveJokeAction(_ sender: Any) {
        
        if self.jokeCoreTV.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")) != "" && self.jokeTitleTF.text?.trimmingCharacters(in: CharacterSet.init(charactersIn: " ")) != ""  {
            if isModifying == false {
            self.saveJokesIntoDataBase(jokeTitle: self.jokeTitleTF.text!, jokeCore: self.jokeCoreTV.text!)
            }else{
               guard let indexs = self.tableV.indexPathsForSelectedRows else {
                return
                }
                self.saveJokesIntoDataBase(jokeTitle: self.jokeTitleTF.text!, jokeCore: self.jokeCoreTV.text!, index: indexs[0].section)
            }
        }
    }
    
    @IBAction func cencelJokeAction(_ sender: Any) {
        self.hideAddJokes(hide: true)
    }
    func constructJson(json:[Joke],jokeTitle:String,jokeCore:String) -> JSON{
        if jokeTitle != "" && jokeCore != "" {
        var result = "["
        for j in json {
            result = result + "{\"jokeId\":\"" + j.name!
            result = result + "\",\"joke\":\"" + j.jokeTxt! + "\",\"userId\":\"\"},"
        }
        result = result + "{\"jokeId\":\"" + jokeTitle + "\",\"joke\":\"" + jokeCore + "\",\"userId\":\"\"}]"
        return JSON(stringLiteral: result)
        }else{
            var result = "["
            for j in json {
                result = result + "{\"jokeId\":\"" + j.name!
                result = result + "\",\"joke\":\"" + j.jokeTxt! + "\",\"userId\":\"\"},"
            }
            if result.count != 1 {
            result.removeLast()
            }
            result = result + "]"
            return JSON(stringLiteral: result)
        }
    }
    func constructJson(json:[Joke],jokeTitle:String,jokeCore:String,index:Int) -> JSON{
        if jokeTitle != "" && jokeCore != "" {
            var result = "["
            for j in json {
                result = result + "{\"jokeId\":\"" + j.name!
                result = result + "\",\"joke\":\"" + j.jokeTxt! + "\",\"userId\":\"\"},"
            }
            result = result + "{\"jokeId\":\"" + jokeTitle + "\",\"joke\":\"" + jokeCore + "\",\"userId\":\"\"}]"
            return JSON(stringLiteral: result)
        }else{
            var result = "["
            var i = 0
            for j in json {
                if i != index {
                result = result + "{\"jokeId\":\"" + j.name!
                result = result + "\",\"joke\":\"" + j.jokeTxt! + "\",\"userId\":\"\"},"
                }else{
                    result = result + "{\"jokeId\":\"" + jokeTitle
                    result = result + "\",\"joke\":\"" + jokeCore + "\",\"userId\":\"\"},"
                }
                i = i + 1
            }
            if result.count != 1 {
                result.removeLast()
            }
            result = result + "]"
            return JSON(stringLiteral: result)
        }
    }
    func getJokesFromDataBase(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
       // self.tableV.isHidden = false
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseString { response in
                if response.data != nil {
                    guard var stringToParse = response.result.value else {
                        return
                    }
                    if stringToParse != "" {
                        print("stringToParse : ",stringToParse)
                        stringToParse.removeFirst()
                        stringToParse.removeLast()
                        stringToParse = stringToParse.replacingOccurrences(of: "\\", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        let data = JSON(stringLiteral: response.result.value!)
                        print("JokesResponse : ",data)
                        if data != JSON.null {
                            
                            let parsedString = stringToParse.data(using: String.Encoding.utf8, allowLossyConversion: false)
                            do{
                                
                                var JokesJSON = try JSONDecoder().decode([Jokes].self, from: parsedString!)
                              
                                if JokesJSON.count != 0 {
                                    for i in 0...((JokesJSON.count) - 1) {
                                        self.myJokes.append(Joke(name: JokesJSON[i].jokeId == "" ? "Unknown" : JokesJSON[i].jokeId, jokeTxt: JokesJSON[i].joke))
                                    }
                                }
                                if self.isPlayingOrProfile == false {
                                    self.jokesRandom = self.myJokes
                                    
                                    self.tableV.reloadData()
                                }
                            }catch{
                                print("error converting",error.localizedDescription)
                            }
                        }
                        
                    }
                    //self.jokes = data
                    
                }else{
                }
                
            }
            
        }catch {
            
        }
    }
    func saveJokesIntoDataBase(jokeTitle:String,jokeCore:String){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let res = self.constructJson(json: self.myJokes, jokeTitle: jokeTitle, jokeCore: jokeCore)
            self.myJokes.append(Joke(name: jokeTitle, jokeTxt: jokeCore))
            self.jokesRandom.append(Joke(name: jokeTitle, jokeTxt: jokeCore))
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    self.hideAddJokes(hide: true)
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.tableV.reloadData()
                }else{
                   
                }
                
            }
            
        }catch {
            
        }
    }
    func saveJokesIntoDataBase(jokeTitle:String,jokeCore:String,index: Int){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let res = self.constructJson(json: self.myJokes, jokeTitle: jokeTitle, jokeCore: jokeCore,index:index)
            for i in 0...(self.myJokes.count - 1) {
                if i == index {
                    self.myJokes[i].name = jokeTitle
                    self.myJokes[i].jokeTxt = jokeCore
                    break
                }
            }
            self.jokesRandom = self.myJokes
            self.tableV.reloadData()
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    self.hideAddJokes(hide: true)
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.tableV.reloadData()
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
    func saveJokesIntoDataBase(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            self.myJokes = self.jokesRandom
            let res = self.constructJson(json: self.myJokes, jokeTitle: "", jokeCore: "")
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "listJokes" : res.rawValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.updateJokes, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let data = JSON(response.data ?? Data())
                    self.hideAddJokes(hide: true)
                    print("REQU1 : ",data)
                    //self.jokes = data
                    self.tableV.reloadData()
                }else{
                    
                }
                
            }
            
        }catch {
            
        }
    }
}
