//
//  goAds.swift
//  YLYL
//
//  Created by macbook on 2019-09-27.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds
class goAds: UIViewController, GADInterstitialDelegate {
    
    var intertial :GADInterstitial!
    override func viewDidLoad() {
        super.viewDidLoad()
        intertial.delegate = self
    }
    var isFirst = true
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
          self.intertial.present(fromRootViewController: self)
    }
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
         //self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
        self.navigationController?.popViewController(animated: true)
        /*self.dismiss(animated: true) {
            
        } */
    }
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("ADs errror : ",error.localizedDescription)
      
        intertial = reloadInterstitialAd()
    }
    func reloadInterstitialAd() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        
         self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
        
    }
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        //isAdActive = true
        //NotificationCenter.default.removeObserver(self)
    }
}

