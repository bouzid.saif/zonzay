//
//  GifBuyController.swift
//  YLYL
//
//  Created by macbook on 3/28/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SnapKit
import AVKit
import ScaledCenterCarousel
class GifBuyController: ServerUpdateDelegate,YTScaledCenterCarouselDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    var selectedIndex: UInt = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var zonzL: UILabel!
    @IBOutlet weak var ZonzContainer : UIView!
    @IBOutlet weak var BuyBTN: UIButton!
    @IBOutlet weak var WinnerContainer: UIView!
    @IBOutlet weak var LoserContainer : UIView!
    @IBOutlet weak var WinnerLBL : UILabel!
    @IBOutlet weak var LoserLBL : UILabel!
    
    var gifItems : JSON = []
    var gifOwnedItems : JSON = []
    var winnerGif : [GifsBuy] = []
    var allGif : [GifsBuy] = []
    var loserGif : [GifsBuy] = []
    var winnerOwnedGifs : [GifsBuy] = []
    var loserOwnedGifs : [GifsBuy] = []
    var allOwnedGif : [GifsBuy] = []
    var constraints : [IndexPath : Constraint] = [:]
    var constraintsSelected : [IndexPath : Bool] = [:]
    var sizeCell : CGSize = CGSize.zero
    var currentselectedIndex : IndexPath = IndexPath(row: -1, section: 0)
    var selectedCell : IndexPath = IndexPath(row: 0, section: 0)
    @IBOutlet weak var flowLayout: UPCarouselFlowLayout!
   /*fileprivate var pageSize: CGSize {
       let layout = self.collectionView.collectionViewLayout as! YTScaledCenterCarouselLayout
        //layout.
        var pageSize = self.collectionView.frame.height * 0.6
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        } else {
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    } */
    fileprivate var orientation: UIDeviceOrientation {
        return .portrait
    }
    @IBAction func BackButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func setupLayout() {
        
        let layout = self.collectionView.collectionViewLayout as! YTScaledCenterCarouselLayout
        self.collectionView.decelerationRate = UIScrollView.DecelerationRate.fast

        layout.normalCellHeight =  (self.collectionView.size.height * 0.7)
        layout.centerCellHeight = self.collectionView.size.height * 0.9
        layout.normalCellWidth = (self.collectionView.size.height * 0.7)
        layout.centerCellWidth = self.collectionView.size.height * 0.9
        layout.proposedContentOffset = CGPoint(x: 100, y: 0)
       //self.collectionView.isPagingEnabled = true
        
       // layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 60)
        
        
    }
    func selectFilter(filter: Int){
        if filter == 0 {
            if self.WinnerContainer.backgroundColor != UIColor.white {
            self.LoserContainer.backgroundColor = UIColor.clear
            self.LoserLBL.textColor = UIColor.white.withAlphaComponent(0.7)
            UIView.animate(withDuration: 0.2) {
                self.WinnerContainer.backgroundColor = UIColor.white
                
                self.WinnerLBL.textColor = UIColor(red: 23/255, green: 188/255, blue: 209/255, alpha: 1)
                UIView.transition(with: self.collectionView, duration: 0.35, options: .transitionCrossDissolve, animations: {
                    CATransaction.begin()
                    
                    CATransaction.setCompletionBlock({
                        if self.collectionView.numberOfItems(inSection: 0) != 0 {
                        self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                            self.selectedIndex = 0
                            
                            self.collectionView.scrollToTop(animated: true)
                    //self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                        }
                    })
                    self.collectionView.reloadSections([0])
                    CATransaction.commit()
                }, completion: nil)
               
            }
            }
        }else{
             if self.LoserContainer.backgroundColor != UIColor.white {
                
            self.WinnerContainer.backgroundColor = UIColor.clear
            self.WinnerLBL.textColor = UIColor.white.withAlphaComponent(0.7)
            UIView.animate(withDuration: 0.2) {
                self.LoserContainer.backgroundColor = UIColor.white
                
                self.LoserLBL.textColor = UIColor(red: 23/255, green: 188/255, blue: 209/255, alpha: 1)
                UIView.transition(with: self.collectionView, duration: 0.35, options: .transitionCrossDissolve, animations: {
                    CATransaction.begin()
                    CATransaction.setCompletionBlock({
                        if self.collectionView.numberOfItems(inSection: 0) != 0 {
                            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                            self.selectedIndex = 0
                            
                            self.collectionView.scrollToTop(animated: true)
                            //self.selectedIndex = 0
                           // self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                        }
                    })
                    self.collectionView.reloadSections([0])
                    CATransaction.commit()
                }, completion: nil)
            }
            }
        }
    }
    @objc func WinnerSelect(_ sender:UITapGestureRecognizer) {
        if self.WinnerContainer.backgroundColor != UIColor.white {
            self.FilterStore.text = Localization("ALL")
        }
        self.selectFilter(filter: 0)
        
        
    }
    @objc func LoserSelect(_ sender:UITapGestureRecognizer) {
        if self.LoserContainer.backgroundColor != UIColor.white {
            self.FilterStore.text = Localization("ALL")
        }
        
        self.selectFilter(filter: 1)
    }
    var winnerPath = ""
    var loserPath = ""

    func initGifDefault () {
        guard let winnerP = UserDefaults.standard.value(forKey: "WinnerPath") as? String else {
            return
        }
        print("WinnerPath : ",winnerP)
         winnerPath = winnerP
        
        guard let loserP = UserDefaults.standard.value(forKey: "LoserPath") as? String else {
            return
        }
        print("LoserPath : ",loserP)
       loserPath = loserP
  
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       self.BuyBTN.isHidden =  true
        self.BuyBTN.contentEdgeInsets.left = 5.0
         self.BuyBTN.contentEdgeInsets.right = 5.0
       self.initGifDefault()
        self.WinnerLBL.text = Localization("Gif Store")
        self.LoserLBL.text =  Localization("GifGallery")
        self.FilterStore.text = Localization("ALL")
        self.configureZonzContainer()
         self.setupLayout()
       self.collectionView.dataSource = self
        self.paginator = YTScaledCenterCarouselPaginator(collectionView: self.collectionView, delegate: self)
        self.getGifs(firstTime: true)
        self.getOwnedGifs(firstTime: true)
        self.getZonz()
        //self.collectionView.delegate = self
        //self.collectionView.dataSource = self
       self.selectFilter(filter: 0)
        let tapWinnerGesture = UITapGestureRecognizer(target: self, action: #selector(self.WinnerSelect(_:)))
         let tapLoserGesture = UITapGestureRecognizer(target: self, action: #selector(self.LoserSelect(_:)))
        self.WinnerContainer.isUserInteractionEnabled = true
        self.LoserContainer.isUserInteractionEnabled = true
        self.WinnerContainer.addGestureRecognizer(tapWinnerGesture)
        self.LoserContainer.addGestureRecognizer(tapLoserGesture)
        
        self.BuyBTN.addTarget(self, action: #selector(self.buyGifsAction(_:)), for: .touchUpInside)
    }
    @objc func buyGifsAction(_ sender: UIButton) {
        if WinnerContainer.backgroundColor == UIColor.white {
            let index = self.collectionView.indexPathsForSelectedItems
             if self.FilterStore.text == Localization("ALL") {
              self.buyGif(gif: self.allGif[Int(self.selectedIndex)], index: Int(self.selectedIndex))
        }else if self.FilterStore.text == Localization("winnerText") {
                self.buyGif(gif: self.winnerGif[Int(self.selectedIndex)], index: Int(self.selectedIndex))
            }else{
                 self.buyGif(gif: self.loserGif[Int(self.selectedIndex)], index: Int(self.selectedIndex))
                }
            
        }else{
            let index = self.collectionView.indexPathsForSelectedItems
            
             if self.FilterStore.text == Localization("ALL") {
                if self.allOwnedGif[Int(self.selectedIndex)].getCategory() == "Winner" {
                UserDefaults.standard.setValue(self.allOwnedGif[Int(self.selectedIndex)].getExternalPath(), forKey: "WinnerPath")
                    UserDefaults.standard.synchronize()
                   
                    self.winnerPath = self.allOwnedGif[Int(self.selectedIndex)].getExternalPath()
                    guard let cell = self.collectionView.cellForItem(at: IndexPath(row: Int(self.selectedIndex), section: 0)) as? CustomGifsCell else {
                        return
                    }
                    
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                        cell.favoriteIMG.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                    }) { (verif) in
                        cell.favoriteIMG.image = UIImage(named: "favoriteON")
                         UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
                         }) { (verifTwo) in
                           cell.favoriteIMG.transform = CGAffineTransform.identity
                            self.BuyBTN.isHidden = true
                        }
                    }
                }else{
                  UserDefaults.standard.setValue(self.allOwnedGif[Int(self.selectedIndex)].getExternalPath(), forKey: "LoserPath")
                    UserDefaults.standard.synchronize()
                
                     self.loserPath = self.allOwnedGif[Int(self.selectedIndex)].getExternalPath()
                    guard let cell = self.collectionView.cellForItem(at: IndexPath(row: Int(self.selectedIndex), section: 0)) as? CustomGifsCell else {
                        return
                    }
                    UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                        cell.favoriteIMG.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                    }) { (verif) in
                        cell.favoriteIMG.image = UIImage(named: "favoriteON")
                        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
                        }) { (verifTwo) in
                            cell.favoriteIMG.transform = CGAffineTransform.identity
                            self.BuyBTN.isHidden  = true
                        }
                    }
                }
             }else if self.FilterStore.text == Localization("winnerText") {
                UserDefaults.standard.setValue(self.winnerOwnedGifs[Int(self.selectedIndex)].getExternalPath(), forKey: "WinnerPath")
                UserDefaults.standard.synchronize()
                
                self.winnerPath = self.winnerOwnedGifs[Int(self.selectedIndex)].getExternalPath()
                guard let cell = self.collectionView.cellForItem(at: IndexPath(row: Int(self.selectedIndex), section: 0)) as? CustomGifsCell else {
                    return
                }
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                    cell.favoriteIMG.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                }) { (verif) in
                    cell.favoriteIMG.image = UIImage(named: "favoriteON")
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
                    }) { (verifTwo) in
                        cell.favoriteIMG.transform = CGAffineTransform.identity
                        self.BuyBTN.isHidden  = true
                    }
                }
            }else{
                UserDefaults.standard.setValue(self.loserOwnedGifs[Int(self.selectedIndex)].getExternalPath(), forKey: "LoserPath")
                UserDefaults.standard.synchronize()
                  self.loserPath = self.loserOwnedGifs[Int(self.selectedIndex)].getExternalPath()
                guard let cell = self.collectionView.cellForItem(at: IndexPath(row: Int(self.selectedIndex), section: 0)) as? CustomGifsCell else {
                    return
                }
                UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseIn], animations: {
                    cell.favoriteIMG.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                }) { (verif) in
                    cell.favoriteIMG.image = UIImage(named: "favoriteON")
                    UIView.animate(withDuration: 0.2, delay: 0, options: [.curveEaseOut], animations: {
                    }) { (verifTwo) in
                        cell.favoriteIMG.transform = CGAffineTransform.identity
                        self.BuyBTN.isHidden  = true
                    }
                }
            }
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.WinnerContainer.backgroundColor == UIColor.white {
            print("ITEMS Store")
            if self.FilterStore.text == Localization("ALL") {
        return self.allGif.count
            }else if self.FilterStore.text == Localization("winnerText") {
                return self.winnerGif.count
            }else{
                return self.loserGif.count
            }
        }else{
            print("ITEMS Gallery ALL :",self.allOwnedGif.count)
            print("ITEMS Gallery Winner :",self.winnerOwnedGifs.count)
            print("ITEMS Gallery Loser : ",self.loserOwnedGifs.count)

            if self.FilterStore.text == Localization("ALL") {
                return self.allOwnedGif.count
            }else if self.FilterStore.text == Localization("winnerText") {
                return self.winnerOwnedGifs.count
            }else{
                return self.loserOwnedGifs.count
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? CustomGifsCell {
        cell.removeLayer()
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CustomGifsCell
        cell.indexPath = indexPath
        cell.isSelected = self.selectedIndex == indexPath.row
        let viewGif = cell.viewWithTag(1)
        let imageThumb = cell.viewWithTag(3) as! UIImageView
            if self.WinnerContainer.backgroundColor == UIColor.white {
                print("CELL Store")
                if self.FilterStore.text == Localization("ALL") {
                    
                    imageThumb.setImage(with: URL(fileURLWithPath: self.allGif[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    //imageThumb.isHidden = true
                    cell.configureVideo(url: self.allGif[indexPath.row].getPathGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.configureLBLS(name: self.allGif[indexPath.row].getNomGif(), price: self.allGif[indexPath.row].getPriceGif())
                
                }else if self.FilterStore.text == Localization("winnerText") {
                    imageThumb.setImage(with: URL(fileURLWithPath: self.winnerGif[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    cell.configureVideo(url: self.winnerGif[indexPath.row].getPathGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.configureLBLS(name: self.winnerGif[indexPath.row].getNomGif(), price: self.winnerGif[indexPath.row].getPriceGif())
                }else{
                    imageThumb.setImage(with: URL(fileURLWithPath: self.loserGif[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    cell.configureVideo(url: self.loserGif[indexPath.row].getPathGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.configureLBLS(name: self.loserGif[indexPath.row].getNomGif(), price: self.loserGif[indexPath.row].getPriceGif())
                }
        
            }else{
                print("CELL Gallery")

                if self.FilterStore.text == Localization("ALL") {
                    
                    imageThumb.setImage(with: URL(fileURLWithPath: self.allOwnedGif[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    cell.configureVideo(url: self.allOwnedGif[indexPath.row].getPathGif())
                    cell.configureLBLS(name: self.allOwnedGif[indexPath.row].getNomGif(), price: self.allOwnedGif[indexPath.row].getPriceGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.PriceGif.isHidden = true
                    cell.PriceImageIcon.isHidden = true
                    cell.favoriteIMG.image = self.allOwnedGif[indexPath.row].getPathGif() == self.winnerPath ? UIImage(named: "favoriteON") : UIImage(named: "favoriteOFF")
                    if cell.favoriteIMG.image == UIImage(named: "favoriteOFF") {
                         cell.favoriteIMG.image = self.allOwnedGif[indexPath.row].getPathGif() == self.loserPath ? UIImage(named: "favoriteON") : UIImage(named: "favoriteOFF")
                    }
                    cell.favoriteIMG.isHidden = false
                    
                }else if self.FilterStore.text == Localization("winnerText") {
                    imageThumb.setImage(with: URL(fileURLWithPath: self.winnerOwnedGifs[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    cell.configureVideo(url: self.winnerOwnedGifs[indexPath.row].getPathGif())
                    cell.configureLBLS(name: self.winnerOwnedGifs[indexPath.row].getNomGif(), price: self.winnerOwnedGifs[indexPath.row].getPriceGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.PriceGif.isHidden = true
                    cell.PriceImageIcon.isHidden = true
                     cell.favoriteIMG.image = self.allOwnedGif[indexPath.row].getPathGif() == self.winnerPath ? UIImage(named: "favoriteON") : UIImage(named: "favoriteOFF")
                    cell.favoriteIMG.isHidden = false
                }else{
                    imageThumb.setImage(with: URL(fileURLWithPath: self.loserOwnedGifs[indexPath.row].getThumbGif()), placeholder: nil, transformer: nil, progress: nil, completion: nil)
                    cell.configureVideo(url: self.loserOwnedGifs[indexPath.row].getPathGif())
                    cell.configureLBLS(name: self.loserOwnedGifs[indexPath.row].getNomGif(), price: self.loserOwnedGifs[indexPath.row].getPriceGif())
                    cell.imageURLForAfter = self.allGif[indexPath.row].getThumbGif()
                    cell.PriceGif.isHidden = true
                    cell.PriceImageIcon.isHidden = true
                    cell.favoriteIMG.image = self.allOwnedGif[indexPath.row].getPathGif() == self.loserPath ? UIImage(named: "favoriteON") : UIImage(named: "favoriteOFF")
                    cell.favoriteIMG.isHidden = false
                }
                
            }
      /*  if self.constraintsSelected[indexPath] == nil{
            var constraint : Constraint!
            viewGif!.snp.makeConstraints { (const) in
                constraint = const.bottom.equalTo(cell).offset(0).constraint
            }
            
            self.constraintsSelected[indexPath] = false
            self.constraints[indexPath] = constraint
        } */
       
        return cell
        
    }
/*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    print("we are good")
    let height = collectionView.frame.size.height
    let width = collectionView.frame.size.width
    // in case you you want the cell to be 40% of your controllers view
    if sizeCell == CGSize.zero {
        sizeCell = CGSize(width: width * 0.7, height: height * 0.7)
    }else{
        if  indexPath == self.selectedCell {
            return CGSize(width: (width * 0.7) + 60, height: (height * 0.7 ) + 60)
        }
    }
    
    return CGSize(width: width * 0.7, height: height * 0.7)
} */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CustomGifsCell
        print("/////")
        
       // collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .centeredHorizontally)
      
         //print(cell?.contentView.constraints)
    }
    func animateCellSize(cell: CustomGifsCell?,indexPath :IndexPath) {
       // self.BuyBTN.alpha = 0
        
        if self.WinnerContainer.backgroundColor != .white {
            self.BuyBTN.setTitle(Localization("MakeAD"), for: .normal)
        }else{
            self.BuyBTN.setTitle(Localization("Buy"), for: .normal)
        }
        if self.WinnerContainer.backgroundColor != .white && cell?.favoriteIMG.isHidden == false {
            
            if cell?.favoriteIMG.image == UIImage(named: "favoriteON") || (self.winnerPath == self.allOwnedGif[indexPath.row].getExternalPath()) || (self.loserPath == self.allOwnedGif[indexPath.row].getExternalPath()) {
                self.BuyBTN.isHidden =  true
                cell?.favoriteIMG.image = UIImage(named: "favoriteON")
            }else{
                
                self.BuyBTN.isHidden =  false
            }
            
        }else{
            self.BuyBTN.isHidden =  false
        }
        
        
        //  SocketIOManager.sharedInstance.sendToMiddleWar()
        let viewGif = cell?.viewWithTag(1)
        
        /* if ((cell?.size.width)! + 60) == (sizeCell.width ) {
         self.constraintsSelected[indexPath] = true
         }else if ((cell?.size.width)! - 60) < (sizeCell.width - 60){
         self.constraintsSelected[indexPath] = false
         } */
       // if self.constraintsSelected[indexPath] == false {
            
        /*    viewGif?.constraints.forEach({ (const) in
                if const.identifier == "RemoveMe" {
                    print("RemoveMe")
                    const.constant = -37
                }
            })
            viewGif?.layoutIfNeeded() */
          /*  viewGif?.snp.updateConstraints({ (const) in
               const.bottom.equalTo(cell!).constraint.update(offset: -37)
               // const.bottom.equalTo(cell!).offset(-37).labeled("RemoveMe")
                
            }) */
      /*      cell?.thumbImage.constraints.forEach({ (const) in
                if const.identifier == "RemoveMM" {
                     print("RemoveMM")
                const.constant = -37
                }
            })
            cell?.layoutIfNeeded() */
           /* cell?.thumbImage.snp.updateConstraints({ (const) in
                 const.bottom.equalTo(cell!).constraint.update(offset: -37)
                //const.bottom.equalTo(cell!).offset(-37).labeled("RemoveMM")
            }) */
        print("Cell Details : ",cell?.moviePlayer?.currentItem?.asset)
     
            cell?.playLayer()
           // self.constraintsSelected[indexPath] = true
            /*UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseInOut], animations: {
               // self.BuyBTN.alpha = 1
                viewGif?.snp.updateConstraints({ (const) in
                    const.bottom.equalTo(cell!).offset(-37).labeled("RemoveMe")
                    
                })
                cell?.thumbImage.snp.updateConstraints({ (const) in
                    const.bottom.equalTo(cell!).offset(-37).labeled("RemoveMM")
                })
              /*  cell?.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                cell?.size = CGSize(width: (cell?.size.width)! + 60, height: (cell?.size.height)! + 60)
                cell?.origin.x -= 30
                cell?.origin.y -= 30
                cell?.layoutIfNeeded() */
            }) { _ in
                //let imageVi = cell?.viewWithTag(3) as! UIImageView
               // imageVi.isHidden = true
               
            } */
            
       // }else{
         //   print("already selected")
            
            
          /*  UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseInOut], animations: {
                self.BuyBTN.alpha = 0
                viewGif?.snp.updateConstraints({ (const) in
                    const.bottom.equalTo(cell!).offset(0).labeled("RemoveMe")
                })
                cell?.thumbImage.snp.updateConstraints({ (const) in
                    const.bottom.equalTo(cell!).offset(0).labeled("RemoveMM")
                })
                cell?.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                cell?.size = CGSize(width: (cell?.size.width)! - 60, height: (cell?.size.height)! - 60)
                cell?.origin.x += 30
                cell?.origin.y += 30
                cell?.layoutIfNeeded()
            }) { _ in
                //let imageVi = cell?.viewWithTag(3) as! UIImageView
               // imageVi.isHidden = false
                cell?.updateFrame()
                cell?.removeLayer()
                self.constraintsSelected[indexPath] = false
            } */
      //  }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    
        
       
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupLayout()
        if self.allGif.count != 0 {
          /*  DispatchQueue.main.asyncAfter(deadline: .now() + 1.2) {
                self.snapToCenter()
            } */
     
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       
         snapToCenter()
      /*  if  let selected = self.collectionView.indexPathsForSelectedItems {
            for sel in selected {
                if let cell = collectionView.cellForItem(at: sel) as? CustomGifsCell {
                    let imageVi =  cell.viewWithTag(3) as! UIImageView
                    cell.removeLayer()
                    imageVi.isHidden = false
                    
                    
                }
                self.collectionView.deselectItem(at: sel, animated: false)
                self.collectionView.delegate?.collectionView?(self.collectionView, didDeselectItemAt: sel)
            }
        }
         snapToCenter()
        self.BuyBTN.isHidden = true */
       
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if !decelerate {
            snapToCenter()
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //FIX ME:
        //print("DID SCROLL")
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("Deselected")
   /*     guard let cell = collectionView.cellForItem(at: indexPath) as? CustomGifsCell else {
            return
        }
        print(cell.GifName.text)
         let viewGif = cell.viewWithTag(1)
       
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
           // self.BuyBTN.alpha = 0
            viewGif?.snp.updateConstraints({ (const) in
                const.bottom.equalTo(cell).offset(0).labeled("RemoveMe")
            })
            cell.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            cell.size = CGSize(width: cell.size.width - 60, height: cell.size.height - 60)
            cell.origin.x += 30
            cell.origin.y += 30
            cell.layoutIfNeeded()
        }) { _ in
            let imageVi = cell.viewWithTag(3) as! UIImageView
            imageVi.isHidden = false
            cell.updateFrame()
            cell.removeLayer()
             self.constraintsSelected[indexPath] = false
        } */
    
        
    }
    func configureZonzContainer(){
        self.ZonzContainer.layer.cornerRadius = 7.5
        self.ZonzContainer.layer.borderColor = UIColor.white.cgColor
        self.ZonzContainer.layer.borderWidth = 1.0
        self.ZonzContainer.layer.masksToBounds = true
    }
    func getZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getZonz, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getZonz : ",data)
                let zonz = data["zonz"].stringValue
                if zonz != "" {
                    self.zonzL.text = zonz
                    
                }else{
                    self.zonzL.text = "0"
                }
            }
            
        }catch {
            
        }
    }
    func getGifs(firstTime:Bool){
         self.gifItems = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
          
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getGifs, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getGifs : ",data)
                if data != JSON.null {
                       if !data["message"].exists() {
                        self.gifItems = data
                        }
                    self.winnerGif = []
                    self.loserGif = []
                    self.allGif = []
                    self.collectionView.reloadData()
                    if self.gifItems.arrayObject?.count != 0 {
                        for i in 0...((self.gifItems.arrayObject?.count)! - 1) {
                            var names = URL(string:  self.gifItems[i]["pathGif"].stringValue)?.absoluteString.components(separatedBy: "/")
                        
                            var namesTwo = self.gifItems[i]["thumbGif"].stringValue.components(separatedBy: "/")
                            let t = names![(names!.count - 1)]
                            print("TTTTTTTTTT: ",t)
                             let d = namesTwo[(namesTwo.count - 1)]
                            print("DDDDDDDDDD: ",d)
                            self.allGif.append(GifsBuy(priceGif: self.gifItems[i]["priceGif"].stringValue, nomGif: self.gifItems[i]["nomGif"].stringValue, id: self.gifItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifItems[i]["category"].stringValue, externalP: self.gifItems[i]["pathGif"].stringValue))
                            if self.gifItems[i]["category"].stringValue == "Winner" {
                                self.winnerGif.append(GifsBuy(priceGif: self.gifItems[i]["priceGif"].stringValue, nomGif: self.gifItems[i]["nomGif"].stringValue, id: self.gifItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifItems[i]["category"].stringValue, externalP: self.gifItems[i]["pathGif"].stringValue))
                            }else{
                                self.loserGif.append(GifsBuy(priceGif: self.gifItems[i]["priceGif"].stringValue, nomGif: self.gifItems[i]["nomGif"].stringValue, id: self.gifItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifItems[i]["category"].stringValue, externalP: self.gifItems[i]["pathGif"].stringValue))
                            }
                        }
                    }
                print(self.gifItems.arrayObject?.count)
                    if firstTime {
                    CATransaction.begin()
                    CATransaction.setCompletionBlock({
                       self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                        self.selectedIndex = 0
                        self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                    })
                        self.BuyBTN.isHidden = false
                   self.collectionView.reloadData()
                    CATransaction.commit()
                }
                }else{
                    
                  
                }
            }
            
        }catch {
            
        }
    }
    func getOwnedGifs(firstTime:Bool){
        self.gifOwnedItems = []
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.getMyGifs, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("getOwnedGifs : ",data)
                if data != JSON.null {
                    if !data["message"].exists() {
                    self.gifOwnedItems = data
                    }
                    self.winnerOwnedGifs = []
                    self.loserOwnedGifs = []
                    self.allOwnedGif = []
                    //self.collectionView.reloadData()
                   
                    if self.gifOwnedItems.arrayObject?.count != 0 {
                        for i in 0...((self.gifOwnedItems.arrayObject?.count)! - 1) {
                            
                            var names = self.gifOwnedItems[i]["pathGif"].stringValue.components(separatedBy: "/")
                            var namesTwo = self.gifOwnedItems[i]["thumbGif"].stringValue.components(separatedBy: "/")
                            let t = names[(names.count - 1)]
                            let d = namesTwo[(namesTwo.count - 1)]
                            print("TTTTTTTTTT: ",t)
                            print("DDDDDDDDDD: ",d)
                            self.allOwnedGif.append(GifsBuy(priceGif: self.gifOwnedItems[i]["priceGif"].stringValue, nomGif: self.gifOwnedItems[i]["nomGif"].stringValue, id: self.gifOwnedItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifOwnedItems[i]["category"].stringValue, externalP: self.gifOwnedItems[i]["pathGif"].stringValue))
                            if self.gifOwnedItems[i]["category"].stringValue == "Winner" {
                                self.winnerOwnedGifs.append(GifsBuy(priceGif: self.gifOwnedItems[i]["priceGif"].stringValue, nomGif: self.gifOwnedItems[i]["nomGif"].stringValue, id: self.gifOwnedItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifOwnedItems[i]["category"].stringValue, externalP: self.gifOwnedItems[i]["pathGif"].stringValue))
                            }else{
                                self.loserOwnedGifs.append(GifsBuy(priceGif: self.gifOwnedItems[i]["priceGif"].stringValue, nomGif: self.gifOwnedItems[i]["nomGif"].stringValue, id: self.gifOwnedItems[i]["_id"].stringValue, pathGif: t, thumbGif: d, category: self.gifOwnedItems[i]["category"].stringValue, externalP: self.gifOwnedItems[i]["pathGif"].stringValue))
                            }
                        }
                    }
                    print("ITEMS Gallery ALL :",self.gifOwnedItems.arrayObject?.count)
                    print("ITEMS Gallery Winner :",self.winnerOwnedGifs.count)
                    print("ITEMS Gallery Loser : ",self.loserOwnedGifs.count)
                   
                }else{
                    
                   print("JSON NULL")
                }
            }
            
        }catch {
            
        }
    }
    func buyGif(gif: GifsBuy,index:Int){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            var  dict =  "["
          
            dict = dict + "{\"gifId\" : \"\(gif.getId())\"}"
            dict = dict + "]"
            let settings : Parameters = [
                "userId" : a["_id"].stringValue,
                "gifZonz" : gif.getPriceGif(),
                "gifs" : dict
                
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.buyGifs, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                
                print("BuyGifs : ",data)
                if data != JSON.null {
                    if data["message"].exists() {
                        if data["message"].stringValue == "User does not have enough zonz" {
                            let alert = UIAlertController(title: Localization("Gif Store"), message: Localization("ZonzEnough"), preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel ,handler: nil))
                            print("OOK")
                            self.present(alert, animated: true, completion: nil)
                        
                        }
                    }else{
                        let totalZonz = Int(self.zonzL.text!)!
                        self.zonzL.text = String(totalZonz - Int(gif.getPriceGif())!)
                        self.collectionView.delegate!.collectionView!(self.collectionView, didSelectItemAt: IndexPath(row: index, section: 0))
                        //let viewSelected = self.WinnerContainer.backgroundColor == UIColor.white ? "winner" : "loser"
                        if  self.FilterStore.text == Localization("winnerText") {
                            self.winnerOwnedGifs.append(self.winnerGif[index])
                             self.allOwnedGif.append(self.winnerGif[index])
                             self.winnerGif.remove(at: index)
                            self.collectionView.performBatchUpdates({
                                self.collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
                            }, completion: { _ in
                                self.collectionView.reloadData()
                                self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                                self.selectedIndex = 0
                                self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                            })
                        }else if self.FilterStore.text == Localization("loserText"){
                            self.loserOwnedGifs.append(self.winnerGif[index])
                             self.allOwnedGif.append(self.winnerGif[index])
                            self.loserGif.remove(at: index)
                            self.collectionView.performBatchUpdates({
                                self.collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
                            }, completion: { _ in
                                self.collectionView.reloadData()
                                self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                                self.selectedIndex = 0
                                self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                            })
                        }else{
                            self.allOwnedGif.append(self.winnerGif[index])
                            if self.winnerGif[index].getCategory() == "Winner" {
                                  self.winnerOwnedGifs.append(self.winnerGif[index])
                                  self.winnerGif.remove(at: index)
                            }else{
                            self.loserOwnedGifs.append(self.winnerGif[index])
                                 self.loserGif.remove(at: index)
                            }
                            self.allGif.remove(at: index)
                            self.collectionView.performBatchUpdates({
                                self.collectionView.deleteItems(at: [IndexPath(row: index, section: 0)])
                            }, completion: { _ in
                                self.collectionView.reloadData()
                                self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
                                self.selectedIndex = 0
                                self.paginator?.delegate.carousel(self.collectionView, didSelectElementAt: 0)
                            })
                        }
                        let alert = UIAlertController(title: Localization("Gif Store"), message: Localization("You have successfully brought") + gif.getNomGif(), preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: Localization("OK"), style: .cancel ,handler: { _ in
                             //self.getGifs(firstTime: false)
                        }))
                        
                        print("OOK")
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    
                }
            }
            
        }catch {
            
        }
    }
    
    @IBAction func LeftArrowAction(_ sender: UIButton) {
        if self.FilterStore.text == Localization("ALL") {
            self.FilterStore.text = Localization("loserText")
            
        }else if self.FilterStore.text == Localization("winnerText") {
            self.FilterStore.text = Localization("ALL")
        }else{
           self.FilterStore.text = Localization("winnerText")
        }
        UIView.transition(with: self.collectionView, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.collectionView.reloadSections([0])
        }, completion: { (verif) in
             self.removeFirstCell = false
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
            self.selectedIndex = 0
            
            self.collectionView.scrollToTop(animated: false)
             //self.collectionView.scrollToTop(animated: true)
        })
        
    }
    
    @IBOutlet weak var FilterStore: UILabel!
    var removeFirstCell = true
    @IBAction func RightArrowAction(_ sender: UIButton) {
        if self.FilterStore.text == Localization("ALL") {
            self.FilterStore.text = Localization("winnerText")
        }else if self.FilterStore.text == Localization("winnerText") {
            self.FilterStore.text = Localization("loserText")
        }else{
            self.FilterStore.text = Localization("ALL")
        }
        UIView.transition(with: self.collectionView, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.collectionView.reloadSections([0])
        }, completion: { (verif) in
            self.removeFirstCell = false
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .centeredHorizontally, animated: true)
            self.selectedIndex = 0
           
            self.collectionView.scrollToTop(animated: false)
             //self.collectionView.scrollToTop(animated: true)
        })
    }
    var collectionViewDataSource : YTScaledCenterCarouselDataSource?
    var paginator : YTScaledCenterCarouselPaginator?
}
extension GifBuyController {

    func snapToCenter() {
        
        let centerPoint = view.convert(view.center, to: collectionView)
        guard let centerIndexPath = collectionView.indexPathForItem(at: centerPoint) else {
            return
        }
        print("IndexToPlay : ",centerIndexPath.row)
        if currentselectedIndex != centerIndexPath {
            currentselectedIndex = centerIndexPath
       // collectionView.delegate?.collectionView?(self.collectionView, didSelectItemAt: centerIndexPath)
            
        }else{
            print("Same Cell")
        }
        let cell = self.collectionView.cellForItem(at: centerIndexPath) as? CustomGifsCell
        self.constraintsSelected[centerIndexPath] = false
        self.collectionView.performBatchUpdates(nil, completion: nil)
        //self.animateCellSize(cell: cell, indexPath: centerIndexPath)
       
        //ollectionView.scrollToItem(at: centerIndexPath, at: .centeredHorizontally, animated: true)
    }
}
extension GifBuyController : YTScaledCenterCarouselPaginatorDelegate{
    
    
    func carousel(_ collectionView: UICollectionView!, didSelectElementAt selectedIndex: UInt) {
        print("One Time Slection : ",selectedIndex)
        guard let cell = self.collectionView.cellForItem(at: IndexPath(row: Int(selectedIndex), section: 0)) as? CustomGifsCell else {
        return
        }
        if let layers = cell.thumbImage.layer.sublayers {
        if layers.contains(where: { (layer) -> Bool in
            layer.name == "Video"
        }) != false {
            self.animateCellSize(cell: cell, indexPath: IndexPath(row: Int(selectedIndex), section: 0))
        }else{
         print("Already Selected")
        }
        }else{
         self.animateCellSize(cell: cell, indexPath: IndexPath(row: Int(selectedIndex), section: 0))
        }
    }
    func carousel(_ collectionView: UICollectionView!, didScrollToVisibleCells indexPathes: [Any]!) {
   
        if let indexPathes = indexPathes as? [CustomGifsCell] {
            let sort = NSSortDescriptor(key: "row", ascending: true)
            let ordered = indexPathes.sorted { (cellOne, cellTwo) -> Bool in
                if cellOne.indexPath?.compare(cellTwo.indexPath!) == ComparisonResult.orderedAscending {
                return true
                }else{
                return false
                }
                
            }
            print("Ordered: ",ordered)
        if indexPathes.count == 2 {
            
            let FirstIndex = ordered[0].indexPath?.row
           
            let lastIndex =  ordered[indexPathes.count - 1].indexPath?.row
            
            if FirstIndex == 0  && (Int(self.selectedIndex) == 0){
                let cell = self.collectionView.cellForItem(at: IndexPath(row: 1, section: 0)) as! CustomGifsCell
                  if removeFirstCell{
                cell.removeLayer()
                  }else{
                    removeFirstCell = true
                }
            }else if lastIndex == (self.collectionView.numberOfItems(inSection: 0) - 1) {
            let cell = self.collectionView.cellForItem(at: IndexPath(row: FirstIndex!, section: 0)) as! CustomGifsCell
                if removeFirstCell{
                    cell.removeLayer()
                }else{
                    removeFirstCell = true
                }
                
            }
            
            
        }
        if indexPathes.count == 3 {
            print(indexPathes)
            let cellRight =  ordered.first
            let cellLeft = ordered.last
            if removeFirstCell{
                cellRight?.removeLayer()
                cellLeft?.removeLayer()
            }else{
                removeFirstCell = true
            }
           
            
        }
        }
        
    }
    
    
}
