//
//  VideoGameResultController.swift
//  YLYL
//
//  Created by macbook on 1/31/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import GoogleMobileAds
protocol VideoGameResultControllerDelegate {
    func revengeCallback(secondUser : String,room:NSNumber,Type:GameType)
}
class VideoGameResultController: FixUIController {
    var results : [String : Any] = [:]
     var interstitial: GADInterstitial!
    var selectedJokes : [String] = []
     var JokesResults : [Int] = []
    var JokesResults5 : [Int] = []
    var jokeOne : String = ""
    var jokeTwo : String = ""
    var jokeThree : String = ""
    var faceFrame : Float = 0.0
    var scoreOne : Int = 0
    var scoreTwo : Int = 0
    var scoreThree : Int = 0
    var scoreFourth : Int = 0
    var scoreFive : Int = 0
    var scoreSix : Int = 0
    var faceUserX : String = ""
    var faceUserY : String = ""
    var timeElapsedJanus : Float = 0.0
    var FriendOrMatch : Bool = false
    var Owner : String = ""
    var roomId : NSNumber = 0
    var scoresAll : [Int] = []
    var  delegate : VideoGameResultControllerDelegate?
    @IBOutlet weak var revenge : UIButton!
    @IBOutlet weak var exit : UIButton!
       @IBOutlet weak var containerAnim: AniamtionFinalView!
    var navigation : UINavigationController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = false

        print("Navigation: ",navigation)
        addZonz()
         interstitial = GADInterstitial(adUnitID: "ca-app-pub-1626125969955246/8494582550")
         let request = GADRequest()
        //interstitial.load(request)
        //interstitial.delegate = self
        var finalRes = GameWinner.draw
        configureMasterSound()
        ScriptBase.sharedInstance.pathVideoJanus = ""
        if (results["who"] as! String) == "slave" {
           
            switch (results["winnerResult"] as! GameWinner) {
            case .draw:
                finalRes = .draw
            case .masterWinner:
                finalRes = .masterLoser
            case .masterLoser:
                finalRes = .masterWinner
                
            }
        }else{
            switch (results["winnerResult"] as! GameWinner) {
            case .draw:
                finalRes = .draw
            case .masterWinner:
                finalRes = .masterLoser
            case .masterLoser:
                finalRes = .masterWinner
                
            }
        }
        if finalRes == .draw {
            self.revenge.setTitle(Localization("revenge"), for: .normal)
        }else if finalRes == .masterLoser {
            self.revenge.setTitle(Localization("replay"), for: .normal)
        }else{
            self.revenge.setTitle(Localization("revenge"), for: .normal)
        }
        if results.count == 0 {
            
        }else{
            if (results["who"] as! String) == "slave" {
            if (results["isBefore"] as! Bool ) == false {
                //Check This Monday
                switch results["winnerResult"] as! GameWinner {
                case .draw :
                    SocketIOManager.sharedInstance.StopGameSlave(pathSlave: results["PathMaster"] as! String, receiverMaster: results["userMaster"] as! String, userSlave: results["receiverSlave"] as! String, gamemode: 60, winnerResult: 2, jokeOne: self.jokeOne, jokeTwo: self.jokeTwo, jokeThree: self.jokeThree, scoreOne: JokesResults[0], scoreTwo: JokesResults[1], scoreThree: JokesResults[2], scoreFour: JokesResults[3], scoreFive: JokesResults[4], scoreSix: JokesResults[5], faceFrame: self.faceFrame, faceUserX: self.faceUserX, faceUserY: self.faceUserY,timeElapsedJanus: self.timeElapsedJanus, score5One: JokesResults5[0], score5Two: JokesResults5[1], score5Three: JokesResults5[2], score5Fourth: JokesResults5[3], score5Five: JokesResults5[4], score5Six: JokesResults5[5],jokesCounter: scoresAll)
                   
                case .masterLoser:
                    SocketIOManager.sharedInstance.StopGameSlave(pathSlave: results["PathMaster"] as! String, receiverMaster: results["userMaster"] as! String, userSlave: results["receiverSlave"] as! String, gamemode: 60, winnerResult: 1, jokeOne: self.jokeOne, jokeTwo: self.jokeTwo, jokeThree: self.jokeThree, scoreOne: JokesResults[0], scoreTwo: JokesResults[1], scoreThree: JokesResults[2], scoreFour: JokesResults[3], scoreFive: JokesResults[4], scoreSix: JokesResults[5], faceFrame: self.faceFrame, faceUserX: self.faceUserX, faceUserY: self.faceUserY,timeElapsedJanus: self.timeElapsedJanus, score5One: JokesResults5[0], score5Two: JokesResults5[1], score5Three: JokesResults5[2], score5Fourth: JokesResults5[3], score5Five: JokesResults5[4], score5Six: JokesResults5[5],jokesCounter: scoresAll)
                case .masterWinner:
                    SocketIOManager.sharedInstance.StopGameSlave(pathSlave: results["PathMaster"] as! String, receiverMaster: results["userMaster"] as! String, userSlave: results["receiverSlave"] as! String, gamemode: 60, winnerResult: 0, jokeOne: self.jokeOne, jokeTwo: self.jokeTwo, jokeThree: self.jokeThree, scoreOne: JokesResults[0], scoreTwo: JokesResults[1], scoreThree: JokesResults[2], scoreFour: JokesResults[3], scoreFive: JokesResults[4], scoreSix: JokesResults[5], faceFrame: self.faceFrame, faceUserX: self.faceUserX, faceUserY: self.faceUserY,timeElapsedJanus: self.timeElapsedJanus, score5One: JokesResults5[0], score5Two: JokesResults5[1], score5Three: JokesResults5[2], score5Fourth: JokesResults5[3], score5Five: JokesResults5[4], score5Six: JokesResults5[5],jokesCounter: scoresAll)
                }
           
            }else{
                SocketIOManager.sharedInstance.StopGameSlave(pathSlave: results["PathMaster"] as! String, receiverMaster: results["userMaster"] as! String, userSlave: results["receiverSlave"] as! String, gamemode: 0, winnerResult: 1, jokeOne: self.jokeOne, jokeTwo: self.jokeTwo, jokeThree: self.jokeThree, scoreOne: JokesResults[0], scoreTwo: JokesResults[1], scoreThree: JokesResults[2], scoreFour: JokesResults[3], scoreFive: JokesResults[4], scoreSix: JokesResults[5], faceFrame: self.faceFrame, faceUserX: self.faceUserX, faceUserY: self.faceUserY,timeElapsedJanus: self.timeElapsedJanus, score5One: JokesResults5[0], score5Two: JokesResults5[1], score5Three: JokesResults5[2], score5Fourth: JokesResults5[3], score5Five: JokesResults5[4], score5Six: JokesResults5[5],jokesCounter: scoresAll)
                }
            }else{
                 //master do nothing here
                }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("CloseGameResult"), object: nil, queue: nil) { (notif) in
            if self.FriendOrMatch  {
                SocketIOManager.sharedInstance.removeFromMatchMaking(roomId: self.roomId.stringValue)
                  }
                  self.ok = false
                  self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
             self.navigationController?.popToRootViewController(animated: true)
        }
    }
    func presentAds(){
        if interstitial.isReady {
           
            interstitial.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
        }
    }
    func configureMasterSound(){
        if SoundBackGround.enabled {
            if let url = SoundBackGround.soundsBundle.url(forResource: "background_music", withExtension: "mp3") {
                if let playerXX = SoundBackGround.sounds[url] {
                    playerXX.play()
                }
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        var finalRes = GameWinner.draw
        if (results["who"] as! String) == "slave" {
            switch (results["winnerResult"] as! GameWinner) {
            case .draw:
                finalRes = .draw
            case .masterWinner:
                finalRes = .masterLoser
            case .masterLoser:
                finalRes = .masterWinner
                
            }
        }else{
            switch (results["winnerResult"] as! GameWinner) {
            case .draw:
                finalRes = .draw
            case .masterWinner:
                finalRes = .masterLoser
            case .masterLoser:
                finalRes = .masterWinner
                
            }
        }
         let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
        containerAnim.addBougiAnimation(userImage: a["userImageURL"].stringValue, result:  finalRes)
        }catch{
            
        }
        presentAds()
        SocketIOManager.sharedInstance.reactivateOnFinishGame()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func addZonz(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.registerSocket(Name: a["_id"].stringValue)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            Alamofire.request(ScriptBase.sharedInstance.addZonzEndGame, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                let data = JSON(response.data ?? Data())
                print("addZonz : ",data)
                
               
                
            }
            
        }catch {
            
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        if ok {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                 var tab : [String: Any] = [:]
            if revenge.title(for: .normal) == "replay" && FriendOrMatch == false {
               
                tab["secondUser"] = a["_id"].stringValue != (results["userMaster"] as! String) ? (results["userMaster"] as! String) : (results["receiverSlave"] as! String)
                tab["room"] = self.roomId
                tab["Type"] = GameType.ReplayFriend
            }else if revenge.title(for: .normal) == "replay" && FriendOrMatch {
                tab["secondUser"] = a["_id"].stringValue != (results["userMaster"] as! String) ? (results["userMaster"] as! String) : (results["receiverSlave"] as! String)
                tab["room"] = self.roomId
                tab["Type"] = GameType.ReplayMatch
            }else if revenge.title(for: .normal) == "revenge" && FriendOrMatch  == false{
               
                tab["secondUser"] = a["_id"].stringValue != (results["userMaster"] as! String) ? (results["userMaster"] as! String) : (results["receiverSlave"] as! String)
                tab["room"] = self.roomId
                tab["Type"] = GameType.RevengeFriend
            }else if revenge.title(for: .normal) == "revenge" && FriendOrMatch {
                tab["secondUser"] = a["_id"].stringValue != (results["userMaster"] as! String) ? (results["userMaster"] as! String) : (results["receiverSlave"] as! String)
                tab["room"] = self.roomId
                tab["Type"] = GameType.RevengeMatch
            }
                if Owner == "master" {
                 NotificationCenter.default.post(name: NSNotification.Name.init("Revenge"), object: tab)
                }else{
                     NotificationCenter.default.post(name: NSNotification.Name.init("RevengeSlave"), object: tab)
                }
            }catch{
                
            }
           
        }
    }
    var ok = false
    @IBAction func revengeAction(_ sender:UIButton){
       
            print(results)
            self.ok = true
            self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
              self.navigationController?.popToRootViewController(animated: true)
            if Owner == "master" {
              //   self.navigation?.popToRootViewController(animated: true)
               // self.navigationController?.popToRootViewController(animated: true)
              /*  self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name.init("Dissmiss"), object: nil)
                } */
            }else{
              /*  self.dismiss(animated: true) {
                    NotificationCenter.default.post(name: Notification.Name.init("Dissmiss"), object: nil)
                } */
                  self.navigationController?.popToRootViewController(animated: true)
              /*  for view in (self.navigationController?.viewControllers)! {
                    print("ViewControllers : ", view)
                    let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
                    
                    if view .isKind(of: StartViewController.self)  {
                        //self.navigationController.po
                        self.navigationController?.popToViewController(view, animated: true)
                        break
                    }else if  view .isKind(of: HomeTabBarController.self) {
                        self.navigationController?.popToViewController(vc, animated: true)
                    }
                } */
                // self.navigation?.popToRootViewController(animated: true)
            }
        
            
            
        
    }
    @IBAction func quitAction(_ sender : UIButton) {
        if FriendOrMatch  {
            SocketIOManager.sharedInstance.removeFromMatchMaking(roomId: roomId.stringValue)
        }
        self.ok = false
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        
        
        
        if Owner == "master" {
            
         /*  self.dismiss(animated: true) {
                NotificationCenter.default.post(name: Notification.Name.init("Dissmiss"), object: nil)
            } */
              self.navigationController?.popToRootViewController(animated: true)
           // self.navigation?.popToRootViewController(animated: true)
          
        }else{
        
            /*self.dismiss(animated: true) {
                NotificationCenter.default.post(name: Notification.Name.init("Dissmiss"), object: nil)
            } */
              self.navigationController?.popToRootViewController(animated: true)
            // self.dismiss(animated: true, completion: nil)
          //  self.navigation?.popToRootViewController(animated: true)
        }
    }
}
extension VideoGameResultController : GADInterstitialDelegate {
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
    }
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("ADs errror : ",error.localizedDescription)
        
        interstitial = reloadInterstitialAd()
    }
    func reloadInterstitialAd() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-1626125969955246/8494582550")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
      

    }
    
}
