//
//  HomeTabBarController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 07/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//
import UIKit

class HomeTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hidesBottomBarWhenPushed = true
         self.tabBar.unselectedItemTintColor = UIColor.white.withAlphaComponent(0.5)
        DispatchQueue.main.async {
            let mode = (UIApplication.shared.delegate as! AppDelegate).NormalMode
            let index = (UIApplication.shared.delegate as! AppDelegate).indexToShow
            
            if mode {
            self.selectedIndex = 2
                self.setTabBarVisible(visible: true, duration: 3, animated: false)
           // let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "test")
             //self.present(vc, animated: false, completion: nil)
                
            }else{
                self.selectedIndex = index
                if self.selectedIndex == 0 {
                    DispatchQueue.main.asyncAfter(deadline: .now()  + 2) {
                         NotificationCenter.default.post(name: NSNotification.Name.init("ShowVideoFromNotif"), object: (UIApplication.shared.delegate as! AppDelegate).videoId)
                    }
                   
                }
            }
        }
       
        // make self the UITabBarControllerDelegate
        self.delegate = self
         NotificationCenter.default.addObserver(self, selector: #selector(self.addBadge(notification:)), name: NSNotification.Name(rawValue: "AddBadgeVideo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addBadgeRequest(notification:)), name: NSNotification.Name(rawValue: "AddBadgeRequest"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.addBadgeChat(notification:)), name: NSNotification.Name(rawValue: "AddBadgeChat"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteBadge(notification:)), name: NSNotification.Name(rawValue: "DeleteBadgeVideo"), object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteBadgeChat(notification:)), name: NSNotification.Name(rawValue: "DeleteBadgeRequest"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.DeleteBadgeChat(notification:)), name: NSNotification.Name(rawValue: "DeleteBadgeChat"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.AddNotSee(notification:)), name: NSNotification.Name(rawValue: "addNotSee"), object: nil)
    }
    
    func selectTab(Number:Int){
       
        self.selectedIndex = Number
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
           item.badgeValue = nil
        if(item.tag == 0)
        {
            
        }
        else if(item.tag == 1)
        {
           
            
        }else if(item.tag == 2)
        {
           
        }
        else
        {
            
        }
        print("Item Tag : ",item.tag)
    }
    @objc func addBadge (notification: NSNotification)
    {
        DispatchQueue.main.async {
            
        
        let tabBarController = self
        tabBarController.delegate = self
        if !(tabBarController.selectedIndex == 0) {
            if tabBarController.viewControllers![0].tabBarItem.badgeValue == nil {
                 DispatchQueue.main.async {
                tabBarController.viewControllers![0].tabBarItem.badgeValue = "1"
                }
            }else {
                 DispatchQueue.main.async {
                tabBarController.viewControllers![0].tabBarItem.badgeValue = String(Int(tabBarController.viewControllers![0].tabBarItem.badgeValue!)! + 1 )
                }
            }
        }
            }
    }
    @objc func DeleteBadge (notification: NSNotification)
    {
         DispatchQueue.main.async {
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![0].tabBarItem.badgeValue != nil {
             DispatchQueue.main.async {
            tabBarController.viewControllers![0].tabBarItem.badgeValue = nil
            }
        }
        }
        
    }
    @objc func addBadgeRequest (notification: NSNotification)
    {
         DispatchQueue.main.async {
        let tabBarController = self
        tabBarController.delegate = self
        if !(tabBarController.selectedIndex == 1) {
            if tabBarController.viewControllers![1].tabBarItem.badgeValue == nil {
                 DispatchQueue.main.async {
                tabBarController.viewControllers![1].tabBarItem.badgeValue = "1"
              
               
                if ((UIApplication.shared.delegate) as! AppDelegate).NotifThree != -1 {
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifThree += 1
                }else{
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifThree = 1
                }
                  }
                
            }else {
                 DispatchQueue.main.async {
                tabBarController.viewControllers![1].tabBarItem.badgeValue = String(Int(tabBarController.viewControllers![1].tabBarItem.badgeValue!)! + 1 )
              
                if ((UIApplication.shared.delegate) as! AppDelegate).NotifThree != -1 {
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifThree += 1
                }else{
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifThree = 1
                }
                }
            }
        }
        }
    }
    @objc func DeleteBadgeRequest (notification: NSNotification)
    {
         DispatchQueue.main.async {
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![1].tabBarItem.badgeValue != nil {
            tabBarController.viewControllers![1].tabBarItem.badgeValue = nil
            
        }
        }
    }
    @objc func addBadgeChat (notification: NSNotification)
    {
         DispatchQueue.main.async {
        let tabBarController = self
        tabBarController.delegate = self
        if !(tabBarController.selectedIndex == 1) {
            if tabBarController.viewControllers![1].tabBarItem.badgeValue == nil {
               
                     tabBarController.viewControllers![1].tabBarItem.badgeValue = "1"
                
               
                if ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo != -1 {
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo += 1
                }else{
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo = 1
                }
                
            }else {
              
                tabBarController.viewControllers![1].tabBarItem.badgeValue = String(Int(tabBarController.viewControllers![1].tabBarItem.badgeValue!)! + 1 )
             
                if ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo != -1 {
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo += 1
                }else{
                    ((UIApplication.shared.delegate) as! AppDelegate).NotifTwo = 1
                }
                       
            }
        }
        }
    }
    @objc func DeleteBadgeChat (notification: NSNotification)
    {
         DispatchQueue.main.async {
        let tabBarController = self
        tabBarController.delegate = self
        
        if tabBarController.viewControllers![1].tabBarItem.badgeValue != nil {
             
            tabBarController.viewControllers![1].tabBarItem.badgeValue = nil
            
            
        }
        }
        
    }
    @objc func AddNotSee (notification: NSNotification)
    {
        let count = notification.object as! Int
        let tabBarController = self
        tabBarController.delegate = self
       
            DispatchQueue.main.async {
                 tabBarController.viewControllers![1].tabBarItem.badgeValue = "\(count)"
            }
            
        
        
    }
    
}
