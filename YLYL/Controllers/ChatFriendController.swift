//
//  ChatFriendController.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 21/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
class ChatFriendController: FixUIController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,JokesFragmentDelegate,GrowingTextViewDelegate
{
    func getJokesFromFragment(jokes: [String]) {
        self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            let settings : Parameters = [
                "firstUserId" : a["_id"].stringValue
            ]
            let headers : HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            print(settings)
            print(headers)
            var FirstAttempt = 0
            Alamofire.request(ScriptBase.sharedInstance.createRoom, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                if response.data != nil {
                    let janus = VideoRoomController()
                    
                    let data = JSON(response.data ?? Data())
                    janus.attachRoomToJanus(roomId: data["roomNumber"].numberValue, completionHandler: { json in
                        
                        let room = Room(id: data["roomId"].stringValue, roomNumber: data["roomNumber"].stringValue, firstUserId: data["roomFirstUserId"].stringValue, secondUserId: "")
                        SocketIOManager.sharedInstance.videoRoomRequestAccepted(completionHandler: { json in
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            print("Game accepted")
                            print(json)
                            if FirstAttempt == 0 {
                                FirstAttempt = 1
                                self.alert.dismiss(animated: true, completion: nil)
                                var i = 0
                                for view in (self.navigationController?.viewControllers)! {
                                    if view .isKind(of: VideoGameController.self) || view .isKind(of: VideoGameResultController.self) {
                                        self.navigationController?.viewControllers.remove(at: i)
                                    }
                                    i = i + 1
                                }
                                //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
                                //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
                                let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
                                vc.willAppear = true
                                vc.Owner = "master"
                                vc.FriendOrMatch = false
                                vc.roomId = NSNumber(value: Int(json[0])!)
                                vc.SecondUserId = self.contact!.getId()
                                vc.navigation = self.navigationController
                                //self.navigationController?.present(vc, animated: true, completion: nil)
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                                
                                //self.navigationController?.dismiss(animated: true, completion: nil)
                            }
                        })
                        SocketIOManager.sharedInstance.videoRoomRequestRefused(completionHandler: { json in
                            print("Game Refused")
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            let alertR = UIAlertController(title: Localization("ZonzayChallenge"), message: Localization("ZonzayFriendCancel"), preferredStyle: .alert)
                            self.navigationController?.dismiss(animated: true, completion: nil)
                            let ok = UIAlertAction(title: Localization("OK"), style: .default, handler: { alert in
                                
                            })
                            alertR.addAction(ok)
                            self.present(alertR, animated: true, completion: nil)
                            
                        })
                        SocketIOManager.sharedInstance.joinVideoRoomRequest(userId: a["_id"].stringValue, userName: a["userName"].stringValue, SecondUserId: self.contact!.getId(), VideoRoom: room.getRoomNumber(), gameType: GameType.Friend)
                        self.alert = UIAlertController(title: nil, message: Localization("PleaseWait"), preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: Localization("Cancel"), style: .cancel , handler: { (alert) in
                            SocketIOManager.sharedInstance.removeVideoRoomRequestAccepted()
                            SocketIOManager.sharedInstance.removeVideoRoomRequestRefused()
                            //self.navigationController?.dismiss(animated: true, completion: nil)
                        })
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        self.alert.addAction(cancelAction)
                        self.alert.view.addSubview(loadingIndicator)
                        self.navigationController!.present( self.alert, animated: true, completion: nil)
                        print("REQU1 : ",data)
                        //self.jokes = data
                    })
                    
                    
                }else{
                }
                
            }
            
        }catch {
            
        }
    }
    
    //Views
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var tableV: UITableView!
    @IBOutlet weak var defaultMessage: UILabel!
    @IBOutlet weak var inputTextField: PlaceholderTextView!
    @IBOutlet weak var userTargetName : UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewIsTyping: UIView!
    
    @IBOutlet weak var YouLBL: UILabel!
    
    @IBOutlet weak var InterestUser: UILabel!
    
    @IBOutlet weak var challengeBTN: UIButton!
    @IBOutlet weak var isTypingGif: UIImageView!
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var sendBTN : UIButton!
    @IBOutlet weak var bottomIsTyping: NSLayoutConstraint!
    var  soundPlayer : AVAudioPlayer!
    var videoLimit : String = ""
    @IBAction func chalengeAction(_ sender: UIButton) {
        let subscription = UserDefaults.standard.value(forKey: "Subscription") as! String
    if subscription == "false" && !(Int(videoLimit)! >= 5) {
            let alertR = UIAlertController(title: Localization("ZonzayChallenge"), message: Localization("ZonzayVideoMax"), preferredStyle: .alert)
            self.navigationController?.dismiss(animated: true, completion: nil)
            let ok = UIAlertAction(title: Localization("Subscribe"), style: .default, handler: { alert in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ZonzayPlusPayment") as! ZonzayPlusPayment
                vc.navigation = self.navigationController!
                vc.tabbar = self.tabBarController!
                self.navigationController?.pushViewController(vc, animated: true)
            })
            let cancel = UIAlertAction(title: Localization("Cancel"), style: .cancel, handler: nil)
            alertR.addAction(ok)
            alertR.addAction(cancel)
            self.present(alertR, animated: true, completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
            vc.delegate = self
            vc.isPlayingOrProfile = true
            //self.IndexUser = sender.indexPath
            self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBOutlet weak var UserIMG: RoundedUIImageView!
    @IBOutlet weak var UserNameTopLBL: UILabel!
    
    @IBOutlet weak var connectedIconIMG: UIImageView!
    var RoomQ : ChatRoom!
    var messages : [Message] = []
    var messagesJSON  = JSON()
    var Conversation = JSON()
    let barHeight: CGFloat = 50
    var user : User?
    var contact : User?
    @IBOutlet weak var bottomtable: NSLayoutConstraint!
    @IBOutlet weak var TypingName: UILabel!
    @IBAction func backTouchUpInside(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
       
        appDelegate.convRemoteNumbers[Conversation["_id"].stringValue]! = 0
         self.tabBarController?.setTabBarVisible(visible: true, duration: 0, animated: true)
        navigationController?.popViewController(animated: true)
        
        NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
    }
    @IBAction func plusTouchUpInside(_ sender: Any) {
        
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
       // SocketIOManager.sharedInstance.Typing(room: Room.getId(), userId: (user?.getId())!, contact: (contact?.getId())!)
    }
    func determineNSeenMessages(){
        var IdsMessageNSeen : OrderedDictionary <String,Bool> = [:]
        if self.messagesJSON.count != 0 {
            for i in 0...(self.messagesJSON.count) {
                if (self.messagesJSON[i]["expediteur"]["_id"].stringValue == contact!.getId() ) {
                    if self.messagesJSON[i]["etat"].stringValue == "Seen" {
                      
                    IdsMessageNSeen[self.messagesJSON[i]["_id"].stringValue] = true
                    }else{
                IdsMessageNSeen[self.messagesJSON[i]["_id"].stringValue] = false
                    }
                   
                }
            }
        }
        if IdsMessageNSeen.count != 0 {
            var  dict =  ""
            var i = 0
            for ( key ,_ ) in IdsMessageNSeen {
            if i == (IdsMessageNSeen.count - 1 ) {
                //dict = dict + "{\"messageId\" : \"\(vid)\"}"
                    dict = key
                }
                i = i + 1
            }
            print(IdsMessageNSeen)
            if IdsMessageNSeen[dict] == false {
                
            SocketIOManager.sharedInstance.seenMessage(sendData: dict)
            }
        }
        
        //messages
    }
    
    var lastIndexSender : Int = -1
    override func viewDidLoad() {
        self.bindToKeyboard()
        NavigationFieldToolbar.appearance().barStyle = .blackTranslucent
        NavigationFieldToolbar.appearance().backgroundColor = UIColor(red: 78/255, green: 73/255, blue: 52/255, alpha: 1)
        do {
        
        soundPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "message_beep", ofType: "mp3")!))
            soundPlayer.prepareToPlay()
        self.textView.nextNavigationField = self.textView
            self.textView.delegate = self
        }catch {
            
        }
        let userConnected = (UIApplication.shared.delegate as! AppDelegate).ConnectedUser
        if userConnected.contains(contact!.getId()) {
            self.connectedIconIMG.image = UIImage(named: "ConnectedIcon")
        }else{
             self.connectedIconIMG.image = UIImage(named: "Disconnectedicon")
            self.challengeBTN.isEnabled = false
        }
        if contact!.getProfileImage() != "" {
            self.UserIMG.setImage(with: URL(string: contact!.getProfileImage()))
        }
        super.viewDidLoad()
        textView.delegate = self
         NotificationCenter.default.addObserver(self, selector: #selector(self.handleReload(notification:)), name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: false)
       SocketIOManager.sharedInstance.getSeenMessages { (messages) in
            self.loadData(animated: false)
         SocketIOManager.sharedInstance.seenMessage(sendData: messages["_id"].stringValue)
        }
        self.customization()
        //self.userTargetName.text = contact!.getFirstName() + " " + contact!.getLastName()
        self.userTargetName.text = contact!.getUserName()
        if self.contact?.getPassionList() != "" {
            var resString = ""
            let resIntersts = self.contact!.getPassionList().components(separatedBy: ";")
            for res in resIntersts {
                resString  = resString + "#" + res
            }
            self.InterestUser.text = resString
        }else{
            self.InterestUser.isHidden = true
        }
        //self.inputTextField.delegate = self
        //self.inputTextField.keyboardAppearance = .dark
        self.configureIsTypingGif()
        //TypingName.text = contact!.getFirstName() + " " + contact!.getLastName() + " is Typing..."
        //self.inputTextField.attributedPlaceholder = NSAttributedString(string: Localization("TypeMessage"), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        self.textView.placeholder = Localization("TypeMessage")
        //self.inputTextField.placeholderText = Localization("TypeMessage")
       // self.title = contact!.getFirstName() + " " + contact!.getLastName()
        self.title = contact!.getUserName()
        self.view.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: self.view.bounds, andColors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1.0),UIColor(red: 6/255, green: 59/255, blue: 70/255, alpha: 1.0)])
        self.loadData(animated: false)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userTypingNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserStopTypingNotification(notification:)), name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: nil)
        self.tableV.keyboardDismissMode = .interactive
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        //self.inputBar.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
        //self.inputBar.layer.position = CGPoint(x:  self.inputBar.frame.width  / 2, y:  self.inputBar.frame.height)
        
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    @objc func handleReload(notification: NSNotification) {
        print("Reload Conversation")
        let userConnected = (UIApplication.shared.delegate as! AppDelegate).ConnectedUser
        if userConnected.contains(contact!.getId()) {
            self.connectedIconIMG.image = UIImage(named: "ConnectedIcon")
        }else{
            self.connectedIconIMG.image = UIImage(named: "Disconnectedicon")
            self.challengeBTN.isEnabled = false
        }
        
    }
    func configureIsTypingGif(){
        self.viewIsTyping.isHidden = true
          self.isTypingGif.setGifImage(UIImage(gifName: "Typing-animation.gif"))
        self.isTypingGif.startAnimatingGif()
    
    }
    var maxNumberOfLines : CGFloat = 5
    var expectedHeight: CGFloat = 40
    @IBOutlet weak var textView : GrowingTextView!
    @IBOutlet weak var heightContainerTextView: NSLayoutConstraint!
    
    @IBOutlet weak var bottomTextViewConst: NSLayoutConstraint!
    var font: UIFont? = UIFont(name: "Roboto-Regular", size: 16.0)
    var lastHeight : CGFloat = 40
    private func updateSize() {
        
        var maxHeight = CGFloat.greatestFiniteMagnitude
    
        if maxNumberOfLines > 0 {
            maxHeight = (ceil(font!.lineHeight) * maxNumberOfLines) + textView.textContainerInset.top + textView.textContainerInset.bottom
        }
        
        let roundedHeight = roundHeight()
        
        if roundedHeight >= maxHeight {
            expectedHeight = maxHeight
            textView.isScrollEnabled = true
        } else {
            expectedHeight = roundedHeight
            textView.isScrollEnabled = false
        }
       // heightContainerTextView.constant = expectedHeight > 40 ? expectedHeight + 10 : 50
       
        if self.lastHeight != (expectedHeight) {
            //self.inputBar.frame.size.height =
            self.textView.frame.size.height =  expectedHeight + 2.5
            //self.inputAccessoryView?.size.height = expectedHeight + 12.5
            self.heightContainerTextView.constant = expectedHeight + 12.5
            self.lastHeight = expectedHeight
            self.inputBar.layoutIfNeeded()
            self.textView.layoutIfNeeded()
            self.ensureCaretDisplaysCorrectly()
          /*  UIView.animate(withDuration: 0.1, animations: {
                self.inputBar.layoutIfNeeded()
                self.textView.layoutIfNeeded()
            }) { (verif) in
                self.ensureCaretDisplaysCorrectly()
            } */
        }
        
        print("expectedHeight : ",expectedHeight)
       
       // self.inputBar.setNeedsDisplay()
    
        //textViewDelegate?.textViewHeightChanged(textView: self, newHeight:expectedHeight)
        //textView.height
       
    }
    func textViewDidChange(_ textView: UITextView) {
        updateSize()
    }
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
       // self.heightContainerTextView.constant = 10 + height
      //  self.inputAccessoryView?.frame.size.height = self.heightContainerTextView.constant
        
    /*    print("did change height : ",self.heightContainerTextView.constant)
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.inputAccessoryView?.layoutIfNeeded()
            self.inputBar.layoutIfNeeded()
        }, completion: nil) */
        print("InputAccessory : ",self.inputAccessoryView?.size)
    }
    private func ensureCaretDisplaysCorrectly() {
        guard let range = textView.selectedTextRange else {
            return
        }
        
        DispatchQueue.main.async {
            let rect = self.textView.caretRect(for: range.end)
            UIView.performWithoutAnimation({ () -> Void in
                self.textView.scrollRectToVisible(rect, animated: false)
            })
        }
    }
    /**
     Calculates the correct height for the text currently in the textview as we cannot rely on contentsize to do the right thing
     */
    private func roundHeight() -> CGFloat {
        var newHeight: CGFloat = 0
        
        if let font = font {
            let attributes = [NSAttributedString.Key.font: font]
            let boundingSize = CGSize(width: textView.frame.size.width - textView.textContainerInset.left - textView.textContainerInset.right, height: .greatestFiniteMagnitude)
            let size = textView.text.boundingRect(with: boundingSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
            newHeight = ceil(size.height)
        }
        
        if let font = font, newHeight < font.lineHeight {
            newHeight = font.lineHeight
        }
        
        return newHeight + textView.textContainerInset.top + textView.textContainerInset.bottom
    }
    @objc func handleUserStopTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            var names = ""
            var totalTypingUsers = 0
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1]
            let roomId = typingUsersDictionary[0]
            print("userStopped:",typingUsersDictionary)
            if userId ==  self.contact?.getId()  && roomId == self.RoomQ.getId() {
                DispatchQueue.main.async {
                     if self.viewIsTyping.isHidden == false {
                        print("stooped ")
                self.viewIsTyping.isHidden = true
                    }
                }
            }
            
        }
    }
    @objc func handleUserTypingNotification(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let userId = typingUsersDictionary[1]
            let roomId = typingUsersDictionary[0]
            print("userTyping:",typingUsersDictionary)
            print("user Id =  ",userId, " ",self.contact?.getId() )
            print("room Id =  ",roomId, " ",self.RoomQ.getId() )

            print(" test One : ",userId == self.contact?.getId())
            print(" test Two : ",roomId == self.RoomQ.getId())
            if userId == self.contact?.getId()  && roomId == self.RoomQ.getId(){
                DispatchQueue.main.async {
                    print("started typing")
                    if self.viewIsTyping.isHidden {
                        
                     self.viewIsTyping.isHidden = false
                    }
                }
            
            }
            
            
        }
    }
   
    @IBAction func sendAction(_ sender: UIButton) {
        if let text = self.textView.text {
            if text.count > 0 {
                //self.sendMessageToSocket(text)
                self.sendMessages()
                //self.inputTextField.text = ""
            }
        }
    }
    func sendMessageToSocket(_ text : String) {
        SocketIOManager.sharedInstance.sendMessage(chatRoom: RoomQ.getId(), user: user!.getId(), contact: contact!.getId(), message: text)
        self.inputTextField.resignFirstResponder()
    }
    /*
     *send message action
     *
     */
    func sendMessages() {
        print ("heey")
        if textView.text!.trimmingCharacters(in: .whitespaces) != "" {
            self.sendBTN.isEnabled = false
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            SocketIOManager.sharedInstance.sendMessage(idNickname: a["_id"].stringValue, nickname: self.contact!.getId(), msg: self.textView.text!.trimmingCharacters(in: .whitespaces), conv: self.Conversation["_id"].stringValue,photo:a["userImageURL"].stringValue)
            self.sendBTN.isEnabled = true
            self.textView.text = ""
            self.defaultMessage.isHidden = true
            self.tableV.isHidden = false
            self.loadData(animated: true)
           
        }else{
            textView.text = ""
        }
    }
    @IBAction func showOptionsAction(_ sender: UIButton) {
       self.animeExtraButtons(toHide: false)
    }
    @IBAction func showMessage(_ sender:UIButton) {
        self.animeExtraButtons(toHide: true)
    }
    
    func animeExtraButtons(toHide : Bool) {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3){
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3){
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    var keyboardHeight : CGFloat = 0
    var intitalXPos : CGFloat = 0
    var initialYPos : CGFloat = 0
    @objc func showKeyboard(notification: Notification) {
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            print("Show Keyboard")
            let height = frame.cgRectValue.height
          
           // self.tableV.contentInset.bottom =  2
            //self.bottomIsTyping.constant = 8
            //self.tableV.scrollIndicatorInsets.bottom = height + 50
            //self.bottomConstraint.constant = height + 50
            //self.view.layoutIfNeeded()
          /*  if initialYPos == 0 {
             intitalXPos = self.view.frame.origin.x
             initialYPos = self.view.frame.origin.y
                print("Intial X Y : ",intitalXPos,initialYPos )
                 self.view.layer.anchorPoint = CGPoint(x: 0.5, y: 1)
            }
              let userInfo: NSDictionary = notification.userInfo! as NSDictionary
            let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
            let keyboardSize = keyboardInfo.cgRectValue.size
            print("keyboardSize.height: ",keyboardSize.height)
            
              keyboardHeight = keyboardSize.height + 16
           
            
            
            self.view.frame = CGRect(x: intitalXPos, y: initialYPos, width: self.view.frame.width, height: self.view.frame.height - (keyboardSize.height))
              print("Show Intial X Y : ",self.view.frame.origin.x,self.view.frame.origin.y )
            self.view.layoutIfNeeded()
           // self.view.layoutMargins = contentInsets
            //scrollView.contentInset = contentInsets
            //scrollView.scrollIndicatorInsets = contentInsets */
            if self.messagesJSON.count > 0 {
                self.tableV.scrollToBottomKeyboard()
            }
        }
    }
    @objc func hideKeyboard(notification: Notification) {
        
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
           print("HideKeyboard")
             print("keyboardSize.height: ",frame.cgRectValue.size.height)
            // self.view.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
             // self.view.frame = CGRect(x: intitalXPos, y: initialYPos, width: self.view.frame.width, height: self.view.frame.height + (keyboardHeight))
            // print("Hide Intial X Y : ",self.view.frame.origin.x,self.view.frame.origin.y )
          //  self.view.layoutIfNeeded()
            //self.bottomIsTyping.constant = 12
            //self.tableV.scrollIndicatorInsets.bottom = height + 50
            //self.bottomConstraint.constant = height + 50
            //self.view.layoutIfNeeded()
            
           
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.messagesJSON.count != 0 {
        return self.messagesJSON.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.frame.size = CGSize(width: (cell?.frame.size.width)!, height: (cell?.frame.size.height)! + 8)
        if indexPath.row == (self.messagesJSON.count - 1) {
            guard let indexPaths = self.tableV.indexPathsForVisibleRows else {
                return
            }
            print("Visibles : ",indexPaths)
            if indexPaths.contains(indexPath) {
                
                 print("OUT OF BOX")
            }
            print("OUT OF CELLS")
            
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var sense = ""
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let a =  JSON(data: dataFromString!)
        if self.messagesJSON[indexPath.row]["expediteur"]["_id"].stringValue == a["_id"].stringValue {
            sense = "left"
        }else{
            sense = "right"
        }
        switch sense {
        case "right":
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            
           
            
            cell.message.text = self.messagesJSON[indexPath.row]["message"].stringValue
            return cell
        case "left" :
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            cell.message.text = self.messagesJSON[indexPath.row]["message"].stringValue
            if self.lastIndexSender != -1 {
            if indexPath.row == self.lastIndexSender {
                if self.messagesJSON[indexPath.row]["etat"].stringValue == "NSeen" && self.messagesJSON[indexPath.row]["expediteur"]["_id"].stringValue != contact!.getId() {
                    print("Not SEEN")
                    cell.sentLBL.text = Localization("Sent")
                    print("willDisplayNotSeen")
                    cell.sentLBL.isHidden = false
                    
                    cell.checkMark.isHidden = true
                }
                if self.messagesJSON[indexPath.row]["etat"].stringValue == "Seen" && self.messagesJSON[indexPath.row]["expediteur"]["_id"].stringValue != contact!.getId() {
                    print("SEEN")
                    
                    cell.sentLBL.text = Localization("Seen")
                    cell.sentLBL.isHidden = false
                    cell.checkMark.isHidden = false
                }
            }else{
                cell.sentLBL.isHidden = true
                cell.checkMark.isHidden = true
            }
            }
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
   
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        print("self.tableV.contentInset.bottom : ",self.tableV.contentInset.bottom)
        print("self.tableV.contentInset.bottom : ",self.inputBar.size.height)
        // self.tableV.contentInset.bottom = self.inputBar.size.height + 300
        self.tableV.scrollToBottom(animated: true)
        SocketIOManager.sharedInstance.sendStartTypingMessage(Conv: Conversation["_id"].stringValue , nickname: a["_id"].stringValue)
        return true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.stopmessage(Conv: Conversation["_id"].stringValue , nickname: a["_id"].stringValue)
        return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        SocketIOManager.sharedInstance.getChatMessage { (messageInfo) -> Void in
            DispatchQueue.main.async(execute: { () -> Void in
                if messageInfo[0]["userOneId"].stringValue == self.contact?.getId() && messageInfo[0]["userTwoId"].stringValue == a["_id"].stringValue {
                self.defaultMessage.isHidden = true
                self.tableV.isHidden = false
                
                print("*******************************")
                print(messageInfo as JSON)
                let s = messageInfo as JSON
                var x = " { \"id\" : 400, "
                x = x + " \"etat\" : \"NSeen\" ,"
                x = x + " \"message\" : \"\(messageInfo[0]["message"].stringValue)\","
                x = x + "\"conversation\" : {} ,"
                x = x + "\"expediteur\" : { "
                x = x + "\"_id\" : \"\(messageInfo[0]["userOneId"].stringValue)\" , \"last_message_date\" : \"\(messageInfo[0]["currentDateTime"].stringValue)\" },"
                x = x + "\"sender\" : { "
                x = x + "\"_id\" : \"\(messageInfo[0]["userTwoId"].stringValue)\" , \"last_message_date\" : \"\(messageInfo[0]["currentDateTime"].stringValue)\" },"
                x = x + " \"date\" : \"\""
                x = x + "}"
                print("9bal : ", x)
                let p = JSON.parse(x)
                if messageInfo[0]["userOneId"].stringValue != a["_id"].stringValue {
                    self.soundPlayer.play()
                }
                // print("saifMessage ",p.description)
                self.loadData(animated: false)
               
                }
            })
        }
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.inputBar.backgroundColor = UIColor.clear
        //self.view.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showKeyboard(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideKeyboard(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        SocketIOManager.sharedInstance.stopReceivingMessages()
        SocketIOManager.sharedInstance.stopNewChatMessage()
    }
    func customization() {
        self.tableV.delegate = self
        self.tableV.dataSource = self
        self.tableV.estimatedRowHeight = self.barHeight
        self.tableV.rowHeight = UITableView.automaticDimension
        self.tableV.contentInset.bottom = 16
    }
func loadData(animated:Bool){
    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
    var a = JSON(data: dataFromString!)
        
        let settings : Parameters = [
            "userId" : a["_id"].stringValue,
            "convId" : self.Conversation["_id"].stringValue
        ]
        let headers : HTTPHeaders = [
            "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
        ]
    Alamofire.request(ScriptBase.sharedInstance.getAllMessages  , method: .post, parameters: settings, encoding: JSONEncoding.default,headers : headers)
    .responseJSON { response in
    let b = JSON(response.data)
    print("Message:",b)
    let c = JSON(b.arrayObject)
    
    if c.count != 0 {
    if !c["message"].exists() {
    self.defaultMessage.isHidden = true
    self.tableV.isHidden = false
    self.messagesJSON = c
        for i in 0...(self.messagesJSON.count - 1) {
            if self.messagesJSON[i]["expediteur"]["_id"].stringValue == a["_id"].stringValue {
                self.lastIndexSender = i
            }
        }
    //print("count:",self.Messages.count)
        
    self.tableV.reloadData()
    if animated {
        self.tableV.scrollToRow(at: IndexPath(row: self.messagesJSON.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: true)
    }
    else{
          self.determineNSeenMessages()
        self.tableV.scrollToRow(at: IndexPath(row: self.messagesJSON.count - 1, section: 0), at: UITableView.ScrollPosition.bottom, animated: false)
    }
        }
    }
    else {
    self.defaultMessage.isHidden = false
    self.tableV.isHidden = true
    }
    
    
    }
    
    }
    @objc func keyboardWillChange(notification: Notification) {
        
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let curFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let deltaY = targetFrame.origin.y - curFrame.origin.y
        
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.view.origin.y+=deltaY
            
        },completion: nil)
        
    }
}
extension UITableView {
    func scrollToBottomKeyboard(animated: Bool = true, scrollPostion: UITableView.ScrollPosition = .bottom) {
        let no = self.numberOfRows(inSection: 0)
        if no > 0 {
            let index = IndexPath(row: no - 1, section: 0)
            scrollToRow(at: index, at: scrollPostion, animated: animated)
        }
    }
}
extension ChatFriendController{
    func bindToKeyboard(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
 
}
