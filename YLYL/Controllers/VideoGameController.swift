//
//  VideoGameController.swift
//  YLYL
//
//  Created by macbook on 1/17/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//
import UIKit
class VideoGameController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let image : UIImageView = UIImageView()
        image.image?.rotate(radians: 90)
    }
}
/*import Foundation

import SwiftyJSON
import GoogleMobileVision */

/*class VideoGameController: UIViewController,RTCPeerConnectionDelegate,WebSocketDelegate,RTCEAGLVideoViewDelegate,BWHorizontalTableViewDelegate,BWHorizontalTableViewDataSource,SRCountdownTimerDelegate,RTCVideoRenderer {
    @IBOutlet weak var widthVideos: NSLayoutConstraint!
    var FriendOrMatch = false
    var TimeGame : Float = 20.0
    func setSize(_ size: CGSize) {
        
    }
    /*
     *
     *Our detector
     */
    var detector = GMVDetector(ofType: GMVDetectorTypeFace, options: [
        GMVDetectorFaceLandmarkType : GMVDetectorFaceLandmark.all.rawValue,
        GMVDetectorFaceClassificationType : GMVDetectorFaceClassification.all.rawValue,
        GMVDetectorFaceTrackingEnabled : false
        
        ])
    /*
     *
     *Detect Face method
     */
    func detectFace(frame:RTCVideoFrame?) {
        if frame != nil {
           
            
            if let i420 = frame?.newI420() {
                
                //let tempFrame = RTCVideoFrame(pixelBuffer: (i420.nativeHandle), rotation: ._90, timeStampNs: (i420.timeStampNs))
                //photoOutput.capturePhoto(with: photoSettings, delegate: self)
                
                let image = CIImage(cvPixelBuffer: self.convert(frame:i420)!)
                
                let finalImage = UIImage(ciImage: image)
                guard let finalImageRotated = finalImage.rotate(radians: (.pi / 2)) else {
                    return
                }
              /*  DispatchQueue.main.async { [unowned self] in
                    //self.delegate?.captured(image: uiImage)
                    let imageView  = UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame
                        .width, height: self.view.frame.height))
                    imageView.image = finalImageRotated
                    self.view.addSubview(imageView)
                    self.view.bringSubviewToFront(imageView)
                } */
                    var detected = false
                    //let vision =
                
                    let faces = self.detector?.features(in: finalImageRotated, options: nil)  as? [GMVFaceFeature]
                    
                    for face in faces! {
                        self.FaceUserX = face.leftEyePosition.x
                        self.FaceUserY = face.leftEyePosition.y
                        if (face.hasSmilingProbability) {
                            detected = true
                            self.smileProba = Float(face.smilingProbability)
                            
                            if face.smilingProbability > 0.5 {
                                print("Smiling...")
                            }
                        }
                    }
                    if faces?.count == 0 {
                        print("Scan Failed: Found nothing to scan")
                        self.FaceUserX = 0
                        self.FaceUserY = 0
                        self.smileProba = -3.0
                    }else if detected == false{
                        self.FaceUserX = 0
                        self.FaceUserY = 0
                        print("No Faces Detected")
                        self.smileProba = -2.0
                    }else{
                        print("detected")
                    }
                }
        }
    }
    func renderFrame(_ frame: RTCVideoFrame?) {
        i = i + 1
        if i % 60 == 0 && isControllerWorking{
            self.detectFace(frame: frame)
        }
    }
    var i = 0
    var isControllerWorking = true
    var timerProba : Timer? = nil
    var smileProba : Float = 0.0
    var getSmile = true
    var getSmiled = true
    @IBOutlet weak var TimeLabel: UILabel!
    @objc func handleSmileProba(timer: Timer){
        if isControllerWorking {
        let smile = Int(self.smileProba * 100)
        if ((smile < 10 && smile > 0) || smile == 0) {
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
            self.userNotFound.isHidden = true
            self.getSmile = true
            jokeBar.setIntensity(1)
        }else if (smile >= 10 && smile <= 20){
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
             self.userNotFound.isHidden = true
            self.getSmile = true
            jokeBar.setIntensity(2)
        }else if (smile > 20 && smile <= 40) {
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
            self.userNotFound.isHidden = true
            self.getSmile = true
            jokeBar.setIntensity(3)
        }else if (smile > 40 && smile < 60) {
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
            self.userNotFound.isHidden = true
            self.getSmile = true
            jokeBar.setIntensity(4)
        }else if (smile >= 60) {
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
            if getSmiled {
                self.getSmiled = false
                 self.changeLife()
            SocketIOManager.sharedInstance.NotifyChangeLife(userId: self.SecondUserId)
                if detectionSmileSleeper != nil {
                    detectionSmileSleeper.invalidate()
                    detectionSmileSleeper = nil
                }
                detectionSmileSleeper = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { time in
                   self.getSmiled = true
                })
            }
            self.userNotFound.isHidden = true
            jokeBar.setIntensity(5)
        }else if (smile == -300 ) {
            jokeBar.setIntensity(1)
            self.getSmiled = true
            if detectionSmileSleeper != nil {
                detectionSmileSleeper.invalidate()
                detectionSmileSleeper = nil
            }
           self.userNotFound.isHidden = false
            if getSmile {
                self.getSmile = false
            if detectionFaceSleeper != nil {
                detectionFaceSleeper.invalidate()
                detectionFaceSleeper = nil
            }
            detectionFaceSleeper = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { time in
                 self.changeLife()
                SocketIOManager.sharedInstance.NotifyChangeLife(userId: self.SecondUserId)
                self.getSmile = true
            })
            }
        }
        }
    }
    func changeLife(){
        if lifeThree.isHidden {
            if lifeTwo.isHidden {
                if lifeOne.isHidden{
                }else{
                     lifeOne.isHidden = true
                    print("You loose")
                    self.lifeOneTime = (self.recordTime) * 1000
                    self.LoseGame()
                }
            }else{
                 lifeTwo.isHidden = true
                self.lifeTwoTime = (self.recordTime) * 1000
            }
        }else{
            lifeThree.isHidden = true
            self.lifeThreeTime = (self.recordTime) * 1000
            
        }
    }
    func changeRemoteLife(){
        if lifeThreeRemote.isHidden {
            if lifeTwoRemote.isHidden {
                if lifeOneRemote.isHidden{
                }else{
                    lifeOneRemote.isHidden = true
                }
            }else{
                lifeTwoRemote.isHidden = true
            }
        }else{
            lifeThreeRemote.isHidden = true
        }
    }
    /*
     *
     *Convert Frame to PixelBuffer so then we convert that to UIImage
     */
    func convert(frame:RTCVideoFrame) -> CVPixelBuffer?{
        let fLock = NSLock()
            fLock.lock()
        let planeSize = calculatePlaneSize(forFrame: frame)
        let yPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.ySize)
        let uPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.uSize)
        let vPlane = UnsafeMutablePointer<GLubyte>.allocate(capacity: planeSize.vSize)
        memcpy(yPlane, frame.dataY, planeSize.ySize)
        memcpy(uPlane, frame.dataU, planeSize.uSize)
        memcpy(vPlane, frame.dataV,  planeSize.vSize)
        let width = frame.width
        let height = frame.height
        var pixelBuffer: CVPixelBuffer? = nil
        var err: CVReturn;
        err = CVPixelBufferCreate(kCFAllocatorDefault, Int(width), Int(height), kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange, nil, &pixelBuffer)
        if (err != 0) {
            NSLog("Error at CVPixelBufferCreate %d", err)
            return nil
        }
        if let pixelBuffer = pixelBuffer {
            CVPixelBufferLockBaseAddress(pixelBuffer, .readOnly)
            let yPlaneTo = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 0)
            memcpy(yPlaneTo, yPlane, planeSize.ySize)
            
            let uvRow: Int = planeSize.uSize*2/Int(width)
            
            let halfWidth: Int = Int(width)/2
            
            if let uPlaneTo = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer, 1) {
                let uvPlaneTo = uPlaneTo.bindMemory(to: GLubyte.self, capacity: Int(uvRow*halfWidth*2))
                
                for i in 0..<uvRow {
                    for j in 0..<halfWidth {
                        let dataIndex: Int = Int(i) * Int(halfWidth) + Int(j)
                        let uIndex: Int = (i * Int(width)) + Int(j) * 2
                        let vIndex: Int = uIndex + 1
                        
                        uvPlaneTo[uIndex] = uPlane[dataIndex]
                        uvPlaneTo[vIndex] = vPlane[dataIndex]
                    }
                }
            }
            
        }
        fLock.unlock()
        if pixelBuffer != nil {
            return pixelBuffer
        }
        return nil
        
    }
    /*
     *
     *Util for conver frame to PixelBuffer
     */
    fileprivate func calculatePlaneSize(forFrame frame: RTCVideoFrame)
        -> (ySize: Int, uSize: Int, vSize: Int){
            let baseSize = Int(frame.width * frame.height) * MemoryLayout<GLubyte>.size
            return (baseSize, baseSize / 4, baseSize / 4)
    }
    @IBOutlet weak var tableH : BWHorizontalTableView!
    @IBOutlet weak var LoadingView: SRCountdownTimer!
    @IBOutlet weak var ContainerLoaderView: UIView!
    @IBOutlet weak var lifeOne : UIImageView!
    var lifeOneTime : Float = 0
    @IBOutlet weak var lifeTwo : UIImageView!
    var lifeTwoTime : Float = 0
    @IBOutlet weak var lifeThree : UIImageView!
    var lifeThreeTime : Float = 0
    @IBOutlet weak var lifeOneRemote: UIImageView!
    @IBOutlet weak var lifeTwoRemote: UIImageView!
    @IBOutlet weak var lifeThreeRemote: UIImageView!
    @IBOutlet weak var userNotFound: UIImageView!
    @IBOutlet weak var jokeBar: PhasedSeekBar!
    private let kARDMediaStreamId = "ARDAMS"
    private let kARDAudioTrackId = "ARDAMSa0"
    private let kARDVideoTrackId = "ARDAMSv0"
    var factory: RTCPeerConnectionFactory?
    @IBOutlet weak  var localView: RTCCameraPreviewView!
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    var websocket: WebSocketChannel?
    var peerConnectionDict: [AnyHashable : Any] = [:]
    var publisherPeerConnection: RTCPeerConnection?
    var localTrack: RTCVideoTrack?
    var localAudioTrack: RTCAudioTrack?
    var height: Int = 0
    var Owner = "master"
    var selectedJokes : [String] = []
    var roomId : NSNumber!
    var SecondUserId: String!
    var detectionFaceSleeper: Timer!
    var detectionSmileSleeper : Timer!
    var streamCounter : Timer!
    var recordTime : Float = 0
    var PathMaser : String!
    var FaceUserX : CGFloat = 0
    var FaceUserY : CGFloat = 0
    var winner : Bool = false
    var dataToSend : [String: Any] = [:]
    var navigation : UINavigationController? = nil
    @objc func listenFromResult(_ notification :Notification){
        self.dismiss(animated: true){
            NotificationCenter.default.removeObserver(self, name: Notification.Name.init("Dissmiss"), object: nil)
               NotificationCenter.default.post(name: Notification.Name.init("DissmissJoke"), object: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.listenFromResult(_:)), name: Notification.Name.init("Dissmiss"), object: nil)
        self.TimeLabel.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.StreamBegin(_:)), name: NSNotification.Name(rawValue: "NSNotifMediaBegin"), object: nil)
        SocketIOManager.sharedInstance.StopGameNotifier { json in
            //I am the winner
            self.winner = true
            
            self.GameOver(isBefore: true, winner: GameWinner.masterLoser)
        }
        SocketIOManager.sharedInstance.StopGameNotifierAtEnd { json in
            
            self.GameOver(isBefore: false, winner: self.calculateTheWinner())
        }
        
        PathMaser = self.randomStringWithLength()
        PathMaser = PathMaser.replacingOccurrences(of: " ", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: ":", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: "+", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: "-", with: "")
        PathMaser = PathMaser.replacingOccurrences(of: ".", with: "")
        self.prepareSockets()
        self.tabBarController?.setTabBarVisible(visible: false, duration: 0, animated: true)
        tableH.delegate = self
        tableH.dataSource = self
        tableH.reloadData()
        tableH.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        tableH.delegate?.horizontalTableView!(tableH, didSelectColumnAt: IndexPath(row: 0, section: 0))
    }
    var willAppear = true
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if willAppear {
            self.isControllerWorking = false
            timerProba =  Timer.scheduledTimer(timeInterval: 0.004, target: self, selector: #selector(self.handleSmileProba(timer:)), userInfo: nil, repeats: true)
            LoadingView.delegate = self
            LoadingView.timerFinishingText = "Let's Go..."
            LoadingView.labelTextColor = .white
            LoadingView.labelFont = UIFont(name: "Arial-BoldMT", size: 40)
            LoadingView.start(beginingValue: 5, interval: 1)
        }
    }
   
    func timerDidEnd() {
        //let url = URL(string: "ws://zonzay.zapto.org:8188")
        
        let url = URL(string: ScriptBase.janus_URI)
        if let url = url {
            websocket = WebSocketChannel(url: url,number:self.roomId)
        }
        websocket!.delegate = self
        self.remoteView.delegate = self
        peerConnectionDict = [AnyHashable : Any]()
        factory = RTCPeerConnectionFactory()
        localTrack = createLocalVideoTrack()
        localAudioTrack = createLocalAudioTrack()
        self.LoadingView.isHidden = true
        self.ContainerLoaderView.isHidden = true
        self.TimeLabel.isHidden = false
       
    }
    @objc func StreamBegin(_ notification: Notification){
        streamCounter = Timer.scheduledTimer(withTimeInterval: 0.001, repeats: true, block: { _ in
            self.recordTime = self.recordTime + 0.001
            
            if self.TimeGame - self.recordTime == 5.0 || self.TimeGame - self.recordTime == 4.0 || self.TimeGame - self.recordTime == 3.0 || self.TimeGame - self.recordTime == 2.0 || self.TimeGame - self.recordTime == 1.0 {
            }
            if self.recordTime >= self.TimeGame {
                self.streamCounter.invalidate()
    
                self.determineTheWinner()
            }else if self.recordTime <= self.TimeGame - 5 {
                self.TimeLabel.text = String(round(self.TimeGame - self.recordTime))
                 self.TimeLabel.textColor = UIColor.white
            }else{
                 self.TimeLabel.text = String(round(self.TimeGame - self.recordTime))
                if  (self.TimeGame - 5)...(self.TimeGame - 4) ~= self.recordTime  {
                self.TimeLabel.textColor = UIColor.white
                }else if  (self.TimeGame - 4)...(self.TimeGame - 3) ~= self.recordTime{
                    self.TimeLabel.textColor = UIColor.red
                }else if  (self.TimeGame - 2)...(self.TimeGame - 1) ~= self.recordTime {
                    self.TimeLabel.textColor = UIColor.white
                }
            }
        })
    }
    func determineTheWinner(){
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            if self.Owner == "master" {
            //   SocketIOManager.sharedInstance.LoseGame(PathM: PathMaser, faceUserX: FaceUserX.description, faceUserY: FaceUserY.description, timeDetection: String(self.recordTime * 1000), receiverSlave: self.SecondUserId,userMaster: a["_id"].stringValue, gamemode: 60, lifeOne: String(self.lifeOneTime), lifeTwo: String(self.lifeTwoTime), lifeThree: String(self.lifeThreeTime), scoreOneLocal: self., scoreTwoLocal: <#Int#>)
            }
        }catch{
            
        }
        switch countHearts() {
        case .draw:
            self.GameOver(isBefore: false, winner: GameWinner.draw)
        case .masterWinner:
             self.GameOver(isBefore: false, winner: GameWinner.masterWinner)
        case .masterLoser:
            
            self.GameOver(isBefore: false, winner: GameWinner.masterLoser)
        }
        
    }
    func calculateTheWinner() -> GameWinner{
        switch countHearts() {
        case .draw:
            return .draw
        case .masterWinner:
            return .masterLoser
        case .masterLoser:
            
        return .masterWinner
        }
    }
    func countHearts() -> GameWinner{
        var local  = 3
        var remote = 3
        if self.lifeThree.isHidden {
            local -=  1
        if self.lifeTwo.isHidden {
                local -= 1
                if self.lifeOne.isHidden {
                    local -= 1
                }
            }
            
        }
        if self.lifeThreeRemote.isHidden {
            remote -=  1
            if self.lifeTwoRemote.isHidden {
                remote -= 1
                if self.lifeOneRemote.isHidden {
                    remote -= 1
                }
            }
            
        }
        if local == remote {
            return GameWinner.draw
        }else if local > remote{
           return GameWinner.masterWinner
        }else{
            return GameWinner.masterLoser
        }
      


    }
    func randomStringWithLength() -> String{
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = 8
        let date = Date()
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0...(len - 1){
            let length = UInt32(letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
            
        }
        return "ios" + (randomString as String) + date.description
    }
    func LoseGame(){
        
        let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
          //  SocketIOManager.sharedInstance.LoseGame(PathM: PathMaser, faceUserX: FaceUserX.description, faceUserY: FaceUserY.description, timeDetection: String(self.recordTime * 1000), receiverSlave: self.SecondUserId,userMaster: a["_id"].stringValue, gamemode: 0, lifeOne: String(self.lifeOneTime), lifeTwo: String(self.lifeTwoTime), lifeThree: String(self.lifeThreeTime))
            self.GameOver(isBefore: true,winner: GameWinner.masterLoser)
        }catch{
            
        }
    }
    func GameOver(isBefore:Bool,winner:GameWinner){
        if self.detectionFaceSleeper != nil {
            self.detectionFaceSleeper.invalidate()
            self.detectionFaceSleeper = nil
            print("detectionFaceSleeper nil")
        }
        if self.detectionSmileSleeper != nil {
            self.detectionSmileSleeper.invalidate()
            self.detectionSmileSleeper = nil
            print("detectionSmileSleeper nil")
        }
        if timerProba != nil {
            self.timerProba?.invalidate()
            self.timerProba = nil
            print("timerProba nil")
        }
        if streamCounter != nil{
            self.streamCounter.invalidate()
            self.streamCounter = nil
            print("streamCounter nil")
        }
      
            addFiveToJanus(isBefore:isBefore,winner: winner)
        
    }
    func addFiveToJanus(isBefore:Bool,winner:GameWinner){
        print("One")
        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) {time in
            print("Five")
            if isBefore {
            if self.winner {
                
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false) 
                do {
                    let a = try JSON(data: dataFromString!)
                    self.dataToSend =  [
                    "PathMaster" : self.PathMaser,
                    "receiverSlave" : a["_id"].stringValue,
                    "userMaster" : self.SecondUserId,
                    "winnerResult" : GameWinner.masterLoser,
                    "who" : "slave",
                    "isBefore" : isBefore
                    
                ]
                    
                    
                }catch{
                    
                }
            }else{
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                self.dataToSend = [
                    "winnerResult" : winner,
                    "receiverSlave" : self.SecondUserId,
                    "userMaster" : a["_id"].stringValue,
                    "who" : "master"
                ]
                }catch{
                    
                }
                }
            }else{
                if self.Owner != "master" {
                    
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                        self.dataToSend =  [
                            "PathMaster" : self.PathMaser,
                            "receiverSlave" : a["_id"].stringValue,
                            "userMaster" : self.SecondUserId,
                            "winnerResult" : winner,
                            "isBefore" : isBefore,
                            "who" : "slave"
                            
                        ]
                        
                        
                    }catch{
                        
                    }
                }else{
                    let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                    
                    let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                    do {
                        let a = try JSON(data: dataFromString!)
                    self.dataToSend = [
                     "winnerResult" : winner,
                     "who" : "master",
                     "isBefore" : isBefore,
                     "receiverSlave" : self.SecondUserId,
                     "userMaster" : a["_id"].stringValue,
                    ]
                    }catch{
                        
                    }
                }
            }
            var i = 0
            for key in self.peerConnectionDict.keys {
                var jc: JanusConnection? = nil
                
                jc = self.peerConnectionDict[key] as? JanusConnection
                if i == 0 && self.Owner == "master" {
                
                    self.onLeaving(jc?.handleId)
                    
                }else if i == 1 && self.Owner == "slave" {
                     self.onLeaving(jc?.handleId)
                }
                    i = i + 1
            }
            self.publisherPeerConnection?.close()
            self.isControllerWorking = false
            self.recordTime = 0.0
            self.performSegue(withIdentifier: "push_finished", sender: self)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "push_finished" {
            (segue.destination as! VideoGameResultController).results = dataToSend
            (segue.destination as! VideoGameResultController).FriendOrMatch = self.FriendOrMatch
            (segue.destination as! VideoGameResultController).Owner = self.Owner
            (segue.destination as! VideoGameResultController).roomId = self.roomId
           // (segue.destination as! VideoGameResultController).lifeOneTime = self.lifeOneTime
           //gue.destination as! VideoGameResultController).lifeThreeTime = self.lifeThreeTime
          //  (segue.destination as! VideoGameResultController).navigation = self.navigation
            resetView()
        }
    }
    func resetView(){
        self.willAppear = false
        self.LoadingView.isHidden = false
        self.ContainerLoaderView.isHidden = false
        self.TimeLabel.isHidden = true
        websocket?.closeConnection()
        websocket?.delegate = nil
        self.websocket = nil
        self.remoteView.delegate = nil
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NSNotifMediaBegin"), object: nil)
        SocketIOManager.sharedInstance.StopGameNotifierClose()
        SocketIOManager.sharedInstance.StopGameNotifierAtEndClose()
        SocketIOManager.sharedInstance.LifeChangedNotifierStop()
        self.tableH.delegate = nil
        self.tableH.dataSource = nil
        if localView.captureSession.isRunning {
            localView.captureSession.stopRunning()
        }
    }
    func prepareSockets(){
        SocketIOManager.sharedInstance.LifeChangedNotifier { json in
            self.changeRemoteLife()
        }
    }
    
    func numberOfSections(in tableView: BWHorizontalTableView!) -> Int {
        return 1
    }
    
    func horizontalTableView(_ tableView: BWHorizontalTableView!, numberOfColumnsInSection section: Int) -> Int {
        return self.selectedJokes.count
    }
    
    func horizontalTableView(_ tableView: BWHorizontalTableView!, cellForColumnAt indexPath: IndexPath!) -> BWHorizontalTableViewCell! {
        var cell  = tableView.dequeueReusableCell(withIdentifier: "HorizontalTableViewCell") as? JokeBarViewCell
        if cell == nil {
            cell = JokeBarViewCell(reuseIdentifier: "HorizontalTableViewCell")
            cell?.selectionStyle = UITableViewCell.SelectionStyle.blue
            cell?.RoundedView.clipsToBounds = true
            cell?.JokeLabel.text = self.selectedJokes[indexPath.row]
            cell?.RoundedView.layer.cornerRadius = (cell?.RoundedView.frame.width)! / 2
            cell?.RoundedView.layer.borderWidth = 1
            cell?.RoundedView.layer.borderColor = UIColor(hexString: "1FA5C2")?.cgColor
            cell?.RoundedView.layer.backgroundColor = UIColor.white.cgColor
        }
        return cell
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, didSelectColumnAt indexPath: IndexPath!) {
        print("selected:",indexPath.row)
        
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForColumnAt indexPath: IndexPath!) -> CGFloat {
        return 70.0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, widthForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, viewForHeaderInSection section: Int) -> UIView! {
        let H = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return H
    }
    func horizontalTableView(_ tableView: BWHorizontalTableView!, viewForFooterInSection section: Int) -> UIView! {
        let H = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        return H
    }
    func videoView(_ videoView: RTCEAGLVideoView, didChangeVideoSize size: CGSize) {
        var rect: CGRect = videoView.frame
        rect.size = size
        print(String(format: "========didChangeVideoSize %fx%f", size.width, size.height))
        for layer in videoView.subviews {
            print(layer)
            layer.layer.contentsGravity = .resizeAspect
        }
        videoView.contentMode = .redraw
        
    }
    
    func onPublisherJoined(_ handleId: NSNumber!) {
        offerPeerConnection(handleId)
    }
    func onPublisherRemoteJsep(_ handleId: NSNumber!, dict jsep: [AnyHashable : Any]!) {
        let jc: JanusConnection = peerConnectionDict[handleId] as! JanusConnection
        let answerDescription = RTCSessionDescription(fromJSONDictionary: jsep)
        jc.connection.setRemoteDescription(answerDescription!, completionHandler: { error in
        })
    }
    
    func subscriberHandleRemoteJsep(_ handleId: NSNumber!, dict jsep: [AnyHashable : Any]!) {
        let peerConnection: RTCPeerConnection? = createPeerConnection()
        let jc = JanusConnection()
        jc.connection = peerConnection
        jc.handleId = handleId
        peerConnectionDict[handleId] = jc
        let answerDescription = RTCSessionDescription(fromJSONDictionary: jsep)
        peerConnection?.setRemoteDescription(answerDescription!, completionHandler: { error in
        })
        let mandatoryConstraints = ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        peerConnection?.answer(for: constraints, completionHandler: { sdp, error in
            peerConnection?.setLocalDescription(sdp!, completionHandler: { error in
            })
            self.websocket!.subscriberCreateAnswer(handleId, sdp: sdp, number: self.roomId)
        })
        
    }
    
    func onLeaving(_ handleId: NSNumber!) {
        let jc: JanusConnection? = peerConnectionDict[handleId] as? JanusConnection
        jc?.connection.close()
        jc?.connection = nil
        var videoTrack: RTCVideoTrack? = jc?.videoTrack
        videoTrack?.remove((jc?.videoView)!)
        videoTrack = nil
        peerConnectionDict.removeValue(forKey: handleId)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        //here we will set our messages for the user where maybe the janus connection will be lost
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        var janusConnection: JanusConnection?
        for key in peerConnectionDict.keys {
            var jc: JanusConnection? = nil
            
            jc = peerConnectionDict[key] as? JanusConnection
            if peerConnection == jc?.connection {
                janusConnection = jc
                break
            }
        }
        
        DispatchQueue.main.async(execute: {
            print("Stream Count: ",stream.videoTracks.count)
            if stream.videoTracks.count != 0 {
                let remoteVideoTrack = stream.videoTracks[0]
                let frame = self.createRemoteView()?.frame
                let remoteView: RTCEAGLVideoView? = self.createRemoteView()
                if let remoteView = remoteView {
                    remoteVideoTrack.add(remoteView)
                }
                janusConnection?.videoTrack = remoteVideoTrack
                janusConnection?.videoView = remoteView
                self.remoteView.frame = frame!
                NotificationCenter.default.post(name: NSNotification.Name(rawValue : "NSNotifMediaBegin"), object: nil)
                self.isControllerWorking = true
                self.SetSessionPlayerOn()
            }
        })
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("=========didRemoveStream")
        
    }
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        print("=========didGenerateIceCandidate==\(candidate.sdp)")
        var handleId: NSNumber?
        for key in peerConnectionDict.keys {
        var jc: JanusConnection? = nil
            jc = peerConnectionDict[key] as? JanusConnection
            if peerConnection == jc?.connection {
                handleId = jc?.handleId
                break
            }
        }
        if candidate != nil {
            websocket!.trickleCandidate(handleId, candidate: candidate)
        } else {
            websocket!.trickleCandidateComplete(handleId)
        }
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("=========didRemoveIceCandidates")
    }
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    func createRemoteView() -> RTCEAGLVideoView? {
        return self.remoteView
    }
    func createPublisherPeerConnection() {
        publisherPeerConnection = createPeerConnection()
        _ = createAudioSender(publisherPeerConnection)
        _ = createVideoSender(publisherPeerConnection)
        
    }
    func defaultPeerConnectionConstraints() -> RTCMediaConstraints? {
        let optionalConstraints = ["DtlsSrtpKeyAgreement": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: optionalConstraints)
        return constraints
    }
    func defaultSTUNServer() -> RTCIceServer? {
        let array = ["turn:numb.viagenie.ca"]
        return RTCIceServer(urlStrings: array, username: "webrtc@live.com", credential: "muazkh")
    }
    func createPeerConnection() -> RTCPeerConnection? {
        let constraints: RTCMediaConstraints? = defaultPeerConnectionConstraints()
        let config = RTCConfiguration()
        let iceServers : NSMutableArray =  NSMutableArray(array: [defaultSTUNServer()!])
        config.iceServers = iceServers as! [RTCIceServer]
        config.iceTransportPolicy = .all
        let peerConnection: RTCPeerConnection? = factory!.peerConnection(with: config, constraints: constraints!, delegate: self)
        return peerConnection
    }
    func offerPeerConnection(_ handleId: NSNumber?) {
        createPublisherPeerConnection()
        let jc = JanusConnection()
        jc.connection = publisherPeerConnection
        jc.handleId = handleId
        if let handleId = handleId {
            peerConnectionDict[handleId] = jc
        }
        guard let publisherPeerConnection = publisherPeerConnection else{
            print("this one")
            return
        }
        guard let defaultOffer = self.defaultOfferConstraints() else {
            print("this")
            return
        }
        publisherPeerConnection.offer(for: defaultOffer , completionHandler: { sdp, error in
            self.publisherPeerConnection!.setLocalDescription(sdp!, completionHandler: { error in
                self.websocket!.publisherCreateOffer(handleId, sdp: sdp, path: self.PathMaser)
            })
        })
    }
    func defaultMediaAudioConstraints() -> RTCMediaConstraints? {
        let mandatoryConstraints = [kRTCMediaConstraintsLevelControl: kRTCMediaConstraintsValueFalse]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constraints
    }
    func defaultOfferConstraints() -> RTCMediaConstraints? {
        let mandatoryConstraints = ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"]
        let constraints = RTCMediaConstraints(mandatoryConstraints: mandatoryConstraints, optionalConstraints: nil)
        return constraints
    }
    func createLocalAudioTrack() -> RTCAudioTrack? {
        let constraints: RTCMediaConstraints? = defaultMediaAudioConstraints()
        let source: RTCAudioSource? = factory!.audioSource(with: constraints)
        let track: RTCAudioTrack? = factory!.audioTrack(with: source!, trackId: kARDAudioTrackId)
        return track
    }
    func createAudioSender(_ peerConnection: RTCPeerConnection?) -> RTCRtpSender? {
        let sender: RTCRtpSender? = peerConnection?.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: kARDMediaStreamId)
        if (localAudioTrack != nil) {
            sender?.track = localAudioTrack
        }
        return sender
    }
    //Local Camera Flux Creation
    func createLocalVideoTrack() -> RTCVideoTrack? {
        let cameraConstraints = RTCMediaConstraints(mandatoryConstraints: currentMediaConstraint() as? [String : String], optionalConstraints: nil)
        let source: RTCAVFoundationVideoSource? = factory!.avFoundationVideoSource(with: cameraConstraints)
        source?.adaptOutputFormat(toWidth: 480, height: 360, fps: 30)
        let localVideoTrack: RTCVideoTrack? = factory!.videoTrack(with: source!, trackId: kARDVideoTrackId)
        #if !targetEnvironment(simulator)
        //this is Real device running
         localVideoTrack?.add(self)
        #endif
        localView!.captureSession = source?.captureSession
        let previewLayer = (localView!.layer as! AVCaptureVideoPreviewLayer)
        previewLayer.videoGravity = AVLayerVideoGravity.resize
        let frame = self.localView.frame
        previewLayer.frame.size = CGSize(width: frame.size.width, height: self.widthVideos.constant)
        //localView.layer.contentsGravity = CALayerContentsGravity.resizeAspectFill
        //localView.captureSession.commitConfiguration()
        //localView!.captureSession.
        return localVideoTrack
    }
    func createVideoSender(_ peerConnection: RTCPeerConnection?) -> RTCRtpSender? {
        let sender: RTCRtpSender? = peerConnection?.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: kARDMediaStreamId)
        
        if (localTrack != nil) {
            print("localTrack != nil")
            sender?.track = localTrack
        }else{
            print("localTrack == nil")
        }
        return sender
    }
    func currentMediaConstraint() -> [AnyHashable : Any]? {
        var mediaConstraintsDictionary: [AnyHashable : Any]? = nil
        let width = self.view.frame.width
        print(width.description)
        let widthConstraint = "480"
        let heightConstraint = "360"
        let frameRateConstrait = "60"
        if widthConstraint != "" && heightConstraint != "" {
            mediaConstraintsDictionary = [kRTCMediaConstraintsMinWidth: widthConstraint, kRTCMediaConstraintsMaxWidth: widthConstraint, kRTCMediaConstraintsMinHeight: heightConstraint, kRTCMediaConstraintsMaxHeight: heightConstraint, kRTCMediaConstraintsMaxFrameRate: frameRateConstrait]
        }
        return mediaConstraintsDictionary
    }
    func SetEarSpeackerOn(){
        do{
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [])
            
        }catch{
            
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        }catch {
            
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.none)
        }catch{
            
        }
    }
    func SetSessionPlayerOn(){
        do {
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [])
        }catch{
            print("PlayAndRecord: ",error.localizedDescription )
        }
        do {
            try AVAudioSession.sharedInstance().setActive(true)
        }catch{
             print("setActive: ",error.localizedDescription )
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
        }catch{
             print("overrideOutputAudioPort: ",error.localizedDescription )
        }
    }
    func SetSessionPlayerOff(){
        do{
            try AVAudioSession.sharedInstance().setActive(false)
        }catch{
            
        }
    }
}
*/

