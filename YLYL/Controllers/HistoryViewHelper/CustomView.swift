//
//  CustomView.swift
//  CarouselViewExample
//
//  Created by Matteo Tagliafico on 03/04/16.
//  Copyright © 2016 Matteo Tagliafico. All rights reserved.
//

import UIKit
protocol CarouselDelegate {
    func friendButtonTapped(index:Int,isfriend:Bool)
}
@IBDesignable
class CustomView: TGLParallaxCarouselItem {
    
    // MARK: - outlets
    @IBOutlet fileprivate weak var numberLabel: UILabel!
    @IBOutlet weak var userImage : RoundedUIImageView!
    @IBOutlet weak var userFullName : UILabel!
    @IBOutlet weak var userFlag : UIImageView!
    @IBOutlet weak var friendButton : UILabel!
     var delegate : CarouselDelegate?
    var index : Int?
    // MARK: - properties
    fileprivate var containerView: UIView!
    fileprivate let nibName = "CustomView"
    
    @IBInspectable
    var number: Int = 0 {
        didSet{
            
        }
    }
    
    
    // MARK: - init
    convenience init(frame: CGRect, number: Int) {
        self.init(frame: frame)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(containerView)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    @objc func friendGesture (_ sender: UITapGestureRecognizer) {
        delegate?.friendButtonTapped(index: self.index!, isfriend: self.friendButton.text == "Add as Friend" ? true: false)
    }
    
    // MARK: - methods
    fileprivate func setup() {
        layer.masksToBounds = false
        layer.shadowRadius = 30
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.65
        let tapgesture = UITapGestureRecognizer(target: self, action: #selector(self.friendGesture(_:)))
        tapgesture.numberOfTapsRequired = 1
        friendButton.isUserInteractionEnabled = true
        friendButton.addGestureRecognizer(tapgesture)
    }
}
