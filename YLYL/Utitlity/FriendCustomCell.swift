//
//  FriendCustomCell.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 21/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
protocol FriendMessageCustomCellDelegate : class {
    func cellDidTapPlay(_ sender: FriendCustomCell)
    func cellDidTapMessage(_ sender: FriendCustomCell)
}
class FriendCustomCell : UITableViewCell {
    
    //Views
    @IBOutlet weak var friendProfileImageView: UIImageView!
    @IBOutlet weak var friendConnectView: UIImageView!
    @IBOutlet weak var friendNameLabel: UILabel!
    @IBOutlet weak var Interestes: UILabel!
    @IBOutlet weak var NotifView : UIView!
     @IBOutlet weak var NotifNBLBL : UILabel!
    var isCellFriends : Bool = true
     var indexPath:IndexPath!
    @IBOutlet weak var challengeBTN: UIButton!
    var idUser = ""
    @IBOutlet weak var offlineLBL: UILabel!
    weak var delegate: FriendMessageCustomCellDelegate?
    func configureView() {
        
        //Make rounded imageView and view
        if isCellFriends {
            friendConnectView.isHidden = true
        }else{
            friendConnectView.isHidden  = false
        }
        
    }
    @IBAction func PlayFriendTouchUpInside(_ sender: Any) {
        
        delegate?.cellDidTapPlay(self)
        
    }
    
    @IBAction func MessageFriendTouchUpInside(_ sender: Any) {
        
        delegate?.cellDidTapMessage(self)
        
    }
}
