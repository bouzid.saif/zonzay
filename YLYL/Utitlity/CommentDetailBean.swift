//
//  CommentDetailBean.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  CommentDetailBean {
    private var id : String?
    private var nickname : String? = ""
    private var userLogo : String? = ""
    private var content : String? = ""
    private var createDate : String?
    private var userId : String?
    private var message : String?
    private var imgId : String? = ""
    private var replyTotal : Int? = 0
    private var replyList : [ReplyDetailBean]? = []
    init(nickname: String, content: String,createDate : String ) {
        self.nickname = nickname
   
        self.content = content
        self.createDate = createDate
        
    }
 init(id:String,userId:String,message:String,createDate:String,nickname:String,userLogo:String,content:String,imgID:String,replyTotal:Int,replyList:[ReplyDetailBean]) {
    self.id = id
    self.nickname = nickname
     self.userLogo = userLogo
    self.content = content
    self.createDate = createDate
    
    
    self.userId = userId
    
    self.message = message
    self.imgId = imgID
    self.replyTotal = replyTotal
    self.replyList  = replyList
    }
    
    func setNickname(_ nickname : String) {
        self.nickname = nickname
    }
    func setUserLogo(_ logo : String) {
        self.userLogo = logo
    }
   
    func setContent(_ content : String) {
        self.content = content
    }
    func setCreatedDate(_ created : String) {
        self.createDate = created
    }
    
    func setUserId(_ userId : String) {
        self.userId = userId
    }
    func setMessage(_ message : String) {
        self.message = message
    }
    func setImgId(_ imgID : String) {
        self.imgId = imgID
    }
    func setReplyTotal(_ replyT : Int) {
        self.replyTotal = replyT
    }
    func setReplyList(_ replyL : [ReplyDetailBean]) {
        self.replyList = replyL
    }

    
    func getNickname() -> String {
        return self.nickname!
    }
    func getUserLogo() -> String{
        return self.userLogo!
    }
   
    func getContent() -> String{
        return self.content!
    }
    func getCreatedDate() -> String{
        return self.createDate!
    }
    
    func getUserId() -> String {
        return self.userId!
    }
    func getMessage() -> String {
         return self.message!
    }
    func getImgId() -> String {
       return  self.imgId!
    }
    func getReplyTotal() -> Int {
       return self.replyTotal!
    }
    func getReplyList() -> [ReplyDetailBean] {
       return self.replyList!
    }
    
    
}

