//
//  Message.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  Message {
    private var id : String?
    private var message : String?
    private var createdAt : String?
    private var userId : String?
    private var userName : String?
    private var userImageUrl : String?
    private var sense : String?
    private var receiverID:String?
    init(id:String, message: String, createdAt: String, userId : String,userName: String, userImageUrl: String) {
        self.id = id
        self.message = message
        self.createdAt = createdAt
        self.userId = userId
        self.userName = userName
        self.userImageUrl = userImageUrl
        
    }
    init(id:String, message: String, createdAt: String, userId : String,userName: String, userImageUrl: String,receiverID:String) {
        self.id = id
        self.message = message
        self.createdAt = createdAt
        self.userId = userId
        self.userName = userName
        self.userImageUrl = userImageUrl
        self.receiverID = receiverID
    }
    func setSense(_ sense : String){
        self.sense = sense
    }
    func getSense() -> String {
        return self.sense!
    }
    func setMessage(_ message : String) {
        self.message = message
    }
    func setCreatedAt(_ created : String) {
        self.createdAt = created
    }
    func setUserId(_ user : String) {
        self.userId = user
    }
    func setUserName(_ username : String) {
        self.userName = username
    }
    func setUserImageUrl(_ url : String) {
        self.userImageUrl = url
    }
    func getMessage() -> String {
        return self.message!
    }
    func getCreatedAt() -> String {
        return self.createdAt!
    }
    func getUserId() -> String{
        return self.userId!
    }
    func getUserName() -> String{
        return self.userName!
    }
    func getUserImage() -> String{
        return self.userImageUrl!
    }
    
}

