//
//  Filter.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  Filter {
    private var id : String?
    private var name : String?
    private var model : String?
    private var imageFilter : UIImage?
    
    init(id:String, name: String, model: String, imageFilter : UIImage) {
        self.id = id
        self.name = name
        self.model = model
        self.imageFilter = imageFilter
        
    }
    func setName(_ name : String) {
        self.name = name
    }
    func setModel(_ model : String) {
        self.model = model
    }
    func setImageFilter(_ imgF : UIImage) {
        self.imageFilter = imgF
    }
    func getName() -> String {
        return self.name!
    }
    func getModel() -> String {
        return self.model!
    }
    func getImageFilter() -> UIImage{
        return self.imageFilter!
    }
}
