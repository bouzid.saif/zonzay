//
//  ScriptBase.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
class ScriptBase {
    static var sharedInstance = ScriptBase()
    private init() {
        
    }
     //****************BackEnd-Server**************************///
    //"192.168.1.102"
    static let isRemote = true
    static let DNSServer =  isRemote == true ? "zonzay.com" : "192.168.1.102"
    static let Base_URL = "http://" + DNSServer + ":3030"
    static let Base_URLMiddle = "http://" + DNSServer + ":5000"
    static let socket_URL = "http://" + DNSServer + ":3030"
    static let socket_URLMiddle = "http://" + DNSServer + ":5000"
    static let socket_URLUpdate = "http://" + DNSServer + ":5001"
    static let janus_URI = "ws://" + DNSServer + ":8188"
    static let Video_URL = "http://" + DNSServer + "/"
     //****************USERS**************************///
    var testScript = "http://" + DNSServer + ":8088/janus/"
    var JANUS_URL = "http://" + DNSServer + ":8088/janus/"
    
     /*
     *POST
     *firstName,lastName,password,age,email
     */
    var registerUser = Base_URL + "/users/registerUser"
    
    /*
     *POST
     *firstName,lastName,password,age,email
     */
    var registerApi = Base_URL + "/users/registerAPI"
    
    /*
     *POST
     *userId
     */
    var uploadImage = Base_URL + "/users/uploadImage"
    
    /*
     *POST
     *email,password
     */
    var login = Base_URL + "/users/login"
    
    /*
     *POST
     *Header : x-access-token
     *userId
     */
    var friendList = Base_URL + "/users/FriendsListIOS"
    
    /*
     *POST
     *Header : x-access-token
     *userId
     */
    var allUsers = Base_URL + "/users/allUsers"
    
    /*
     *POST
     *Header : x-access-token
     *userId
     */
    var news = Base_URL + "/users/news"
    
   
    var setIosPlayerId = Base_URL + "/users/setNotification"
    /*
     *POST
     *Header : x-access-token
     *userEmail
     */
    var forgotPassword = Base_URL + "/users/forgot"
    
    /*
     *POST
     *Header : x-access-token
     *firstName,lastName,age,userImageURL,userName
     */
    var updateUser = Base_URL + "/users/updateUser"
    
     var updatePrivacyUser = Base_URL + "/users/updatePrivacyAccount"
    
    var historyList = Base_URL + "/users/historyList"
    
    var blockUser = Base_URL + "/users/blockUser"
    var blockedUserList = Base_URL + "/users/blockedUserList"
    var unblockUser = Base_URL + "/users/unblockUser"
    var checkBlockUser = Base_URL + "/users/checkBlockUser"
    var searchUser = Base_URL + "/users/getUsers"
    var reportUser = Base_URL + "/users/reportUser"
    var checkConncetion = Base_URL + "/users/checkConnection"
    //****************JOKES**************************///
    
    /**
     *POST
     *Header : x-access-token
     *userId,listJokes
     */
    var updateJokes = Base_URL + "/jokes/updateJokes"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var getJokes = Base_URL + "/jokes/getJokes"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var getZonz = Base_URL + "/users/user"
    
    var getGifs = Base_URL + "/users/storeGifs"
    var getAllGifs =  Base_URL + "/users/getAllGifs"
    var getMyGifs = Base_URL + "/users/ownedGifs"
    var buyGifs = Base_URL + "/users/gifPurchase"
    var getUserSubscription = Base_URL + "/users/getUserSubscription"
    var updateInterests = Base_URL + "/users/updateInterests"
    var getInterests = Base_URL + "/users/getInterests"
    
    //****************preferences**************************///
    
    /**
     *POST
     *Header : x-access-token
     *userId,listPreferences
     */
    var updatePreferences = Base_URL + "/preferences/updatePreferences"
    
    //****************ROOM**************************///
    
    /**
     *POST
     *Header : x-access-token
     *firstUserId
     */
    var createRoom = Base_URL + "/room/createRoom"
    
    /**
     *POST
     *Header : x-access-token
     *secondUserId
     */
    var updateRoom = Base_URL + "/room/updateRoom"
    
     //****************friendsRequest**************************///
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var friendRequestList = Base_URL + "/friendsRequest/friendRequestList"
    
    /**
     *POST
     *Header : x-access-token
     *senderId,receiverId,friendRequestDate
     */
    var sendFriendRequest = Base_URL + "/friendsRequest/sendFriendRequest"
    
    /**
     *POST
     *Header : x-access-token
     *userId,userFriendId
     * message => error
     */
    var unFriendRequest = Base_URL + "/users/unfriendUser"
    /**
     *POST
     *Header : x-access-token
     *userId,userBlockedId
     * message => error
     */
    var unBlockUserRequest = Base_URL + "/users/unblockUser"
    /**
     *POST
     *Header : x-access-token
     *userId,userBlockedId
     * message => error
     */
    var blockUserRequest = Base_URL + "/users/blockUser"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var deleteFriendRequest = Base_URL + "/friendsRequest/deleteFriendRequest"
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var sendedFriendRequestList = Base_URL + "/friendsRequest/sendedFriendRequestList"
    
    /**
     *POST
     *Header : x-access-token
     *friendRequestId
     */
    var acceptFriendRequest = Base_URL + "/friendsRequest/acceptFriendRequest"
    
    /**
     *POST
     *Header : x-access-token
     *friendRequestId
     */
    var declineFriendRequest = Base_URL + "/friendsRequest/deleteFriendRequest"
    
   //****************Video**************************///
    
    /**
     *POST
     *Header : x-access-token
     *userId,[videoId : ""]
     */
    var deleteVideo = Base_URL + "/video/deleteVideo"
    
    //****************Actuality**************************///
    var getActuality = Base_URL + "/video/videoList"
    var seenVideo = Base_URL + "/video/seenVideo"
    
    var getActualityUser = Base_URL + "/video/videoListByUser"
    
    var likeVideo = Base_URL + "/like/addLike"
    
    var disLikeVideo =  Base_URL + "/like/removeLike"
    var getAllComments = Base_URL + "/comment/getAllComments"
    var addComment = Base_URL  + "/comment/addComment"
    var removeComment = Base_URL + "/comment/removeComment"
    var updateComment = Base_URL + "/comment/updateComment"
    //**************ZONZ*********************************//
    
    /**
     *POST
     *Header : x-access-token
     *userId,duration(String minuscule)
     */
    var filterPayment = Base_URL + "/users/purchase"
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var filterPaymentWithZonz = Base_URL + "/users/purchaseWeekZonz"
    
    /**
     *POST
     *Header : x-access-token
     *userId,purchaseZonz(String numbers),purchaseMoney(String number)
     */
    var zonzPayment = Base_URL + "/users/zonzPurchase"
    
    /*
     POST
     Header : x-access-token
     userId
     */
    var addZonzEndGame = Base_URL + "/users/zonzScore"
    var addZonzAD = Base_URL + "/users/zonzAdvertising"
    /************************************NEWCHAT******************************************/
    
    /**
     *POST
     *Header : x-access-token
     *userId
     */
    var getAllConversations = Base_URLMiddle + "/conversations/getAllConversations"
    
    /**
     *POST
     *Header : x-access-token
     *userOneId, userTwoId
     */
    var addConversation = Base_URLMiddle + "/conversations/addConversation"
    
    /**
     *POST
     *Header : x-access-token
     *userOneId, userTwoId, message, convId
     */
    var addMessage = Base_URLMiddle + "/messages/addMessage"
    
    /**
     *POST
     *Header : x-access-token
     *convId
     */
    var getAllMessages = Base_URLMiddle + "/messages/getAllMessages"
    
    //Tools
    var pathVideoJanus = ""
}
