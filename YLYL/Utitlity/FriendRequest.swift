//
//  FriendRequest.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  FriendRequest {
    private var id : String?
    private var senderId : String?
    private var receiverId : String?
    private var friendRequestDate : String?
    private var friendRequestAccepted : Bool?
    private var sender : User?
    init(id:String, receiverId: String, friendRequestDate: String, senderId : String,friendRequestAccepted: Bool, sender: User) {
        self.id = id
        self.receiverId = receiverId
        self.friendRequestDate = friendRequestDate
        self.friendRequestAccepted = friendRequestAccepted
        self.sender = sender
        self.senderId = senderId
        
    }
    func setReceiverId(_ receiver : String) {
        self.receiverId = receiver
    }
    func setFriendRequestDate(_ date : String) {
        self.friendRequestDate = date
    }
    func setFriendRequestAccepted(_ accepted : Bool) {
        self.friendRequestAccepted = accepted
    }
    func setSender(_ sender : User) {
        self.sender = sender
    }
    func setSenderId(_ sender : String) {
        self.senderId = sender
    }
    
    func getReceiverId() -> String {
        return self.receiverId!
    }
    func getFriendRequestDate() -> String {
        return self.friendRequestDate!
    }
    func isFriendRequestAccepted() -> Bool{
        return self.friendRequestAccepted!
    }
    func getSender() -> User{
        return self.sender!
    }
    func getSenderId() -> String{
        return self.senderId!
    }
    func getId() -> String {
        return self.id!
    }
    
}
