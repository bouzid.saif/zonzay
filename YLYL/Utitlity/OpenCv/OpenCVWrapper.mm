//
//  OpenCVWrapper.m
//  YLYL
//
//  Created by macbook on 2019-11-08.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//
#import <opencv2/opencv.hpp>
#import <opencv2/objdetect/objdetect.hpp>
#import <opencv2/objdetect.hpp>
#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import "OpenCVWrapper.h"

@implementation OpenCVWrapper
- (size_t) isThisWorking:(UIImage*) image {
    static cv::CascadeClassifier _faceCascade;

   
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt2"  ofType:@"xml"];
    NSLog(@"Path : %@", faceCascadePath);
    _faceCascade.load([faceCascadePath UTF8String]);
    
    //Convert UIImage to Mat Image
    
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);

     CGFloat cols = image.size.width;

     CGFloat rows = image.size.height;

     cv::Mat cvMat(rows, cols, CV_8UC4);
    CGBitmapInfo bitmapFlags = kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault;

     CGContextRef context = CGBitmapContextCreate(cvMat.data, cols, rows, 8,  cvMat.step[0], colorSpace, bitmapFlags );

    CGContextDrawImage(context, CGRectMake(0, 0, cols, rows), image.CGImage);

    CGContextRelease(context);

     CGColorSpaceRelease(colorSpace);

     cv::Mat matImage;

     cvtColor(cvMat, matImage, CV_RGB2GRAY);
    
    std::vector<cv::Rect> faces;

     _faceCascade.detectMultiScale(matImage, faces, 1.1, 4, CV_HAAR_DO_ROUGH_SEARCH, cv::Size(30, 30));
    size_t j = 0;
    
    if (faces.size() <= 0) {
        return  0;
    }
    for( size_t i = 0; i < faces.size(); i++ )
    {
        j = j + 1;
    
    }
    return j;
    //return faces;
}
@end
