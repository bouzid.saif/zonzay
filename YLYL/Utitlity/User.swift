//
//  User.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  User  {
    private var id : String?
    private var firstName : String?
    private var LastName : String?
    private var token : String?
    private var email : String?
    private var age : String?
    private var interetList : [Interest]? = []
    private var jokesList : [Jokes]? = []
    private var imageProfileUrl : String? = ""
    private var image : UIImage? = UIImage(named: "logo_saif")
    private var country  : String? = ""
    private var lastConnectionTime : String? = ""
    private var Connected :String? = ""
    private var roomList : [Room]? = []
    private var username : String?
    private var listPassion : String?
    init(id:String, userFirstName: String, userLastName: String, userAge : String,userEmail: String , token : String) {
        self.id = id
        self.firstName = userFirstName
        self.LastName = userLastName
        self.age = userAge
        self.email = userEmail
        self.token = token
    }
    init(id:String, userFirstName: String, userLastName: String, userAge : String,userEmail: String , token : String,photo:String) {
        self.id = id
        self.firstName = userFirstName
        self.LastName = userLastName
        self.age = userAge
        self.email = userEmail
        self.token = token
        self.imageProfileUrl = photo
    }
    init(id:String, userFirstName: String, userLastName: String, userAge : String,userEmail: String , token : String,photo:String,username:String) {
        self.id = id
        self.firstName = userFirstName
        self.LastName = userLastName
        self.age = userAge
        self.email = userEmail
        self.token = token
        self.imageProfileUrl = photo
        self.username = username
    }
    init(id:String, userFirstName: String, userLastName: String, userAge : String,userEmail: String , token : String,imageProfileUrl: String,username:String) {
        self.id = id
        self.firstName = userFirstName
        self.LastName = userLastName
        self.age = userAge
        self.email = userEmail
        self.token = token
        self.imageProfileUrl = imageProfileUrl
        self.username = username
    }
    init(id:String, userFirstName: String, userLastName: String,imageProfileUrl: String,username:String,listPassion:String) {
        self.id = id
        self.firstName = userFirstName
        self.LastName = userLastName
        self.imageProfileUrl = imageProfileUrl
        self.username = username
        self.listPassion = listPassion
    }
    func setFirstName(_ firstName : String) {
        self.firstName = firstName
    }
    func setLastName(_ lastName : String) {
        self.LastName = lastName
    }
    func setToken(_ token : String) {
         self.token = token
    }
    func setEmail(_ email : String) {
         self.email = email
    }
    func setAge(_ age : String) {
         self.age = age
    }
    func setInterestList(_ list : [Interest]) {
         self.interetList = list
    }
    func setJokesList(_ jokes : [Jokes]) {
         self.jokesList = jokes
    }
    func setProfileImage(_ image : String) {
         self.imageProfileUrl = image
    }
    func setImageDefault(_ image : UIImage) {
        self.image = image
    }
    func setCountry(_ country : String) {
         self.country = country
    }
    func setLastTime(_ time : String) {
         self.lastConnectionTime = time
    }
    func setUserName( _ username : String) {
        self.username = username
    }
    func setConnected(_ connected : String) {
         self.Connected = connected
    }
    func setRoomList(_ rooms : [Room]) {
         self.roomList = rooms
    }
    func setPassionList(_ passions : String) {
        self.listPassion = passions
    }
    func getFirstName() -> String {
        return self.firstName!
    }
    func getLastName() -> String {
        return self.LastName!
    }
    func getToken() -> String{
        return self.token!
    }
    func getEmail() -> String{
        return self.email!
    }
    func getAge() -> String{
        return self.age!
    }
    func getInterestList() -> [Interest] {
        return self.interetList!
    }
    func getJokesList() -> [Jokes]{
        return self.jokesList!
    }
    func getProfileImage() -> String{
        return self.imageProfileUrl!
    }
    func getDefaultImage() -> UIImage{
        return self.image!
    }
    func getCountry() -> String{
        return self.country!
    }
    func getLastTimeConnected() -> String{
        return self.lastConnectionTime!
    }
    func isConnected() -> String {
        return self.Connected!
    }
    func getRoomList() -> [Room]{
        return self.roomList!
    }
    func getId() -> String {
        return self.id!
    }
    func getUserName () -> String {
        return self.username!
    }
    func getPassionList () -> String {
        return self.listPassion!
    }
   
}
