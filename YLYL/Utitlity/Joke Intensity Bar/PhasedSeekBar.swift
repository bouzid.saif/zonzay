//
//  PhasedSeekBar.swift
//  YLYLSample
//
//  Created by macbook on 2019-01-17.
//  Copyright © 2019 Velox-IT. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
@IBDesignable
open class PhasedSeekBar: UIView {
   
    @IBInspectable
    @objc open var On_Off : Bool = true  {
        didSet {
            self.setNeedsDisplay()
        }
    }
    @IBOutlet weak var VOne : UIView!
    @IBOutlet weak var VTwo : UIView!
    @IBOutlet weak var VThree : UIView!
    @IBOutlet weak var VFourth : UIView!
    @IBOutlet weak var VFive : UIView!
    @IBOutlet weak var LabelOne : UILabel!
    @IBOutlet weak var LabelTwo : UILabel!
    @IBOutlet weak var LabelThree : UILabel!
    @IBOutlet weak var LabelFourth : UILabel!
    @IBOutlet weak var LabelFive : UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setNeedsDisplay()
        
    }
    func setIntensity(_ intensity : Int){
        clearViews()
        switch intensity {
        case 1:
            self.VOne.isHidden = false
        case 2:
              self.VTwo.isHidden = false
        case 3:
              self.VThree.isHidden = false
        case 4:
              self.VFourth.isHidden = false
        case 5:
              self.VFive.isHidden = false
        default:
            break
        }
    }
    func clearViews(){
          self.VOne.isHidden = true
          self.VTwo.isHidden = true
          self.VThree.isHidden = true
          self.VFourth.isHidden = true
          self.VFive.isHidden = true
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setNeedsDisplay()
    }
    open override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
    override open func layoutSubviews() {
     
        
            roundCorners(corners: [.topLeft, .topRight, .bottomLeft,.bottomRight], radius: self.frame.height / 4)
        self.backgroundColor = GradientColor(gradientStyle: .topToBottom, frame: self.frame, colors: [UIColor(hexString: "FE2E2E"),UIColor(hexString: "FFFF00"),UIColor(hexString: "00FF00")])
            for v in self.subviews {
                if (v.tag == 1 || v.tag == 2 || v.tag == 3 || v.tag == 4 || v.tag == 5) {
                    v.layer.cornerRadius = v.frame.width / 2
                    v.layer.masksToBounds = true
                }
            }
        
    }
}
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
