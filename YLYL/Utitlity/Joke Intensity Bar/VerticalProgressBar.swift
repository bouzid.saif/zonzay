//
//  VerticalProgressBar.swift
//  VHProgressBar
//
//  Created by Sohei Miyakura on 2018/11/21.
//

import UIKit

@IBDesignable
open class VerticalProgressBar: UIView {
    
    fileprivate var progressView: UIView!
    fileprivate var animator: UIViewPropertyAnimator!
    fileprivate var isAnimating: Bool = false
    
    @IBInspectable public var bgColor: UIColor = UIColor.white {
        didSet {
            configureView()
        }
    }
    
    @IBInspectable public var barColor: UIColor = UIColor.init(red: 52/255, green: 181/255, blue: 240/255, alpha: 1) {
        didSet {
            configureView()
        }
    }
    
    @IBInspectable public var frameColor: UIColor = UIColor.init(red: 161/255, green: 161/255, blue: 161/255, alpha: 1) {
        didSet {
            configureView()
        }
    }
    
    @IBInspectable public var frameBold: CGFloat = 0.1 {
        didSet {
            configureView()
        }
    }
    
    @IBInspectable public var pgHeight: CGFloat = 200 {
        didSet {
            configureView()
        }
    }
    
    @IBInspectable public var pgWidth: CGFloat = 20 {
        didSet {
            configureView()
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initProgressView()
    }
    
    required  public init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        initProgressView()
    }
}

extension VerticalProgressBar {
    fileprivate func initProgressView() {
        progressView = UIView()
        addSubview(progressView)
    }
    fileprivate func configureProgressView() {
       
        progressView.frame.size.height = pgHeight
        progressView.frame.size.width = pgWidth
        progressView.frame.origin.y = self.bounds.origin.y + pgHeight
        progressView.layer.cornerRadius = pgWidth / 2
       
    }
    
    fileprivate func configureView() {
        setBackgroundColor()
        setFrameColor()
        setFrameBold()
        setProgressBarHeight()
        setProgressBarWidth()
        setProgressBarRadius()
    }
    
    fileprivate func setBackgroundColor() {
        self.backgroundColor = bgColor
    }
    
    fileprivate func setFrameColor() {
        self.layer.borderWidth = frameBold
    }
    
    fileprivate func setFrameBold() {
        self.layer.borderColor = frameColor.cgColor
    }
    
    fileprivate func setProgressBarHeight() {
        self.frame.size.height = pgHeight
    }
    
    fileprivate func setProgressBarWidth() {
        self.frame.size.width = pgWidth
    }
    
    fileprivate func setProgressBarRadius() {
        self.layer.cornerRadius = pgWidth / 2
    }
}

extension VerticalProgressBar {
    
    open func animateProgress(duration: CGFloat,  progressValue: CGFloat) {
        if !(0 < progressValue || progressValue < 1.0) {
            return
        }
        configureProgressView()
        let timing: UICubicTimingParameters = UICubicTimingParameters(animationCurve: .easeInOut)
        animator = UIViewPropertyAnimator(duration: TimeInterval(duration), timingParameters: timing)
        animator.addAnimations {
            self.progressView.frame.size.height = self.pgHeight * progressValue
            self.progressView.backgroundColor = UIColor.init(gradientStyle: .topToBottom, withFrame: CGRect(x: self.progressView.frame.origin.x, y: self.progressView.frame.origin.y, width: self.pgWidth, height: self.pgHeight), andColors: [UIColor(red: 229/255, green: 44/255, blue: 44/255, alpha: 1),UIColor.white])
        }
        animator.startAnimation()
    }
    
    open func startAnimation(type: String, duration: CGFloat) {
        if isAnimating {
            return
        }
        switch type {
        case "normal":
            runAnimation(reverse: false, duration: duration)
            break
        case "reverse":
            runAnimation(reverse: true, duration: duration)
            break
        default:
            break
        }
    }
    
    fileprivate func runAnimation(reverse: Bool, duration: CGFloat) {
        configureProgressView()
        animator = UIViewPropertyAnimator.runningPropertyAnimator(withDuration: TimeInterval(duration), delay: 0.0, options: [.curveEaseInOut], animations: {
            UIView.setAnimationRepeatCount(1000)
            UIView.setAnimationRepeatAutoreverses(reverse)
            self.progressView.frame.size.height = self.pgHeight
        }, completion: { _ in
        })
        isAnimating = true
        animator.startAnimation()
    }
    
    open func stopAnimation() {
        if !isAnimating {
            return
        }
        isAnimating = false
        animator.stopAnimation(true)
    }
    
    open func getProgress() -> CGFloat {
        return self.progressView.frame.size.height
    }
}
extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}
class ProgressBarView: UIView {
    // MARK: - Private Variables
    private var backgroundImage : UIView!
    private var progressView : UIImageView!
    private let animationDuration : Double = 0.2
    
    // MARK: - Overriden Methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initBar()
    }
    override var intrinsicContentSize: CGSize {
         return CGSize(width: frame.size.width, height: frame.size.height)
    }
    
    // MARK: - Public Methods
    /**
     Initializes the progress bar background and also the progress level view.
     The background is equal to the parent view frame.
     */
    func initBar() {
        // make the container with rounded corners and clear background.
        self.layer.cornerRadius = frame.size.width / 2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.clear
        
        // background image / this view being the same width and height as the parent doesn't need to round the corners. It will take the parent frame.
        let backgroundRect = CGRect(x: 0.0, y: 0.0, width: Double(frame.size.width), height: Double(frame.size.height))
        backgroundImage = UIView(frame: backgroundRect)
        backgroundImage.clipsToBounds = true
        backgroundImage.backgroundColor = UIColor.yellow
        addSubview(backgroundImage)
        
        //level of progress
        let progressRect = CGRect(x: 0.0, y: Double(frame.size.height), width: Double(frame.size.width), height: 0.0)
        progressView = UIImageView(frame: progressRect)
        progressView.layer.cornerRadius = frame.size.width / 2
        progressView.layer.masksToBounds = true
        progressView.backgroundColor = UIColor.blue
        addSubview(progressView)
    }
    /**
     Sets the progress level from a value, animated.
     - Parameter currentValue : The value that needs to be displayed as a progress bar.
     - Parameter threshold : Optional. The max percentage that the progress bar will display.
     */
    func setProgressValue(currentValue : CGFloat , threshold  : CGFloat = 100.0) {
        let yOffset = ((threshold - currentValue) / threshold) * frame.size.height / 1
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, options: [.curveEaseInOut], animations: {
            self.progressView.frame.size.height = self.frame.size.height - yOffset
            self.progressView.frame.origin.y = yOffset
        }, completion: nil)
    }
    /**
     Sets the background color of the progress view.
     This color will be displayed underneath the progress view.
     */
    func setBackColor(color : UIColor) {
        backgroundImage.backgroundColor = color
    }
    /**
     Sets the background color of the progress view.
     This is the color that will display the value you have inserted.
     */
    func setProgressColor(color : UIColor) {
        progressView.backgroundColor = color
    }
}
