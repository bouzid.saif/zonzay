//
//  ReplyDetailBean.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  ReplyDetailBean {
    private var id : String?
    private var nickname : String?
    private var userLogo : String?
    private var commentId : String?
    private var content : String?
     private var status : String?
     private var createDate : String?
    init(id:String, nickname: String, userLogo : String,commentId: String, content: String,status: String,createDate : String ) {
        self.id = id
        self.nickname = nickname
        self.userLogo = userLogo
        self.commentId = commentId
        self.content = content
        self.status = status
        self.createDate = createDate
        
    }
    func setNickname(_ nickname : String) {
        self.nickname = nickname
    }
    func setUserLogo(_ logo : String) {
        self.userLogo = logo
    }
    func setCommentId(_ commentID : String) {
        self.commentId = commentID
    }
    func setContent(_ content : String) {
        self.content = content
    }
    func setStatus(_ status : String) {
        self.status = status
    }
    func setCreatedDate(_ created : String) {
        self.createDate = created
    }
    
    func getNickname() -> String {
        return self.nickname!
    }
    func getUserLogo() -> String{
        return self.userLogo!
    }
    func getCommentId() -> String{
        return self.commentId!
    }
    func getContent() -> String{
        return self.content!
    }
    func getStatus() -> String{
        return self.status!
    }
    func getCreatedDate() -> String{
        return self.createDate!
    }
    
}

