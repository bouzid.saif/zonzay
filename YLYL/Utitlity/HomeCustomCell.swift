//
//  HomeCustomCell.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 20/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit

class HomeCustomCell: UITableViewCell {
    
    //Views
    @IBOutlet weak var videoProfileImageView: UIImageView!
    @IBOutlet weak var videoProfileNameLabel: UILabel!
    @IBOutlet weak var videoTimeLabel: UILabel!
    
    @IBOutlet weak var firstUserProfileImageView: RoundedUIImageView!
    @IBOutlet weak var SecondUserProfileImageView: RoundedUIImageView!
    @IBOutlet weak var firstUserName : UILabel!
    @IBOutlet weak var secondUserName : UILabel!
    var PathVideo : String = ""
    func setVideo(name: String ) {
        videoProfileNameLabel.text = name
    }
    func configureCell(nameVideo:String,videoDate:String,firstUserName:String,secondUserName:String,pathVideo :String) {
        
        self.videoProfileNameLabel.text = nameVideo
        self.firstUserName.text = firstUserName
        self.secondUserName.text = secondUserName
        self.PathVideo =  pathVideo
        let dateFormatter = DateFormatter()
        let date = dateFormatter.date(fromSwapiString: videoDate)
        let dateFormatterNow = DateFormatter()
        dateFormatterNow.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        dateFormatterNow.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatterNow.locale = Locale(identifier: "en_US_POSIX")
        let dateNow = dateFormatterNow.date(fromSwapiString: dateFormatterNow.string(from: Date()))
    if monthsBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
        if daysBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
            if hoursBetweenDates(startDate: date!, endDate: dateNow!) == 0 {
                //time here
                self.videoTimeLabel.text = "\(minutesBetweenDates(startDate: date!, endDate: dateNow!)) minutes ago"
            }else{
                //hours equal
                 self.videoTimeLabel.text = "\(hoursBetweenDates(startDate: date!, endDate: dateNow!)) hours ago"
            }
        }else{
            //days equal
            self.videoTimeLabel.text = "\(daysBetweenDates(startDate: date!, endDate: dateNow!)) days ago"
        }
    }else{
     //months equal
        self.videoTimeLabel.text = "\(monthsBetweenDates(startDate: date!, endDate: dateNow!)) months ago"
    }
    
        }
    func yearsBetweenDates(startDate:Date, endDate:Date) -> Int
    {
        
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.year]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        print("Months:",components.year!)
        return components.year!
        
    }
    func monthsBetweenDates(startDate:Date, endDate:Date) -> Int
    {
       
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.month]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        print("Months:",components.month!)
        return components.month!
        
    }
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.day]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.day!
        
    }
    func hoursBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.hour]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        print("startDate:",startDate)
        print("startDate:",endDate)
        print("count:",components.hour!)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.hour!
        
    }
    func minutesBetweenDates(startDate:Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let unit:Set<Calendar.Component> = [.minute]
        let components = calendar.dateComponents(unit, from: startDate, to: endDate)
        //calendar.components(unit, fromDate: startDate, toDate: endDate, options: nil)
        //let components = calendar.compare(startDate, to: endDate, toGranularity: Calendar.Component.day)
        
        return components.minute!
        
    }
}
extension DateFormatter {
    func date(fromSwapiString dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = TimeZone(abbreviation: "UTC")
        self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func date(fromTodayDate dateString: String) -> Date? {
        // SWAPI dates look like: "2014-12-10T16:44:31.486000Z"
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
        self.timeZone = NSTimeZone.system
        //self.locale = Locale(identifier: "en_US_POSIX")
        return self.date(from: dateString)
    }
    func date(noChange  dateString : String) -> Date? {
        print(dateString)
        self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SZ"
       
        return self.date(from: dateString)
        
    }
}
