//
//  BuyZonz.swift
//  YLYL
//
//  Created by macbook on 3/29/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import ChameleonFramework
import StoreKit
import Alamofire
import SwiftyJSON
@IBDesignable
class BuyZonz : UIView,SKProductsRequestDelegate,PKIStateDelegate{
    @IBOutlet weak var zonzContainer: UIView!
    
    @IBOutlet weak var MyZonzLBL: UILabel!
    
    @IBOutlet weak var zonzImageTOP: UIImageView!
    
    @IBOutlet weak var zonzPointsLBLTOP: UILabel!
    
    @IBOutlet weak var zonzTextLBL: UILabel!
    
    @IBOutlet weak var zonzHamburgerOne: UIImageView!
    
    @IBOutlet weak var zonzMoneyOne: UILabel!
    @IBOutlet weak var zonzHamburgerTwo: UIImageView!
    
    @IBOutlet weak var zonzMoneyTwo: UILabel!
    @IBOutlet weak var zonzHamburgerThree: UIImageView!
    
    @IBOutlet weak var zonzMoneyThree: UILabel!
    
    @IBOutlet weak var zonzHamburgerFour: UIImageView!
    
    @IBOutlet weak var zonzMoneyFour: UILabel!
    
    @IBOutlet weak var zonzHamburgerFive: UIImageView!
    
    @IBOutlet weak var zonzMoneyFive: UILabel!
    
    @IBOutlet weak var pointsLBLOne: UILabel!
    
    @IBOutlet weak var pointsLBLTwo: UILabel!
    
    @IBOutlet weak var pointsLBLThree: UILabel!
    
    @IBOutlet weak var pointsLBLFour: UILabel!
    
    @IBOutlet weak var pointsLBLFive: UILabel!
    
    @IBOutlet weak var lineOne: UIImageView!
    
    @IBOutlet weak var lineTwo: UIImageView!
    
    @IBOutlet weak var lineThree: UIImageView!
    
    @IBOutlet weak var lineFour: UIImageView!
    
    @IBOutlet weak var lineFive: UIImageView!
    
    @IBOutlet weak var zonzBroughtContainer: UIView!
    
    @IBOutlet weak var zonzBroughtLBL: UILabel!
    @IBOutlet weak var ZOne : UIImageView!
    @IBOutlet weak var ZTwo : UIImageView!
    @IBOutlet weak var ZThree : UIImageView!
    @IBOutlet weak var ZFour : UIImageView!
    @IBOutlet weak var ZFive : UIImageView!

    @IBOutlet weak var animateZonzBroughtConst: NSLayoutConstraint!
    let Zonz200 = "com.regystone.zonzay.zonz200"
    let Zonz300 = "com.regystone.zonzay.zonz300"
    let Zonz500 = "com.regystone.zonzay.zonz500"
    let Zonz900 = "com.regystone.zonzay.zonz900"
    let Zonz1000 = "com.regystone.zonzay.zonz1000"
    var products : [SKProduct] = []
    fileprivate var containerView: UIView!
    fileprivate let nibName = "BuyZonz"
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print("received Something From this Request : ",request.debugDescription)
        print("The response Was : ",response.invalidProductIdentifiers)
    }
    func hideBuyOne(hide:Bool) {
        self.zonzMoneyOne.isHidden = hide
        self.zonzHamburgerOne.isHidden = hide
        self.lineOne.isHidden = hide
        self.pointsLBLOne.isHidden = hide
        self.ZOne.isHidden = hide
    }
    func hideBuyTwo(hide:Bool) {
        self.zonzMoneyTwo.isHidden = hide
        self.zonzHamburgerTwo.isHidden = hide
        self.lineTwo.isHidden = hide
        self.pointsLBLTwo.isHidden = hide
        self.ZTwo.isHidden = hide
    }
    func hideBuyThree(hide:Bool) {
        self.zonzMoneyThree.isHidden = hide
        self.zonzHamburgerThree.isHidden = hide
        self.lineThree.isHidden = hide
        self.pointsLBLThree.isHidden = hide
        self.ZThree.isHidden = hide
    }
    func hideBuyFour(hide:Bool) {
        self.zonzMoneyFour.isHidden = hide
        self.zonzHamburgerFour.isHidden = hide
        self.lineFour.isHidden = hide
        self.pointsLBLFour.isHidden = hide
        self.ZFour.isHidden = hide
    }
    func hideBuyFive(hide:Bool) {
        self.zonzMoneyFive.isHidden = hide
        self.zonzHamburgerFive.isHidden = hide
        self.lineFive.isHidden = hide
        self.pointsLBLFive.isHidden = hide
        self.ZFive.isHidden = hide
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = bounds
        containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        containerView.backgroundColor = GradientColorLocationSaif(gradientStyles: .topToBottom, frame: self.containerView.frame, colors: [UIColor(red: 31/255, green: 165/255, blue: 194/255, alpha: 1), UIColor(red: 16/255, green: 83/255, blue: 97/255, alpha: 1)], limit: NSNumber(value: 0.8))
        zonzPointsLBLTOP.adjustsFontForContentSizeCategory = true
        zonzPointsLBLTOP.adjustsFontSizeToFitWidth = true
        PKIAPHandler.shared.delegate = self
        PKIAPHandler.shared.setProductIds(ids: [Zonz200,Zonz300,Zonz500,Zonz900,Zonz1000])
        PKIAPHandler.shared.productsRequest = SKProductsRequest(productIdentifiers: Set([Zonz200,Zonz300,Zonz500,Zonz900,Zonz1000]))
        
        
        //PKIAPHandler.shared.productsRequest.delegate = self
        PKIAPHandler.shared.fetchAvailableProducts { (skProducts) in
            self.products = skProducts
            print("SK :",skProducts)
            
            for product in skProducts {
                print(product.productIdentifier)
                
            }
            self.products  = skProducts.sorted(by: { (sk1, sk2) -> Bool in
                if sk1.price > sk2.price {
                    return true
                }else{
                    return false
                }
                
            })
            
            print("SK Order: ",skProducts )
            for product in self.products {
                print(product.productIdentifier)
                DispatchQueue.main.async {
                    if product.productIdentifier == self.Zonz200 {
                        self.zonzMoneyOne.text = product.localizedPrice!
                    }
                    if product.productIdentifier == self.Zonz300 {
                        self.zonzMoneyTwo.text = product.localizedPrice!
                    }
                    if product.productIdentifier == self.Zonz500 {
                        self.zonzMoneyThree.text = product.localizedPrice!
                    }
                    if product.productIdentifier == self.Zonz900 {
                        self.zonzMoneyFour.text = product.localizedPrice!
                    }
                    if product.productIdentifier == self.Zonz1000 {
                        self.zonzMoneyFive.text = product.localizedPrice!
                    }
                }
                
            }
        }
        configureZonzContainer()
        createGestures()
        addSubview(containerView)
       // self.zonzBroughtContainer.origin.x = (UIScreen.main.bounds.width / 2) - 87
      //  self.zonzBroughtContainer.origin.y = 0
    }
    func animateWithPrice(view:String) {
        var width : CGFloat = 0
        var height: CGFloat = 0
        switch view {
        case "One":
            self.zonzBroughtLBL.text = "+100"
        case "Two":
            self.zonzBroughtLBL.text = "+400"
        case "Three":
            self.zonzBroughtLBL.text = "+800"
        case "Four":
            self.zonzBroughtLBL.text = "+1700"
        case "Five":
            self.zonzBroughtLBL.text = "+3100"
        default:
            break
        }
        width = self.zonzBroughtContainer.frame.width
        height = self.zonzBroughtContainer.frame.height
        
        self.zonzBroughtContainer.autoresizesSubviews = true
        //self.zonzBroughtContainer.size = CGSize.zero
        /*self.zonzBroughtContainer.subviews.forEach { (vi) in
            vi.size = CGSize.zero
            vi.layoutIfNeeded()
        } */
       // self.zonzBroughtContainer.layoutIfNeeded()
        self.zonzBroughtContainer.isHidden = false
       
        self.zonzBroughtContainer.layoutIfNeeded()
        UIView.transition(with: self.zonzBroughtContainer, duration: 1.4, options: [.transitionCrossDissolve], animations: {
              self.animateZonzBroughtConst.constant = 0
            self.zonzBroughtContainer.setNeedsUpdateConstraints()
            self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil)
      /*  UIView.animate(withDuration: 3, delay: 0, options: [.transitionCurlDown], animations: {
           
           self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil) */
       /* UIView.animate(withDuration: 0.8, delay: 0, options: [.curveEaseInOut], animations: {
            self.zonzBroughtContainer.size = CGSize(width: width, height: height)
            self.zonzBroughtContainer.layoutIfNeeded()
        }, completion: nil) */
    }
    func createGestures(){
        let tapgestureOne = UITapGestureRecognizer(target: self, action: #selector(self.buyTwoHundred(_:)))
        self.zonzHamburgerOne.isUserInteractionEnabled = true
        self.zonzHamburgerOne.addGestureRecognizer(tapgestureOne)
        //
        let tapgestureTwo = UITapGestureRecognizer(target: self, action: #selector(self.buyThreeHundred(_:)))
        self.zonzHamburgerTwo.isUserInteractionEnabled = true
        self.zonzHamburgerTwo.addGestureRecognizer(tapgestureTwo)
        //
        let tapgestureThree = UITapGestureRecognizer(target: self, action: #selector(self.buyFiveHundred(_:)))
        self.zonzHamburgerThree.isUserInteractionEnabled = true
        self.zonzHamburgerThree.addGestureRecognizer(tapgestureThree)
        //
        let tapgestureFour = UITapGestureRecognizer(target: self, action: #selector(self.buyNineHundred(_:)))
        self.zonzHamburgerFour.isUserInteractionEnabled = true
        self.zonzHamburgerFour.addGestureRecognizer(tapgestureFour)
        //
        let tapgestureFive = UITapGestureRecognizer(target: self, action: #selector(self.buyOneThousend(_:)))
        self.zonzHamburgerFive.isUserInteractionEnabled = true
        self.zonzHamburgerFive.addGestureRecognizer(tapgestureFive)
        
        
    }
    @objc func buyTwoHundred(_ sender: UITapGestureRecognizer) {
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
        self.hideBuyFive(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
         PKIAPHandler.shared.purchase(product: self.products[4]) { (purchaseAlertType, product, transaction) in
            print(purchaseAlertType.message)
              if transaction != nil {
                self.buyZonzays(zonz: "100", product: product!, completionHandler: { (verif) in
        if verif  {
               
            self.hideBuyOne(hide: true)
            self.zonzPointsLBLTOP.alpha = 0
            let initImageX = self.zonzImageTOP.origin.x
             let initImageY = self.zonzImageTOP.origin.y
            let initImageSize = self.zonzImageTOP.size
             let initLBLX = self.zonzPointsLBLTOP.origin.x
            let initLBLY = self.zonzPointsLBLTOP.origin.y
            let initLBLSize = self.zonzPointsLBLTOP.size
            
            UIView.animate(withDuration: 1.0, animations: {
                self.zonzImageTOP.origin.x = self.zonzBroughtContainer.origin.x + 11
                self.zonzImageTOP.origin.y = self.zonzBroughtContainer.origin.y + 37
                self.zonzImageTOP.size = CGSize(width: 65.28, height: 72.71)
                self.zonzPointsLBLTOP.origin.x = (self.zonzBroughtContainer.origin.x + 11 + 65.28) +  8
                 self.zonzPointsLBLTOP.origin.y = (self.zonzBroughtContainer.origin.y + 37 + 36.355) - 17.5
                self.zonzPointsLBLTOP.size = CGSize(width: 75, height: 39)
                // self.zonzPointsLBLTOP.alpha = 1
            }, completion:{ _ in
                self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                let initVal = Int(self.zonzPointsLBLTOP.text!)!
                _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                    self.zonzPointsLBLTOP.tag += 50
                    self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                    
                    if self.zonzPointsLBLTOP.tag == (initVal + 100) {
                        
                time.invalidate()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                            self.zonzPointsLBLTOP.alpha = 0
                            UIView.animate(withDuration: 0.6, animations: {
                                self.zonzImageTOP.origin.x = initImageX
                                self.zonzImageTOP.origin.y = initImageY
                                self.zonzImageTOP.size = initImageSize
                                self.zonzPointsLBLTOP.origin.x = initLBLX
                                self.zonzPointsLBLTOP.origin.y = initLBLY
                                self.zonzPointsLBLTOP.size = initLBLSize
                            })
                            UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                self.zonzPointsLBLTOP.alpha = 1
                            }, completion: { _ in})
                        })
                        
                    }
                })
                
            })
            UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                self.zonzPointsLBLTOP.alpha = 1
            }, completion: { _ in
              
                
            })
            }
            })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyThree(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyFive(hide: false)
                }
                
       }
        }
       
    }
    
    @objc func buyThreeHundred(_ sender: UITapGestureRecognizer) {
        self.hideBuyOne(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
        self.hideBuyFive(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[3]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "400", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.origin.x
                    let initImageY = self.zonzImageTOP.origin.y
                    let initImageSize = self.zonzImageTOP.size
                    let initLBLX = self.zonzPointsLBLTOP.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.origin.x = self.zonzBroughtContainer.origin.x + 11
                        self.zonzImageTOP.origin.y = self.zonzBroughtContainer.origin.y + 37
                        self.zonzImageTOP.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.origin.x = (self.zonzBroughtContainer.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.origin.y = (self.zonzBroughtContainer.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 200
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 400) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.origin.x = initImageX
                                        self.zonzImageTOP.origin.y = initImageY
                                        self.zonzImageTOP.size = initImageSize
                                        self.zonzPointsLBLTOP.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }
                    })
                }else{
                    self.hideBuyThree(hide: false)
                    self.hideBuyOne(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyFive(hide: false)
                }
                
            }
        }
    }
    @objc func buyFiveHundred(_ sender: UITapGestureRecognizer) {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyFour(hide: true)
        self.hideBuyFive(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[2]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "800", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.origin.x
                    let initImageY = self.zonzImageTOP.origin.y
                    let initImageSize = self.zonzImageTOP.size
                    let initLBLX = self.zonzPointsLBLTOP.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.origin.x = self.zonzBroughtContainer.origin.x + 11
                        self.zonzImageTOP.origin.y = self.zonzBroughtContainer.origin.y + 37
                        self.zonzImageTOP.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.origin.x = (self.zonzBroughtContainer.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.origin.y = (self.zonzBroughtContainer.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 400
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 800) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.origin.x = initImageX
                                        self.zonzImageTOP.origin.y = initImageY
                                        self.zonzImageTOP.size = initImageSize
                                        self.zonzPointsLBLTOP.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyOne(hide: false)
                    self.hideBuyFive(hide: false)
                }
                
            }
        }
    }
    @objc func buyNineHundred(_ sender: UITapGestureRecognizer) {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFive(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[1]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "1700", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyOne(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.origin.x
                    let initImageY = self.zonzImageTOP.origin.y
                    let initImageSize = self.zonzImageTOP.size
                    let initLBLX = self.zonzPointsLBLTOP.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.origin.x = self.zonzBroughtContainer.origin.x + 11
                        self.zonzImageTOP.origin.y = self.zonzBroughtContainer.origin.y + 37
                        self.zonzImageTOP.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.origin.x = (self.zonzBroughtContainer.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.origin.y = (self.zonzBroughtContainer.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 850
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 1700) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.origin.x = initImageX
                                        self.zonzImageTOP.origin.y = initImageY
                                        self.zonzImageTOP.size = initImageSize
                                        self.zonzPointsLBLTOP.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyThree(hide: false)
                    self.hideBuyFive(hide: false)
                    self.hideBuyOne(hide: false)
                }
                
            }
        }
    }
    @objc func buyOneThousend(_ sender: UITapGestureRecognizer) {
        self.hideBuyOne(hide: true)
        self.hideBuyTwo(hide: true)
        self.hideBuyThree(hide: true)
        self.hideBuyFour(hide: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            PKIAPHandler.shared.purchase(product: self.products[0]) { (purchaseAlertType, product, transaction) in
                print(purchaseAlertType.message)
                if transaction != nil {
                    self.buyZonzays(zonz: "3100", product: product!, completionHandler: { (verif) in
                        if verif  {
                    self.hideBuyFive(hide: true)
                    self.zonzPointsLBLTOP.alpha = 0
                    let initImageX = self.zonzImageTOP.origin.x
                    let initImageY = self.zonzImageTOP.origin.y
                    let initImageSize = self.zonzImageTOP.size
                    let initLBLX = self.zonzPointsLBLTOP.origin.x
                    let initLBLY = self.zonzPointsLBLTOP.origin.y
                    let initLBLSize = self.zonzPointsLBLTOP.size
                    
                    UIView.animate(withDuration: 1.0, animations: {
                        self.zonzImageTOP.origin.x = self.zonzBroughtContainer.origin.x + 11
                        self.zonzImageTOP.origin.y = self.zonzBroughtContainer.origin.y + 37
                        self.zonzImageTOP.size = CGSize(width: 65.28, height: 72.71)
                        self.zonzPointsLBLTOP.origin.x = (self.zonzBroughtContainer.origin.x + 11 + 65.28) +  8
                        self.zonzPointsLBLTOP.origin.y = (self.zonzBroughtContainer.origin.y + 37 + 36.355) - 17.5
                        self.zonzPointsLBLTOP.size = CGSize(width: 75, height: 39)
                        // self.zonzPointsLBLTOP.alpha = 1
                    }, completion:{ _ in
                        self.zonzPointsLBLTOP.tag = Int(self.zonzPointsLBLTOP.text!)!
                        let initVal = Int(self.zonzPointsLBLTOP.text!)!
                        _ = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (time) in
                            self.zonzPointsLBLTOP.tag += 1550
                            self.zonzPointsLBLTOP.text = "\(self.zonzPointsLBLTOP.tag)"
                            
                            if self.zonzPointsLBLTOP.tag == (initVal + 3100) {
                                
                                time.invalidate()
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.2, execute: {
                                    self.zonzPointsLBLTOP.alpha = 0
                                    UIView.animate(withDuration: 0.6, animations: {
                                        self.zonzImageTOP.origin.x = initImageX
                                        self.zonzImageTOP.origin.y = initImageY
                                        self.zonzImageTOP.size = initImageSize
                                        self.zonzPointsLBLTOP.origin.x = initLBLX
                                        self.zonzPointsLBLTOP.origin.y = initLBLY
                                        self.zonzPointsLBLTOP.size = initLBLSize
                                    })
                                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                                        self.zonzPointsLBLTOP.alpha = 1
                                    }, completion: { _ in})
                                })
                                
                            }
                        })
                        
                    })
                    UIView.animate(withDuration: 0.2,delay : 0.8, animations: {
                        self.zonzPointsLBLTOP.alpha = 1
                    }, completion: { _ in
                        
                        
                    })
                        }
                    })
                }else{
                    self.hideBuyTwo(hide: false)
                    self.hideBuyThree(hide: false)
                    self.hideBuyFour(hide: false)
                    self.hideBuyOne(hide: false)
                }
                
            }
        }
    }
    func configureZonzContainer(){
        self.zonzContainer.layer.cornerRadius = 10
        self.zonzContainer.layer.borderColor = UIColor.white.cgColor
        self.zonzContainer.layer.borderWidth = 1.0
        self.zonzContainer.layer.masksToBounds = true
    }
    func GradientColorLocationSaif(gradientStyles: UIGradientStyle, frame: CGRect, colors: [UIColor],limit:NSNumber) -> UIColor {
        return color(withGradientStyles: gradientStyles, withFrame: frame, andColors: colors,limit:limit) ?? UIColor.blue
    }
    func color(withGradientStyles gradientStyle: UIGradientStyle, withFrame frame: CGRect, andColors colors: [Any]?,limit : NSNumber) -> UIColor? {
        //Create our background gradient layer
        let backgroundGradientLayer = CAGradientLayer()
        
        //Set the frame to our object's bounds
        backgroundGradientLayer.frame = frame
        backgroundGradientLayer.locations = [NSNumber(value: 0.0),limit ]
        //To simplfy formatting, we'll iterate through our colors array and create a mutable array with their CG counterparts
        var cgColors: [AnyHashable] = []
        for color in colors as? [UIColor] ?? [] {
            let cg = color.cgColor
            cgColors.append(cg)
            
        }
        switch gradientStyle {
            
        case UIGradientStyle.leftToRight:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Specify the direction our gradient will take
            backgroundGradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            backgroundGradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        case UIGradientStyle.radial:
            
            return nil
        case UIGradientStyle.topToBottom:
            fallthrough
        default:
            
            //Set out gradient's colors
            backgroundGradientLayer.colors = cgColors
            
            //Convert our CALayer to a UIImage object
            UIGraphicsBeginImageContextWithOptions(backgroundGradientLayer.bounds.size, _: false, _: UIScreen.main.scale)
            if let context = UIGraphicsGetCurrentContext() {
                backgroundGradientLayer.render(in: context)
            }
            let backgroundColorImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            // self.setGradientImage(backgroundColorImage)
            if let backgroundColorImage = backgroundColorImage {
                return UIColor(patternImage: backgroundColorImage)
            }
            return nil
        }
    }
    func paymentState(state: SKPaymentTransactionState) {
        if state == .failed {
            self.hideBuyOne(hide: false)
            self.hideBuyTwo(hide: false)
            self.hideBuyThree(hide: false)
            self.hideBuyFour(hide: false)
            self.hideBuyFive(hide: false)
        }
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle.main
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    @objc func buyZonzays(zonz:String,product:SKProduct,completionHandler : @escaping ((Bool) -> Void)){
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    
                    let settings : Parameters = [
                        "userId" : a["_id"].stringValue,
                        "purchaseZonz" : zonz,
                        "purchaseMoney" : product.price.description(withLocale: Locale.current)
                    ]
                    let headers : HTTPHeaders = [
                        "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                    ]
                    print(settings)
                    print(headers)
                    Alamofire.request(ScriptBase.sharedInstance.zonzPayment, method: .post, parameters: settings, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                        let data = JSON(response.data)
                        if data != JSON.null {
                            completionHandler(true)
                          
                            
                        }else{
                          completionHandler(false)
                        }
                        
                    }
                    
                    
                }catch {
                 completionHandler(false)
                }
    }
    
}
