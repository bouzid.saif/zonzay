//
//  Actualite.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  Actualite {
    private var userA : User?
    private var userB : User?
    private var videoYLYLA : VideoYLYL?
    private var videYLYLB : VideoYLYL?
    private var game : String?
    init(userA:User, userB: User, videoYLYLA: VideoYLYL, videYLYLB : VideoYLYL,game:String) {
        self.userA = userA
        self.userB = userB
        self.videoYLYLA = videoYLYLA
        self.videYLYLB = videYLYLB
        self.game = game
        
    }
    func setUserA(_ user : User) {
        self.userA = user
    }
    func setUserB(_ user : User) {
        self.userB = user
    }
    func setVideoYLYLA(_ video : VideoYLYL) {
        self.videoYLYLA = video
    }
    func setVideoYLYLB(_ video : VideoYLYL) {
        self.videYLYLB = video
    }
    func setGame(_ game : String) {
        self.game = game
    }
    func getUserA() -> User {
        return self.userA!
    }
    func getUserB() -> User {
        return self.userB!
    }
    func getVideoYLYLA() -> VideoYLYL{
        return self.videoYLYLA!
    }
    func getVideoYLYLB() -> VideoYLYL{
        return self.videYLYLB!
    }
    func getGame() -> String{
        return self.game!
    }
}
