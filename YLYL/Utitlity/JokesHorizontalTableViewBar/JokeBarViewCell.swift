//
//  JokeBarViewCell.swift
//  YLYL
//
//  Created by macbook on 1/21/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
class JokeBarViewCell: BWHorizontalTableViewCell {
    
    @IBOutlet weak var BackGroundView : UIView!
    @IBOutlet weak var RoundedView : UIView!
    @IBOutlet weak var JokeLabel : UILabel!
    fileprivate var containerView: UIView!
    fileprivate let nibName = "JokeBarViewCell"
    func xibSetup() {
        containerView = loadViewFromNib()
        containerView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        //containerView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(containerView)
        containerView.backgroundColor  = UIColor.clear
    }
    override init!(reuseIdentifier: String!) {
         super.init(reuseIdentifier: reuseIdentifier)
        
        xibSetup()
    }
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: 60, height: 60))
        xibSetup()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    func loadViewFromNib() -> UIView {
        let bundle = Bundle.main
        let nib = UINib(nibName: nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
