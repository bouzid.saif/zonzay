//
//  CommentBean.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  CommentBean {
    private var id : String?
    private var message : String?
    private var date : String?
    private var userComment : User?
    private var videoId: String?
    init(id:String, message: String, userComment : User,date: String, videoId: String) {
        self.id = id
        self.message = message
        self.userComment = userComment
        self.date = date
        self.videoId = videoId
        
    }
    func setMessage(_ message : String) {
        self.message = message
    }
    func setUser(_ user : User) {
        self.userComment = user
    }
    func setDate(_ date : String) {
        self.date = date
    }
    func setVideoId(_ data : String) {
        self.videoId = data
    }
    func getMessage() -> String {
        return self.message!
    }
    func getUser() -> User{
        return self.userComment!
    }
    func getDate() -> String{
        return self.date!
    }
    func getVideoId() -> String{
        return self.videoId!
    }
    
}

