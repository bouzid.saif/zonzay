//
//  VideoYLYL.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  VideoYLYL {
    private var id : String?
    private var videoFirstUserId : User?
    private var videoSecondUserId : User?
    private var videoroomId : Room?
    private var videoDate : String?
    private var pathVideo : String?
    private var likesList : [String]?
    private var seen: String?
    private var commentBeanList : [CommentDetailBean]?
    private var commentList : [String]?
    init(id:String, videoFirstUserId: User, videoSecondUserId: User, videoroomId : Room,videoDate: String, pathVideo: String, commentList: [CommentDetailBean]) {
        self.id = id
        self.videoFirstUserId = videoFirstUserId
        self.videoSecondUserId = videoSecondUserId
        self.videoroomId = videoroomId
        self.videoDate = videoDate
        self.pathVideo = pathVideo
        self.commentBeanList = commentList
        
    }
    init(id:String, videoFirstUserId: User, videoSecondUserId: User, videoroomId : Room,videoDate: String, pathVideo: String, commentList: [CommentDetailBean],listLikes:JSON,seen:String,listComments:JSON) {
        self.id = id
        self.videoFirstUserId = videoFirstUserId
        self.videoSecondUserId = videoSecondUserId
        self.videoroomId = videoroomId
        self.videoDate = videoDate
        self.pathVideo = pathVideo
        self.commentBeanList = commentList
        self.seen = seen
        if listLikes.arrayObject?.count != 0 {
            self.likesList = []
            for i in 0...((listLikes.arrayObject!.count) - 1) {
                print("LIKE////////§§§",listLikes[i].stringValue)
                self.likesList?.append(listLikes[i].stringValue)
            }
        }else{
            self.likesList = []
        }
        if listComments.arrayObject?.count != 0 {
            self.commentList = []
            for i in 0...((listComments.arrayObject!.count) - 1) {
                print("Comment////////§§§",listComments[i].stringValue)
                self.commentList?.append(listComments[i].stringValue)
            }
        }else{
            self.commentList = []
        }
        
    }
    func setVideoFirstUser(_ firstU : User) {
        self.videoFirstUserId = firstU
    }
    func setVideoSecondUser(_ secondU : User) {
        self.videoSecondUserId = secondU
    }
    func setVideoroom(_ videoR : Room) {
        self.videoroomId = videoR
    }
    func setVideoDate(_ videoD : String) {
        self.videoDate = videoD
    }
    func setPathVideo(_ imageV : String) {
        self.pathVideo = imageV
    }
    func setCommentBeanList(_ list : [CommentDetailBean]) {
        self.commentBeanList = list
    }
    func setSeen(_ seen: String) {
        self.seen = seen
    }
    func getVideoId() -> String {
        return self.id!
    }
    func getVideoFirstUser() -> User {
        return self.videoFirstUserId!
    }
    func getVideoSecondUser() -> User {
        return self.videoSecondUserId!
    }
    func getVideoroom() -> Room{
        return self.videoroomId!
    }
    func getVideoDate() -> String{
        return self.videoDate!
    }
    func getPathVideo() -> String{
        return self.pathVideo!
    }
    func getCommentBeanList() ->  [CommentDetailBean] {
        return self.commentBeanList!
    }
    
    func getLikes() -> [String]{
        return self.likesList!
    }
    func getComments() -> [String]{
    
    return self.commentList!
    }
    
    func getSeen() -> String {
        return self.seen!
    }
 }
