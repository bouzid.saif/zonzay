//
//  Room.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  Room {
    private var id : String?
    private var roomNumber : String?
    private var firstUserId : String?
    private var secondUserId : String?
    
    init(id:String, roomNumber: String, firstUserId: String, secondUserId : String) {
        self.id = id
        self.roomNumber = roomNumber
        self.firstUserId = firstUserId
        self.secondUserId = secondUserId
        
    }
    func setRoomNumber(_ room : String) {
        self.roomNumber = room
    }
    func setFirstUser(_ first : String) {
        self.firstUserId = first
    }
    func setSecondUser(_ second : String) {
        self.secondUserId = second
    }
    func getRoomNumber() -> String {
        return self.roomNumber!
    }
    func getFirstUser() -> String {
        return self.firstUserId!
    }
    func getSecondUser() -> String{
        return self.secondUserId!
    }
}

