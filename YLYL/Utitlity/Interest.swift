//
//  Interest.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  Interest {
    private var id : String?
    private var name : String?
    private var picture : UIImage?
    private var selected : Bool?
   
    init(id:String, name: String, picture: UIImage, selected : Bool) {
        self.id = id
        self.name = name
        self.picture = picture
        self.selected = selected
        
    }
    func setName(_ name : String) {
        self.name = name
    }
    func setPicture(_ picture : UIImage) {
        self.picture = picture
    }
    func setSelected(_ selected : Bool) {
        self.selected = selected
    }
    func getName() -> String {
        return self.name!
    }
    func getPicture() -> UIImage {
        return self.picture!
    }
    func isSelected() -> Bool{
        return self.selected!
    }
}
