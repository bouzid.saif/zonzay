#import "GSKExampleData.h"

@interface GSKExampleData ()

@property (nonatomic) NSString *username;
@property (nullable, nonatomic) Class viewControllerClass;
@property (nullable, nonatomic) Class headerViewClass;
@property (nullable, nonatomic) NSString *nibName;

@end

@implementation GSKExampleData

+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId headerViewClass:(Class)headerViewClass {
    GSKExampleData *data = [[self alloc] init];
    data.username = username;
    data.headerViewClass = headerViewClass;
    return data;
}

+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId headerViewNibName:(NSString *)nibName {
    GSKExampleData *data = [[self alloc] init];
    data.username = username;
    data.nibName = nibName;
    return data;
}

+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId viewControllerClass:(Class)viewControllerClass {
    GSKExampleData *data = [[self alloc] init];
    data.username = username;
    data.viewControllerClass = viewControllerClass;
    return data;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _headerViewInitialHeight = 240;
    }
    return self;
}

@end
