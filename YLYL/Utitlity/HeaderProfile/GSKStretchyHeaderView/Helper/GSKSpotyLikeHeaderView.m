#import "GSKSpotyLikeHeaderView.h"
#import "UIView+GSKLayoutHelper.h"
#import "GSKGeometry.h"
#import <Masonry/Masonry.h>

static const CGSize kUserImageSize = {.width = 64, .height = 64};

@interface GSKSpotyLikeHeaderView ()



@end

@implementation GSKSpotyLikeHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
       // self.minimumContentHeight = 64;
        self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1];
        [self setupViews];
        [self setupViewConstraints];
    }
    return self;
}

- (void)setupViews {
    self.tipsBackground = [[UIView alloc] init];
    self.tipsBackground.backgroundColor = [UIColor whiteColor];
     [self addSubview:self.tipsBackground ];
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoTest"]];
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.backgroundImageView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.backgroundImageView];
    
    self.blurredBackgroundImageView = [[UIView alloc] init];
    self.blurredBackgroundImageView.backgroundColor = [[UIColor lightTextColor] colorWithAlphaComponent:0.0f];
    
    self.blurredBackgroundImageView.frame = self.bounds;
    self.blurredBackgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self addSubview:self.blurredBackgroundImageView];

    self.userImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"artist"]];
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.cornerRadius = kUserImageSize.width / 2;
    self.userImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userImageView.layer.borderWidth = 4;
    [self addSubview:self.userImageView];

    self.title = [[UILabel alloc] init];
    self.title.text = @"Very important artist";
    self.title.textColor = [UIColor whiteColor];
    self.title.font = [UIFont boldSystemFontOfSize:20];
    [self addSubview:self.title];

    self.followButton = [[UIButton alloc] init];
    [self.followButton setTitle:@"  Send Friend Request  " forState:UIControlStateNormal];
    self.followButton.backgroundColor = [UIColor colorWithRed:31.0/255.0 green:165.0/255.0 blue:194.0/255.0 alpha:1];
    self.followButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    
    //UIColor(red: , green: , blue: , alpha: 1)
    self.followButton.layer.cornerRadius = 4;
    self.followButton.layer.borderWidth = 0;
    self.followButton.layer.borderColor = [UIColor grayColor].CGColor;
    [self.followButton addTarget:self
                          action:@selector(didTapFollowButton:)
                forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.followButton];
    self.followButton.frame = CGRectMake(self.followButton.frame.origin.x, self.followButton.frame.origin.y, 200, 40);
}

- (void)setupViewConstraints {
    [self.tipsBackground mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(self.mas_height);
    }];
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(self.mas_width);
        make.height.equalTo(self.mas_height);
    }];

    [self.blurredBackgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.backgroundImageView);
    }];

    [self.userImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY).offset(-10);
        make.width.equalTo(@(kUserImageSize.width));
        make.height.equalTo(@(kUserImageSize.height));
    }];

    [self.title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.userImageView.mas_bottom).offset(10);
    }];

    [self.followButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.top.equalTo(self.title.mas_bottom).offset(3);
        make.width.equalTo ( @160);
        make.height.equalTo(@30);
    }];

}

- (void)didChangeStretchFactor:(CGFloat)stretchFactor {
    CGFloat alpha = 1;
    CGFloat blurAlpha = 1;
    CGFloat alphaTemp = 1;
    if (stretchFactor > 1) {
        printf(">1");
        alpha = CGFloatTranslateRange(stretchFactor, 1, 1.12, 1, 0);
        blurAlpha = alpha;
        
        self.backgroundImageView.alpha = 1;
    } else if (stretchFactor < 0.8) {
        printf("<0.8");
        alphaTemp = CGFloatTranslateRange(stretchFactor, 0.2, 0.8, 0, 1);
        alpha = CGFloatTranslateRange(stretchFactor, 0.4, 0.8, 0, 1);
        self.backgroundImageView.alpha = alphaTemp;
    }
   // printf("stretchhhhhhhhhhhhhhhhhhh");
    alpha = MAX(0, alpha);
    self.blurredBackgroundImageView.alpha = blurAlpha;
    //self.backgroundImageView.alpha = blurAlpha;
    self.userImageView.alpha = alpha;
    self.title.alpha = alpha;
    self.followButton.alpha = alpha;
}

- (void)didTapFollowButton:(id)sender {
    //[self.followButton setTitle:@"  FOLLOWING  " forState:UIControlStateNormal];
    [self.delegate requestAction:self.followButton];
}

@end
