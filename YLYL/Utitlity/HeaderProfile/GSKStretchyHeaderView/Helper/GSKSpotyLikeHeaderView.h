#import "GSKStretchyHeaderView.h"

@protocol GKSportyLikeHeaderDelegate <NSObject>
-(void) requestAction:(UIButton *)button;
@end
@interface GSKSpotyLikeHeaderView : UIView
@property (nonatomic) UIImageView *backgroundImageView;
@property (nonatomic) UIView *tipsBackground;
@property (nonatomic) UIView *blurredBackgroundImageView;
@property (nonatomic) UIImageView *userImageView; // redondear y fondo blanco
@property (nonatomic) UILabel *title;
@property (nonatomic) UIButton *followButton;
@property (nonatomic, weak) id<GKSportyLikeHeaderDelegate> delegate;
@end
