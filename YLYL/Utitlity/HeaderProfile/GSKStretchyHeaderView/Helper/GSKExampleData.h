#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface GSKExampleData : NSObject

@property (nonatomic, readonly) NSString *username;

@property (nullable, nonatomic, readonly) Class viewControllerClass;
@property (nullable, nonatomic, readonly) Class headerViewClass;
@property (nullable, nonatomic, readonly) NSString *nibName;
@property (nonatomic) BOOL navigationBarVisible; // default NO
@property (nonatomic) CGFloat headerViewInitialHeight; // default 240

+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId headerViewClass:(Class)headerViewClass;
+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId headerViewNibName:(NSString *)nibName;
+ (instancetype)dataWithAll:(NSString *)username photo:(NSString*)photo owner:(NSString*)owner userId:(NSString*)userId viewControllerClass:(Class)viewControllerClass;

@end

NS_ASSUME_NONNULL_END
