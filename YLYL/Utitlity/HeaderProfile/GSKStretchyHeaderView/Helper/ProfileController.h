@import UIKit;
@class VideoYLYL;
#import "GSKExampleData.h"
NS_ASSUME_NONNULL_BEGIN

@interface ProfileController : UICollectionViewController

- (instancetype)initWithData:(GSKExampleData *)data;
@property (nonatomic) NSMutableArray *videos;
@end

NS_ASSUME_NONNULL_END

