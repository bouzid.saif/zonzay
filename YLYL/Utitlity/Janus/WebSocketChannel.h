
#import <Foundation/Foundation.h>
#import "WebRTC/WebRTC.h"



@protocol WebSocketDelegate;

typedef NS_ENUM(NSInteger, ARDSignalingChannelState) {
    kARDSignalingChannelStateClosed,
    kARDSignalingChannelStateOpen,
    kARDSignalingChannelStateCreate,
    kARDSignalingChannelStateAttach,
    kARDSignalingChannelStateJoin,
    kARDSignalingChannelStateOffer,
    kARDSignalingChannelStateError
};
@protocol WebSocketDelegate <NSObject>
- (void)onPublisherJoined:(NSNumber *)handleId;
- (void)onPublisherRemoteJsep:(NSNumber *)handleId dict:(NSDictionary *)jsep;
- (void)subscriberHandleRemoteJsep: (NSNumber *)handleId dict:(NSDictionary *)jsep;
- (void)onLeaving:(NSNumber *)handleId;
@end

@interface WebSocketChannel : NSObject

@property(nonatomic, weak) id<WebSocketDelegate> delegate;

- (instancetype)initWithURL:(NSURL *)url number:(NSNumber *)number;

- (void)publisherCreateOffer:(NSNumber *)handleId sdp:(RTCSessionDescription *)sdp path:(NSString *)path;

- (void)subscriberCreateAnswer:(NSNumber *)handleId sdp: (RTCSessionDescription *)sdp number: (NSNumber *)number;

- (void)trickleCandidate:(NSNumber *)handleId candidate: (RTCIceCandidate *)candidate;

- (void)trickleCandidateComplete:(NSNumber *)handleId;

- (void)startRecording:(NSNumber *)handleId path:(NSString *)path sdp:(RTCSessionDescription *)sdp;

- (void)closeConnection;

@end
