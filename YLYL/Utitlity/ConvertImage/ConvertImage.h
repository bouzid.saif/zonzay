//
//  ConvertImage.h
//  YLYL
//
//  Created by macbook on 2019-10-04.
//  Copyright © 2019 Bouzid Seifeddine. All rights reserved.
//

#ifndef ConvertImage_h
#define ConvertImage_h
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UtilityImage : NSObject
+ (UIImage *)imageFromSampleBufferWebRTC:(CMSampleBufferRef)sampleBuffer;
@end
#endif /* ConvertImage_h */
