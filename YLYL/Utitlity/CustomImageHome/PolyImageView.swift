//
//  PolyImageView.swift
//  PolyImageView
//
//  Created by Amornchai Kanokpullwad on 3/18/2558 BE.
//  Copyright (c) 2558 Amornchai Kanokpullwad. All rights reserved.
//
import UIKit
@IBDesignable
class PolyImageView: UIView {
    
    @IBInspectable let count = 8
    @IBInspectable  let inset: CGFloat = 20
    @IBInspectable let lineWidth: CGFloat = 10
    @IBInspectable let lineColor = UIColor.lightGray
    @IBInspectable var sense: Bool = true {
        didSet{
            if sense {
                sensePath = "right"
            }else{
                sensePath = "left"
            }
        }
       
    }
    var sensePath : String = "right"
    private var bezierPath: UIBezierPath?
    let imageView = UIImageView()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    private func setup() {
        addSubview(imageView)
         imageView.image = UIImage(named: "artist")
    }
    
    override func draw(_ rect: CGRect) {
        if let path = bezierPath {
            lineColor.setStroke()
            path.stroke()
        }
    }
    func createFirstPath() -> UIBezierPath{
          let rect = bounds;
          let path = UIBezierPath()
        path.lineWidth = 5
        let startPoint = CGPoint(x:  rect.maxX, y: 0)
        path.move(to: startPoint)
        let pointRightDown = CGPoint(x: rect.maxX - ((rect.maxX * 14) / 100), y: rect.maxY)
        path.addLine(to: pointRightDown)
        path.move(to: pointRightDown)
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.minY))
        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: startPoint)
        return path
    }
    func createSecondPath() -> UIBezierPath{
        let rect = bounds;
        let path = UIBezierPath()
        path.lineWidth = 5
        let startPoint = CGPoint(x:  ((rect.maxX * 14) / 100), y: 0)
        path.move(to: startPoint)
        let pointRightUp = CGPoint(x: rect.maxX, y: 0)
        path.addLine(to: pointRightUp)
        path.move(to: pointRightUp)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.move(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.move(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: startPoint)
        return path
    }
    override func layoutSubviews() {
        
        super.layoutSubviews()
        let path =  sensePath == "right" ? self.createFirstPath() : self.createSecondPath()
        imageView.frame = bounds
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        imageView.layer.mask = maskLayer
    
        bezierPath = path
        setNeedsDisplay()
    }
    
}

@IBDesignable
class UIImageViewWithMask: UIImageView {
    var maskImageView = UIImageView()
    
    @IBInspectable
    var maskImage: UIImage? {
        didSet {
            maskImageView.image = maskImage
            updateView()
        }
    }
    
    // This updates mask size when changing device orientation (portrait/landscape)
    override func layoutSubviews() {
        super.layoutSubviews()
        updateView()
    }
    
    func updateView() {
        if maskImageView.image != nil {
            maskImageView.frame = bounds
            mask = maskImageView
        }
    }
}
