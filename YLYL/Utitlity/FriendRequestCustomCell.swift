//
//  FriendRequestCustomCell.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 21/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit

protocol FriendRequestCustomCellDelegate : class {
    func cellDidTapAccept(_ sender: FriendRequestCustomCell)
    func cellDidTapDecline(_ sender: FriendRequestCustomCell)
}

class FriendRequestCustomCell : UITableViewCell{
    
    //Views
    @IBOutlet weak var friendProfileImageView: UIImageView!
    @IBOutlet weak var friendNameLabel: UILabel!
    @IBOutlet weak var Interestes : UILabel!
    
    @IBOutlet weak var acceptFriendView: UIButton!
    @IBOutlet weak var declineFriendView: UIButton!
    
    //Vars
    weak var delegate: FriendRequestCustomCellDelegate?
    var indexPath:IndexPath!
    
    func configureView() {
    }
    
    @IBAction func acceptFriendrequestTouchUpInside(_ sender: Any) {
        
        delegate?.cellDidTapAccept(self)
        
    }
    
    @IBAction func declineFriendRequestTouchUpInside(_ sender: Any) {
        
        delegate?.cellDidTapDecline(self)
        
    }
    
}
