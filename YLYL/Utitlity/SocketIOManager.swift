//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Ahmed Haddar on 03/10/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import UIKit
import SocketIO
import SwiftyJSON
open class SocketIOManager : JokesFragmentDelegate {
    var currentPresentedViewController : UIViewController!
    var rootNavigationController: UINavigationController!
    var friendsConnected : [String] = []
    var connectedMiddleWar : Bool = false
    
    func getJokesFromFragment(jokes: [String]) {
        self.selectedJokes = jokes
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.4) {
        NotificationCenter.default.post(name: NSNotification.Name.init("VideoRoomListener"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name.init("VideoRoom"), object: nil)
        }
       
       /* let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        do {
            let a = try JSON(data: dataFromString!)
            var gameType = GameType.Friend
            switch self.gameInfo[3] {
            case GameType.Friend.rawValue:
                gameType = GameType.Friend
                
            case GameType.ReplayFriend.rawValue :
                gameType = GameType.ReplayFriend

            case GameType.ReplayMatch.rawValue :
                gameType = GameType.ReplayMatch
                
            case GameType.RevengeFriend.rawValue :
                gameType = GameType.RevengeFriend
            
            case GameType.RevengeMatch.rawValue :
                gameType = GameType.RevengeMatch
                
            default:
                break
            }
           
            //let storyboard = UIStoryboard(name: "VideoCall", bundle: nil)
            //let vc = storyboard.instantiateViewController(withIdentifier: "VideoCall") as! VideoGameController
            let vc = UIStoryboard(name: "VideoCall", bundle: nil).instantiateViewController(withIdentifier: "VideoCall") as! VideoGameRework
            vc.willAppear = true
            vc.Owner = "slave"
            vc.roomId = NSNumber(value: Int(self.gameInfo[2])!)
            vc.SecondUserId = self.gameInfo[0]
            vc.selectedJokes = self.selectedJokes
            //let currentVc: UIViewController? = (UIApplication.shared.delegate?.window?!.rootViewController as? UINavigationController)?.topViewController
            let viewUnknown =  presentStartViewController()
             self.acceptVideoRoomRequest(userId: a["_id"].stringValue, senderId: self.gameInfo[0], senderName: self.gameInfo[1], roomId: self.gameInfo[2], gameType: gameType)
           
            print("ToPresentFromJokes : ", UIApplication.shared.keyWindow?.visibleViewController())
           
            if let topViewControler =  UIApplication.shared.keyWindow?.visibleViewController() {
               vc.navigation = UINavigationController()
                topViewControler.navigationController?.pushViewController(vc, animated: true)
            } */
        
        
        
        
        
        
          /*  if viewUnknown.isKind(of: AuthentificationController.self) {
                vc.navigation = self.currentPresentedViewController.navigationController
                self.currentPresentedViewController.navigationController?.pushViewController(vc, animated: true)
            }else{
                if viewUnknown.isKind(of: UINavigationController.self) {
                 vc.navigation = (viewUnknown as! UINavigationController)
                (viewUnknown as! UINavigationController).pushViewController(vc, animated: true)
                }else{
                   viewUnknown.navigationController?.pushViewController(vc, animated: true)
                }
            } */
            
           // DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                
               
               // currentVc.navigationController?.pushViewController(vc, animated: true)
                
            //}
            
        
    }
    func presentStartViewController() -> UIViewController{
        let controllersFromTab : [UIViewController.Type] = [HomeViewController.self,FriendsViewController.self,StartViewController.self,HistoryViewController.self,HomeTabBarController.self]
        let controllersFromOthers : [UIViewController.Type] = [ChatFriendController.self,BuyZonzayController.self,SettingsViewController.self,HomeNextViewController.self,JokesRework.self,VideoGameResultController.self,ProfileTestTable.self,ZonzayPlusPayment.self,EditProfile.self]
        var presentedViewController = UIApplication.shared.delegate?.window!?.rootViewController
        if presentedViewController is UINavigationController {
            presentedViewController = (presentedViewController as! UINavigationController).visibleViewController
        }
        print("PresentedViewController : ",presentedViewController)
        
        let verifTab = controllersFromTab.contains { (view) -> Bool in
            if presentedViewController! .isKind(of: view) {
                return true
            }else{
                 return false
            }
           
        }
        if verifTab {
            (presentedViewController as! HomeTabBarController).selectTab(Number: 2)
            return ((presentedViewController as! HomeTabBarController).viewControllers![2])
        }else{
            let verifController = controllersFromOthers.contains { (view) -> Bool in
                if presentedViewController! .isKind(of: view) {
                    
                    return true
                }else {
                  return false
                }
                
            }
            if verifController {
                if presentedViewController!.isKind(of: ChatFriendController.self) || presentedViewController!.isKind(of: HomeNextViewController.self) || presentedViewController!.isKind(of: JokesRework.self) || presentedViewController!.isKind(of: VideoGameResultController.self) {
                    presentedViewController?.navigationController?.popToRootViewController(animated: true)
                    
                }else{
                    presentedViewController?.dismiss(animated: true, completion: nil)
                }
            }
        }
        if presentedViewController! .isKind(of: SplashScreenController.self) {
            presentedViewController = self.rootNavigationController.topViewController
            return presentedViewController!
        }
     return UIViewController()
    }
    var disconnects = 0
    public static let sharedInstance = SocketIOManager()
    private var manager : SocketManager
     private var managerMiddle : SocketManager
    private var managerUpdateServer : SocketManager
    private var socket : SocketIOClient
    private var socketMiddle : SocketIOClient
    private var socketUpdateServer : SocketIOClient
    private var chatRooms:[ChatRoom] = []
    var selectedJokes : [String] = []
    var gameInfo : [String] = []
    var alert: UIAlertController = UIAlertController(title: "Server", message: "Reconnecting...", preferredStyle: .alert)
    var connected = 0
    var didConnect = false
    //51.254.37.192
    //, config: [.log(true)]
    
    func detectDisconnect(){
        socket.on(clientEvent: .error){ data,ack in
            print(" disconnectd")
            if self.disconnects == 0 {
            self.disconnects = self.disconnects + 1
            print(self.currentViewController())
                if self.currentViewController().restorationIdentifier != "SplashScreenController" {
            self.currentViewController().present(self.alert, animated: true, completion: nil)
                }
            }
            }
           // self.navigationController!.present( self.alert, animated: true, completion: nil)
        
    }
    func currentViewController() -> UIViewController{
        return (UIApplication.shared.delegate?.window?!.rootViewController)!
    }
    
    private init() {
        print("INIT")
        manager = SocketManager(socketURL: URL(string: ScriptBase.socket_URL)!, config: [.reconnects(true),.log(true)])
        managerMiddle = SocketManager(socketURL: URL(string: ScriptBase.socket_URLMiddle)!, config: [.reconnects(true),.log(false)])
         managerUpdateServer = SocketManager(socketURL: URL(string: ScriptBase.socket_URLUpdate)!, config: [.reconnects(true),.log(false)])
        socket = manager.defaultSocket
        socketMiddle = managerMiddle.defaultSocket
       socketUpdateServer = managerUpdateServer.defaultSocket
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        self.alert.view.addSubview(loadingIndicator)
    }
   
    var OneTimeRequest = true
    func establishConnection() {
        print("connecting..")
        listenForOtherMessages()
        listenForOtherMessagesFromMiddle()
        listenForOtherMessagesFromUpdate()
        listenDisconnectUpdate()
        socket.connect()
        socketMiddle.connect()
        socketUpdateServer.connect()
        
        self.listenToUpdateServer()
        
        self.videoRoomRequest {json in
        
            if self.OneTimeRequest {
                NotificationCenter.default.post(name: NSNotification.Name.init("CloseGameResult"), object: nil)
                self.OneTimeRequest = false
          
            print("VideoListener: ",json)
            self.gameInfo = json
            
            let tab = self.determineMessage(gameType: self.gameInfo[3])
            let alert = UIAlertController(title: tab[0], message: tab[1], preferredStyle: .alert)
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            let navigation = UINavigationController()
            navigation.viewControllers.append(UIViewController())
            navigation.isNavigationBarHidden = true
            alertWindow.rootViewController = navigation
            alertWindow.windowLevel = UIWindow.Level.alert + 1
            alertWindow.makeKeyAndVisible()
            alert.addAction(UIAlertAction(title: Localization("accept"), style: .default, handler: { alert in
                let storyboard = UIStoryboard(name: "JokesFragment", bundle: nil)
                NotificationCenter.default.post(name: NSNotification.Name.init("DissmissAll"), object: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "JokesFragment") as! JokesRework
                vc.delegate = self
                vc.isPlayingOrProfile = true
                vc.pushedOrModal = false
                let navigation = UINavigationController(rootViewController: vc)
                navigation.isNavigationBarHidden = true
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.navigation.present(navigation, animated: true, completion: nil)
                //let presented = self.presentStartViewController()
               /* if  presented .isKind(of: AuthentificationController.self) {
                    print("The Controller: ",self.currentPresentedViewController)
                    self.currentPresentedViewController.navigationController?.pushViewController(vc, animated: true)
                    //self.rootNavigationController.pushViewController(vc, animated: true)
                }else{
                    let currentVc: UINavigationController? = (UIApplication.shared.delegate?.window?!.rootViewController as? UINavigationController)
                     print(currentVc)
                     currentVc?.pushViewController(vc, animated: true)
                    } */
            }))
            alert.addAction(UIAlertAction(title: Localization("decline"), style: .destructive, handler: { alert in
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                 self.OneTimeRequest = true
                var gameType = GameType.Friend
                switch self.gameInfo[3] {
                case GameType.Friend.rawValue:
                    gameType = GameType.Friend
                    
                case GameType.ReplayFriend.rawValue :
                    gameType = GameType.ReplayFriend
                    
                case GameType.ReplayMatch.rawValue :
                    gameType = GameType.ReplayMatch
                    
                case GameType.RevengeFriend.rawValue :
                    gameType = GameType.RevengeFriend
                    
                case GameType.RevengeMatch.rawValue :
                    gameType = GameType.RevengeMatch
                    
                default:
                    break
                }
                
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                do {
                    let a = try JSON(data: dataFromString!)
                    
                    self.declineVideoRoomRequest(userId: a["_id"].stringValue, senderId: json[0], senderName: json[1], roomId: json[2], gameType: gameType)
                    
                }catch{
                    
                }
            }))
                print("AlertWindow: ",(alertWindow.rootViewController as! UINavigationController).viewControllers[0].present)
            //(alertWindow.rootViewController as! UINavigationController).viewControllers.append(alert)
    
    (UIApplication.shared.delegate! as? AppDelegate)!.window?.rootViewController?.present(alert, animated: true, completion: nil)
            //alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
              }
            
            
        }
        
    }
    func isModal( _ view:UIViewController) -> Bool {
        if (view.presentingViewController != nil) {
            return true
        }
    if view.navigationController?.presentingViewController?.presentedViewController == view.navigationController {
        return true
    }
    if (view.tabBarController?.presentingViewController is UITabBarController) {
        return true
    }

    return false
}

    func determineMessage(gameType: String) -> [String]{
        switch self.gameInfo[3] {
            case GameType.Friend.rawValue:
           return ["GameChallenge", "You have a new request to play with your friend " + self.gameInfo[1]]
            case GameType.ReplayFriend.rawValue :
           
             return ["GameChallenge", "Your friend " + self.gameInfo[1] + " want's to replay, do you accept?" ]
            case GameType.ReplayMatch.rawValue :
            
             return ["GameChallenge", "Your last Opponent " + self.gameInfo[1] + " want's to replay, do you accept?" ]
            case GameType.RevengeFriend.rawValue :
             return ["GameChallenge", "Your friend " + self.gameInfo[1] + " want's a revenge, do you accept?" ]
            case GameType.RevengeMatch.rawValue :
            return ["GameChallenge", "Your last Opponent " + self.gameInfo[1] + " want's a revenge, do you accept?" ]
            
            default:
            return ["",""]
        }
    }
    func deactivateDuringGame(){
        self.OneTimeRequest = false
    }
    func reactivateOnFinishGame(){
        self.OneTimeRequest = true
    }
    func setChatRooms(chatRoom:[ChatRoom] ){
        self.chatRooms = chatRoom
    }
    func appendChatRoom(chatR:ChatRoom){
        self.chatRooms.append(chatR)
    }
    func getChatRooms() -> [ChatRoom] {
        return self.chatRooms
    }
    func clearChatRooms() {
        self.chatRooms.removeAll()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    func joinRoom(chatRoom: String,user:String) {
        socket.emit("join", chatRoom,user)
    }
    /*
     *Emit join then it has json object {"event" : "message", "senderID" : ,"receiverID" , "message"},{"event" : type, "userID" : ....}
     *
     */
    func getMessageJoin(completionHandler : @escaping (_ messageInfo : JSON) -> Void ) {
        socket.on("messageJoin") { (dataArray,socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            completionHandler(finalRes)
            
        }
    }
    func sendMessage(chatRoom:String,user:String,contact:String,message:String) {
        socket.emit("message",chatRoom,user,contact,message)
    }
    /*
     *this a message received it has json object {"user" : , "message" : {"message" : ,"createdAt" : , "_id" : ,"userFirstName" : ,"userLastName" : , "userImageURL"}}
     *
     */
    func getMessages(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("message") { (dataArray, socketAck) -> Void in
            
            let results = dataArray
            let finalRes = JSON(results)
            completionHandler(finalRes)
            
        }
    }
    func getLeavers(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("leave") { (dataArray, socketAck) -> Void in
            //Nothing
            
        }
    }
    func getAvailableUsers(completionHandler : @escaping (_ messageInfo : [String]) -> Void) {
            socket.on("availableUsers") {(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
                if let finalRes = finalRes[0].arrayObject as? [String] {
                    completionHandler(finalRes)
                }else{
                    completionHandler([])
                }
                
        }
        guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            return
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let a =  JSON(data: dataFromString!)
        socket.emit("availableUsers", a["_id"].stringValue)
        }
    func addRoom(userId: String,FriendId:String) {
        print("addRoom:",userId,FriendId)
        socket.emit("addRoom", userId,FriendId)
    }
    func getNewMessages(completionHandler : @escaping (_ messageInfo : JSON) -> Void)
    {
        socket.on("newMessage") { (dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            print("newMessage",finalRes)
            completionHandler(finalRes)
        }
    }
    func getCreatedChatRoom(completionHandler : @escaping (_ messageInfo : JSON) -> Void) {
        socket.on("newChatRoom") {(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            print("newChatRoom",finalRes)
            completionHandler(finalRes)
        }
    }
    func StopCreatedChatRoom(){
        socket.off("newChatRoom")
    }
    func Typing(room:String,userId:String,contact:String)  {
        socket.emit("isTyping",room, userId,contact)
    }
    func isTyping(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("isTyping") {(dataArray, socketAck) -> Void in
            completionHandler(dataArray)
            
        }
    }
    func listenForOtherMessages(){
        socket.on(clientEvent: .connect){ data, ack in
            print("socket connected")
            if self.disconnects > 0 {
              self.disconnects = self.disconnects - 1
            }
            self.socket.on("currentAmount") {data, ack in
                guard let cur = data[0] as? Double else { return }
                self.socket.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
                    self.socket.emit("update", ["amount": cur + 2.50])
                    }
            ack.with("Got your currentAmount", "dude")
                
            }
            guard let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                return
            }
            
            
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            do {
                let a = try JSON(data: dataFromString!)
                SocketIOManager.sharedInstance.updateSocketId(userId: a["_id"].stringValue)
                self.alert.dismiss(animated: true, completion: nil)
                
            }catch{
                
            }
    
        }
             }
    func listenForOtherMessagesFromMiddle(){
        socketMiddle.on(clientEvent: .connect){ data, ack in
            print("socket connected")
            self.socketMiddle.on("currentAmount") {data, ack in
                guard let cur = data[0] as? Double else { return }
                self.socketMiddle.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
                    self.socketMiddle.emit("update", ["amount": cur + 2.50])
                }
                ack.with("Got your currentAmount", "dude")
                
            }
    }
        }
    func listenForOtherMessagesFromUpdate(){
        socketUpdateServer.on(clientEvent: .connect){ data, ack in
            print("socketUpdate connected")
            self.connectedMiddleWar = true
            self.socketUpdateServer.on("currentAmount") {data, ack in
                guard let cur = data[0] as? Double else { return }
                self.socketUpdateServer.emitWithAck("canUpdate", cur).timingOut(after: 0) {data in
                    self.socketUpdateServer.emit("update", ["amount": cur + 2.50])
                }
                ack.with("Got your currentAmount", "dude")
                
            }
        }
    }
    func listenDisconnectUpdate(){
        socketUpdateServer.on(clientEvent: .reconnect){ data, ack in
            print("socket Update disconnected")
              self.connectedMiddleWar = false
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.restartApplication()
            //NotificationCenter.default.post(name: NSNotification.Name.init("closeUpdate"), object: nil)
            }
        
    }
    func testSocketEmit(userId:String){
        socket.emit("matchMaking", userId)
    }
    func cancelChallenge(userId:String){
        socket.emit("cancelRoom", userId)
    }
    func joinVideoRoomRequest(userId:String,userName:String,SecondUserId:String,VideoRoom:String,gameType : GameType){
        socket.emit("joinVideoRoomRequest", userId,userName,SecondUserId,VideoRoom,gameType.rawValue)
            
        }
    func videoRoomRequestAccepted(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
    socket.on("videoRoomRequestAccepted"){(dataArray, socketAck) -> Void in
        let results = dataArray
        //let finalRes = JSON(results)
        
        completionHandler(results as! [String])
    }
   
    
        }
    
    func removeVideoRoomRequestAccepted(){
        socket.off("videoRoomRequestAccepted")
    }
    func videoRoomRequestRefused(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("videoRoomRequestRefused"){(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            
            completionHandler(finalRes)
        }
    
        }
    func removeVideoRoomRequestRefused(){
        socket.off("videoRoomRequestRefused")
    }
    func videoRoomRequest(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
        socket.on("videoRoomRequest"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("dataArray: ",dataArray)
           // let finalRes = JSON(results)
            if let res = results as? [String] {
            completionHandler(res )
            }else{
                return
            }
        }
    }
    func removeVideoRoomRequest(){
        socket.off("videoRoomRequest")
    }
    func acceptVideoRoomRequest(userId:String,senderId:String,senderName:String,roomId:String,gameType : GameType){
        socket.emit("acceptVideoRoomRequest", userId,senderId,senderName,roomId,gameType.rawValue)
    }
    func declineVideoRoomRequest(userId:String,senderId:String,senderName:String,roomId:String,gameType : GameType){
        socket.emit("declineVideoRoomRequest", userId,senderId,senderName,roomId,gameType.rawValue)
    }
    func registerSocket(Name:String) {
        socket.emit("register", Name)
    }
    func registerSocketResponse(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("messageRegister"){(dataArray, socketAck) -> Void in
            let results = dataArray
            let finalRes = JSON(results)
            
            completionHandler(finalRes)
        }
        
    }
    func updateSocketId(userId:String) {
        socket.emit("updateId", userId)
        if userId != "" {
            let appd = (UIApplication.shared.delegate) as! AppDelegate
            appd.loadData()
           
        SocketIOManager.sharedInstance.connectToChatServer(id_user: userId) { (verif) in
                self.listenAllUsers()
        }
           /* SocketIOManager.sharedInstance.getAvailableUsers { (users) in
            print("AvailableUsers : ",users)
            if users.count != 0 {
                self.friendsConnected = users
               
            }
            self.clientDisconnectFromRoom() */
            //socket.on("client", callback: <#T##NormalCallback##NormalCallback##([Any], SocketAckEmitter) -> ()#>)
            /* let tempUsers = users[0]
             if tempUsers.arrayObject?.count != 0 {
             
             for i in 0...((self.FriendsL.count) - 1) {
             if tempUsers.contains(where: { (S, J) -> Bool in
             J.stringValue == self.FriendsL[i].getId()
             }) {
             self.FriendsL[i].setConnected("1")
             }
             }
             if self.ViewSelector.selectedIndex == 1 {
             self.friendsTableView.reloadData()
             }i
             } */
        }
        }
    func getAllChatRooms(completionHandler : @escaping (_ messageInfo : JSON) -> Void){
        socket.on("chatrooms"){(dataArray, socketAck) -> Void in
            let results = dataArray
            
            let finalRes = JSON(results)
            print("ChatRoomReceived:",finalRes)
            completionHandler(finalRes)
        }
    }
    func StopgetAllChatRooms(){
        socket.off("chatrooms")
    }
    func startNotifyChatRooms(userId:String) {
        socket.emit("chatrooms", userId)
    }
    func sendMatchMaking(userId:String) {
        socket.emit("matchMaking", userId)
    }
    func sendMatchMakingPremium(userId:String,minAge:String,maxAge:String,Gender:String) {
        
        socket.emit("matchMaking", userId,Int(minAge)!,Int(maxAge)!,Gender)
    }
    func getOppents(completionHandler : @escaping (_ messageInfo : [Any]) -> Void){
        socket.on("getOpponent") {(dataArray, socketAck) -> Void in
            if (!(dataArray[0] is NSNull)) && (!(dataArray[1] is NSNull)) {
                
            let results = dataArray[0] as! String
            let room = dataArray[1] as! NSNumber
                let whois = dataArray[2] as! String
                let temp = [results,room,whois] as [Any]
                completionHandler(temp)
            }else{
                completionHandler([])
            }
           
                
            //let finalRes = JSON(results)
            
            
        }
            
        }
    func removeFromMatchMaking(roomId:String){
        socket.emit("exitRoom", roomId)
    }
    func searchUsers(userId:String,text:String,completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("getUsersResponse") {(dataArray, socketAck) -> Void in
            let results = dataArray[0]
            print("Users: ",results)
            completionHandler(results)
        }
        socket.emit("getUsers", userId,text.lowercased())
    }
    func NotifyChangeLife(userId:String){
        socket.emit("onLifeChanged", userId)
    }
    func LifeChangedNotifier(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("lifeChanged"){(dataArray, socketAck) -> Void in
            let results = dataArray
            completionHandler(results)
            
        }
    }
    func closeLifeChangedNotifier(){
        socket.off("lifeChanged")
    }
    func gameWasLeaved(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("videoRoomExited"){(dataArray, socketAck) -> Void in
            print("User leaved with no reason")

            let results = dataArray
            completionHandler(results)
            
        }
    }
    func gameWasLeavedWithUserDecision(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("gameExitCnx"){(dataArray, socketAck) -> Void in
            print("User leaved with decision")
            let results = dataArray
            completionHandler(results)
            
        }
    }
    func closeGameWasLeaved(){
        socket.off("videoRoomExited")
    }
    func closeGameWasLeavedWithNoFace(){
        socket.off("gameExitCnx")
    }
    func leaveGame(userMaster:String,userSlave:String,pathVideo:String) {
        
        socket.emit("leaveVideoRoom", userMaster,userSlave,pathVideo)
    }
    func leaveGameWithUserDecision(userMaster:String,userSlave:String,pathVideo:String){
        socket.emit("onGameExitCnx", userSlave,pathVideo)
    }
    func LifeChangedNotifierStop(){
        socket.off("lifeChanged")
    }
    func jokeChangedStop(){
        socket.off("jokeChanged")
    }
    func StopGameNotifier(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("StopGame"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("STOPPPPGAMMEEEEEE: ",results)
            completionHandler(results)
        }
    }
    func StopGameNotifierClose(){
        socket.off("StopGame")
    }
    func StopGameNotifierAtEnd(completionHandler : @escaping (_ messageInfo : Any) -> Void){
        socket.on("StopGameEndTime"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("STOPPPPGAMMEEEEEE: ",results)
            completionHandler(results)
            
        }
    }
    func StopGameNotifierAtEndClose(){
        socket.off("StopGameEndTime")
    }
    func LoseGame(PathM:String,faceUserX:String,faceUserY:String,timeDetection:String,receiverSlave: String,userMaster:String,gamemode:Int,jokeOne:String,jokeTwo:String,jokeThree:String,scoreOneLocal : Int,scoreTwoLocal : Int,scoreThreeLocal : Int,scoreOneRemote : Int,scoreTwoRemote : Int,scoreThreeRemote: Int,faceFrame:Float,timeElapsedJanus:Float,score5One:Int,score5Two:Int,score5Three:Int,score5Fourth:Int,score5Five:Int,score5Six:Int,jokesCounter:[Int]){
        var pathWin1 = ""
        var pathLose1 = ""
        if UserDefaults.standard.value(forKey: "WinnerPath") != nil {
            pathWin1 = UserDefaults.standard.value(forKey: "WinnerPath") as! String
               }
         if UserDefaults.standard.value(forKey: "LoserPath") != nil {
            pathLose1 = UserDefaults.standard.value(forKey: "LoserPath") as! String
        }
        let constructedJson : JSON = self.constructJson(json: jokesCounter)
        print("LoseGame :",constructedJson)
        socket.emit("LoseGame", [
            "pathMaster" : PathM,
            "faceX1" : faceUserX ,
            "faceY1" : faceUserY,
            "timeDetection" : timeDetection,
            "receiverId" : receiverSlave,
            "userMaster" : userMaster,
            "gameMode" : gamemode,
            "jokeThree" : jokeThree,
            "jokeTwo" : jokeTwo,
            "jokeOne" : jokeOne,
            "scoreTwo" : String(scoreOneLocal),
            "scoreFour" :  String(scoreTwoLocal),
            "scoreSix" :  String(scoreThreeLocal),
            "scoreEight" :  String(scoreOneRemote),
            "scoreTen" : String(scoreTwoRemote),
            "scoreTwelve" : String(scoreThreeRemote),
            "tl1" : (faceFrame * 1000),
            "pathWin1" : pathWin1,
            "pathLose1" : pathLose1,
            "timeElapsedJanus":(timeElapsedJanus * 1000),
            "scoreOne" : score5One,
            "scoreThree" : score5Two,
            "scoreFive" : score5Three,
            "scoreSeven" : score5Fourth,
            "scoreNine" : score5Five,
            "scoreEleven" : score5Six,
            "scores" : constructedJson.rawValue
            ])
    }
    func constructJson(json:[Int]) -> JSON{
        var result = "["
        for i in 1...59 {
            result = result + "{\"score\(i)\":\"\(json[i - 1])\"},"
        }
            result = String(result.dropLast())
             result = result + "]"
        return JSON(stringLiteral: result)
    }
    

   /* func constructTargetString(_ input:[String : Any],smiles:[Int]) -> [String : Any]{
        var test : [String : Any] = input
        for i in 1...(smiles.count) {
            let number = numbers["\(i)"]
            //test["\"\(numbers["\"\()\"])\"]
        }
        return input
    }*/
    /*func testBougiNumbers(tab: [Int]) {
    let constructedJson : JSON = self.constructJson(json: tab)
        socket.emit("testSocket", ["scores" : constructedJson.rawValue])
        
    }*/
    func StopGameSlave(pathSlave:String,receiverMaster:String,userSlave:String,gamemode:Int,winnerResult:Int,jokeOne:String,jokeTwo:String,jokeThree:String,scoreOne: Int,scoreTwo:Int,scoreThree:Int,scoreFour:Int,scoreFive:Int,scoreSix:Int,faceFrame:Float,faceUserX:String,faceUserY:String,timeElapsedJanus:Float,score5One:Int,score5Two:Int,score5Three:Int,score5Fourth:Int,score5Five:Int,score5Six:Int,jokesCounter:[Int]){
        var pathWin2 = ""
        var pathLose2 = ""
        if UserDefaults.standard.value(forKey: "WinnerPath") != nil {
            pathWin2 = UserDefaults.standard.value(forKey: "WinnerPath") as! String
              }
         if UserDefaults.standard.value(forKey: "LoserPath") != nil {
            pathLose2 = UserDefaults.standard.value(forKey: "LoserPath") as! String
           }
        let constructedJson : JSON = self.constructJson(json: jokesCounter)
        socket.emit("StopGameListener", [
            "pathSlave" : pathSlave,
            "receiverMaster" : receiverMaster,
            "userSlave" : userSlave,
            "gameMode" : gamemode,
            "winnerResult" : String(winnerResult),
            "jokeOne" : jokeOne,
            "jokeTwo" : jokeTwo,
            "jokeThree" : jokeThree,
            "scoreTwo" : String(scoreOne),
            "scoreFour" : String(scoreTwo),
            "scoreSix" : String(scoreThree),
            "scoreEight" : String(scoreFour),
            "scoreTen" : String(scoreFive),
            "scoreTwelve" : String(scoreSix),
            "faceX2" : faceUserX ,
            "faceY2" : faceUserY,
            "tl2" : (faceFrame * 1000),
            "pathWin2" : pathWin2,
            "pathLose2" : pathLose2,
            "timeElapsedJanus" : (timeElapsedJanus * 1000),
            "scoreOne" : score5One,
            "scoreThree" : score5Two,
            "scoreFive" : score5Three,
            "scoreSeven" : score5Fourth,
            "scoreNine" : score5Five,
            "scoreEleven" : score5Six,
            "scores" : constructedJson.rawValue
            ])
        
    }
    func StopGameSlaveEndTime(pathSlave:String,receiverMaster:String,userSlave:String,gamemode:Int,winnerResult:Int,jokeOne:String,jokeTwo:String,jokeThree:String,scoreOne: Int,scoreTwo:Int,scoreThree:Int,scoreFour:Int,scoreFive:Int,scoreSix:Int){
        socket.emit("StopGameListenerModeS", [
            "pathSlave" : pathSlave,
            "receiverMaster" : receiverMaster,
            "userSlave" : userSlave,
            "gameMode" : gamemode,
            "winnerResult" : String(winnerResult),
            "jokeOne" : jokeOne,
            "jokeTwo" : jokeTwo,
            "jokeThree" : jokeThree,
            "scoreOne" : String(scoreOne),
            "scoreTwo" : String(scoreTwo),
            "scoreThree" : String(scoreThree),
            "scoreFour" : String(scoreFour),
            "scoreFive" : String(scoreFive),
            "scoreSix" : String(scoreSix)
            ])
        
    }
    func test(userId:String){
        socket.emit("test", userId)
    }
    func notifyRound(userId:String,owner:String) {
        print("please your turn")
        socket.emit("onjokeChanged", userId,owner)
        
    }
    func RoundFinished(completionHandler : @escaping (_ messageInfo : [String]) -> Void)
    {
        socket.on("jokeChanged"){(dataArray, socketAck) -> Void in
            let results = dataArray
            print("RoundFinished: ",results)
            completionHandler(results as! [String])
            
        }
    }
    func synchronizeAndroid(userId:String){
        socket.emit("deviceSynchronisation", userId)
    }
    func listenFromAndroid(completionHandler : @escaping (_ messageInfo : [String]) -> Void){
        socket.on("deviceSynchronisation"){(dataArray, socketAck) -> Void in
        let results = dataArray
        print("deviceSynchronisation: ",results)
        completionHandler([])
        
    }
    }
    func closeListenFromAndroid(){
        socket.off("deviceSynchronisation")
    }
    func RoundFinishedStop(){
        socket.off("jokeChanged")
    }
    func sendToMiddleWar(){
        socketUpdateServer.on("Saif") { (lol, loool) in
            print("received Saif")
        }
        socketUpdateServer.emit("Middleware", "CC middleWare")
    }
    func clientDisconnectFromRoom(){
      /*  socket.on("clientDisRoom") { (dataArray, socketAck) -> Void in
            let results = dataArray
            if let res = results[0] as? String {
                self.friendsConnected.removeString(text: res)
                NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
            }else{
                print("dataBaseError : ",results)
            }
            
        } */
    }
    func clientConnectedFromRoom(){
     /*   socket.on("availableConnectedFriends") { (dataArray, socketAck) -> Void in
            let results = dataArray
            if let res = results[0] as? String {
                self.friendsConnected.append(res)
                NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
            }else{
                print("dataBaseError : ",results)
            }
            
        } */
    }
    /* CHAT Sockets */
    func connectUsername(id:String,completionHandler : @escaping ((Bool) -> Void)){
        socketMiddle.emit("connectUser", id)
        completionHandler(true)
        
        listenForOtherSocketChat()
    }
    func connectToServerWithNickname(nickname: String,userid: String,first:Bool) {
        
        
        socketMiddle.emit("subscribe",["room" :  nickname,"userid" : userid ])
      
       
    }
    func unscubscribe(Conv:String,nickname: String) {
        socketMiddle.emit("unsubscribe",["room" :  nickname,"userid" : nickname , "pathVideo" : ScriptBase.sharedInstance.pathVideoJanus])
    }
    func SetOnline(room:String,UserId:String){
        socketMiddle.emit("userAlreadyOnline",["room" : room , "userid" : UserId])
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: JSON) -> Void) {
        socketMiddle.on("newChatMessage") { (dataArray, socketAck) -> Void in
            
            print("allo")
            // print(dataArray)
            var messageDictionary = JSON.parse(dataArray.description)
            //messageDictionary["conv"] = dataArray[5] as! Int as AnyObject?
            completionHandler(messageDictionary )
        }
    }
    func stopNewChatMessage(){
        socketMiddle.off("newChatMessage")
    }
    func getSeenMessages(completionHandler: @escaping (_ messageInfo: JSON) -> Void) {
        socketMiddle.on("seenMessage") { (dataArray, socketAck) -> Void in
            print("userSeen:",dataArray)
            let messageDictionary = JSON.parse(dataArray.description)
           completionHandler(messageDictionary)
        }
    }
    func stopReceivingMessages(){
        socketMiddle.off("seenMessage")
    }
    func sendStartTypingMessage(Conv:String,nickname: String) {
        print("yekteb")
        socketMiddle.emit("startTyping", Conv,nickname)
    }
    func stopmessage(Conv:String,nickname: String) {
        socketMiddle.emit("stopTyping", Conv,nickname)
    }
    func connectToChatServer(id_user:String,completionHandler : @escaping ((Bool) -> Void)) {
        
    socketMiddle.emit("MyConnect" ,["user_id" : id_user ])
        
        completionHandler(true)
    }
    func sendMessage(idNickname: String, nickname: String, msg : String, conv : String,photo: String) {
        socketMiddle.emit("chatMessage",["userOneId" : idNickname , "userTwoId" : nickname, "message" : msg, "conv" : conv, "senderphoto" : photo ])
    }
    func seenMessage(sendData:String) {
        //let js = JSON.parse(sendData)
        socketMiddle.emit("onSeenMessage",["messageId" : sendData])
    }
  
    func listenToUpdateServer(){
        socketUpdateServer.on("updateServer") { (dataArray, socketAck) -> Void in
            print("updateServer : " ,dataArray)
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "closeAppUpdate"), object: nil)
   // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        //socketUpdateServer.emit("updateS", "SS")
    }
    
    private func listenForOtherSocketChat() {
        socketMiddle.on("userConnectUpdate") { (dataArray, socketAck) -> Void in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: dataArray[0] as! [String: AnyObject])
        }
        
        socketMiddle.on("userExitUpdate") { (dataArray, socketAck) -> Void in
            print("Exit:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
            //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userWasDisconnectedNotification"), object: dataArray[0] as! String)
        }
        
        socketMiddle.on("newUserIsTyping") { (dataArray, socketAck) -> Void in
            //print(dataArray[0] as! String)
            //print(dataArray[0] as? [String: AnyObject])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userTypingNotification"), object: dataArray as? [String])
        }
        socketMiddle.on("newUserStoppedTyping") { (dataArray, socketAck) -> Void in
            //print(dataArray[0] as! String)
            //print(dataArray[0] as? [String: AnyObject])
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userStopTypingNotification"), object: dataArray as? [String])
        }
        socketMiddle.on("userOnline") { (dataArray, socketAck) -> Void in
            print("userOnline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnectNow"), object: dataArray as? [String])
            
        }
        socketMiddle.on("setOnline") { (dataArray, socketAck) -> Void in
            print("SetOnline")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userConnectNow2"), object: dataArray as? [String])
            
        }
        socketMiddle.on("userOffline") { (dataArray, socketAck) -> Void in
            print("userOffline:",dataArray)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
        }
        
    }

    private func listenAllUsers(){
    socketMiddle.on("allUsersList") {(dataArray, socketAck) -> Void in
    print("usersConnected:",JSON(dataArray))
       
        let typingUsersDictionary = JSON(dataArray)
        let q = typingUsersDictionary[0]
            print("q: ",typingUsersDictionary[0])
        guard  let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            return
        }
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        
            if q.arrayObject?.count != 0 {
                for i in 0...((q.arrayObject?.count)! - 1) {
                    print("usersWillBeAdded:",q[i]["nickname"].stringValue)
                    if (q[i]["nickname"].stringValue) != a["_id"].stringValue {
                        if q[i]["isConnected"].boolValue {
                            print("usersAdded:",q[i]["nickname"].stringValue)
                            if !self.friendsConnected.contains(q[i]["nickname"].stringValue) {
                        self.friendsConnected.append(q[i]["nickname"].stringValue)
                            }
                        }
                    }
                }
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                appdelegate.ConnectedUser = self.friendsConnected
            }
        
   
    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: dataArray as? [String])
        }
    
    }
    /*
     * Func to parse the JSON file to Json Array
     */
    func getSupportedNumbers() -> JSON {
        
        let filePath = Bundle.main.path(forResource: "Numbers", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
            let json =  JSON(data: data!)
            return json
            
        
        
    }
    
}
extension UIWindow {
    
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: rootViewController)
        }
        return nil
    }
    
    static func getVisibleViewControllerFrom(vc:UIViewController) -> UIViewController {
        if let navigationController = vc as? UINavigationController,
            let visibleController = navigationController.visibleViewController  {
            return UIWindow.getVisibleViewControllerFrom( vc: visibleController )
        } else if let tabBarController = vc as? UITabBarController,
            let selectedTabController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(vc: selectedTabController )
        } else {
            if let presentedViewController = vc.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: presentedViewController)
            } else {
                return vc
            }
        }
    }
    
}
