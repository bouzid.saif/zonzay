//
//  JBSoundRouter.swift
//
//  Created by Josip Bernat on 1/26/15.
//  Copyright (c) 2015 Josip-Bernat. All rights reserved.
//

import Foundation
import AVFoundation

enum JBSoundRoute: Int {

    case NotDefined = 0
    case Speaker
    case Receiver
}

@objc class JBSoundRouter: NSObject {

    let JBSoundRouterDidChangeRouteNotification = "JBSoundRouterDidChangeRouteNotification"
    
    class func routeSound(route: JBSoundRoute) {
    
        let instance: JBSoundRouter = self.sharedInstance
        instance.currentRoute = route
    }
    
    class func currentSoundRoute() -> JBSoundRoute {
        
        let instance: JBSoundRouter = self.sharedInstance
        return instance.currentRoute
    }
    
    class func isHeadsetPluggedIn() -> Bool {
        
        let route: AVAudioSessionRouteDescription = AVAudioSession.sharedInstance().currentRoute
        for port in route.outputs {
            
            let portDescription: AVAudioSessionPortDescription = port as AVAudioSessionPortDescription
            if portDescription.portType == AVAudioSession.Port.headphones || portDescription.portType == AVAudioSession.Port.headsetMic {
                return true
            }
        }
        return false
    }
    
    //MARK: Shared Instance
    //MARK:
    
    private class var sharedInstance : JBSoundRouter {
        
        
     let sharedInstance: JBSoundRouter = { JBSoundRouter() }()

        return sharedInstance
    }
    
    //MARK: Initialization
    //MARK:
    
    override init() {
        
        super.init();
        
        NotificationCenter.default.addObserver(
            forName: AVAudioSession.routeChangeNotification, object: nil,
            queue: OperationQueue.main) { (note) -> Void in
                
                let notification: NSNotification = note as NSNotification
                let dict  = notification.userInfo
                self.JBLog(message: String(format: "AVAudioSessionRouteChangeNotification received. UserInfo: %@", dict!))
                
                self.__handleSessionRouteChangeNotification(notification: note as NSNotification)
        }
        
        NotificationCenter.default.addObserver(
            forName: AVAudioSession.interruptionNotification, object: nil,
            queue: OperationQueue.main) { (note) -> Void in
                
                let notification: NSNotification = note as NSNotification
                let dict = notification.userInfo
                self.JBLog(message: String(format: "AVAudioSessionInterruptionNotification received. UserInfo: %@", dict!))
        }
    }
    
    private func __handleSessionRouteChangeNotification(notification: NSNotification) {
        
        // Because userInfo is an optional we need to check it first.
        if let info = notification.userInfo {
            
            var numberReason: NSNumber = info[AVAudioSessionRouteChangeReasonKey] as! NSNumber
            if let reason = AVAudioSession.RouteChangeReason(rawValue: UInt(numberReason.intValue)) {
                
                switch (reason) {
                    
                case .unknown:
                    JBLog(message: "AVAudioSessionRouteChangeReason.Unknown!")
                    
                case .categoryChange:
                    // We don't want infinite loop here
                    break
                    
                case .newDeviceAvailable:
                    __updateSoundRoute(reason: reason)
                    JBLog(message: "AVAudioSessionRouteChangeReason.NewDeviceAvailable")
                    
                case .oldDeviceUnavailable:
                    __updateSoundRoute(reason: reason)
                    JBLog(message: "AVAudioSessionRouteChangeReason.OldDeviceUnavailable")
                    
                case .override:
                    __updateSoundRoute(reason: reason)
                    JBLog(message: "AVAudioSessionRouteChangeReason.Override")
                    
                case .routeConfigurationChange:
                    __updateSoundRoute(reason: reason)
                    JBLog(message: "AVAudioSessionRouteChangeReason.RouteConfigurationChange")
                    
                case .wakeFromSleep:
                    JBLog(message: "AVAudioSessionRouteChangeReason.WakeFromSleep")
                    
                default:
                    break
                }
            }
        }
    }

    //MARK: Setters
    //MARK:
    
    private var currentRoute: JBSoundRoute = JBSoundRoute.Speaker {
    
        didSet {
            
            self.__updateSoundRoute(reason: AVAudioSession.RouteChangeReason.unknown)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: JBSoundRouterDidChangeRouteNotification), object: nil)
        }
    }
    
    //MARK: Routing
    //MARK:
    
    private func __updateSoundRoute(reason: AVAudioSession.RouteChangeReason) {
        
        if reason == AVAudioSession.RouteChangeReason.newDeviceAvailable {
            
            if JBSoundRouter.isHeadsetPluggedIn() == true {
                self.currentRoute = JBSoundRoute.Receiver
                return
            }
        }
        else if reason == AVAudioSession.RouteChangeReason.oldDeviceUnavailable {
            
            if JBSoundRouter.isHeadsetPluggedIn() == false {
                self.currentRoute = JBSoundRoute.Speaker
                return
            }
        }
        
        var session: AVAudioSession = AVAudioSession.sharedInstance()
        
        if let route: AVAudioSessionRouteDescription = session.currentRoute {
            
            let outputs = route.outputs
                
                for port in route.outputs {
                    
                    let portDescription: AVAudioSessionPortDescription = port as AVAudioSessionPortDescription
                    JBLog(message: portDescription.portType.rawValue)
                    
                    if (self.currentRoute == JBSoundRoute.Receiver && portDescription.portType != AVAudioSession.Port.builtInReceiver) {
                        
                        // Switch to Receiver
                        do {
                            try session.overrideOutputAudioPort(AVAudioSession.PortOverride.none)
                        }catch{
                            print(error.localizedDescription)
                        }
                    }
                    else if (self.currentRoute == JBSoundRoute.Speaker && portDescription.portType != AVAudioSession.Port.builtInSpeaker) {
                        
                        // Switch to Speaker
                        var error: NSError? = nil
                        do {
                        try session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
                        }catch{
                            print(error.localizedDescription)

                        }
                    }
                }
            
        }
        do {
            try session.setCategory(AVAudioSession.Category.playAndRecord)
            try session.setActive(true)
        }catch{
            print(error.localizedDescription)
        }
    }
    
    //MARK: Logging
    //MARK:
    
    func JBLog(message: String, function: String = #function) {
        #if DEBUG
            print("\(function): \(message)")
        #endif
    }
}
