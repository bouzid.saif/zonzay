//
//  ChatRoom.swift
//  YLYL
//
//  Created by macbook on 1/15/19.
//  Copyright © 2019 Seifeddine Bouzid. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  ChatRoom {
    private var id : String?
    private var userId : String?
    private var receiverId : String?
    private var receiverFirstName : String?
    private var receiverLastName : String?
    private var receiverImageURL : String?
    private var soketId: String?
    init(id:String, receiverId: String ,userId : String) {
        self.id = id
        self.receiverId = receiverId
        self.userId = userId
        
    }
    init(id:String, receiverId: String ,userId : String,receiverFirstName :String,receiverLastName:String,receiverImageURL:String,soketId:String) {
        self.id = id
        self.receiverId = receiverId
        self.userId = userId
        self.receiverFirstName = receiverFirstName
        self.receiverLastName = receiverLastName
        self.receiverImageURL = receiverImageURL
        self.soketId = soketId
    }
    init(id:String, receiverId: String ,userId : String,soketId:String) {
        self.id = id
        self.receiverId = receiverId
        self.userId = userId
        self.soketId = soketId
        
    }
    func setReceiverId(_ receiver : String) {
        self.receiverId = receiver
    }
    func setReceiverFirstName(_ firstN : String) {
        self.receiverFirstName = firstN
    }
    func setReceiverLastName(_ lastN : String) {
        self.receiverLastName = lastN
    }
    func setReceiverImageURL(_ imageUrl : String) {
        self.receiverImageURL = imageUrl
    }
    func setUserId(_ user : String) {
        self.userId = user
    }
    func getReceiverId() -> String {
        return self.receiverId!
    }
    func getReceiverFirstName() -> String {
        return self.receiverFirstName!
    }
    func getUserId() -> String{
        return self.userId!
    }
    func getReceiverLastName() -> String{
        return self.receiverLastName!
    }
    func getReceiverImageURL() -> String{
        return self.receiverImageURL!
    }
    func getId() -> String {
        return self.id!
    }
}
