//
//  GifsBuy.swift
//  YLYL
//
//  Created by macbook on 3/29/19.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class  GifsBuy {
    private var priceGif : String?
    private var nomGif : String?
    private var id : String?
    private var pathGif : String?
    private var externalPathGif : String?
    private var thumbGif : String?
    private var category : String?
    let path = SDFileUtils.cacheDirectory() + "/Gifs/"
    init(priceGif:String, nomGif: String, id: String, pathGif : String,thumbGif:String,category:String,externalP:String) {
        self.priceGif = priceGif
        self.nomGif = nomGif
        self.id = id
        self.pathGif = pathGif
        self.thumbGif = thumbGif
        self.category = category
        self.externalPathGif = externalP
    }
    
    func setPriceGif(_ priceGif : String) {
        self.priceGif = priceGif
    }
    func setNomGif(_ nomGif : String) {
        self.nomGif = nomGif
    }
    func setId(_ id : String) {
        self.id = id
    }
    func setPathGif(_ pathGif : String) {
        self.pathGif = pathGif
    }
    func setThumbGif(_ thumbGif : String) {
        self.thumbGif = thumbGif
    }
    func setCategory(_ category : String) {
        self.category = category
    }
    func getPriceGif() -> String {
        return self.priceGif!
    }
    func getNomGif() -> String {
        return self.nomGif!
    }
    func getId() -> String{
        return self.id!
    }
    func getPathGif() -> String{
        return  path + self.pathGif!
    }
    func getThumbGif() -> String{
        return path + self.thumbGif!
    }
    func getPathGifOriginal() -> String{
        return  self.pathGif!
    }
    func getThumbGifOriginal() -> String{
        return self.thumbGif!
    }
    func getCategory() -> String{
        return self.category!
    }
    func getExternalPath() -> String {
        return self.externalPathGif!
    }
}
