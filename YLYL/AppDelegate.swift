//
//  AppDelegate.swift
//  YLYL
//
//  Created by Abdelhamid Sghaier on 02/01/2019.
//  Copyright © 2019 Abdelhamid Sghaier. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import OneSignal
import FBSDKLoginKit
import GoogleSignIn
import TwitterKit
import MapleBacon
import SwiftyStoreKit
import Alamofire
import GoogleMobileAds
//import AppCenter
//import AppCenterAnalytics
//import AppCenterCrashes
import Firebase
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var navigation : UINavigationController = UINavigationController()
    var window: UIWindow?
    static var countryArray : JSON = []
    static var frenchJokes : JSON = []
    static var englishJokes : JSON = []
    static var espagnolJokes : JSON = []
    static var portugalJokes : JSON = []
    static var italianJokes : JSON = []
    var NotifOne = -1
    var NotifTwo = -1
    var NotifThree = -1
    var convRemoteId = ""
    var convRemoteNumbers : [String : Int] = [:]
    var NormalMode = true
    var videoId : JSON = []
    var indexToShow = 2
     var ConnectedUser : [String] = []
    var ConversatioNRoom : JSON = []
    var SectionCritique = 0
    /*
     * Init of KeyboardManager and the country Array
     */
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //MapleBacon.shared.cache.clearDisk()
      
        // Override point for customization after application launch.
        RTCInitializeSSL()
        RTCSetupInternalTracer()
        RTCSetMinDebugLogLevel(RTCLoggingSeverity.error)
        
        FirebaseApp.configure()
        AppDelegate.countryArray = self.getSupported()
        AppDelegate.frenchJokes = self.getSupportedJokesFR()
        AppDelegate.englishJokes = self.getSupportedJokesEN()
        AppDelegate.espagnolJokes = self.getSupportedJokesESP()
        AppDelegate.portugalJokes = self.getSupportedJokesPOR()
        AppDelegate.italianJokes = self.getSupportedJokesITA()
        IQKeyboardManager.shared.enableDebugging = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldToolbarUsesTextFieldTintColor = false
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        
        connectRemoteNotification(launchOptions: launchOptions)
        SocketIOManager.sharedInstance.establishConnection()
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey: "Hz7mAYV0Z7dITKMXtAFJbGJWN", consumerSecret: "2blWBNlAvbndm9es1pGHnsBHtO5hdIsRrF56yNieHCyWx1N2E1")
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback, mode: .default, options: [])
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSesssion is Active")
            } catch{
                print(error)
            }
        }catch{
            print("Appdelegate : ",error)
        }
        SetupIAP()
        
       // MSAppCenter.start("e167e417-6852-4cd4-94e5-64497e86e8df", withServices: [MSAnalytics.self,MSCrashes.self])
        
       // BITHockeyManager.shared().configure(withIdentifier: "e167e417-6852-4cd4-94e5-64497e86e8df")
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.connectedNow(notification:)), name: NSNotification.Name(rawValue: "userConnectNow"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleUserConnectNow2(notification:)), name: NSNotification.Name(rawValue: "userConnectNow2"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.DisconnectNow(notification:)), name: NSNotification.Name(rawValue: "UserDisonnectNow"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.handleConnectedUserUpdateNotification(notification:)), name: NSNotification.Name(rawValue: "userWasConnectedNotification"), object: nil)
        return true
    }
    @objc func handleConnectedUserUpdateNotification(notification: NSNotification) {
        let connectedUserInfo = notification.object as! [String: AnyObject]
        guard let connectedUserNickname = connectedUserInfo["nickname"] as? String else {
            return
        }
        print(connectedUserNickname)
        guard  let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            
            return
        }
        //  lblNewsBanner.text = "User \(connectedUserNickname!.uppercased()) was just connected."
        //showBannerLabelAnimated()
        let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var a = JSON(data: dataFromString!)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        print("Connected:",appDelegate.ConnectedUser)
        if connectedUserNickname != a["_id"].stringValue {
            var y = -1
            var  x = "non exist"
            print("typingUsersDictionary[0] :",connectedUserNickname)
            for b in  appDelegate.ConnectedUser{
                y = y + 1
                if b == connectedUserNickname {
                    x = ""
                }else {
                    x = "non exist"
                }
            }
            
            if !self.ConnectedUser.contains(connectedUserNickname){
                print("x :",x)
                x = connectedUserNickname
            
                appDelegate.ConnectedUser.append(x)
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
                print("Connected:",appDelegate.ConnectedUser)
            }
            
            
        }
    }
    
    @objc func handleUserConnectNow2(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            let typing = JSON.parse(typingUsersDictionary[0])
            
            
            
            
            guard  let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
            
                return
            }
            
            
            
            print("setOnline2:",typingUsersDictionary)
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Connected:",appDelegate.ConnectedUser)
            if typing["userid"].stringValue != a["_id"].stringValue {
                var y = -1
                var  x = "non exist"
                print("typingUsersDictionary[0] :",typing["userid"].stringValue)
                for b in  appDelegate.ConnectedUser{
                    y = y + 1
                    if b == typing["userid"].stringValue {
                        x = ""
                    }else {
                        x = "non exist"
                    }
                }
                if !self.ConnectedUser.contains(typing["userid"].stringValue){
                    print("x :",x)
                    x = typing["userid"].stringValue
                    appDelegate.ConnectedUser.append(x)
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
                    print("Connected:",appDelegate.ConnectedUser)
                }
                
                
            }
        }
        
        
    }
    @objc func connectedNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            
            let q = JSON.parse(typingUsersDictionary[0])
            //print(q)
            
            
            
            guard  let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                
             
            
                
                return
            }
            
            
            
            print("room:",q["room"].stringValue)
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            
            
            print("userConnectNow:",typingUsersDictionary)
            
            SocketIOManager.sharedInstance.SetOnline(room: q["room"].stringValue, UserId: a["_id"].stringValue)
        }
    }
    @objc func DisconnectNow(notification: NSNotification) {
        if let typingUsersDictionary = notification.object as? [String] {
            print("Disconnecting")
            guard  let ab = UserDefaults.standard.value(forKey: "UserZonzay") as? String else{
                
                return
            }
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            
            var a = JSON(data: dataFromString!)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            print("Disconnected:",appDelegate.ConnectedUser)
            if typingUsersDictionary[0] != a["_id"].stringValue {
                var y = -1
                var x = -1
                print("typingUsersDictionary[0] :",typingUsersDictionary[0])
                for b in  appDelegate.ConnectedUser{
                    y = y + 1
                    if b == typingUsersDictionary[0] {
                        x = y
                    }
                }
                print("x :",x)
                if x != -1 {
                    appDelegate.ConnectedUser.remove(at: x)
                }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadConv"), object: nil)
            }
        }
    }
    func SetupIAP(){
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                }
            }
        }
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
    }
    func connectRemoteNotification(launchOptions:[UIApplication.LaunchOptionsKey: Any]?){
         let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true , kOSSettingsKeyInAppLaunchURL: true ]
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.none
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
         UNUserNotificationCenter.current().delegate = self
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            //  _ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            print("Received Notification: \(notification!.payload.notificationID ?? "")")
            print("launchURL = \(notification?.payload.launchURL ?? "None")")
            print("content_available = \(notification?.payload.contentAvailable ?? false)")
        }
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            //_ = SweetAlert().showAlert("Push", subTitle: "Vous etes connecté", style: AlertStyle.success)
            var fullMessage = payload.body
            print("Message = ",fullMessage ?? "")
            
            if payload.additionalData != nil {
                if payload.title != nil {
                    let messageTitle = payload.title
                    print("Message Title = \(messageTitle!)")
                }
                
                let additionalData = payload.additionalData
                if additionalData?["actionSelected"] != nil {
                    fullMessage = fullMessage! + "\nPressed ButtonID: " + (additionalData!["actionSelected"] as! String)
                }
            }
        }
         OneSignal.initWithLaunchOptions(launchOptions, appId: "eb31ef8a-4b75-4ab9-aa0f-ecd3d648dfe9", handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.goToNotfi(launchOptions: launchOptions)
            }
        }
       // OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
    }
    func goToNotfi(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary
        print("saisaisaissisa", remoteNotif)
        if remoteNotif != nil {
            guard let info = remoteNotif as? [String : AnyObject] else {
                return
            }
            guard let Notif = info ["custom"] as? NSDictionary else {
                return
            }
            guard let NotifFinal = Notif ["a"] as? NSDictionary else {
                return
            }
            guard let NotifFinalType = NotifFinal ["typeNotif"] as? String else {
                return
            }
            
            if NotifFinalType == "videoMontage" {
                guard let videoContainer = NotifFinal["video"] as? Any else {
                    return
                }
               // generateLocalNotification(userInfo: userInfo,type: NotificationType.videoMontage)
                NotificationCenter.default.post(name: NSNotification.Name.init("escapeView"), object: nil)
                self.NormalMode = false
                self.indexToShow = 0
                self.videoId = JSON(videoContainer)
                
            }else if NotifFinalType == "friendRequest" {
                NotificationCenter.default.post(name: NSNotification.Name.init("escapeView"), object: nil)
                self.NormalMode = false
                self.indexToShow = 1
            }else if NotifFinalType == "chat" {
                  NotificationCenter.default.post(name: NSNotification.Name.init("escapeView"), object: nil)
                self.NormalMode = false
                self.indexToShow = 1
            }
        }
        else {
            NSLog("//////////////////////////Normal launch")
        }
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("pushNotification")
        print("Notif:",userInfo)
        guard let info = userInfo as? [String : AnyObject] else {
            return
        }
        guard let aps = info["aps"] as? NSDictionary else {
            return
        }
        guard let Notif = info ["custom"] as? NSDictionary else {
            return
        }
        guard let NotifFinal = Notif ["a"] as? NSDictionary else {
            return
        }
        guard let NotifFinalType = NotifFinal ["typeNotif"] as? String else {
            return
        }
        if application.applicationState == .active {
            if NotifFinalType == "videoMontage" {
                generateLocalNotification(userInfo: userInfo,type: NotificationType.videoMontage)
                
            }else if NotifFinalType == "friendRequest" {
                generateLocalNotification(userInfo: userInfo, type: .friendRequest)
            }else if NotifFinalType == "chat" {
                generateLocalNotification(userInfo: userInfo, type: .chat)
            }
        }else if application.applicationState == .background {
            
        }
        if NotifFinalType == "joinVideoRoomRequest" {
            guard let challengeType = NotifFinal["challengeType"] as? String else {
                return
            }
            guard let roomId  = NotifFinal["roomId"] as? String else {
                return
            }
            guard let userId  = NotifFinal["userId"] as? String else {
                return
            }
            
        }
         print("NotifExt:",Notif)
    completionHandler(UIBackgroundFetchResult.newData)
    }
    func generateLocalNotification(userInfo : [AnyHashable : Any],type : NotificationType) {
        guard let info = userInfo as? [String : AnyObject] else {
            return
        }
        guard let aps = info["aps"] as? NSDictionary else {
            return
        }
        guard let Notif = info ["custom"] as? NSDictionary else {
            return
        }
        guard let NotifFinal = Notif ["a"] as? NSDictionary else {
            return
        }
        guard let NotifFinalType = NotifFinal ["typeNotif"] as? String else {
            return
        }
    let localNotification = UNMutableNotificationContent()
    localNotification.title = "Zonzay"
        if type == .videoMontage || type == .friendRequest  {
            localNotification.body =  NotifFinal["message"] as! String
    
        }else{
        localNotification.body = "didn't worked"
        }
    localNotification.sound = UNNotificationSound(named: UNNotificationSoundName("friend_request.mp3"))
        if  type != .chat {
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

    UIApplication.shared.applicationIconBadgeNumber  = 0

    let request = UNNotificationRequest(identifier: "Time for a run!", content: localNotification, trigger: trigger)

    let center = UNUserNotificationCenter.current()
    center.add(request, withCompletionHandler: { error in
        print("Notification created")
        if type == .videoMontage{
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadgeVideo"), object: nil)
        }else if type == .friendRequest {
                self.loadData()
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadgeRequest"), object: nil)
        }else if type == .chat {
            guard let convId = NotifFinal["convId"] as? String else {
                return
            }
            self.convRemoteId = convId
              NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadgeChat"), object: nil)
            guard let remoteNu = self.convRemoteNumbers[convId] else {
                return
            }
            self.convRemoteNumbers[convId] =  self.convRemoteNumbers[convId]! + 1
            NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
        }
    })
        }else{
         AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            guard let convId = NotifFinal["convId"] as? String else {
                return
            }
            self.convRemoteId = convId
            
            
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AddBadgeChat"), object: nil)
            guard let remoteNu = self.convRemoteNumbers[convId] else {
                return
            }
             self.convRemoteNumbers[convId] =  self.convRemoteNumbers[convId]! + 1
            NotificationCenter.default.post(name: NSNotification.Name.init("changesOnFriends"), object: nil)
        }
            
}
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        SDDownloadManager.shared.backgroundCompletionHandler =  completionHandler
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //self.restartApplication()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
       // StartViewController.shared.stopRunningVideo()
       // self.restartApplication()
        
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //StartViewController.shared.layer = AVSampleBufferDisplayLayer()
       // StartViewController.shared.startRunningVideoSession()
       // self.restartApplication()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func restartApplication(){
        
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SplashScreenController") as! SplashScreenController
        guard let window = UIApplication.shared.keyWindow, let rootViewController = window.rootViewController else {
            return
        }
        vc.view.frame = rootViewController.view.frame
        vc.view.layoutIfNeeded()
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = vc
        }, completion: nil)
        
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        quitMessanger()
        SocketIOManager.sharedInstance.closeConnection()
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if (ApplicationDelegate.shared.application(app, open: url, options: options)){
            return true
        }else if (GIDSignIn.sharedInstance()?.handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation]))!{
            return true
        }else if TWTRTwitter.sharedInstance().application(app, open: url, options: options){
            return true
        }else if url.absoluteString.contains("itunes.apple.com") {
            return true
        }
        return false
    }
    /*
     * Func to parse the JSON file to Json Array
     */
    func getSupported() -> JSON {
        
        let filePath = Bundle.main.path(forResource: "Country", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
        do {
            let json = try JSON(data: data!)
            return json
            
        }catch{
            
        }
        return JSON()
        
    }
    func getSupportedJokesFR() -> JSON {
        
        let filePath = Bundle.main.path(forResource:"blagues", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
       
            let json =  JSON(data: data!)
            return json

    }
    func getSupportedJokesEN() -> JSON {
        
        let filePath = Bundle.main.path(forResource: "jokes", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
     
            let json =  JSON(data: data!)
            return json
            
       
        
    }
    func getSupportedJokesESP() -> JSON {
        let filePath = Bundle.main.path(forResource: "jokeESP", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
            let json =  JSON(data: data!)
            return json
            
        
    }
    func getSupportedJokesPOR() -> JSON {
        let filePath = Bundle.main.path(forResource: "jokePORT", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
        let json =  JSON(data: data!)
        return json
        
        
    }
    func getSupportedJokesITA() -> JSON {
        let filePath = Bundle.main.path(forResource: "jokeITA", ofType: "json")
        let data = NSData(contentsOfFile: filePath!) as Data?
        let json =  JSON(data: data!)
        return json
        
        
    }
    func loadData(){
        print("SectionCritique: ",SectionCritique)
        if SectionCritique == 0 {
            SectionCritique = 1
            if (UserDefaults.standard.value(forKey: "UserZonzay") != nil ) {
                let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
                let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
                var a = JSON(data: dataFromString!)
                let settings : Parameters = [
                    "userId" : a["_id"].stringValue
                ]
                let header: HTTPHeaders = [
                    "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
                ]
                print(settings)
                Alamofire.request(ScriptBase.sharedInstance.getAllConversations , method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header)
                    .responseJSON { response in
                           SocketIOManager.sharedInstance.connectUsername(id: a["_id"].stringValue, completionHandler: { (x) in
                             })
                       // print("sent : ",response.request?.httpBody.)
                        let b = JSON(response.data)
                        print("b:",b)
                        let c = JSON(b.arrayObject)
                        if c.count != 0 {
                            self.ConversatioNRoom = c
                            self.SectionCritique = 0
                            print("count:",c.count)
                                var test = false
                            if self.convRemoteNumbers.count == 0 {
                                test = true
                            }
                                var first = true
                                for  i in 0...c.count - 1 {
                                    if i == 0 {
                                        first = true
                                    } else {
                                        first = false
                                    }
                                    if test {
                                        self.convRemoteNumbers[c[i]["_id"].stringValue] = 0
                                    }
                                    
                                    SocketIOManager.sharedInstance.connectToServerWithNickname(nickname: c[i]["_id"].stringValue, userid: a["_id"].stringValue, first: first)
                                    
                                }
                           
                        }
                        self.SectionCritique = 0
                }
            }
    }
}
    func quitMessanger(){
        SectionCritique = 0
        if (UserDefaults.standard.value(forKey: "UserZonzay") != nil ) {
            let ab = UserDefaults.standard.value(forKey: "UserZonzay") as! String
            let dataFromString = ab.data(using: String.Encoding.utf8, allowLossyConversion: false)
            var a = JSON(data: dataFromString!)
            let settings : Parameters = [
                "userId" : a["_id"].stringValue
            ]
            let header: HTTPHeaders = [
                "x-access-token" : a["userToken"].exists() ? a["userToken"].stringValue : a["userRefreshToken"].stringValue
            ]
            Alamofire.request(ScriptBase.sharedInstance.getAllConversations , method: .post, parameters: settings, encoding: JSONEncoding.default,headers : header)
                .responseJSON { response in
                    
                    let b = JSON(response.data)
                    
                    let c = JSON(b.arrayObject)
                    if c.count != 0 {
                        
                        
                        print("count:",c.count)
                        
                        
                        for  i in 0...c.count - 1 {
                            SocketIOManager.sharedInstance.unscubscribe(Conv: c[i]["_id"].stringValue, nickname: a["_id"].stringValue)
                        }
                         SocketIOManager.sharedInstance.closeConnection()
                    }else{
                         SocketIOManager.sharedInstance.closeConnection()
                    }
            }
        }
    }
}

extension UITabBarController {
    func setTabBarVisible(visible:Bool, duration: TimeInterval, animated:Bool) {
        if (tabBarIsVisible() == visible) { return }
        let frame = self.tabBar.frame
        let height = frame.size.height
        let offsetY = (visible ? -height : height)
        
        // animation
        UIView.animate(withDuration: animated ? duration : 0.0) {
            
            self.tabBar.frame = frame.offsetBy(dx: 0, dy: offsetY)
            
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height + offsetY)
            self.view.setNeedsDisplay()
            self.view.layoutIfNeeded()
        }
    }
    
    func tabBarIsVisible() ->Bool {
        print("UIScreen:",UIScreen.main.bounds.height)
        return self.tabBar.frame.origin.y < (UIScreen.main.bounds.height)
    }
}
enum NotificationType : String {
    case videoMontage  = "videoMontage"
    case friendRequest = "friendRequest"
    case chat = "chat"
}
