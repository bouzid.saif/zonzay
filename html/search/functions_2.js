var searchData=
[
  ['basic_5fop_5fm',['basic_op_m',['../structdlib_1_1basic__op__m.html#a252f371180ad6a41b730369ae404ea2c',1,'dlib::basic_op_m']]],
  ['basic_5fop_5fmm',['basic_op_mm',['../structdlib_1_1basic__op__mm.html#aa9407f24c64244a4c57abed2806e01b0',1,'dlib::basic_op_mm']]],
  ['basic_5fop_5fmmm',['basic_op_mmm',['../structdlib_1_1basic__op__mmm.html#a308430a948aa3a9de741afd60e2f8b40',1,'dlib::basic_op_mmm']]],
  ['basic_5fop_5fmmmm',['basic_op_mmmm',['../structdlib_1_1basic__op__mmmm.html#a8659dce439e5d23785b962462247e77c',1,'dlib::basic_op_mmmm']]],
  ['bayesian_5fnetwork_5fjoin_5ftree',['bayesian_network_join_tree',['../classdlib_1_1bayesian__network__join__tree.html#a6998d1b33b05a43bcf18404e99243194',1,'dlib::bayesian_network_join_tree']]],
  ['begin',['begin',['../classdlib_1_1impl_1_1undirected__adjacency__list.html#a5ed2699dacf4380db9ce753f226182cb',1,'dlib::impl::undirected_adjacency_list']]],
  ['bgr_5fpixel',['bgr_pixel',['../structdlib_1_1bgr__pixel.html#a525b55be99d6be3a3a14d7b111f68708',1,'dlib::bgr_pixel']]],
  ['bigint_5fkernel_5f1',['bigint_kernel_1',['../classdlib_1_1bigint__kernel__1.html#afca3cc738cc300c7bb15b6771b90e406',1,'dlib::bigint_kernel_1']]],
  ['bigint_5fkernel_5f2',['bigint_kernel_2',['../classdlib_1_1bigint__kernel__2.html#ad963af5f2ae3634d5e3365b567eefc09',1,'dlib::bigint_kernel_2']]],
  ['bit_5fstream_5fkernel_5f1',['bit_stream_kernel_1',['../classdlib_1_1bit__stream__kernel__1.html#acacd3c0985a8c03ca04fc3b5dd51d770',1,'dlib::bit_stream_kernel_1']]],
  ['block',['block',['../structdlib_1_1timing_1_1block.html#ae66d4eeffb5e9507adce70cd98d13a0e',1,'dlib::timing::block']]],
  ['bnjt_5fimpl',['bnjt_impl',['../classdlib_1_1bayesian__network__join__tree__helpers_1_1bnjt__impl.html#a5e845b2196c0ebddd50c919a1d62ce9a',1,'dlib::bayesian_network_join_tree_helpers::bnjt_impl']]],
  ['box',['box',['../structdlib_1_1image__dataset__metadata_1_1box.html#a4c59a6fe65d8ffb43b4a489dc793fd7d',1,'dlib::image_dataset_metadata::box']]],
  ['buffer_5ftoken',['buffer_token',['../classdlib_1_1cpp__tokenizer__kernel__1.html#a8bdb2c99e8ec673f4cf9a5e9cd2f5734',1,'dlib::cpp_tokenizer_kernel_1::buffer_token(int type, const std::string &amp;token)'],['../classdlib_1_1cpp__tokenizer__kernel__1.html#abb9e83577e49d54635a682aae55316b1',1,'dlib::cpp_tokenizer_kernel_1::buffer_token(int type, char token)']]],
  ['build',['build',['../classdlib_1_1impl_1_1undirected__adjacency__list.html#aab168d6b3ebb806fcadd9e0644b2029f',1,'dlib::impl::undirected_adjacency_list::build()'],['../interface_b_i_t_hockey_manager.html#a2317445ed88307f261c478d69e79b039',1,'BITHockeyManager::build()']]],
  ['build_5fseparable_5fint32_5fpoly_5ffilters',['build_separable_int32_poly_filters',['../namespacedlib.html#ad40d8e02a59c27b349a3e86fb3a9e3e8',1,'dlib']]],
  ['build_5fseparable_5fpoly_5ffilters',['build_separable_poly_filters',['../namespacedlib.html#a029f5e40a44b777401f1d7c1691ed47b',1,'dlib']]],
  ['button_5faction',['button_action',['../classdlib_1_1button__action.html#a489fe184980325f3a46a76e857acb7ca',1,'dlib::button_action']]]
];
