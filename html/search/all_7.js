var searchData=
[
  ['g',['g',['../classdlib_1_1impl_1_1potts__flow__graph.html#a88510bb1f4ca29309e105150e6c7c9b9',1,'dlib::impl::potts_flow_graph::g()'],['../classdlib_1_1impl_1_1general__flow__graph.html#a4ce3d96c94a39b749fb915bb12152314',1,'dlib::impl::general_flow_graph::g()']]],
  ['gamerequestdialog_3adidcompletewithresults_3a',['gameRequestDialog:didCompleteWithResults:',['../protocol_f_b_s_d_k_game_request_dialog_delegate-p.html#ad0ecb2fc690b89a08fae1044ee3bb351',1,'FBSDKGameRequestDialogDelegate-p']]],
  ['gamerequestdialog_3adidfailwitherror_3a',['gameRequestDialog:didFailWithError:',['../protocol_f_b_s_d_k_game_request_dialog_delegate-p.html#ab205b2fb054cd40e3bf243fdfa1505f1',1,'FBSDKGameRequestDialogDelegate-p']]],
  ['gamerequestdialogdidcancel_3a',['gameRequestDialogDidCancel:',['../protocol_f_b_s_d_k_game_request_dialog_delegate-p.html#ab88e9ddb51341468fca4c4131f614b69',1,'FBSDKGameRequestDialogDelegate-p']]],
  ['gate',['gate',['../classdlib_1_1gate.html',1,'dlib']]],
  ['gate_5fexp',['gate_exp',['../classdlib_1_1gate__exp.html',1,'dlib']]],
  ['gate_5fexp_3c_20cnot_3c_20control_5fbit_2c_20target_5fbit_20_3e_20_3e',['gate_exp&lt; cnot&lt; control_bit, target_bit &gt; &gt;',['../classdlib_1_1gate__exp.html',1,'dlib']]],
  ['gate_5fexp_3c_20composite_5fgate_3c_20t_2c_20u_20_3e_20_3e',['gate_exp&lt; composite_gate&lt; T, U &gt; &gt;',['../classdlib_1_1gate__exp.html',1,'dlib']]],
  ['gate_5fexp_3c_20gate_3c_20bits_20_3e_20_3e',['gate_exp&lt; gate&lt; bits &gt; &gt;',['../classdlib_1_1gate__exp.html',1,'dlib']]],
  ['gate_5fexp_3c_20toffoli_3c_20control_5fbit1_2c_20control_5fbit2_2c_20target_5fbit_20_3e_20_3e',['gate_exp&lt; toffoli&lt; control_bit1, control_bit2, target_bit &gt; &gt;',['../classdlib_1_1gate__exp.html',1,'dlib']]],
  ['gate_5ftraits',['gate_traits',['../structdlib_1_1gate__traits.html',1,'dlib']]],
  ['gate_5ftraits_3c_20composite_5fgate_3c_20t_2c_20u_20_3e_20_3e',['gate_traits&lt; composite_gate&lt; T, U &gt; &gt;',['../structdlib_1_1gate__traits_3_01composite__gate_3_01_t_00_01_u_01_4_01_4.html',1,'dlib']]],
  ['gate_5ftraits_3c_20gate_3c_20bits_20_3e_20_3e',['gate_traits&lt; gate&lt; bits &gt; &gt;',['../structdlib_1_1gate__traits_3_01gate_3_01bits_01_4_01_4.html',1,'dlib']]],
  ['gate_5ftraits_3c_20quantum_5fgates_3a_3acnot_3c_20control_5fbit_2c_20target_5fbit_20_3e_20_3e',['gate_traits&lt; quantum_gates::cnot&lt; control_bit, target_bit &gt; &gt;',['../structdlib_1_1gate__traits_3_01quantum__gates_1_1cnot_3_01control__bit_00_01target__bit_01_4_01_4.html',1,'dlib']]],
  ['gate_5ftraits_3c_20quantum_5fgates_3a_3atoffoli_3c_20control_5fbit1_2c_20control_5fbit2_2c_20target_5fbit_20_3e_20_3e',['gate_traits&lt; quantum_gates::toffoli&lt; control_bit1, control_bit2, target_bit &gt; &gt;',['../structdlib_1_1gate__traits_3_01quantum__gates_1_1toffoli_3_01control__bit1_00_01control__bit2_00_01target__bit_01_4_01_4.html',1,'dlib']]],
  ['general_5fflow_5fgraph',['general_flow_graph',['../classdlib_1_1impl_1_1general__flow__graph.html',1,'dlib::impl']]],
  ['general_5fhash',['general_hash',['../classdlib_1_1general__hash.html',1,'dlib']]],
  ['general_5fhash_3c_20domain_20_3e',['general_hash&lt; domain &gt;',['../classdlib_1_1general__hash.html',1,'dlib']]],
  ['general_5fpotts_5fproblem',['general_potts_problem',['../classdlib_1_1impl_1_1general__potts__problem.html',1,'dlib::impl']]],
  ['general_5freturn_5fcodes',['general_return_codes',['../namespacedlib.html#a3d0eb509fdefecd0e9f75bab1a0c97d9',1,'dlib']]],
  ['generatetestcrash',['generateTestCrash',['../interface_b_i_t_crash_manager.html#ac51862adb64255b8f257087d9456409c',1,'BITCrashManager']]],
  ['get',['get',['../classdlib_1_1stack__based__memory__block.html#ac24dd95a513ac2f5a49ffc5eac5a2b8c',1,'dlib::stack_based_memory_block']]],
  ['get_5fcheckerboard_5fproblem',['get_checkerboard_problem',['../namespacedlib.html#a55203124a18267235fe1a9b2afa16e2b',1,'dlib']]],
  ['get_5fcount',['get_count',['../classdlib_1_1binary__search__tree__kernel__1.html#a833d5f1767dabc21cb6da8fe635f53ec',1,'dlib::binary_search_tree_kernel_1::get_count()'],['../classdlib_1_1binary__search__tree__kernel__2.html#acd3914484d74d06782167c1493ca633f',1,'dlib::binary_search_tree_kernel_2::get_count()']]],
  ['get_5ffeats_5ffunctor',['get_feats_functor',['../structdlib_1_1fe__helpers_1_1get__feats__functor.html',1,'dlib::fe_helpers']]],
  ['get_5ffeature_5fextraction_5fregions',['get_feature_extraction_regions',['../classdlib_1_1scan__image__boxes.html#a41243876264d8c2e6029e6ce4faeb5e1',1,'dlib::scan_image_boxes']]],
  ['get_5fhelper',['get_helper',['../structdlib_1_1tuple__helpers_1_1get__helper.html',1,'dlib::tuple_helpers']]],
  ['get_5finterest_5fpoints',['get_interest_points',['../namespacedlib.html#a7166c8238bc7546ff45dc3c6e753ab84',1,'dlib']]],
  ['get_5floss_5ffor_5fsample',['get_loss_for_sample',['../classdlib_1_1structural__svm__graph__labeling__problem.html#af69ed8e1c5ad9108a5e4125b7e41bc1b',1,'dlib::structural_svm_graph_labeling_problem']]],
  ['get_5fnc_5fhelper',['get_nc_helper',['../structdlib_1_1get__nc__helper.html',1,'dlib']]],
  ['get_5fnc_5fhelper_3c_20exp_5ftype_2c_200_20_3e',['get_nc_helper&lt; exp_type, 0 &gt;',['../structdlib_1_1get__nc__helper_3_01exp__type_00_010_01_4.html',1,'dlib']]],
  ['get_5fnr_5fhelper',['get_nr_helper',['../structdlib_1_1get__nr__helper.html',1,'dlib']]],
  ['get_5fnr_5fhelper_3c_20exp_5ftype_2c_200_20_3e',['get_nr_helper&lt; exp_type, 0 &gt;',['../structdlib_1_1get__nr__helper_3_01exp__type_00_010_01_4.html',1,'dlib']]],
  ['get_5fnum_5fdetection_5ftemplates',['get_num_detection_templates',['../classdlib_1_1scan__image__boxes.html#af6bc56d8fa3c810851769bf9a69d24f6',1,'dlib::scan_image_boxes']]],
  ['get_5fpixel_5fintensity',['get_pixel_intensity',['../namespacedlib.html#a167547b0166141c1735e36e941526784',1,'dlib']]],
  ['get_5fsizeof_5fhelper',['get_sizeof_helper',['../structdlib_1_1row__major__layout_1_1get__sizeof__helper.html',1,'dlib::row_major_layout::get_sizeof_helper&lt; T &gt;'],['../structdlib_1_1column__major__layout_1_1get__sizeof__helper.html',1,'dlib::column_major_layout::get_sizeof_helper&lt; T &gt;']]],
  ['get_5fsizeof_5fhelper_3c_20matrix_3c_20t_2c_20nr_2c_20nc_2c_20mm_2c_20l_20_3e_20_3e',['get_sizeof_helper&lt; matrix&lt; T, NR, NC, mm, l &gt; &gt;',['../structdlib_1_1row__major__layout_1_1get__sizeof__helper_3_01matrix_3_01_t_00_01_n_r_00_01_n_c_00_01mm_00_01l_01_4_01_4.html',1,'dlib::row_major_layout::get_sizeof_helper&lt; matrix&lt; T, NR, NC, mm, l &gt; &gt;'],['../structdlib_1_1column__major__layout_1_1get__sizeof__helper_3_01matrix_3_01_t_00_01_n_r_00_01_n_c_00_01mm_00_01l_01_4_01_4.html',1,'dlib::column_major_layout::get_sizeof_helper&lt; matrix&lt; T, NR, NC, mm, l &gt; &gt;']]],
  ['get_5fslider_5fsize',['get_slider_size',['../classdlib_1_1scroll__bar.html#ab173ac1de6af8e88f05da1abb4436942',1,'dlib::scroll_bar']]],
  ['get_5ftext_5frect',['get_text_rect',['../classdlib_1_1text__field.html#a5917968573b12400bc2ba854184b31ca',1,'dlib::text_field::get_text_rect()'],['../classdlib_1_1text__box.html#aecaac4664d2107851f7033c07b737d53',1,'dlib::text_box::get_text_rect()']]],
  ['get_5ftotal_5fcovariance_5fmatrix',['get_total_covariance_matrix',['../classdlib_1_1discriminant__pca.html#abff49b5b930a360f5fbcb232fec0ebc4',1,'dlib::discriminant_pca']]],
  ['get_5ftwiddles',['get_twiddles',['../classdlib_1_1impl_1_1twiddles.html#a2703a902d2b71a97839c36aed107cd71',1,'dlib::impl::twiddles']]],
  ['get_5ftype',['get_type',['../structdlib_1_1tuple_1_1get__type.html',1,'dlib::tuple']]],
  ['gidauthentication',['GIDAuthentication',['../interface_g_i_d_authentication.html',1,'']]],
  ['gidgoogleuser',['GIDGoogleUser',['../interface_g_i_d_google_user.html',1,'']]],
  ['gidprofiledata',['GIDProfileData',['../interface_g_i_d_profile_data.html',1,'']]],
  ['gidsignin',['GIDSignIn',['../interface_g_i_d_sign_in.html',1,'']]],
  ['gidsigninbutton',['GIDSignInButton',['../interface_g_i_d_sign_in_button.html',1,'']]],
  ['gidsignindelegate_2dp',['GIDSignInDelegate-p',['../protocol_g_i_d_sign_in_delegate-p.html',1,'']]],
  ['gidsigninuidelegate_2dp',['GIDSignInUIDelegate-p',['../protocol_g_i_d_sign_in_u_i_delegate-p.html',1,'']]],
  ['gksportylikeheaderdelegate_2dp',['GKSportyLikeHeaderDelegate-p',['../protocol_g_k_sporty_like_header_delegate-p.html',1,'']]],
  ['global_5fdata',['global_data',['../structdlib_1_1logger_1_1global__data.html',1,'dlib::logger']]],
  ['global_5fstate_5ftype',['global_state_type',['../classdlib_1_1conditioning__class__kernel__1_1_1global__state__type.html',1,'dlib::conditioning_class_kernel_1&lt; alphabet_size &gt;::global_state_type'],['../classdlib_1_1conditioning__class__kernel__2_1_1global__state__type.html',1,'dlib::conditioning_class_kernel_2&lt; alphabet_size &gt;::global_state_type'],['../classdlib_1_1conditioning__class__kernel__3_1_1global__state__type.html',1,'dlib::conditioning_class_kernel_3&lt; alphabet_size &gt;::global_state_type'],['../classdlib_1_1conditioning__class__kernel__4_1_1global__state__type.html',1,'dlib::conditioning_class_kernel_4&lt; alphabet_size, pool_size, mem_manager &gt;::global_state_type']]],
  ['go',['go',['../structdlib_1_1tuple__helpers_1_1for__each.html#a47bb37b14a28359eb3443e45056487a7',1,'dlib::tuple_helpers::for_each']]],
  ['gr_5forig',['gr_orig',['../classdlib_1_1zoomable__region.html#a558c48da5865c4dd5c989f81a81e7121',1,'dlib::zoomable_region']]],
  ['gradient_5fnorm_5fstop_5fstrategy',['gradient_norm_stop_strategy',['../classdlib_1_1gradient__norm__stop__strategy.html',1,'dlib']]],
  ['grantedpermissions',['grantedPermissions',['../interface_f_b_s_d_k_login_manager_login_result.html#ae9fe0eaba0b1fddcf8c681c0a1f85e53',1,'FBSDKLoginManagerLoginResult']]],
  ['graph',['graph',['../classdlib_1_1graph.html',1,'dlib']]],
  ['graph_3c_20dlib_3a_3ajoint_5fprobability_5ftable_2c_20dlib_3a_3ajoint_5fprobability_5ftable_20_3e',['graph&lt; dlib::joint_probability_table, dlib::joint_probability_table &gt;',['../classdlib_1_1graph.html',1,'dlib']]],
  ['graph_5fchecker_5fhelper',['graph_checker_helper',['../structdlib_1_1graph__checker__helper.html',1,'dlib']]],
  ['graph_5fchecker_5fhelper_3c_20node_5ftype_2c_20graph_2c_20false_20_3e',['graph_checker_helper&lt; node_type, graph, false &gt;',['../structdlib_1_1graph__checker__helper_3_01node__type_00_01graph_00_01false_01_4.html',1,'dlib']]],
  ['graph_5fimage_5fsegmentation_5fdata_5ft',['graph_image_segmentation_data_T',['../structdlib_1_1impl_1_1graph__image__segmentation__data___t.html',1,'dlib::impl']]],
  ['graph_5fkernel_5f1',['graph_kernel_1',['../classdlib_1_1graph__kernel__1.html',1,'dlib']]],
  ['graph_5flabeler',['graph_labeler',['../classdlib_1_1graph__labeler.html',1,'dlib']]],
  ['graphapiversion',['graphAPIVersion',['../interface_f_b_s_d_k_settings.html#a47af1e2e55b9075b33e105c229787050',1,'FBSDKSettings']]],
  ['graphnode',['graphNode',['../interface_f_b_s_d_k_share_a_p_i.html#acbaa236ab85ea258d5432560b7a2b8e8',1,'FBSDKShareAPI']]],
  ['graphpath',['graphPath',['../interface_f_b_s_d_k_graph_request.html#a10e91143d0aa1301a527f43c60c53535',1,'FBSDKGraphRequest']]],
  ['grayscale_5fpixel_5ftraits',['grayscale_pixel_traits',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20char_20_3e',['grayscale_pixel_traits&lt; char &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20int_20_3e',['grayscale_pixel_traits&lt; int &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20int64_20_3e',['grayscale_pixel_traits&lt; int64 &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20long_20_3e',['grayscale_pixel_traits&lt; long &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20short_20_3e',['grayscale_pixel_traits&lt; short &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20signed_20char_20_3e',['grayscale_pixel_traits&lt; signed char &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20uint64_20_3e',['grayscale_pixel_traits&lt; uint64 &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20unsigned_20char_20_3e',['grayscale_pixel_traits&lt; unsigned char &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20unsigned_20int_20_3e',['grayscale_pixel_traits&lt; unsigned int &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20unsigned_20long_20_3e',['grayscale_pixel_traits&lt; unsigned long &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['grayscale_5fpixel_5ftraits_3c_20unsigned_20short_20_3e',['grayscale_pixel_traits&lt; unsigned short &gt;',['../structdlib_1_1grayscale__pixel__traits.html',1,'dlib']]],
  ['gridoverlayhidden',['gridOverlayHidden',['../interface_t_o_crop_view.html#a1f4a71dcb650217ddc95889c98ff218b',1,'TOCropView']]],
  ['gridoverlayview',['gridOverlayView',['../interface_t_o_crop_view.html#a38a245870fe5fa1fdb4631e7db9090f6',1,'TOCropView']]],
  ['group_5fname',['group_name',['../classdlib_1_1cmd__line__parser__kernel__1_1_1option__t.html#ab7a68be4cb4eb32674a33dbb95ed5c63',1,'dlib::cmd_line_parser_kernel_1::option_t::group_name()'],['../classdlib_1_1cmd__line__parser__option.html#ae3ea20dcb15bf42162071ed78f385255',1,'dlib::cmd_line_parser_option::group_name()']]],
  ['groupdescription',['groupDescription',['../interface_f_b_s_d_k_app_group_content.html#abb46868ac35b14093f032f25c53b8106',1,'FBSDKAppGroupContent']]],
  ['grow',['grow',['../classdlib_1_1min__cut.html#ad01c572aad632297513b1fcfc0f96622',1,'dlib::min_cut']]],
  ['gskairbnbsectiontitleview',['GSKAirbnbSectionTitleView',['../interface_g_s_k_airbnb_section_title_view.html',1,'']]],
  ['gskcollectionviewcell',['GSKCollectionViewCell',['../interface_g_s_k_collection_view_cell.html',1,'']]],
  ['gskexamplebasetableviewcontroller',['GSKExampleBaseTableViewController',['../interface_g_s_k_example_base_table_view_controller.html',1,'']]],
  ['gskexampledata',['GSKExampleData',['../interface_g_s_k_example_data.html',1,'']]],
  ['gskexampledatasource',['GSKExampleDataSource',['../interface_g_s_k_example_data_source.html',1,'']]],
  ['gskspotylikeheaderview',['GSKSpotyLikeHeaderView',['../interface_g_s_k_spoty_like_header_view.html',1,'']]],
  ['gskstretchyheadercontentview',['GSKStretchyHeaderContentView',['../interface_g_s_k_stretchy_header_content_view.html',1,'']]],
  ['gskstretchyheaderview',['GSKStretchyHeaderView',['../interface_g_s_k_stretchy_header_view.html',1,'']]],
  ['gskstretchyheaderview_28layout_29',['GSKStretchyHeaderView(Layout)',['../category_g_s_k_stretchy_header_view_07_layout_08.html',1,'']]],
  ['gskstretchyheaderview_28protected_29',['GSKStretchyHeaderView(Protected)',['../category_g_s_k_stretchy_header_view_07_protected_08.html',1,'']]],
  ['gskstretchyheaderview_28stretchfactor_29',['GSKStretchyHeaderView(StretchFactor)',['../category_g_s_k_stretchy_header_view_07_stretch_factor_08.html',1,'']]],
  ['gskstretchyheaderviewstretchdelegate_2dp',['GSKStretchyHeaderViewStretchDelegate-p',['../protocol_g_s_k_stretchy_header_view_stretch_delegate-p.html',1,'']]],
  ['gsktableviewcell',['GSKTableViewCell',['../interface_g_s_k_table_view_cell.html',1,'']]],
  ['gui_5ferror',['gui_error',['../classdlib_1_1gui__error.html',1,'dlib::gui_error'],['../classdlib_1_1gui__error.html#a79df44246d54d45905564cabd8ea0a6e',1,'dlib::gui_error::gui_error(error_type t)'],['../classdlib_1_1gui__error.html#a695fa7a02393b2154db455350fc9ab9c',1,'dlib::gui_error::gui_error(const std::string &amp;a)'],['../classdlib_1_1gui__error.html#a12b91b87302cdcb3e727f4970b1f8453',1,'dlib::gui_error::gui_error()']]],
  ['gz_5fheader_5fs',['gz_header_s',['../structgz__header__s.html',1,'']]],
  ['gz_5fstate',['gz_state',['../structgz__state.html',1,'']]],
  ['gzfile_5fs',['gzFile_s',['../structgz_file__s.html',1,'']]]
];
