var searchData=
[
  ['has_5fcolumn_5fmajor_5flayout',['has_column_major_layout',['../structdlib_1_1ma_1_1has__column__major__layout.html',1,'dlib::ma']]],
  ['has_5fcolumn_5fmajor_5flayout_3c_20exp_2c_20typename_20enable_5fif_3c_20is_5fsame_5ftype_3c_20typename_20exp_3a_3alayout_5ftype_2c_20column_5fmajor_5flayout_20_3e_20_3e_3a_3atype_20_3e',['has_column_major_layout&lt; EXP, typename enable_if&lt; is_same_type&lt; typename EXP::layout_type, column_major_layout &gt; &gt;::type &gt;',['../structdlib_1_1ma_1_1has__column__major__layout_3_01_e_x_p_00_01typename_01enable__if_3_01is__samb4b8f94a727b90eb54a29fcc20622faf.html',1,'dlib::ma']]],
  ['has_5fmatrix_5fmultiply',['has_matrix_multiply',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fadd_5fexp_3c_20t_2c_20u_20_3e_20_3e',['has_matrix_multiply&lt; matrix_add_exp&lt; T, U &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__add__exp_3_01_t_00_01_u_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fdiv_5fscal_5fexp_3c_20t_20_3e_20_3e',['has_matrix_multiply&lt; matrix_div_scal_exp&lt; T &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__div__scal__exp_3_01_t_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fmul_5fscal_5fexp_3c_20t_2c_20tb_20_3e_20_3e',['has_matrix_multiply&lt; matrix_mul_scal_exp&lt; T, Tb &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__mul__scal__exp_3_01_t_00_01_tb_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fmultiply_5fexp_3c_20t_2c_20u_20_3e_20_3e',['has_matrix_multiply&lt; matrix_multiply_exp&lt; T, U &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__multiply__exp_3_01_t_00_01_u_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fop_3c_20t_20_3e_20_3e',['has_matrix_multiply&lt; matrix_op&lt; T &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__op_3_01_t_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20matrix_5fsubtract_5fexp_3c_20t_2c_20u_20_3e_20_3e',['has_matrix_multiply&lt; matrix_subtract_exp&lt; T, U &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01matrix__subtract__exp_3_01_t_00_01_u_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20op_5fconj_3c_20t_20_3e_20_3e',['has_matrix_multiply&lt; op_conj&lt; T &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01op__conj_3_01_t_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20op_5fconj_5ftrans_3c_20t_20_3e_20_3e',['has_matrix_multiply&lt; op_conj_trans&lt; T &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01op__conj__trans_3_01_t_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fmatrix_5fmultiply_3c_20op_5ftrans_3c_20t_20_3e_20_3e',['has_matrix_multiply&lt; op_trans&lt; T &gt; &gt;',['../structdlib_1_1blas__bindings_1_1has__matrix__multiply_3_01op__trans_3_01_t_01_4_01_4.html',1,'dlib::blas_bindings']]],
  ['has_5fnum_5fnonnegative_5fweights',['has_num_nonnegative_weights',['../structdlib_1_1impl2_1_1has__num__nonnegative__weights.html',1,'dlib::impl2']]],
  ['has_5fnum_5fnonnegative_5fweights_3c_20t_2c_201_20_3e',['has_num_nonnegative_weights&lt; T, 1 &gt;',['../structdlib_1_1impl2_1_1has__num__nonnegative__weights_3_01_t_00_011_01_4.html',1,'dlib::impl2']]],
  ['has_5funsigned_5fkeys',['has_unsigned_keys',['../structdlib_1_1has__unsigned__keys.html',1,'dlib']]],
  ['hash_5fblock',['hash_block',['../classdlib_1_1impl_1_1hash__block.html',1,'dlib::impl']]],
  ['hash_5fmap',['hash_map',['../classdlib_1_1hash__map.html',1,'dlib']]],
  ['hash_5fmap_5fkernel_5f1',['hash_map_kernel_1',['../classdlib_1_1hash__map__kernel__1.html',1,'dlib']]],
  ['hash_5fmap_5fkernel_5fc',['hash_map_kernel_c',['../classdlib_1_1hash__map__kernel__c.html',1,'dlib']]],
  ['hash_5fset',['hash_set',['../classdlib_1_1hash__set.html',1,'dlib']]],
  ['hash_5fset_5fkernel_5f1',['hash_set_kernel_1',['../classdlib_1_1hash__set__kernel__1.html',1,'dlib']]],
  ['hash_5fset_5fkernel_5fc',['hash_set_kernel_c',['../classdlib_1_1hash__set__kernel__c.html',1,'dlib']]],
  ['hash_5fsimilar_5fangles_5f128',['hash_similar_angles_128',['../classdlib_1_1hash__similar__angles__128.html',1,'dlib']]],
  ['hash_5fsimilar_5fangles_5f256',['hash_similar_angles_256',['../classdlib_1_1hash__similar__angles__256.html',1,'dlib']]],
  ['hash_5fsimilar_5fangles_5f512',['hash_similar_angles_512',['../classdlib_1_1hash__similar__angles__512.html',1,'dlib']]],
  ['hash_5fsimilar_5fangles_5f64',['hash_similar_angles_64',['../classdlib_1_1hash__similar__angles__64.html',1,'dlib']]],
  ['hash_5ftable',['hash_table',['../classdlib_1_1hash__table.html',1,'dlib']]],
  ['hash_5ftable_5fkernel_5f1',['hash_table_kernel_1',['../classdlib_1_1hash__table__kernel__1.html',1,'dlib']]],
  ['hash_5ftable_5fkernel_5f2',['hash_table_kernel_2',['../classdlib_1_1hash__table__kernel__2.html',1,'dlib']]],
  ['hash_5ftable_5fkernel_5fc',['hash_table_kernel_c',['../classdlib_1_1hash__table__kernel__c.html',1,'dlib']]],
  ['hashed_5ffeature_5fimage',['hashed_feature_image',['../classdlib_1_1hashed__feature__image.html',1,'dlib']]],
  ['header_5finfo',['header_info',['../structdlib_1_1bdf__font__helpers_1_1bdf__parser_1_1header__info.html',1,'dlib::bdf_font_helpers::bdf_parser']]],
  ['helper',['helper',['../structdlib_1_1oca__problem__c__svm_1_1helper.html',1,'dlib::oca_problem_c_svm&lt; matrix_type, in_sample_vector_type, in_scalar_vector_type &gt;::helper'],['../classdlib_1_1logger__helper__stuff_1_1helper.html',1,'dlib::logger_helper_stuff::helper']]],
  ['helper_5fparallel_5ffor',['helper_parallel_for',['../classdlib_1_1impl_1_1helper__parallel__for.html',1,'dlib::impl']]],
  ['helper_5fparallel_5ffor_5ffunct',['helper_parallel_for_funct',['../classdlib_1_1impl_1_1helper__parallel__for__funct.html',1,'dlib::impl']]],
  ['helper_5fparallel_5ffor_5ffunct2',['helper_parallel_for_funct2',['../classdlib_1_1impl_1_1helper__parallel__for__funct2.html',1,'dlib::impl']]],
  ['helper_5fresize_5fimage',['helper_resize_image',['../classdlib_1_1impl_1_1helper__resize__image.html',1,'dlib::impl']]],
  ['hessian',['hessian',['../structdlib_1_1prob__impl_1_1hessian.html',1,'dlib::prob_impl']]],
  ['hessian_5fpyramid',['hessian_pyramid',['../classdlib_1_1hessian__pyramid.html',1,'dlib']]],
  ['histogram',['histogram',['../structdlib_1_1hog__image_1_1histogram.html',1,'dlib::hog_image']]],
  ['histogram_5fcount',['histogram_count',['../structdlib_1_1fine__hog__image_1_1histogram__count.html',1,'dlib::fine_hog_image']]],
  ['histogram_5fintersection_5fkernel',['histogram_intersection_kernel',['../structdlib_1_1histogram__intersection__kernel.html',1,'dlib']]],
  ['hnnf_5fhelper',['hnnf_helper',['../structdlib_1_1impl2_1_1hnnf__helper.html',1,'dlib::impl2']]],
  ['hog_5fimage',['hog_image',['../classdlib_1_1hog__image.html',1,'dlib']]],
  ['hook_5fstreambuf',['hook_streambuf',['../classdlib_1_1logger_1_1global__data_1_1hook__streambuf.html',1,'dlib::logger::global_data']]],
  ['hostinfo',['hostinfo',['../structdlib_1_1impl1_1_1hostinfo.html',1,'dlib::impl1']]],
  ['hough_5ftransform',['hough_transform',['../classdlib_1_1hough__transform.html',1,'dlib']]],
  ['hsi_5fpixel',['hsi_pixel',['../structdlib_1_1hsi__pixel.html',1,'dlib']]],
  ['hsl',['HSL',['../structdlib_1_1assign__pixel__helpers_1_1_h_s_l.html',1,'dlib::assign_pixel_helpers']]],
  ['http_5fclient',['http_client',['../classdlib_1_1http__client.html',1,'dlib']]],
  ['http_5fparse_5ferror',['http_parse_error',['../classdlib_1_1http__parse__error.html',1,'dlib']]],
  ['huff_5fentropy_5fdecoder',['huff_entropy_decoder',['../structhuff__entropy__decoder.html',1,'']]],
  ['huff_5fentropy_5fencoder',['huff_entropy_encoder',['../structhuff__entropy__encoder.html',1,'']]]
];
