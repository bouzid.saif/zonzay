var searchData=
[
  ['objectid',['objectID',['../interface_f_b_s_d_k_game_request_content.html#a3ea25b3469e4471f3cde2d36fba75127',1,'FBSDKGameRequestContent::objectID()'],['../protocol_f_b_s_d_k_liking-p.html#ae1fb9b5b4792c73a336120f8101574b5',1,'FBSDKLiking-p::objectID()']]],
  ['objecttype',['objectType',['../protocol_f_b_s_d_k_liking-p.html#a7e888287a80f5cc50af1d423e37f3cda',1,'FBSDKLiking-p']]],
  ['ondidcropimagetorect',['onDidCropImageToRect',['../interface_t_o_crop_view_controller.html#ab02c8fb8ea06ff21dcede8c4532ee641',1,'TOCropViewController']]],
  ['ondidcroptocircleimage',['onDidCropToCircleImage',['../interface_t_o_crop_view_controller.html#a4c62617034d98d4d50a954fefaabf838',1,'TOCropViewController']]],
  ['ondidcroptorect',['onDidCropToRect',['../interface_t_o_crop_view_controller.html#a277021ddd94ac91a5a4b976139a616a3',1,'TOCropViewController']]],
  ['ondidfinishcancelled',['onDidFinishCancelled',['../interface_t_o_crop_view_controller.html#aec9ec2e1699e0f8c1d47b0d2fac9502b',1,'TOCropViewController']]],
  ['operationclass',['operationClass',['../interface_s_d_web_image_downloader_config.html#aa6acda8d08e65b15bf169e167abd6a94',1,'SDWebImageDownloaderConfig']]],
  ['options',['options',['../interface_s_d_web_image_downloader_operation.html#a4f2ad3e089bd8130321fa8cceccaafee',1,'SDWebImageDownloaderOperation::options()'],['../interface_s_d_web_image_prefetcher.html#afdc1313a083c12e78cecebfcf22eeb8f',1,'SDWebImagePrefetcher::options()']]],
  ['osbuild',['osBuild',['../interface_b_i_t_crash_details.html#aca0f77711c256405e221ecd80a6ccf9d',1,'BITCrashDetails']]],
  ['osversion',['osVersion',['../interface_b_i_t_crash_details.html#a8cabd262a14db2d78527886b1cc87147',1,'BITCrashDetails']]]
];
