var searchData=
[
  ['qr_5fdecomposition',['qr_decomposition',['../classdlib_1_1qr__decomposition.html',1,'dlib']]],
  ['quantum_5fregister',['quantum_register',['../classdlib_1_1quantum__register.html',1,'dlib']]],
  ['queue',['queue',['../classdlib_1_1queue.html',1,'dlib']]],
  ['queue_3c_20dlib_3a_3amember_5ffunction_5fpointer_2c_20dlib_3a_3amemory_5fmanager_3c_20char_20_3e_3a_3akernel_5f2a_20_3e',['queue&lt; dlib::member_function_pointer, dlib::memory_manager&lt; char &gt;::kernel_2a &gt;',['../classdlib_1_1queue.html',1,'dlib']]],
  ['queue_5fkernel_5f1',['queue_kernel_1',['../classdlib_1_1queue__kernel__1.html',1,'dlib']]],
  ['queue_5fkernel_5f2',['queue_kernel_2',['../classdlib_1_1queue__kernel__2.html',1,'dlib']]],
  ['queue_5fkernel_5fc',['queue_kernel_c',['../classdlib_1_1queue__kernel__c.html',1,'dlib']]],
  ['queue_5fsort_5f1',['queue_sort_1',['../classdlib_1_1queue__sort__1.html',1,'dlib']]]
];
