var searchData=
[
  ['default_5fmemory_5fmanager',['default_memory_manager',['../namespacedlib.html#ae8aaef18fce6452263f46535116308ca',1,'dlib']]],
  ['descriptor_5ftype',['descriptor_type',['../classdlib_1_1nearest__neighbor__feature__image.html#ab40506a64a2bb091ac57fbb1a7b0efa4',1,'dlib::nearest_neighbor_feature_image']]],
  ['domain_5ftype',['domain_type',['../classdlib_1_1hash__map__kernel__1.html#a44fdcde4fd1da1329adb797d30d1304f',1,'dlib::hash_map_kernel_1::domain_type()'],['../classdlib_1_1hash__table__kernel__2.html#a1c4259fb2943e4c4b898070a3a6c4756',1,'dlib::hash_table_kernel_2::domain_type()'],['../classdlib_1_1pair__remover.html#ae77d4c0fb6e38060543ef5323dfeeccc',1,'dlib::pair_remover::domain_type()'],['../classdlib_1_1map__kernel__1.html#acca376ecdfc54ba1cda7f46e50399850',1,'dlib::map_kernel_1::domain_type()']]]
];
