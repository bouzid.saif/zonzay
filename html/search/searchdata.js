var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "_abcdefghijklmnopqrstuvwxyz",
  2: "d",
  3: "_abcdefghijklmnopqrstuvwz~",
  4: "_abcdeghiklmnopqrstv",
  5: "abcdeikmnpstu",
  6: "bgs",
  7: "abcdefghiklmnopqrstuvw",
  8: "acdl",
  9: "dr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "properties",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Properties",
  8: "Friends",
  9: "Pages"
};

