var searchData=
[
  ['kalman_5ffilter',['kalman_filter',['../classdlib_1_1kalman__filter.html',1,'dlib']]],
  ['kcentroid',['kcentroid',['../classdlib_1_1kcentroid.html',1,'dlib']]],
  ['kcentroid_3c_20linear_5fkernel_3c_20t_20_3e_20_3e',['kcentroid&lt; linear_kernel&lt; T &gt; &gt;',['../classdlib_1_1kcentroid_3_01linear__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kcentroid_3c_20offset_5fkernel_3c_20k_20_3e_20_3e',['kcentroid&lt; offset_kernel&lt; K &gt; &gt;',['../classdlib_1_1kcentroid.html',1,'dlib']]],
  ['kcentroid_3c_20offset_5fkernel_3c_20linear_5fkernel_3c_20t_20_3e_20_3e_20_3e',['kcentroid&lt; offset_kernel&lt; linear_kernel&lt; T &gt; &gt; &gt;',['../classdlib_1_1kcentroid_3_01offset__kernel_3_01linear__kernel_3_01_t_01_4_01_4_01_4.html',1,'dlib']]],
  ['kcentroid_3c_20offset_5fkernel_3c_20sparse_5flinear_5fkernel_3c_20t_20_3e_20_3e_20_3e',['kcentroid&lt; offset_kernel&lt; sparse_linear_kernel&lt; T &gt; &gt; &gt;',['../classdlib_1_1kcentroid_3_01offset__kernel_3_01sparse__linear__kernel_3_01_t_01_4_01_4_01_4.html',1,'dlib']]],
  ['kcentroid_3c_20sparse_5flinear_5fkernel_3c_20t_20_3e_20_3e',['kcentroid&lt; sparse_linear_kernel&lt; T &gt; &gt;',['../classdlib_1_1kcentroid_3_01sparse__linear__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative',['kernel_derivative',['../structdlib_1_1kernel__derivative.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20linear_5fkernel_3c_20t_20_3e_20_3e',['kernel_derivative&lt; linear_kernel&lt; T &gt; &gt;',['../structdlib_1_1kernel__derivative_3_01linear__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20offset_5fkernel_3c_20t_20_3e_20_3e',['kernel_derivative&lt; offset_kernel&lt; T &gt; &gt;',['../structdlib_1_1kernel__derivative_3_01offset__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20polynomial_5fkernel_3c_20t_20_3e_20_3e',['kernel_derivative&lt; polynomial_kernel&lt; T &gt; &gt;',['../structdlib_1_1kernel__derivative_3_01polynomial__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20radial_5fbasis_5fkernel_3c_20t_20_3e_20_3e',['kernel_derivative&lt; radial_basis_kernel&lt; T &gt; &gt;',['../structdlib_1_1kernel__derivative_3_01radial__basis__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20sigmoid_5fkernel_3c_20t_20_3e_20_3e',['kernel_derivative&lt; sigmoid_kernel&lt; T &gt; &gt;',['../structdlib_1_1kernel__derivative_3_01sigmoid__kernel_3_01_t_01_4_01_4.html',1,'dlib']]],
  ['kernel_5fderivative_3c_20t_20_3e',['kernel_derivative&lt; T &gt;',['../structdlib_1_1kernel__derivative.html',1,'dlib']]],
  ['kkmeans',['kkmeans',['../classdlib_1_1kkmeans.html',1,'dlib']]],
  ['krls',['krls',['../classdlib_1_1krls.html',1,'dlib']]],
  ['krr_5ftrainer',['krr_trainer',['../classdlib_1_1krr__trainer.html',1,'dlib']]]
];
