var searchData=
[
  ['scalar_5ftype',['scalar_type',['../classdlib_1_1kcentroid.html#a944f0b9b3a567b0fe9c2eabe4edab512',1,'dlib::kcentroid::scalar_type()'],['../classdlib_1_1krls.html#a479a810432b51d19771a050744f196c5',1,'dlib::krls::scalar_type()'],['../classdlib_1_1linearly__independent__subset__finder.html#a9a2a2594c9a4349d86615ae65b5e6438',1,'dlib::linearly_independent_subset_finder::scalar_type()'],['../classdlib_1_1red__impl_1_1objective__derivative.html#a695ee22d78dc7c746d5189f954a4fa18',1,'dlib::red_impl::objective_derivative::scalar_type()'],['../classdlib_1_1multiclass__svm__problem.html#a5d4751a44496a949f5df50ed9767bf30',1,'dlib::multiclass_svm_problem::scalar_type()']]],
  ['sequence_5ftype',['sequence_type',['../classdlib_1_1impl__ss_1_1feature__extractor.html#acae6f8b0289fd494881518b6747064ee',1,'dlib::impl_ss::feature_extractor']]],
  ['set_5fof_5fconnections',['set_of_connections',['../classdlib_1_1server.html#adc784e803847fed6ed38f27006ded176',1,'dlib::server']]],
  ['size_5ftype',['size_type',['../classdlib_1_1std__allocator.html#afbdfb9dc5c127fd82f2b1c9f3b1989bf',1,'dlib::std_allocator']]]
];
