var searchData=
[
  ['unchecked_5fget',['unchecked_get',['../classdlib_1_1type__safe__union.html#a43e243a4490b42e05a4d56eae7e3c898',1,'dlib::type_safe_union::unchecked_get()'],['../classdlib_1_1type__safe__union.html#a27e58e83dedc68a41738db5217cac7f9',1,'dlib::type_safe_union::unchecked_get() const']]],
  ['undirected_5fadjacency_5flist',['undirected_adjacency_list',['../classdlib_1_1impl_1_1undirected__adjacency__list.html#adc9214013367a076852499840f1f59c0',1,'dlib::impl::undirected_adjacency_list']]],
  ['unloadallframes',['unloadAllFrames',['../protocol_s_d_animated_image-p.html#a724bba4f72336a102e8322b323c7695c',1,'SDAnimatedImage-p::unloadAllFrames()'],['../interface_s_d_animated_image.html#aa42d1cea5f8b69ba2a65c9577bd5b4ff',1,'SDAnimatedImage::unloadAllFrames()']]],
  ['unordered_5fpair',['unordered_pair',['../structdlib_1_1unordered__pair.html#acf9616303a651746b28b516bb1f9c6c1',1,'dlib::unordered_pair::unordered_pair()'],['../structdlib_1_1unordered__pair.html#ac1d1f87e23fcd45b1e58d0f3e737866a',1,'dlib::unordered_pair::unordered_pair(const T &amp;a, const T &amp;b)'],['../structdlib_1_1unordered__pair.html#a152a01273d2896bcefaa21383b704cab',1,'dlib::unordered_pair::unordered_pair(const unordered_pair&lt; U &gt; &amp;p)']]],
  ['updateincrementaldata_3afinished_3a',['updateIncrementalData:finished:',['../protocol_s_d_progressive_image_coder-p.html#a29623920fcd12831ac6237687d99a48b',1,'SDProgressiveImageCoder-p']]],
  ['updatemanagershouldsendusagedata_3a',['updateManagerShouldSendUsageData:',['../protocol_b_i_t_update_manager_delegate-p.html#a2afa67e8a0d957c328a6ff75f915794c',1,'BITUpdateManagerDelegate-p']]],
  ['updatemanagerwillexitapp_3a',['updateManagerWillExitApp:',['../protocol_b_i_t_update_manager_delegate-p.html#a902f0fd247e4955f93e7d7af05205732',1,'BITUpdateManagerDelegate-p']]],
  ['urldecode_3a',['URLDecode:',['../interface_f_b_s_d_k_utility.html#ad228bd3c5c619481fdc05d87c530bcd4',1,'FBSDKUtility']]],
  ['urlencode_3a',['URLEncode:',['../interface_f_b_s_d_k_utility.html#a4ca5debbcfbb8dbeeab93fece42cf57a',1,'FBSDKUtility']]],
  ['urlforkey_3a',['URLForKey:',['../protocol_f_b_s_d_k_share_open_graph_value_containing-p.html#a21f89dae3641363d84d29100739c044c',1,'FBSDKShareOpenGraphValueContaining-p']]],
  ['urlwithinboundurl_3asourceapplication_3a',['URLWithInboundURL:sourceApplication:',['../interface_b_f_u_r_l.html#abede3614d1666154ccf350ce7357f6b8',1,'BFURL']]],
  ['urlwithurl_3a',['URLWithURL:',['../interface_b_f_u_r_l.html#a0b209ec98da7b4e1f41a94c301fce3f8',1,'BFURL']]],
  ['useremailforhockeymanager_3acomponentmanager_3a',['userEmailForHockeyManager:componentManager:',['../protocol_b_i_t_hockey_manager_delegate-p.html#a85997dd7a4f4f724880cb05939493e01',1,'BITHockeyManagerDelegate-p']]],
  ['useridforhockeymanager_3acomponentmanager_3a',['userIDForHockeyManager:componentManager:',['../protocol_b_i_t_hockey_manager_delegate-p.html#a1690f6f74c24987e97d1b044f64b3877',1,'BITHockeyManagerDelegate-p']]],
  ['usernameforhockeymanager_3acomponentmanager_3a',['userNameForHockeyManager:componentManager:',['../protocol_b_i_t_hockey_manager_delegate-p.html#a4e434d0cfa4a14313252f761eb9247f3',1,'BITHockeyManagerDelegate-p']]]
];
