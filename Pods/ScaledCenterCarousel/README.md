# ScaledCenterCarousel

[![CI Status](http://img.shields.io/travis/yuriy-tolstoguzov/ScaledCenterCarousel.svg?style=flat)](https://travis-ci.org/yuriy-tolstoguzov/ScaledCenterCarousel)
[![Version](https://img.shields.io/cocoapods/v/ScaledCenterCarousel.svg?style=flat)](http://cocoapods.org/pods/ScaledCenterCarousel)
[![License](https://img.shields.io/cocoapods/l/ScaledCenterCarousel.svg?style=flat)](http://cocoapods.org/pods/ScaledCenterCarousel)
[![Platform](https://img.shields.io/cocoapods/p/ScaledCenterCarousel.svg?style=flat)](http://cocoapods.org/pods/ScaledCenterCarousel)

A carousel-based layout for UICollectionView with scaled center item. 
It contains optional paginator to force user to select single item which will be presented exaclty at center of carousel.
Before using library, make sure to have a look at Example project.

![alt tag](https://raw.githubusercontent.com/yuriy-tolstoguzov/ScaledCenterCarousel/master/Example/Assets/ScaledCenterCarousel.gif)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

iOS 7+

## Installation

ScaledCenterCarousel is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ScaledCenterCarousel"
```

## Author

yuriy-tolstoguzov, Yuriy.Tolstoguzov@gmail.com

## License

ScaledCenterCarousel is available under the MIT license. See the LICENSE file for more info.
